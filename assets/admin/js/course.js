$(document).ready(function(){
    $('#table').DataTable();
    //$('#form')[0].reset();
	$(document).on('fileselect', '.view_upload_file :file', function(event, numFiles, label) {
		var input = $(this).closest('.select-file').find(':text'),
		    log = numFiles > 1 ? numFiles + ' files selected' : label;
		if (input.length) {
		    input.val(log);
		} else {
			log;
		}
	});
	$(document).on('change', '.view_upload_file :file', function() {
	    var input = $(this),
	    numFiles = input.get(0).files ? input.get(0).files.length : 1,
	    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	    input.trigger('fileselect', [numFiles, label]);
	});
});
function getSubjectlist(){
	var course_id = $('.select_course option:selected').val();
	if(course_id!='select'){
		$.ajax({
	        url : base_url+"Api/Api/subjectList?id=" + course_id,
	        success: function(result)
	        {
	        	var res = result.data;
		        if(result.status==true){
			        var html = '';
			        	html+='<option value="select">Select Subject</option>';
			        for(var i=0;i<res.length;i++){
			        	html+='<option value='+res[i].subject_id+'>'+res[i].subject_name+'</option>';
			        }
			        $('.select_subject').html(html);
		        }
		        else{
			        $('.select_subject').html('<option value="select">Select Subject</option>');
		        }

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $('#msg').html('Error adding / update data!');
	        }
	    })
	}
	else{
		var html='';
		$('.select_subject').html('<option value="select">Select Subject</option>');
	}
}
function addMoretopic(){

	var j = $('.hidden_count').val();
	var i = parseInt(j)+1;
	$('.hidden_count').val(i);
	var html = '<div id="topic_'+i+'"><span class="pull-right"><i class="fa fa-window-close" onclick="deleteTopics('+i+');" aria-hidden="true"></i></span><div class="col-md-12 add_course_bg_color">\n\
				<div class="row add_course_padding">\n\
					<div class="col-md-6 col-sm-12 col-xs-12">\n\
					    <input type="hidden" name="topic'+i+'[topic_id]" value="" id="id'+i+'">\n\
						<div class="form_set">\n\
							<label>Topic</label>\n\
							<input class="form-control select_topic" id="topic'+i+'" name="topic'+i+'[topic_name]">\n\
							<span class="err_topic'+i+'" style="color: red; display: none;">Please enter topic.</span>\n\
						</div>\n\
					</div>\n\
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive">\n\
						<div class="form_set">\n\
							<label>Duration</label>\n\
							<div class="hours_input_set">\n\
							  <input class="form-control" id="duration'+i+'" name="topic'+i+'[duration]">\n\
							  <span class="err_duration'+i+'" style="color: red; display: none;">Please enter duration.</span>\n\
							</div>\n\
						</div>\n\
					</div>\n\
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive">\n\
						<div class="form_set">\n\
							<label>Cost</label>\n\
							<input class="form-control" id="cost'+i+'" name="topic'+i+'[cost]">\n\
							<span class="err_cost'+i+'" style="color: red; display: none;">Please enter cost.</span>\n\
						</div>\n\
					</div>\n\
				</div>\n\
				<div class="row add_course_padding">\n\
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive">\n\
						<div class="form_set">\n\
							<label>Preview Video</label>\n\
							<div class="view_upload_file select-file">\n\
								<div class="custom-file-input custom_file_width">\n\
									<input type="file" name="topic'+i+'[preview_video]">\n\
									<input type="text" class="form-control" placeholder="Upload" id="preview_video'+i+'" name="topic'+i+'[preview_video]">\n\
									<button type="button" class="btn upload_btn"><i class="fa fa-upload" aria-hidden="true"></i></button>\n\
									<span class="err_preview'+i+'" style="color: red; display: none;">Please upload preview video.</span>\n\
								</div>\n\
							</div>\n\
						</div>\n\
					</div>\n\
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive ">\n\
						<div class="form_set">\n\
							<label>Question Bank</label>\n\
							<div class="view_upload_file select-file">\n\
								<div class="custom-file-input custom_file_width">\n\
									<input type="file" name="topic'+i+'[question_bank]">\n\
									<input type="text" class="form-control" placeholder="Upload" id="question_bank'+i+'" name="topic'+i+'[question_bank]">\n\
									<button type="button" class="btn upload_btn"><i class="fa fa-upload" aria-hidden="true"></i></button>\n\
									<span class="err_question'+i+'" style="color: red; display: none;">Please upload question bank.</span>\n\
								</div>\n\
							</div>\n\
						</div>\n\
						<div id="questionBank'+i+'"><a href="javascript:void(0)"  style="text-decoration: none;" onclick="downloadQuestionBank(this,\'question_bank\')">download</a></div>\n\
					</div>\n\
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive">\n\
						<div class="form_set">\n\
							<label>Notes</label>\n\
							<div class="view_upload_file select-file">\n\
								<div class="custom-file-input custom_file_width">\n\
									<input type="file" name="topic'+i+'[notes]">\n\
									<input type="text" class="form-control" placeholder="Upload" id="notes'+i+'" name="topic'+i+'[notes]">\n\
									<button type="button" class="btn upload_btn"><i class="fa fa-upload" aria-hidden="true"></i></button>\n\
									<span class="err_notes'+i+'" style="color: red; display: none;">Please upload notes.</span>\n\
								</div>\n\
							</div>\n\
						</div>\n\
					</div>\n\
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive">\n\
						<div class="form_set">\n\
							<label>Lesson Video</label>\n\
							<div class="view_upload_file select-file">\n\
								<div class="custom-file-input custom_file_width">\n\
									<input type="file" name="topic'+i+'[lession_video]">\n\
									<input type="text" class="form-control" placeholder="Upload" id="lession_video'+i+'" name="topic'+i+'[lession_video]">\n\
									<button type="button" class="btn upload_btn"><i class="fa fa-upload" aria-hidden="true"></i></button>\n\
									<span class="err_lession'+i+'" style="color: red; display: none;">Please upload lession video.</span>\n\
								</div>\n\
							</div>\n\
						</div>\n\
					</div>\n\
				</div>\n\
			</div>';
	
	   $('.add_more_topic').append(html);
}

function add_topic(){
	var formData = new FormData( $("#topic_form")[0]);
	formData.set('course_id', $("#course_name option:selected").val());
	formData.set('subject_id', $("#subject_name option:selected").val());
	res = valid();
	if(res){
		$.ajax({
	        url : base_url+"Api/Api/addTopic",
	        type: "POST",
	        data: formData, 
	        cache: false, 
	        async: false,
	        processData: false,
	        contentType: false,
	        
	        success: function(data)
	        {
	          if(data.status==true){
	            $(".alert").css('display','block');
	            $(".alert").attr('class', 'alert alert-success');
	            $('#msg').html(data.message);
	            $("#topic_form")[0].reset();
	           // window.location.replace(base_url+'manage_course');
	          }else{
	             $(".alert").css('display','block');
	             $(".alert").attr('class', 'alert alert-danger');
	             $('#msg').html(data.message);
	          }
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $(".alert").css('display','block');
	            $(".alert").attr('class', 'alert alert-danger');
	            $('#msg').html('Error adding / update data!');
	        }
	    })
	}
}

function deleteTopics(i)
{
	var topic_id = $('#id'+i).val();
	$(`#topic_${i}`).remove();
	var j = $('.hidden_count').val();
	$('.hidden_count').val(j-1);
	var formData = new FormData();
	formData.set('topic_id', topic_id);
	if(topic_id){
		$.ajax({
	        url : base_url+"Api/Api/deleteTopic",
	        type: "POST",
	        data: formData, 
	        cache: false, 
	        async: false,
	        processData: false,
	        contentType: false,
	        
	        success: function(data)
	        {
		         /* if(data.status==true){
		            $(".alert").css('display','block');
		            $(".alert").attr('class', 'alert alert-success');
		            $('#msg').html(data.message);
		            $("#topic_form")[0].reset();
		           // window.location.replace(base_url+'manage_course');
		          }else{
		             $(".alert").css('display','block');
		             $(".alert").attr('class', 'alert alert-danger');
		             $('#msg').html(data.message);
		          }*/
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $(".alert").css('display','block');
	            $(".alert").attr('class', 'alert alert-danger');
	            $('#msg').html('Error adding / update data!');
	        }
		})
	} 
}

function valid() {
	var flag = 1;
	var cnt = $('#hidden_count').val();
	var course = $('#course_name option:selected').val();
	var subject = $('#subject_name option:selected').val();
	if (course == "select") {
		$(".err_course").show().delay(3000).fadeOut();
		flag = 0;
	}
	else {
		$(".err_course" + i).hide();
		flag = 1;
	}
	if (subject == "select") {
		$(".err_subject").show().delay(3000).fadeOut();
		flag = 0;
		
	} else {
		$(".err_subject" + i).hide();
		flag = 1;
	}
	for (var i = 1; i <= cnt; i++) {

		if ($("#topic"+i).val() == "") {
			$(".err_topic" + i).show().delay(3000).fadeOut();
			flag = 0;
			
		} else {
			$(".err_topic" + i).hide();
			flag = 1;
		}
		if ($("#duration" + i).val() == "") {
			$(".err_duration" + i).show().delay(3000).fadeOut();
			flag = 0;

		} else {
			$(".err_duration" + i).hide();
			flag = 1;
		}

		if ($("#cost" + i).val() == "") {
			$(".err_cost" + i).show().delay(3000).fadeOut();
			flag = 0;
		} else {
			$(".err_cost" + i).hide();
			flag = 1;
		}
		if($("#cost" + i).val() != '')
	    {
			if ($.isNumeric($("#cost" + i).val())) {
				$(".err_cost" + i).hide();
				flag = 1;
			} else {
				$(".err_cost" + i).html('Please enter valid amount').show().delay(3000).fadeOut();
				flag = 0;
			}
		}

		if ($("#preview_video" + i).val() == "") {
			$(".err_preview" + i).show().delay(3000).fadeOut();
			flag = 0;
		} else {

			$(".err_preview" + i).hide();
			flag = 1;
		}
		if ($("#question_bank" + i).val() == "") {
			$(".err_question" + i).show().delay(3000).fadeOut();
			flag = 0;
		} else {
			$(".err_question" + i).hide();
			flag = 1;
		}
		if ($("#notes" + i).val() == "") {
			$(".err_notes" + i).show().delay(3000).fadeOut();
			flag = 0;
		} else {
			$(".err_notes" + i).hide();
			flag = 1;
		}
		if ($("#lession_video" + i).val() == "") {
			$(".err_lession" + i).show().delay(3000).fadeOut();
			flag = 0;
		} else {
			$(".err_lession" + i).hide();
			flag = 1;
		}

		var filename1 = $("#preview_video" + i).val();
		if(filename1){
			var extension1 = filename1.replace(/^.*\./, '');
			if(extension1 != 'mp4' && extension1 != 'mp5'){
				$(".err_preview" + i).html('Please upload only mp4 or mp5 file.').show().delay(3000).fadeOut();
				flag = 0;
			}
			else {
				$(".err_preview" + i).hide();
				flag = 1;
			}
		}

		var filename2 = $("#question_bank" + i).val();
		if(filename2){
			var extension2 = filename2.replace(/^.*\./, '');
			if(extension2 != 'xlsx' && extension2 != 'csv' && extension2 != 'xls'){
				$(".err_question" + i).html('Please upload only xlsx or csv or xls file.').show().delay(3000).fadeOut();
				flag = 0;
			}
			else {
				$(".err_question" + i).hide();
				flag = 1;
			}
		}

		var filename3 = $("#notes" + i).val();
		if(filename3){
			var extension3 = filename3.replace(/^.*\./, '');
			if(extension3 != 'pdf'){
				$(".err_notes" + i).html('Please upload only pdf file.').show().delay(3000).fadeOut();
				flag = 0;
			}
			else {
				$(".err_notes" + i).hide();
				flag = 1;
			}
		}

		var filename4 = $("#lession_video" + i).val();
		if(filename4){
			var extension4 = filename4.replace(/^.*\./, '');
			if(extension4 != 'mp4' && extension4 != 'mp5'){
				$(".err_lession" + i).html('Please upload only mp4 or mp5 file.').show().delay(3000).fadeOut();
				flag = 0;
			}
			else {
				$(".err_lession" + i).hide();
				flag = 1;
			}
		}
	}
	return flag;
}
 function getTopicdetails(){
 	var course_id = $('.select_course option:selected').val();
 	var subject_id = $('.select_subject option:selected').val();
 	var formData = new FormData();
	formData.set('course_id', course_id);
	formData.set('subject_id', subject_id);
 	if(course_id && subject_id){
 		$.ajax({
	        url : base_url+"Api/Api/getTopicdetails",
	        type: "POST",
	        data: formData, 
	        cache: false, 
	        async: false,
	        processData: false,
	        contentType: false,
	        success: function(data)
	        {
	        	console.log(data);
	          if(data.status==true){
	            $('.add_more_topic').html('');
	            var res = data.data;
	            $('.hidden_count').val('0');
	            var j=1;
	            for(var i=0;i<res.length;i++){
	            	addMoretopic();
	            	var duration = res[i]['duration'];
	            	var avoid = '.000000';
	            	duration = duration.replace(avoid,'');
	            	//alert(duration);
	            	$("#topic"+j).val(res[i]['topic_name']);
	            	$("#duration"+j).val(duration);
	            	$("#cost"+j).val(res[i]['cost']);
	            	$("#preview_video"+j).val(res[i]['preview_video']);
	            	$("#question_bank"+j).val(res[i]['question_bank_file']);
	            	$("#questionBank"+j+" a").attr('title',res[i]['question_bank_file']);
	            	$("#notes"+j).val(res[i]['notes']);
	            	$("#lession_video"+j).val(res[i]['lesson_video']);
	            	$("#id"+j).val(res[i]['topic_id']);
					j++;
	            }
	          }else{
	             $(".alert").css('display','block');
	             $(".alert").attr('class', 'alert alert-danger');
	             $('#msg').html(data.message);
	          }
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $(".alert").css('display','block');
	            $(".alert").attr('class', 'alert alert-danger');
	            $('#msg').html('Error adding / update data!');
	        }
	    })
 	}
}
function update_topic(){
	var formData = new FormData( $("#update_topic_form")[0]);
	formData.set('course_id', $(".select_course option:selected").val());
	formData.set('subject_id', $(".select_subject option:selected").val());
	res = valid();
	if(res){
		$.ajax({
	        url : base_url+"Api/Api1/updateTopic",
	        type: "POST",
	        data: formData, 
	        cache: false, 
	        async: false,
	        processData: false,
	        contentType: false,
	        success: function(data)
	        {
	          if(data.status==true){
	            $(window).scrollTop(0);
	            $(".alert").css('display','block');
	            $(".alert").attr('class', 'alert alert-success');
	            $('#msg').html(data.message);
	            $("#topic_form")[0].reset();
	           // window.location.replace(base_url+'manage_course');
	          }else{
	             $(".alert").css('display','block');
	             $(".alert").attr('class', 'alert alert-danger');
	             $('#msg').html(data.message);
	          }
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $(".alert").css('display','block');
	            $(".alert").attr('class', 'alert alert-danger');
	            $('#msg').html('Error adding / update data!');
	        }
	    })
	}




}

function downloadQuestionBank(val,str)
{
	//alert(action);
	window.location.href=base_url+'download-file/'+val.title+'/'+str;

	
	/*if("" != val.title)
	{
		$.ajax({

			type : "POST",
			url : base_url+'download-file',
			data :
			{
				// 'action' : action,
				'file' : val.title
			},
			success : function(response)
			{
				console.log(response);
			}

		});
	}*/
}