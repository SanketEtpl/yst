$(document).ready(function(event){
 // $('#form')[0].reset();

  $('#password').bind('paste', function (e) {
     e.preventDefault();
     $('.err_password').html("You cannot paste text into this textbox!").show().delay(3000).fadeOut();
  });
  $('#remember_me').on('click',function(){
    var c = $("input[type='checkbox']").val();
    alert(c);
  });
});

$(document).keypress(function(event){

  if(13 == event.keyCode)
  {
    var username = $('#username').val();
    var password = $('#password').val();
    var user = $('.usertype').val();

    if("" != username && "" != password)
    {
    	if('student' == user)
    	{
    		studentLogin();
    	}
    	else if('admin' == user)
    	{
    		loginMe();
    	}
        
    }
  }

});

function loginMe(){
  var formData = new FormData( $("#form")[0] );
  var url;
  res = valid();
  if(res){
     $.ajax({
        url : base_url+"Api/Api/loginMe",
        type: "POST",
        data: formData, 
        cache: false, 
        async: false,
        processData: false,
        contentType: false,
        success: function(data)
        {
          if(data.status==true){
            // $(".alert").css('display','block');
            // $(".alert").attr('class', 'alert alert-success');
            // $('#msg').html(data.message);
            //$("#form")[0].reset();
            $(window).scrollTop(0);
              setTimeout(function(){
                 window.location.replace(base_url+'home');
              }, 500);
          }else{
             $(".alert").css('display','block');
             $(".alert").attr('class', 'alert alert-danger');
             $('#msg').html(data.message);
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-danger');
            $('#msg').html('Error adding / update data!');
        }
    });
  }

}

function valid(status){
  var username = $('.username').val();
  var password = $('.password').val();
  
  var flag = '1';

  if(username == '')
  {
    $('.err_username').show().delay(3000).fadeOut();
    flag = 0;
  }
  else
  {
    $('.err_username').hide();
  }

  if(username != '')
  {
      if($.trim(username) == '')
      {
        $('.err_username').show().delay(3000).fadeOut();
        flag = 0;
      }
      else
      {
        $('.err_name').hide();
      }
      if(username != ''){
        var res = isValidEmailAddress($.trim(username));
        if(res == true){
          $('.err_username').hide();
        }
        else{
          $('.err_username').show().delay(3000).fadeOut();
          flag = 0;
        }
      }
  }

  if(password == '')
  {
    $('.err_password').show().delay(3000).fadeOut();
    flag = 0;
  }
  else
  {
    $('.err_password').hide();
  }

  if(password != '')
  {
      if($.trim(password) == '')
      {
        $('.err_password').show().delay(3000).fadeOut();
        flag = 0;
      }
      else
      {
        $('.err_password').hide();
      }
      // if(password != ''){
      //   var res = isvalidPassword($.trim(password));
      //   //alert(res);
      //   if(res == true){
      //     $('.err_password').hide();
      //   }
      //   else{
      //     $('.err_password').show().delay(3000).fadeOut();
      //     flag = 0;
      //   }
      // }
  }
  return flag;
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}
function isvalidPassword(password){
    //var pattern = new RegExp(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,15}$/);
    var pattern = new RegExp(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])(?=.*\d).{8,15}$/);
    return pattern.test(password);
}

function forgot_password(){
    var formData = new FormData( $("#forgot_pass")[0] );
    var email_address = $('#email_address').val();
    var flag = '1';
    if(email_address == '')
    {
      $('.err_email').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_email').hide();
    }
    if(email_address != ''){
      if($.trim(email_address) == '')
        {
          $('.err_email').show().delay(3000).fadeOut();
          flag = 0;
        }
        else
        {
          $('.err_email').hide();
        }
      var res = isValidEmailAddress($.trim(email_address));
        if(res == true){
          $('.err_email').hide();
        }
        else{
          $('.err_email').show().delay(3000).fadeOut();
          flag = 0;
        }
    }
    if(flag == '1'){
         $.ajax({
          url : base_url+"Api/Api/forgot_password",
          type: "POST",
          data: formData, 
          cache: false, //$('#form').serialize(),
          async: false,
          processData: false,
          contentType: false,
          //dataType: "JSON",
         // mimeType:"multipart/form-data",
          success: function(data)
          {
            if(data.status==true){
              $(".alert").css('display','block');
              $(".alert").attr('class', 'alert alert-success');
              $('#msg').html(data.message);
              $("#form")[0].reset();
              window.location.replace(base_url+'home');
            }else{
               $(".alert").css('display','block');
               $(".alert").attr('class', 'alert alert-danger');
               $('#msg').html(data.message);
            }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              $(".alert").css('display','block');
              $(".alert").attr('class', 'alert alert-danger');
              $('#msg').html('Error adding / update data!');
          }
      });

    }
}

function studentLogin(){
  var formData = new FormData( $("#student_form")[0] );
  var url;
  res = valid();
  if(res){
     $.ajax({
        url : base_url+"Api/Api/studentLogin",
        type: "POST",
        data: formData, 
        cache: false,
        async: false,
        processData: false,
        contentType: false,
        success: function(data)
        {
          if(data.status==true){
            // $(".alert").css('display','block');
            // $(".alert").attr('class', 'alert alert-success');
            // $('#msg').html(data.message);
            //$("#student_form")[0].reset();
            window.location.replace(base_url+'student-home');
            /*$(window).scrollTop(0);
              setTimeout(function(){
              }, 3000);*/
          }else{
             $(".alert").css('display','block');
             $(".alert").attr('class', 'alert alert-danger');
             $('#msg').html(data.message);
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-danger');
            $('#msg').html('Error adding / update data!');
        }
    });
  }

}
