
function show_form()
{
  //alert();
  $( "#applicable_from_date" ).datepicker({dateFormat: 'yy-mm-dd'});
  $( "#applicable_to_date" ).datepicker({dateFormat: 'yy-mm-dd'});
  $('#manage_content_logo_form').toggle();
}

//save opffer-------------
var save_method5='add'; //for save method string
var table;
function save_offer()
{
 //alert(save_method5);
    var formData = new FormData( $("#form")[0] );
    var url;
    if(save_method5 == 'add')
    {
      url = base_url+"Api/Api/save_offer";//alert(url);
    }
    else
    {
      url = base_url+"Api/Api/update_offer";
    }
    res = 1;//valid();//alert(res);
    if(res)
    {  
      $.ajax({
        url : url,
        type: "POST",
        data: formData, 
        cache: false, //$('#form').serialize(),
        async: false,
        processData: false,
        contentType: false,
        //dataType: "JSON",
       // mimeType:"multipart/form-data",
        success: function(data)
        {//alert(data.status);
          if(data.status==true){
            // $(".alert").css('display','block');
            // $(".alert").attr('class', 'alert alert-success');
            // $('#msg').html(data.message);
            $('.model_content p').html('<h5>'+data.message+'</h5');
            $('#add_course_popup').modal('show');
            $("#form")[0].reset();
            $(window).scrollTop(0);
           
          }else{
             $(".alert").css('display','block');
             $(".alert").attr('class', 'alert alert-danger');
             $('#msg').html(data.message);
             $(window).scrollTop(0);
          }
          //save_method ='';
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            //alert('Error adding / update data');
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-danger');
            $('#msg').html('Error adding / update data!');
            $(window).scrollTop(0);
        }
    });

  }
}
//-----delete offer-----

function delete_offer(id)
{//alert(id);
  if(confirm('Are you sure you want to delete this Offer ?'))
  {
    $.ajax({
      url : base_url+"Api/Api/delete_offer/",
      type: "POST",
      dataType: "JSON",
      data:{id:id},
      success: function(data)
      { //alert(data);alert(data.message);
         //$(".alert").css('display','block');
         //$('#msg').html(data.message);
          $('.model_content p').html('<h5>'+data.message+'</h5');
          $('#add_course_popup').modal('show');
          $(window).scrollTop(0);
        // $('.offer').load(document.URL + ' .offer');
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
         // alert('Error deleting data');
          $(".alert").css('display','block');
          $(".alert").attr('class', 'alert alert-danger');
          $('#msg').html('Error deleting data!');
          $(window).scrollTop(0);
      }
    });

  }
}

//---edit offer
function edit_offer(id)
    {//alert(id);
      //$('#manage_content_logo_form').show();
      show_form();
      save_method5 = 'update';
      $('#form')[0].reset(); // reset form on modals
      $(".field_error").css('display','none');
      $.ajax({
            url : base_url+"Api/Api/get_offer_byId/",
            type: "POST",
            dataType: "JSON",
            data:{id:id},
            success: function(data)
            {
              //console.log(data);
              $('[name="id"]').val(data.data.id);
              $('[name="title"]').val(data.data.title);
              $('[name="applicable_to_date"]').val(data.data.applicable_to_date);
              $('[name="applicable_from_date"]').val(data.data.applicable_from_date);
              $('[name="details"]').val(data.data.details);
              
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $(".alert").css('display','block');
                $(".alert").attr('class', 'alert alert-danger');
                $('#msg').html('There is problem retriving data!');
                $(window).scrollTop(0);
            }
           
        });
    }
