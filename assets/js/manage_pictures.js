function show_pic_form()
{
  //alert();
 // $('#faq_div').show();
   $('#pic_div').toggle();
   $('#picImgDiv').hide();
   $("#pic_from")[0].reset();

}
//-----delete pic-----

function deletePic(id)
{//alert(id);
  if(confirm('Are you sure you want to delete this Picture ?'))
  {
    $.ajax({
      url : base_url+"Api/Api/deletePictureOrClip/",
      type: "POST",
      dataType: "JSON",
      data:{id:id},
      success: function(data)
      { 
          $('.model_content p').html('<h5>'+data.message+'</h5');
          $('#add_course_popup').modal('show');
          $(window).scrollTop(0);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
         // alert('Error deleting data');
          $(".alert").css('display','block');
          $(".alert").attr('class', 'alert alert-danger');
          $('#msg').html('Error deleting data!');
          $(window).scrollTop(0);
      }
    });

  }
}


//save opffer-------------
var save_method2='add'; //for save method string
var table;
function savePic()
{
 //alert(save_method2);
    var formData = new FormData( $("#pic_from")[0] );
    var url;
    if(save_method2 == 'add')
    {
      url = base_url+"Api/Api/savePictureOrClip";//alert(url);
    }
    else
    {
      url = base_url+"Api/Api/update_picturesOrCLip";
    }
    res = 1;//valid();//alert(res);
    if(res)
    {  
      $.ajax({
        url : url,
        type: "POST",
        data: formData, 
        cache: false, //$('#form').serialize(),
        async: false,
        processData: false,
        contentType: false,
        //dataType: "JSON",
       // mimeType:"multipart/form-data",
        success: function(data)
        {//alert(data.status);
          if(data.status==true){
            //$(".alert").css('display','block');
            //$(".alert").attr('class', 'alert alert-success');
            //$('#msg').html(data.message);
            $('.model_content p').html('<h5>'+data.message+'</h5');
            $('#add_course_popup').modal('show');
            $("#pic_from")[0].reset();
            $(window).scrollTop(0);
           
          }else{
             $(".alert").css('display','block');
             $(".alert").attr('class', 'alert alert-danger');
             $('#msg').html(data.message);
             $(window).scrollTop(0);
          }
          //save_method ='';
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            //alert('Error adding / update data');
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-danger');
            $('#msg').html('Error adding / update data!');
            $(window).scrollTop(0);
        }
    });

  }
}


//---edit picture
function editPic(id)
    {
      show_pic_form();
      save_method2 = 'update';
      $('#pic_from')[0].reset(); // reset form on modals
      $(".field_error").css('display','none');
      $.ajax({
            url : base_url+"Api/Api/get_pic_byId/",
            type: "POST",
            dataType: "JSON",
            data:{id:id},
            success: function(data)
            {
              console.log(data);
              $('[name="id"]').val(data.data.id);
              $('[name="name"]').val(data.data.name);
              $('#pic_image').attr('src',base_url+'uploads/manage_content/picture/'+data.data.image);
              $('#picImgDiv').show();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $(".alert").css('display','block');
                $(".alert").attr('class', 'alert alert-danger');
                $('#msg').html('There is problem retriving data!');
                $(window).scrollTop(0);
            }
           
        });
    }
