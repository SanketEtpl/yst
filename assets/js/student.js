$(document).ready(function(){
  $('#stud_table').DataTable();
  
});

function addUser(){
  var formData = new FormData( $("#addnewuser")[0] );
  var url;
  res = valid();
  if(res){
     $.ajax({
        url : base_url+"Api/Api/addUser",
        type: "POST",
        data: formData, 
        cache: false, 
        async: false,
        processData: false,
        contentType: false,
        success: function(data)
        {
          if(data.status==true){
            $('#add_new_new_popup').modal('hide');
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-success');
            $('#msg').html(data.message);
            setTimeout(function(){
               window.location.reload(1);
            }, 5000);
            $("#addnewuser")[0].reset();
          }else{
             $('#add_new_new_popup').modal('hide');
             $(".alert").css('display','block');
             $(".alert").attr('class', 'alert alert-danger');
             $('#msg').html(data.message);
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#add_new_new_popup').modal('hide');
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-danger');
            $('#msg').html('Error adding / update data!');
        }
    });
  }
}


function valid(status){
    var first_name = $('#first_name').val();
    var last_name = $('#last_name').val();
    var email_id = $('#email_id').val();
    
    var flag = '1';

    if(first_name == '')
    {
      $('.err_fname').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_fname').hide();
    }

    if(first_name != '')
    {
        if($.trim(first_name) == '')
        {
          $('.err_fname').show().delay(3000).fadeOut();
          flag = 0;
        }
        else
        {
          $('.err_name').hide();
        }
    }

    if(last_name == '')
    {
      $('.err_lname').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_lname').hide();
    }

    if(last_name != '')
    {
        if($.trim(last_name) == '')
        {
          $('.err_lname').show().delay(3000).fadeOut();
          flag = 0;
        }
        else
        {
          $('.err_lname').hide();
        }
    }
    if(email_id == '')
    {
      $('.err_email').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_email').hide();
    }
    if(email_id != '')
    {
        if($.trim(email_id) == '')
        {
          $('.err_email').show().delay(3000).fadeOut();
          flag = 0;
        }
        else
        {
          $('.err_email').hide();
        }
        if(email_id != ''){
          var res = isValidEmailAddress($.trim(email_id));
          if(res == true){
            $('.err_email').hide();
          }
          else{
            $('.err_email').show().delay(3000).fadeOut();
            flag = 0;
          }
        }
    }
    return flag;
}
  function isValidEmailAddress(emailAddress) {
      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      return pattern.test(emailAddress);
  }

