
function show_faqform()
{
  //alert();
 // $('#faq_div').show();
   $('#faq_div').toggle();
}

//save opffer-------------
var save_method4='add'; //for save method string
var table;
function save_faq()
{
 //alert(save_method4);
    var formData = new FormData( $("#faq_form")[0] );
    var url;
    if(save_method4 == 'add')
    {
      url = base_url+"Api/Api/saveFaq";//alert(url);
    }
    else
    {
      url = base_url+"Api/Api/updateFaq";
    }
    res = 1;//valid();//alert(res);
    if(res)
    {  
      $.ajax({
        url : url,
        type: "POST",
        data: formData, 
        cache: false, //$('#form').serialize(),
        async: false,
        processData: false,
        contentType: false,
        //dataType: "JSON",
       // mimeType:"multipart/form-data",
        success: function(data)
        {//alert(data.status);
          if(data.status==true){
            //$(".alert").css('display','block');
            //$(".alert").attr('class', 'alert alert-success');
            //$('#msg').html(data.message);
            $('.model_content p').html('<h5>'+data.message+'</h5');
            $('#add_course_popup').modal('show');
            $("#form")[0].reset();
            $(window).scrollTop(0);
           
          }else{
             $(".alert").css('display','block');
             $(".alert").attr('class', 'alert alert-danger');
             $('#msg').html(data.message);
             $(window).scrollTop(0);
          }
          //save_method ='';
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            //alert('Error adding / update data');
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-danger');
            $('#msg').html('Error adding / update data!');
            $(window).scrollTop(0);
        }
    });

  }
}
//-----delete faq-----

function deleteFaq(id)
{//alert(id);
  if(confirm('Are you sure you want to delete this FAQ ?'))
  {
    $.ajax({
      url : base_url+"Api/Api/deleteFaq/",
      type: "POST",
      dataType: "JSON",
      data:{id:id},
      success: function(data)
      { 
          $('.model_content p').html('<h5>'+data.message+'</h5');
          $('#add_course_popup').modal('show');
          $(window).scrollTop(0);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
         // alert('Error deleting data');
          $(".alert").css('display','block');
          $(".alert").attr('class', 'alert alert-danger');
          $('#msg').html('Error deleting data!');
          $(window).scrollTop(0);
      }
    });

  }
}


//---edit faq
function editFaq(id)
    {//alert(id);
      show_faqform();
      save_method4 = 'update';
      $('#form')[0].reset(); // reset form on modals
      $(".field_error").css('display','none');
      $.ajax({
            url : base_url+"Api/Api/get_faq_byId/",
            type: "POST",
            dataType: "JSON",
            data:{id:id},
            success: function(data)
            {
              //console.log(data);
              $('[name="id"]').val(data.data.id);
              $('[name="question"]').val(data.data.question);
              $('[name="answer"]').val(data.data.answer);
              $('html, body').animate({
                  scrollTop: $("#faq_div").offset().top
              }, 200);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $(".alert").css('display','block');
                $(".alert").attr('class', 'alert alert-danger');
                $('#msg').html('There is problem retriving data!');
                $(window).scrollTop(0);
            }       
        });
    }
