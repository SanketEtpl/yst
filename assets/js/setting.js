function changePass() {
	var oldTextPAss=$('#oldpassword').val();//alert(oldTextPAss);
	var oldDBPAss=$('#oldPassDB').val();//alert(oldDBPAss);
	var new_password=$('#new_password').val();//alert(oldTextPAss);
	var rtype_password=$('#rtype_password').val();//alert(oldDBPAss);
	if(oldTextPAss==oldDBPAss)
	{//alert('old same');
		if(new_password !="" && rtype_password!="")
		{
	    	res = valid();alert(res);
    	if(res)
    	{
	    	if(new_password==rtype_password)
			{
			//alert('new same');
			 var formData = new FormData( $("#form")[0] );
		     var url;
		     url = base_url+"admin/Setting/change_password";//alert(url);
		    
		      $.ajax({
			        url : url,
			        type: "POST",
			        data: formData, 
			        cache: false, //$('#form').serialize(),
			        async: false,
			        processData: false,
			        contentType: false,
			        dataType: "JSON",
			       // mimeType:"multipart/form-data",
			        success: function(data)
			        {//alert(data.status);
			          if(data.status=='success'){
			            $(".alert").css('display','block');
			            $(".alert").attr('class', 'alert alert-success');
			            $('.alert').html(data.msg).fadeOut(3000);
			            $("#form")[0].reset();
			          setTimeout(location.reload.bind(location), 3000);
			            $(window).scrollTop(0);
			           
			          }else{
			             $(".alert").css('display','block');
			             $(".alert").attr('class', 'alert alert-danger');
			             $('#msg').html(data.message);
			             $(window).scrollTop(0);
			          }
			        },
			        error: function (jqXHR, textStatus, errorThrown)
			        {
			            //alert('Error adding / update data');
			            $(".alert").css('display','block');
			            $(".alert").attr('class', 'alert alert-danger');
			            $('#msg').html('Error adding / update data!');
			            $(window).scrollTop(0);
			        }
		   	    });
			}else{
			$('#retypep_err').show().fadeOut(3000);
			}
		  }//*	
	 	}else{
	  		$('#passmiss_err').show().fadeOut(3000);
	  	}	
	}else{
		//alert('not same');
		$('#oldp_err').show().fadeOut(3000);
	}
}

function valid(){//alert();
	var flag = '1';
	 if($('#new_password').val() != ''){
        var res = isvalidPassword($.trim($('#new_password').val()));//alert(res);
        if(res == true){
          $('#err_npass').hide();
        }
        else{
          $('#err_npass').html('Password field should between 8 and 15 characters containing combination of alphabets and numbers, and must contain atleast one special symbol and one captial alphabet').show().delay(10000).fadeOut();
          flag = 0;
        }
    }return flag;
}

function isvalidPassword(password){
    //var pattern = new RegExp(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,15}$/);
    var pattern = new RegExp(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])(?=.*\d).{8,15}$/);
    return pattern.test(password);
}