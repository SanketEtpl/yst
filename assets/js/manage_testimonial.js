$(document).ready(function(){
  $('.rating_star').on('click',function(){//alert();
      var rating = $(this).val();
      $('#rating_star').val(rating);
      var flag=0;
      for(var i=0;i<=5;i++){
        if(flag==0){
         $('.star'+i).removeClass('fa fa-star-o').addClass('fa fa-star');
        }
        else{
         $('.star'+i).removeClass('fa fa-star').addClass('fa fa-star-o');
        }
        if(i==rating){
          flag=1;
        }
      }
  });
});
function show_testi_form()
{
  //alert();
 // $('#faq_div').show();
   $('#testi_div').toggle();
   $('#testimImgDiv').hide();
   $("#testi_form")[0].reset();

}
//-----delete Testimonial-----

function deleteTestim(id)
{//alert(id);
  if(confirm('Are you sure you want to delete this Testimonial ?'))
  {
    $.ajax({
      url : base_url+"Api/Api/deleteTestimonials/",
      type: "POST",
      dataType: "JSON",
      data:{id:id},
      success: function(data)
      { 
          $('.model_content p').html('<h5>'+data.message+'</h5');
          $('#add_course_popup').modal('show');
          $(window).scrollTop(0);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
         // alert('Error deleting data');
          $(".alert").css('display','block');
          $(".alert").attr('class', 'alert alert-danger');
          $('#msg').html('Error deleting data!');
          $(window).scrollTop(0);
      }
    });

  }
}


//save opffer-------------
var save_method1='add'; //for save method string
var table;
function saveTestim()
{
 //alert(save_method1);
    var formData = new FormData( $("#testi_form")[0] );
    var url;
    if(save_method1 == 'add')
    {
      url = base_url+"Api/Api/saveTestimonials";//alert(url);
    }
    else
    {
      url = base_url+"Api/Api/updateTestimonials";
    }
    res = 1;//valid();//alert(res);
    if(res)
    {  
      $.ajax({
        url : url,
        type: "POST",
        data: formData, 
        cache: false, //$('#form').serialize(),
        async: false,
        processData: false,
        contentType: false,
        //dataType: "JSON",
       // mimeType:"multipart/form-data",
        success: function(data)
        {//alert(data.message);
          if(data.status==true){
            $('.model_content p').html('<h5>'+data.message+'</h5');
            $('#add_course_popup').modal('show');
            $("#testi_form")[0].reset();
            $(window).scrollTop(0);
           
          }else{
             $(".alert").css('display','block');
             $(".alert").attr('class', 'alert alert-danger').delay(3000).fadeOut();
             $('#msg').html(data.message);
             $(window).scrollTop(0);
          }
          //save_method1 ='';
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            //alert('Error adding / update data');
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-danger');
            $('#msg').html('Error adding / update data!');
            $(window).scrollTop(0);
        }
    });

  }
}


//---edit Testimonial
function editTestim(id)
    {
      show_testi_form();
      save_method1 = 'update';
      $('#testi_form')[0].reset(); // reset form on modals
      $(".field_error").css('display','none');
      $.ajax({
            url : base_url+"Api/Api/get_Testim_byId/",
            type: "POST",
            dataType: "JSON",
            data:{id:id},
            success: function(data)
            {
              console.log(data);
              $('[name="id"]').val(data.data.id);
              $('[name="name"]').val(data.data.name);
              $('[name="comments"]').val(data.data.comments);
                var rating = data.data.rating;
                var flag=0;
                for(var i=0;i<=5;i++)
                {
                  if(flag==0){
                   $('.star'+i).removeClass('fa fa-star-o').addClass('fa fa-star');
                  }
                  else{
                   $('.star'+i).removeClass('fa fa-star').addClass('fa fa-star-o');
                  }
                  if(i==rating){
                    flag=1;
                  }
                }
             // $('[name="id"]').val(data.data.rating);
              $('#testim_image').attr('src',base_url+'uploads/manage_content/testimonials/'+data.data.image);
              $('#testimImgDiv').show();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $(".alert").css('display','block');
                $(".alert").attr('class', 'alert alert-danger');
                $('#msg').html('There is problem retriving data!');
                $(window).scrollTop(0);
            }
           
        });
    }
