$(document).ready(function(){
    $('#duration1').on('click',function(){
       // var date = $('#txt_date').val();
        var start_time = $('#from_time').val();
        var end_time = $('#to_time').val();
        if(start_time && end_time){
            var diff = Math.abs(new Date("1970-1-1"+' '+end_time) - new Date("1970-1-1"+' '+start_time));
            var diff_time = (diff/(60*60*1000))*60;
            $('#duration1').val(diff_time);
        }
        else{
            var duration = $('#duration1').val('');
        }
    });
    $('#tot_duration').on('click',function(){
      var date = $('#date').val();
      var start_time = $('#start_time').val();
      var end_time = $('#end_time').val();
      if(date && start_time && end_time){
          var diff = Math.abs(new Date(date+' '+end_time) - new Date(date+' '+start_time));
          var diff_time = (diff/(60*60*1000))*60;
          $('#tot_duration').val(diff_time);
      }
      else{
          var duration = $('#tot_duration').val('');
      }
  });
    
 });
 function setAvailability(){
    var formData = new FormData( $("#setAvailFrm")[0] );
    var url;
    res = field_valid();
    if(res){
       $.ajax({
          url : base_url+"Api/Api/setAvailability",
          type: "POST",
          data: formData, 
          cache: false, 
          async: false,
          processData: false,
          contentType: false,
          success: function(data)
          {
            if(data.status==true){
              $("#setAvailFrm")[0].reset();
              $(".alert").css('display','block');
              $(".alert").attr('class', 'alert alert-success');
              $('#msg2').html(data.message);
              $(window).scrollTop(0);
              setTimeout(function(){
                 window.location.replace(base_url+'my_calender');
              }, 3000);
            }else{
              $(window).scrollTop(0);
               $('#setAvailFrm').modal('hide');
               $(".alert").css('display','block');
               $(".alert").attr('class', 'alert alert-danger');
               $('#msg2').html(data.message);
            }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              $('#setAvailFrm').modal('hide');
              $(".alert").css('display','block');
              $(".alert").attr('class', 'alert alert-danger');
              $('#msg2').html('Error adding / update data!');
          }
      });
    }
  }
  function field_valid(){
      var course_id = $('.course-id option:selected').val();
      var subject_id = $('.select_subject option:selected').val();
      var topic_id = $('.select_topic option:selected').val();
      var cost = $('#cost').val();
      var date = $('#txt_date').val();
      //alert(date);
      var start_time = $('#from_time').val();
      var end_time = $('#to_time').val();
      var duration = $('#duration1').val();
   
      var flag = '1';
  
      if(course_id == 'Select Course')
      {
        $('.err_course').show().delay(3000).fadeOut();
        flag = 0;
      }
      else
      {
        $('.err_course').hide();
      }
      if(subject_id == 'Select Subject')
      {
        $('.err_subject').show().delay(3000).fadeOut();
        flag = 0;
      }
      else
      {
        $('.err_subject').hide();
      }
      
      if(topic_id == 'Select Topic')
      {
        $('.err_topic').show().delay(3000).fadeOut();
        flag = 0;
      }
      else
      {
        $('.err_topic').hide();
      }
      if(date == '')
      {
        $('.err_date').show().delay(3000).fadeOut();
        flag = 0;
      }
      else
      {
        $('.err_date').hide();
      }
      if(start_time == '')
      {
        $('.err_stime').show().delay(3000).fadeOut();
        flag = 0;
      }
      else
      {
        $('.err_stime').hide();
      }
      if(end_time == '')
      {
        $('.err_etime').show().delay(3000).fadeOut();
        flag = 0;
      } 
      else
      {
        $('.err_etime').hide();
      }
      if(duration == '')
      {
        $('.err_duration').show().delay(3000).fadeOut();
        flag = 0;
      }
      else
      {
        $('.err_duration').hide();
      }
      if(duration != '')
      {
          if($.trim(duration) == '')
          {
            $('.err_duration').show().delay(3000).fadeOut();
            flag = 0;
          }
          else
          {
            $('.err_duration').hide();
          }
          if($.isNumeric(duration)){
            $('.err_duration').hide();
          }
          else{
            $('.err_duration').html('Please enter valid duration.').show().delay(3000).fadeOut();
              flag = 0;
          }
      }
      if(cost == '')
      {
        $('.err_cost').show().delay(3000).fadeOut();
        flag = 0;
      }
      else
      {
        $('.err_cost').hide();
      }
      if(cost != '')
      {
          if($.trim(cost) == '')
          {
            $('.err_cost').show().delay(3000).fadeOut();
            flag = 0;
          }
          else
          {
            $('.err_cost').hide();
          }
          if($.isNumeric(cost)){
            $('.err_cost').hide();
          }
          else{
            $('.err_cost').html('Please enter valid amount.').show().delay(3000).fadeOut();
              flag = 0;
          }
      }
      return flag;
  }
function getSubjectList(){
  var course_id = $('.select_course option:selected').val();
  if(course_id!='selected'){
    $.ajax({
          url : base_url+"Api/Api/subjectList?id=" + course_id,
          success: function(result)
          {
            var res = result.data;
            if(result.status==true){
              var html = '';
                html+='<option value="selected">Select Subject</option>';
              for(var i=0;i<res.length;i++){
                  html+='<option value='+res[i].subject_id+'>'+res[i].subject_name+'</option>';
              }
              $('.select_subjects').html(html);
            }
            else{
              $('.select_subjects').html('<option value="selected">Select Subject</option>');
            }

          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              $('#msg').html('Error adding / update data!');
          }
      })
  }
  else{
    var html='';
    $('.select_subjects').html('<option value="selected">Select Subject</option>');
  }
}
function getTopicList(){
	var subject_id = $('.select_subjects option:selected').val();
	if(subject_id!='selected'){
		$.ajax({
	        url : base_url+"Api/Api/topicList?id=" + subject_id,
	        success: function(result)
	        {
	        	var res = result.data;
		        if(result.status==true){
			        var html = '';
			        	html+='<option value="selected">Select Topic</option>';
			        for(var i=0;i<res.length;i++){
			        	html+='<option value='+res[i].topic_id+'>'+res[i].topic_name+'</option>';
			        }
			        $('.select_topics').html(html);
		        }
		        else{
			        $('.select_topics').html('<option value="selected">Select Topic</option>');
		        }

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $('#msg').html('Error adding / update data!');
	        }
	    })
	}
	else{
		var html='';
		$('.select_topics').html('<option value="selected">Select Topic</option>');
	}
}
function add_session(){
  var formData = new FormData( $("#addSessionfrom")[0] );
  var url;
  res = session_valid();
  if(res){
     $.ajax({
        url : base_url+"Api/Api/addSession",
        type: "POST",
        data: formData, 
        cache: false, 
        async: false,
        processData: false,
        contentType: false,
        success: function(data)
        {
          if(data.status==true){
            $("#addSessionfrom")[0].reset();
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-success');
            $('#msg').html(data.message);
            $(window).scrollTop(0);
            setTimeout(function(){
               window.location.replace(base_url+'my_calender');
            }, 3000);
          }else{
            $(window).scrollTop(0);
             //$('#add_new_new_popup').modal('hide');
             $(".alert").css('display','block');
             $(".alert").attr('class', 'alert alert-danger');
             $('#msg1').html(data.message);
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            //$('#add_new_new_popup').modal('hide');
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-danger');
            $('#msg').html('Error adding / update data!');
        }
    });
  }
}
function session_valid(){
      var course_id = $('.select_course option:selected').val();
	    var subject_id = $('.select_subjects option:selected').val();
	    var topic_id = $('.select_topics option:selected').val();
	    var title = $('#title').val();
	    var date = $('#date').val();
	    var start_time = $('.start_time').val();
	    var end_time = $('.end_time').val();
	    var duration = $('#tot_duration').val();
	    var cost = $('.cost').val();

    var flag = '1';

    if(course_id == 'Select Course')
    {
      $('.err_course1').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_course1').hide();
    }
    if(subject_id == 'Select Subject')
    {
      $('.err_subject1').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_subject1').hide();
    }
    
    if(topic_id == 'Select Topic')
    {
      $('.err_topic1').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_topic1').hide();
    }
    if(title == '')
    {
      $('.err_title1').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_title1').hide();
    }
    if(title != '')
    {
        if($.trim(title) == '')
        {
          $('.err_title1').show().delay(3000).fadeOut();
          flag = 0;
        }
        else
        {
          $('.err_title1').hide();
        }
    }
    if(date == '')
    {
      $('.err_date1').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_date1').hide();
    }
    if(start_time == '')
    {
      $('.err_sdate1').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_sdate1').hide();
    }
    if(end_time == '')
    {
      $('.err_edate1').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_edate1').hide();
    }
    if(duration == '')
    {
      $('.err_duration1').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_duration1').hide();
    }
    if(duration != '')
    {
        if($.trim(duration) == '')
        {
          $('.err_duration1').show().delay(3000).fadeOut();
          flag = 0;
        }
        else
        {
          $('.err_duration1').hide();
        }
        if($.isNumeric(duration)){
        	$('.err_duration1').hide();
        }
        else{
        	$('.err_duration1').html('Please enter valid duration.').show().delay(3000).fadeOut();
            flag = 0;
        }
    }
    if(cost == '')
    {
      $('.err_cost1').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_cost1').hide();
    }
    if(cost != '')
    {
        if($.trim(cost) == '')
        {
          $('.err_cost1').show().delay(3000).fadeOut();
          flag = 0;
        }
        else
        {
          $('.err_cost1').hide();
        }
        if($.isNumeric(cost)){
        	$('.err_cost1').hide();
        }
        else{
        	$('.err_cost1').html('Please enter valid amount.').show().delay(3000).fadeOut();
            flag = 0;
        }
    }
    return flag;
}