
//---edit approach
function editApproach(id)
{//alert(id);
  $('#saving').show();
  $('#approach_content').removeAttr('readonly');
}
//---edit aboutme
function editAbout(id)
{
  $('#saving1').show();
  $('#aboutme_content').removeAttr('readonly');
}
//---edit contact
function editContact(id)
{
  $('#saving2').show();
  $('#contact_content').removeAttr('readonly');
}
//---edit terms
function editTerms(id)
{
  $('#saving3').show();
  $('#terms_content').removeAttr('readonly');
}
//---edit privacy
function editPrivacy(id)
{
  $('#saving4').show();
  $('#privacy_content').removeAttr('readonly');
}

save_method = 'update';
var table;
function saveContent($type)
{
  // alert($type);exit;
    var url;
    if($type=='my_approach'){
      var formData = new FormData( $("#approach_form")[0] );
    }else if($type=='about_me'){
       var formData = new FormData( $("#aboutme_form")[0] );
    }else if($type=='contact_us'){
       var formData = new FormData( $("#contact_form")[0] );
    }else if($type=='terms_condition'){
       var formData = new FormData( $("#terms_form")[0] );
    }else if($type=='privacy_policy'){
       var formData = new FormData( $("#privacy_form")[0] );
    }
    if(save_method == 'update')
    {
      url = base_url+"Api/Api/update_manage_content";
    }
    res = 1;//valid();//alert(res);
    if(res)
    {  
      $.ajax({
        url : url,
        type: "POST",
        data: formData, 
        cache: false, //$('#form').serialize(),
        async: false,
        processData: false,
        contentType: false,
        //dataType: "JSON",
       // mimeType:"multipart/form-data",
        success: function(data)
        {//alert(data.status);
          if(data.status==true){
            $('.model_content p').html('<h5>'+data.message+'</h5');
            $('#add_course_popup').modal('show');
            $(window).scrollTop(0);
           
          }else{
             $(".alert").css('display','block');
             $(".alert").attr('class', 'alert alert-danger');
             $('#msg').html(data.message);
             $(window).scrollTop(0);
          }
          //save_method ='';
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            //alert('Error adding / update data');
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-danger');
            $('#msg').html('Error adding / update data!');
            $(window).scrollTop(0);
        }
    });

  }
}
//-----delete approach-----
/*
function deleteApproach(id)
{//alert(id);
  if(confirm('Are you sure you want to delete this Approach ?'))
  {
    $.ajax({
      url : base_url+"Api/Api/delete_manage_content/",
      type: "POST",
      dataType: "JSON",
      data:{id:id,key_type:'my_approach'},
      success: function(data)
      { 
          $('.model_content p').html('<h5>'+data.message+'</h5');
          $('#add_course_popup').modal('show');
          $(window).scrollTop(0);
        // $('.offer').load(document.URL + ' .offer');
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
         // alert('Error deleting data');
          $(".alert").css('display','block');
          $(".alert").attr('class', 'alert alert-danger');
          $('#msg').html('Error deleting data!');
          $(window).scrollTop(0);
      }
    });

  }
}*/

