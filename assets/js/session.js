$(document).ready(function(){
  $('#manage_session').DataTable();
	 $('#duration').on('click',function(){
	 	var date = $('#select_date').val();
		var start_time = $('#start_time').val();
		var end_time = $('#end_time').val();
		if(date && start_time && end_time){
		 	var diff = Math.abs(new Date(date+' '+end_time) - new Date(date+' '+start_time));
		    var diff_time = (diff/(60*60*1000))*60;
		 	var duration = $('#duration').val(diff_time);
		}
		else{
		 	var duration = $('#duration').val('');
		}
	 });
	 $('#edit_duration').on('click',function(){
	 	var date = $('#edit_date').val();
		var start_time = $('#edit_start_time').val();
		var end_time = $('#edit_end_time').val();
		if(date && start_time && end_time){
		 	var diff = Math.abs(new Date(date+' '+end_time) - new Date(date+' '+start_time));
		    var diff_time = (diff/(60*60*1000))*60;
		 	var duration = $('#edit_duration').val(diff_time);
		}
		else{
		 	var duration = $('#edit_duration').val('');
		}
	 });
   $('#eduration').on('click',function(){
    var date = $('#select_date').val();
    var start_time = $('#start_time').val();
    var end_time = $('#end_time').val();
    if(date && start_time && end_time){
      var diff = Math.abs(new Date(date+' '+end_time) - new Date(date+' '+start_time));
        var diff_time = (diff/(60*60*1000))*60;
      var duration = $('#eduration').val(diff_time);
    }
    else{
      var duration = $('#eduration').val('');
    }
   });
});

function addSession(){
  var formData = new FormData( $("#form_session")[0] );
  var url;
  res = valid();
  if(res){
     $.ajax({
        url : base_url+"Api/Api/addSession",
        type: "POST",
        data: formData, 
        cache: false, 
        async: false,
        processData: false,
        contentType: false,
        success: function(data)
        {
          if(data.status==true){
            $("#form_session")[0].reset();
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-success');
            $('#msg').html(data.message);
            $(window).scrollTop(0);
            setTimeout(function(){
               window.location.replace(base_url+'manage_session');
            }, 3000);
            //window.location.replace(base_url+'manage_session');
          }else{
            $(window).scrollTop(0);
             $('#add_new_new_popup').modal('hide');
             $(".alert").css('display','block');
             $(".alert").attr('class', 'alert alert-danger');
             $('#msg').html(data.message);
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#add_new_new_popup').modal('hide');
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-danger');
            $('#msg').html('Error adding / update data!');
        }
    });
  }
}

function updateSession(){
  var formData = new FormData( $("#update_session")[0] );
  var url;
  res = valid('edit');
  if(res){
     $.ajax({
        url : base_url+"Api/Api/updateSession",
        type: "POST",
        data: formData, 
        cache: false, 
        async: false,
        processData: false,
        contentType: false,
        success: function(data)
        {
          if(data.status==true){
            $("#update_session")[0].reset();
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-success');
            $('#msg').html(data.message);
            $(window).scrollTop(0);
            setTimeout(function(){
              window.location.reload(1);
            }, 500);
            //window.location.replace(base_url+'manage_session');
          }else{
            $(window).scrollTop(0);
             $('#add_new_new_popup').modal('hide');
             $(".alert").css('display','block');
             $(".alert").attr('class', 'alert alert-danger');
             $('#msg').html(data.message);
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#add_new_new_popup').modal('hide');
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-danger');
            $('#msg').html('Error adding / update data!');
        }
    });
  }
}
function deleteSession(session_id){
	var r = confirm("Are you sure delete this record?");
	var formData = new FormData();
	formData.set('session_id', session_id)
	if (r == true) {
	    $.ajax({
	        url : base_url+"Api/Api/deleteSession",
	        type: "POST",
	        data: formData, 
	        cache: false, 
	        async: false,
	        processData: false,
	        contentType: false,
	        success: function(data)
	        {
	          if(data.status==true){
	            $(".alert").css('display','block');
	            $(".alert").attr('class', 'alert alert-success');
	            $('#msg').html(data.message);
	            setTimeout(function(){
	              window.location.reload(1);
	            }, 1000);
	          }else{
	             $('#add_new_new_popup').modal('hide');
	             $(".alert").css('display','block');
	             $(".alert").attr('class', 'alert alert-danger');
	             $('#msg').html(data.message);
	          }
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $('#add_new_new_popup').modal('hide');
	            $(".alert").css('display','block');
	            $(".alert").attr('class', 'alert alert-danger');
	            $('#msg').html('Error adding / update data!');
	        }
	    });
	} else {
	    txt = "You pressed Cancel!";
	}
}
function getSubjectlist(status=''){
  var course_id = $('.course-id option:selected').val();
   //alert(course_id);
	if(status){
	 $('#edit_subject').val('').html('');
	}
	if(course_id!='selected'){
		$.ajax({
	        url : base_url+"Api/Api/subjectList?id=" + course_id,
	        success: function(result)
	        {
	        	var res = result.data;
		        if(result.status==true){
			        var html = '';
			        	html+='<option value="selected">Select Subject</option>';
			        for(var i=0;i<res.length;i++){
			        	  html+='<option value='+res[i].subject_id+'>'+res[i].subject_name+'</option>';
			        }
			        $('.select_subject').html(html);
		        }
		        else{
			        $('.select_subject').html('<option value="selected">Select Subject</option>');
		        }

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $('#msg').html('Error adding / update data!');
	        }
	    })
	}
	else{
		var html='';
		$('.select_subject').html('<option value="selected">Select Subject</option>');
	}
}

function getTopiclist(status=''){
	var subject_id = $('.select_subject option:selected').val();
	if(status){
	  $('#edit_topic').val('').html('');
	}
	if(subject_id!='selected'){
		$.ajax({
	        url : base_url+"Api/Api/topicList?id=" + subject_id,
	        success: function(result)
	        {
	        	var res = result.data;
		        if(result.status==true){
			        var html = '';
			        	html+='<option value="selected">Select Topic</option>';
			        for(var i=0;i<res.length;i++){
			        	html+='<option value='+res[i].topic_id+'>'+res[i].topic_name+'</option>';
			        }
			        $('.select_topic').html(html);
		        }
		        else{
			        $('.select_topic').html('<option value="selected">Select Topic</option>');
		        }

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $('#msg').html('Error adding / update data!');
	        }
	    })
	}
	else{
		var html='';
		$('.select_topic').html('<option value="selected">Select Topic</option>');
	}
}

function valid(status=''){
	if(status){
		var course_id = $('#edit_course option:selected').val();
	    var subject_id = $('#edit_subject option:selected').val();
	    var topic_id = $('#edit_topic option:selected').val();
	    var title = $('#edit_title').val();
	    var date = $('#edit_date').val();
	    var start_time = $('#edit_start_time').val();
	    var end_time = $('#edit_end_time').val();
	    var duration = $('#edit_duration').val();
	    var cost = $('#edit_cost').val();
	}
	else{
	    var course_id = $('#select_course option:selected').val();
	    var subject_id = $('#select_subject option:selected').val();
	    var topic_id = $('#select_topic option:selected').val();
	    var title = $('#select_title').val();
	    var date = $('#select_date').val();
	    var start_time = $('#start_time').val();
	    var end_time = $('#end_time').val();
	    var duration = $('#duration').val();
	    var cost = $('#cost').val();
	}

    var flag = '1';

    if(course_id == 'Select Course')
    {
      $('.err_course').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_course').hide();
    }
    if(subject_id == 'Select Subject')
    {
      $('.err_subject').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_subject').hide();
    }
    
    if(topic_id == 'Select Topic')
    {
      $('.err_topic').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_topic').hide();
    }
    if(title == '')
    {
      $('.err_title').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_title').hide();
    }
    if(title != '')
    {
        if($.trim(title) == '')
        {
          $('.err_title').show().delay(3000).fadeOut();
          flag = 0;
        }
        else
        {
          $('.err_title').hide();
        }
    }
    if(date == '')
    {
      $('.err_date').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_date').hide();
    }
    if(start_time == '')
    {
      $('.err_stime').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_stime').hide();
    }
    if(end_time == '')
    {
      $('.err_etime').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_etime').hide();
    }
    if(duration == '')
    {
      $('.err_duration').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_duration').hide();
    }
    if(duration != '')
    {
        if($.trim(duration) == '')
        {
          $('.err_duration').show().delay(3000).fadeOut();
          flag = 0;
        }
        else
        {
          $('.err_duration').hide();
        }
        if($.isNumeric(duration)){
        	$('.err_duration').hide();
        }
        else{
        	$('.err_duration').html('Please enter valid duration.').show().delay(3000).fadeOut();
            flag = 0;
        }
    }
    if(cost == '')
    {
      $('.err_cost').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_cost').hide();
    }
    if(cost != '')
    {
        if($.trim(cost) == '')
        {
          $('.err_cost').show().delay(3000).fadeOut();
          flag = 0;
        }
        else
        {
          $('.err_cost').hide();
        }
        if($.isNumeric(cost)){
        	$('.err_cost').hide();
        }
        else{
        	$('.err_cost').html('Please enter valid amount.').show().delay(3000).fadeOut();
            flag = 0;
        }
    }
    return flag;
}
function addindividualSession(){
  var formData = new FormData( $("#addindividualSession")[0] );
  res = onetoone_valid();
  if(res){
     $.ajax({
        url : base_url+"Api/Api/addindividualSession",
        type: "POST",
        data: formData, 
        cache: false, 
        async: false,
        processData: false,
        contentType: false,
        success: function(data)
        {
          if(data.status==true){
            $(window).scrollTop(0);
            //$("#update_session")[0].reset();
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-success');
            $('#msg').html(data.message);
            setTimeout(function(){
              window.location.replace(base_url+'one-to-one-booking');
            }, 3000);
          }
          else{
             $('#add_new_new_popup').modal('hide');
             $(".alert").css('display','block');
             $(".alert").attr('class', 'alert alert-danger');
             $('#msg').html(data.message);
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#add_new_new_popup').modal('hide');
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-danger');
            $('#msg').html('Error adding / update data!');
        }
    });
  }
}

function onetoone_valid(){
  var course_id = $('.course-id option:selected').val();
    var subject_id = $('.select_subject option:selected').val();
    var topic_id = $('.select_topic option:selected').val();
    var title = $('#title').val();
    var date = $('#select_date').val();
    var start_time = $('#start_time').val();
    var end_time = $('#end_time').val();
    var duration = $('#eduration').val();

    var flag = '1';

    if(course_id == 'Select Course')
    {
      $('.err_ecourse').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_ecourse').hide();
    }
    if(subject_id == 'Select Subject')
    {
      $('.err_esubject').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_esubject').hide();
    }
    
    if(topic_id == 'Select Topic')
    {
      $('.err_etopic').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_etopic').hide();
    }
    if(title == '')
    {
      $('.err_etitle').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_etitle').hide();
    }
    if(title != '')
    {
        if($.trim(title) == '')
        {
          $('.err_etitle').show().delay(3000).fadeOut();
          flag = 0;
        }
        else
        {
          $('.err_etitle').hide();
        }
    }
    if(date == '')
    {
      $('.err_edate').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_edate').hide();
    }
    if(start_time == '')
    {
      $('.err_estime').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_estime').hide();
    }
    if(end_time == '')
    {
      $('.err_etime').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_etime').hide();
    }
    if(duration == '')
    {
      $('.err_eduration').show().delay(3000).fadeOut();
      flag = 0;
    }
    else
    {
      $('.err_eduration').hide();
    }
    if(duration != '')
    {
        if($.trim(duration) == '')
        {
          $('.err_eduration').show().delay(3000).fadeOut();
          flag = 0;
        }
        else
        {
          $('.err_eduration').hide();
        }
        if($.isNumeric(duration)){
          $('.err_eduration').hide();
        }
        else{
          $('.err_eduration').html('Please enter valid duration.').show().delay(3000).fadeOut();
            flag = 0;
        }
    }
    return flag;
}