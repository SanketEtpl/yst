$(document).ready(function(){
	$('#updateProfile').click(function(){
		//var formData = new FormData( $("#update_profile")[0]);
	    $.ajax({
	    	type : "POST",
	    	url : base_url+'Api/Api/updateStudentProfile',
	    	data : $( "#update_profile" ).serialize(),
	    	success:function(response)
	    	{
	    		var obj = JSON.parse(response);
	    		//console.log(obj);
	    		if(true == obj.status)
	    		{
	    			//$('#success_update_popup').modal('show');
	    			$(".alert").css('display','block');
		            $(".alert").attr('class', 'alert alert-success');
		            $('#msg').html(obj.message);
	    			setTimeout(function(){
	    				window.location.href = base_url+'/my-profile';
	    			},3000);
	    		}
	    		else
	    		{
	    			$('#msg').html(response);
	    		}
	    	}
	    });
	});

	$('#upload').on('click', function () {
        var form_data = new FormData($("#upload_file")[0]);
        $.ajax({
        	url : base_url+'student/profile/uploadProfileImage',
	        type: "POST",
	        data: form_data, 
	        cache: false, 
	        async: false,
	        processData: false,
	        contentType: false,
            success: function (res) {
            	var response = JSON.parse(res);
            	console.log(response.message);
            	if(response.status==true){
                	window.location.reload();
            	}
            	else{
            		$(".alert").css('display','block');
		            $(".alert").attr('class', 'alert alert-success');
                   $('#msg').html(response.message); // display success response from the PHP script
            	}
            },
            error: function (response) {
                $('#msg').html(response.message); // display error response from the PHP script
            }
        });

        //console.log(form_data);
    });

    $('#changePassword').click(function(){
    	res = valid();
    	if(res)
    	{
    		$('#old_password').css('border','1px solid #ccc');
    		$('#new_password').css('border','1px solid #ccc');
    		$('#retype_password').css('border','1px solid #ccc');
	    	$.ajax({
		    	type : "POST",
		    	url : base_url+'change-password',
		    	data : $( "#change_password" ).serialize(),
		    	success:function(response)
		    	{
		    		console.log(response);
		    		var obj = JSON.parse(response);
		    		//console.log(obj);
		    		if(true == obj.status)
		    		{
		    			$('#old_password').css('border','1px solid #ccc');
		    			$('#new_password').css('border','1px solid #ccc');
			    		$('#retype_password').css('border','1px solid #ccc');
			    		$('#successMsg').removeClass('hide');
			    		setTimeout(function(){

			    			$('#successMsg').addClass('hide');	
			    		},2000);
		    		}
		    		else
		    		{
		    			$('#errMsg').removeClass("hide");
		    			$('#errMsg').html("");
		    			$('#errMsg').text("Something went wrong.");
		    			setTimeout(function(){

			    			$('#errMsg').addClass('hide');	
			    		},2000);
		    		}
		    	}
		    });
		}
    });
});

function getState()
{
	var id =  $('.select_country option:selected').val();
	if("" != id && 0 != id)
	{
		$.ajax({

			type : "POST",
			url : base_url+"student/profile/getState",
			data:
			{
				country_id : id
			},
			success:function(response)
			{
				var obj = JSON.parse(response);
				var str = '<option value="0">Select State</option>';
				$.each(obj.data,function(i,value){
					str +='<option value="'+value.st_id+'">'+value.state_name+'</option>';
				});
				$('.select_state').html("");
				$('.select_city').html("");
				$('.select_state').html(str);
			}


		});
	}
	else
	{
		$('.select_state').html("");
		$('.select_city').html("");
	}
}

function getCity()
{ 
	var id = $('.select_state').val();
	if(0 != id)
	{
		$.ajax({
			type : "POST",
			url : base_url+"student/profile/getCity",
			data:
			{
				state_id : id
			},
			success:function(response)
			{
				var obj = JSON.parse(response);
				var str = '<option value="0">Select City</option>';
				$.each(obj.data,function(i,value){
					str +='<option value="'+value.cty_id+'">'+value.city_name+'</option>';
				});
				$('.select_city').html("");

				$('.select_city').html(str);
			}


		});
	}
}

function valid(){
	var flag = '1';
    if("" == $('#old_password').val())
	{
		$(".err_opass").show().delay(3000).fadeOut();
	    var flag = 0;
	}
	if($.trim($('#old_password').val()) == '')
    {
        $('.err_opass').show().delay(3000).fadeOut();
	    var flag = 0;
    }
	if("" == $('#new_password').val())
	{
		$(".err_npass").show().delay(3000).fadeOut();
	    var flag = 0;
	}
	if($.trim($('#new_password').val()) == '')
    {
        $('.err_npass').show().delay(3000).fadeOut();
	    var flag = 0;
    }
    if($('#new_password').val() != ''){
        var res = isvalidPassword($.trim($('#new_password').val()));
        if(res == true){
          $('.err_npass').hide();
        }
        else{
          $('.err_npass').html('Password field should between 8 and 15 characters containing combination of alphabets and numbers, and must contain atleast one special symbol and one captial alphabet').show().delay(3000).fadeOut();
          flag = 0;
        }
    }
    if($.trim($('#retype_password').val()) == '')
    {
        $('.err_rpass').show().delay(3000).fadeOut();
	    var flag = 0;
    }
	if("" == $('#retype_password').val())
	{
		$(".err_rpass").show().delay(3000).fadeOut();
	    var flag = 0;
	}
	else if($('#new_password').val() != $('#retype_password').val())
	{
		$(".err_rpass").html('New password and Re-type password not same.').show().delay(3000).fadeOut();
	    var flag = 0;
	}
	return flag;
    	
}
function isvalidPassword(password){
    //var pattern = new RegExp(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,15}$/);
    var pattern = new RegExp(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])(?=.*\d).{8,15}$/);
    return pattern.test(password);
}