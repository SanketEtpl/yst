function show_clip_form()
{
  $('#clip_div').toggle();
}

//-----delete clip-----
function deleteClip(id)
{//alert(id);
  if(confirm('Are you sure you want to delete this Clip ?'))
  {
    $.ajax({
      url : base_url+"Api/Api/deletePictureOrClip/",
      type: "POST",
      dataType: "JSON",
      data:{id:id},
      success: function(data)
      { 
          $('.model_content p').html('<h5>'+data.message+'</h5');
          $('#add_course_popup').modal('show');
          $(window).scrollTop(0);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
         // alert('Error deleting data');
          $(".alert").css('display','block');
          $(".alert").attr('class', 'alert alert-danger');
          $('#msg').html('Error deleting data!');
          $(window).scrollTop(0);
      }
    });

  }
}

//save clip-------------
var save_method3='add'; //for save method string
var table;
function saveClip()
{
 //alert(save_method3);
    var formData = new FormData( $("#clip_from")[0] );
    var url;
    if(save_method3 == 'add')
    {
      url = base_url+"Api/Api/savePictureOrClip";//alert(url);
    }
    else
    {
      url = base_url+"Api/Api/updatePictureOrClip";
    }
    res = 1;//valid();//alert(res);
    if(res)
    {  
      $.ajax({
        url : url,
        type: "POST",
        data: formData, 
        cache: false, //$('#form').serialize(),
        async: false,
        processData: false,
        contentType: false,
        //dataType: "JSON",
       // mimeType:"multipart/form-data",
        success: function(data)
        {//alert(data.status);
          if(data.status==true){
            //$(".alert").css('display','block');
            //$(".alert").attr('class', 'alert alert-success');
            //$('#msg').html(data.message);
            $('.model_content p').html('<h5>'+data.message+'</h5');
            $('#add_course_popup').modal('show');
            $("#clip_from")[0].reset();
            $(window).scrollTop(0);
           
          }else{
             $(".alert").css('display','block');
             $(".alert").attr('class', 'alert alert-danger');
             $('#msg').html(data.message);
             $(window).scrollTop(0);
          }
          //save_method ='';
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            //alert('Error adding / update data');
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-danger');
            $('#msg').html('Error adding / update data!');
            $(window).scrollTop(0);
        }
    });

  }
}