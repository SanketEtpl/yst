$(document).ready(function(){
  //$('#form')[0].reset();
	  $('#check1').on('click',function(){
	  	var val = $(this).val();
	  	if(val == 'off'){
	  		$(this).val('on');
	  	}
	  	else{
	  		$(this).val('off');
	  	}
	  });
});

function stateList(){
	var country_id = $('.select_country').val();
	var formData = new FormData();
	formData.set('country_id',country_id);
	if(country_id!='select'){
		$.ajax({
	        url : base_url+"Api/Api1/stateList",
	        type: "POST",
	        data: formData, 
	        cache: false, 
	        async: false,
	        processData: false,
	        contentType: false,
	        success: function(result)
	        {
	        	var res = result.data;
		        if(result.status==true){
			        var html = '';
			        	html+='<option value="select">Select State</option>';
			        for(var i=0;i<res.length;i++){
			        	html+='<option value='+res[i].st_id+'>'+res[i].state_name+'</option>';
			        }
			        $('.select_state').html(html);
		        }
		        else{
			        $('.select_state').html('<option value="select">Select State</option>');
		        }

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $('#msg').html('Error adding / update data!');
	        }
	    })
	}
	else{
		var html='';
		$('.select_state').html('<option value="select">Select State</option>');
	}
}

function cityList(){
	var state_id = $('.select_state').val();
	var formData = new FormData();
	formData.set('state_id',state_id);
	if(state_id!='select'){
		$.ajax({
	        url : base_url+"Api/Api1/cityList",
	        type: "POST",
	        data: formData, 
	        cache: false, 
	        async: false,
	        processData: false,
	        contentType: false,
	        success: function(result)
	        {
	        	var res = result.data;
		        if(result.status==true){
			        var html = '';
			        	html+='<option value="select">Select City</option>';
			        for(var i=0;i<res.length;i++){
			        	html+='<option value='+res[i].cty_id+'>'+res[i].city_name+'</option>';
			        }
			        $('.select_city').html(html);
		        }
		        else{
			        $('.select_city').html('<option value="select">Select City</option>');
		        }

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $('#msg').html('Error adding / update data!');
	        }
	    })
	}
	else{
		var html='';
		$('.select_city').html('<option value="select">Select City</option>');
	}
}

function signUp(){
	var formData = new FormData( $("#signup_form")[0]);
	res = valid();
	if(res){
		$.ajax({
	        url : base_url+"Api/Api1/signUp",
	        type: "POST",
	        data: formData, 
	        cache: false, 
	        async: false,
	        processData: false,
	        contentType: false,
	        success: function(data)
	        {
	          if(data.status==true){
	            $("#signup_form")[0].reset();
	            $(".alert").css('display','block');
	            $(".alert").attr('class', 'alert alert-success');
	            $('#msg').html(data.message);
	            $(window).scrollTop(0);
	            setTimeout(function(){
	               window.location.replace(base_url+'student');
	            }, 3000);
	          }else{
	             $(".alert").css('display','block');
	             $(".alert").attr('class', 'alert alert-danger');
	             $('#msg').html(data.message);
	             $(window).scrollTop(0);
	          }
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $(".alert").css('display','block');
	            $(".alert").attr('class', 'alert alert-danger');
	            $('#msg').html('Error adding / update data!');
	        }
	    })
	}
  }

  function valid() {
    var first_name = $('#first_name').val();
	var last_name = $('#last_name').val();
	var contact_no = $('#contact_no').val();
	var email_id = $('#email_id').val();
	var password = $('#password').val();
	var confirm_password = $('#confirm_password').val();
	var address = $('#address').val();
	var select_country = $('#select_country option:selected').val();
	var select_state = $('#select_state option:selected').val();
	var select_city = $('#select_city option:selected').val();
	var zip_code = $('#zip_code').val();
	var check1 = $('#check1').val();
	flag = 1;
	if (first_name == "") {
		$(".err_fname").show().delay(3000).fadeOut();
		flag = 0;
	}
	else if($.isNumeric(first_name)){
		$(".err_fname").html('Please enter valid first name').show().delay(3000).fadeOut();
		flag = 0;
	}
	else if($.trim(first_name) == '')
    {
        $('.err_fname').show().delay(3000).fadeOut();
        flag = 0;
    }
	else {
		$(".err_fname" + i).hide();
		flag = 1;
	}
	if (last_name == "") {
		$(".err_lname").show().delay(3000).fadeOut();
		flag = 0;
	}
	else if($.trim(last_name) == '')
    {
        $('.err_lname').show().delay(3000).fadeOut();
        flag = 0;
    }
	else if($.isNumeric(last_name)){
		$(".err_lname").html('Please enter valid last name').show().delay(3000).fadeOut();
		flag = 0;
	}
	else {
		$(".err_lname" + i).hide();
		flag = 1;
	}
	if (contact_no == "") {
		$(".err_contact").show().delay(3000).fadeOut();
		flag = 0;
	}
	else if(contact_no.length !=  10){
		$(".err_contact").html('Contact number should be 10 digit').show().delay(3000).fadeOut();
		flag = 0;
	}
	else if($.trim(contact_no) == '')
    {
        $('.err_contact').show().delay(3000).fadeOut();
        flag = 0;
    }
	else {
		$(".err_contact" + i).hide();
		flag = 1;
	}
	if (email_id == "") {
		$(".err_email").show().delay(3000).fadeOut();
		flag = 0;
	}
	else {
		$(".err_email" + i).hide();
		flag = 1;
	}
	if(email_id != ''){
		if($.trim(email_id) == '')
        {
          $('.err_email').show().delay(3000).fadeOut();
          flag = 0;
        }
        else
        {
          $('.err_email').hide();
        }
	    var res = isValidEmailAddress($.trim(email_id));
		if(res == true)
		{
          $('.err_email').hide();
        }
        else{
          $('.err_email').show().delay(3000).fadeOut();
          flag = 0;
        }
	}
	if (password == "") {
		$(".err_pass").show().delay(3000).fadeOut();
		flag = 0;
	}
	else {
		$(".err_pass").hide();
		flag = 1;
	}
	if(password != '')
	{
    	if($.trim(password) == '')
	    {
	        $('.err_pass').show().delay(3000).fadeOut();
	        flag = 0;
	    }
	    else
	    {
	        $('.err_pass').hide();
	        flag = 1;
	    }
        var res = isvalidPassword($.trim(password));
        if(res == true){
           $('.err_pass').hide();
           flag = 1;
        }
        else{
           $('.err_pass').html('Password field should between 8 and 15 characters containing combination of alphabets and numbers, and must contain atleast one special symbol and one captial alphabet').show();
           flag = 0;
        }
	}
	if(confirm_password == "") {
		$(".err_cpass").show().delay(3000).fadeOut();
		flag = 0;
	}
	else if(confirm_password != password){
		$(".err_cpass").html('Password and confirm password not same').show().delay(3000).fadeOut();
		flag = 0;
	}
	else {
		$(".err_cpass").hide();
		flag = 1;
	}
	if(address == "") {
		$(".err_add").show().delay(3000).fadeOut();
		flag = 0;
	}
	else {
		$(".err_add").hide();
		flag = 1;
	}
	if(address != '')
	{
    	if($.trim(address) == '')
	    {
	        $('.err_add').show().delay(3000).fadeOut();
	        flag = 0;
	    }
	    else
	    {
	        $('.err_add').hide();
	        flag = 1;
	    }
	}
	if(select_country == "select") {
		$(".err_country").show().delay(3000).fadeOut();
		flag = 0;
	}
	else {
		$(".err_country").hide();
		flag = 1;
	}
	if(select_state == "select") {
		$(".err_state").show().delay(3000).fadeOut();
		flag = 0;
	}
	else {
		$(".err_state").hide();
		flag = 1;
	}
	if(select_city == "select") {
		$(".err_city").show().delay(3000).fadeOut();
		flag = 0;
	}
	else {
		$(".err_city").hide();
		flag = 1;
	}
	if(zip_code == "") {
		$(".err_zip").show().delay(3000).fadeOut();
		flag = 0;
	}
	else if ((zip_code.length)< 5 || (zip_code.length)>5 ){
		$(".err_zip").html('Zipcode should only be 5 digits').show().delay(3000).fadeOut();
		flag = 0;
    }
    else if ( zip_code =( "^[0-9]+$" )){
    	$(".err_zip").html('Zipcode should be numbers only').show().delay(3000).fadeOut();
		flag = 0;
    }
	else {
		$(".err_zip").hide();
		flag = 1;
	}
	if(check1 == "off") {
		$(".err_term").show().delay(3000).fadeOut();
		flag = 0;
	}
	else {
		$(".err_term").hide();
		flag = 1;
	}
   return flag;
  }

  function isValidEmailAddress(emailAddress) {
     var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
     return pattern.test(emailAddress);
  }
  function isvalidPassword(password){
     // var pattern = new RegExp(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,15}$/);
     var pattern = new RegExp(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])(?=.*\d).{8,15}$/);
     return pattern.test(password);
  }