$(document).ready(function(){
	$('#rating-table').DataTable();
	$('.rating_star').on('click',function(){
	  	var rating = $(this).val();
	  	$('#rating_star').val(rating);
	  	var flag=0;
	  	for(var i=0;i<=5;i++){
	  	  if(flag==0){
	  	   $('.star'+i).removeClass('fa fa-star-o').addClass('fa fa-star');
	  	  }
	  	  else{
	  	   $('.star'+i).removeClass('fa fa-star').addClass('fa fa-star-o');
	  	  }
	  	  if(i==rating){
	        flag=1;
	  	  }
	  	}
	});
	$('.edit_rating_star').on('click',function(){
	  	var rating = $(this).val();
	  	$('#edit_rating').val(rating);
	  	var flag=0;
	  	for(var i=0;i<=5;i++){
	  	  if(flag==0){
	  	   $('.star'+i).removeClass('fa fa-star-o').addClass('fa fa-star');
	  	  }
	  	  else{
	  	   $('.star'+i).removeClass('fa fa-star').addClass('fa fa-star-o');
	  	  }
	  	  if(i==rating){
	        flag=1;
	  	  }
	  	}
	});
});
function getSubjectlist(status=''){
	if(status){
	 var course_id = $('#edit_course option:selected').val();
	}
	else{
	 var course_id = $('.select_course option:selected').val();
	}
	if(course_id!='select'){
		$.ajax({
	        url : base_url+"Api/Api/subjectList?id=" + course_id,
	        success: function(result)
	        {
	        	var res = result.data;
		        if(result.status==true){
			        var html = '';
			        	html+='<option value="select">Select Subject</option>';
			        $('.select_subject').html('');
			        for(var i=0;i<res.length;i++){
			        	html+='<option value='+res[i].subject_id+'>'+res[i].subject_name+'</option>';
			        }
			        $('.select_subject').html(html);
		        }
		        else{
			        $('.select_subject').html('<option value="select">Select Subject</option>');
		        }

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $('#msg').html('Error adding / update data!');
	        }
	    })
	}
	else{
		var html='';
		$('.select_subject').html('<option value="select">Select Subject</option>');
	}
}

function getTopiclist(status){
	if(status){
	 var subject_id = $('#edit_subject option:selected').val();
	}
	else{
	 var subject_id = $('.select_subject option:selected').val();
	}
	if(status){
	  $('#edit_topic').val('').html('');
	}
	if(subject_id!='selected'){
		$.ajax({
	        url : base_url+"Api/Api/topicList?id=" + subject_id,
	        success: function(result)
	        {
	        	var res = result.data;
		        if(result.status==true){
			        var html = '';
			        	html+='<option value="selected">Select Topic</option>';
			        $('.select_topic').html('');
			        for(var i=0;i<res.length;i++){
			        	html+='<option value='+res[i].topic_id+'>'+res[i].topic_name+'</option>';
			        }
			        $('.select_topic').html(html);
		        }
		        else{
			        $('.select_topic').html('<option value="selected">Select Subject</option>');
		        }

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $('#msg').html('Error adding / update data!');
	        }
	    })
	}
	else{
		var html='';
		$('.select_topic').html('<option value="selected">Select Subject</option>');
	}
}
function addRating(){
   var formData = new FormData($("#add_rating")[0]);
   res = valid();
   if(res){
   		$.ajax({
	        url : base_url+"Api/Api/addRating",
	        type: "POST",
	        data: formData, 
	        cache: false, 
	        async: false,
	        processData: false,
	        contentType: false,
	        
	        success: function(data)
	        {
	          if(data.status==true){
	            $(".alert").css('display','block').delay(3000).fadeOut();
	            $(".alert").attr('class', 'alert alert-success').delay(3000).fadeOut();
	            $('#msg').html(data.message).delay(3000).fadeOut();
	            $("#add_rating")[0].reset();
	            setTimeout(function(){
	               window.location.reload(1);
	            }, 3000);
	          }else{
	             $(".alert").css('display','block');
	             $(".alert").attr('class', 'alert alert-danger');
	             $('#msg').html(data.message);
	          }
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $(".alert").css('display','block');
	            $(".alert").attr('class', 'alert alert-danger');
	            $('#msg').html('Error adding / update data!');
	        }
	    });
   }

}

function valid() {
	var flag = 1;
	var course = $('.select_course option:selected').val();
	var subject = $('.select_subject option:selected').val();
	var topic = $('.select_topic option:selected').val();
	var rate = $('#rating_star').val();
	var comment = $('#text_comment').val();
	if (course == "Select Course") {
		$(".err_course").show().delay(3000).fadeOut();
		flag = 0;
	}
	if (subject == "Select Subject") {
		$(".err_subject").show().delay(3000).fadeOut();
		flag = 0;
	}
	if (topic == "Select Topic") {
		$(".err_topic").show().delay(3000).fadeOut();
		flag = 0;
	}
	if (rate == "") {
		$(".err_rate").show().delay(3000).fadeOut();
		flag = 0;
	}
	if (comment == "") {
		$(".err_comment").show().delay(3000).fadeOut();
		flag = 0;
	}
	return flag;
}
function getTestiminial(id){
	//alert('hi');
    $('#update_testimonials_popup').modal('show');
    var formData = new FormData();
	formData.set('testimonial_id', id);
    $.ajax({
        url : base_url+"Api/Api/getRating",
        type: "POST",
        data: formData, 
        cache: false, 
        async: false,
        processData: false,
        contentType: false,
        
        success: function(res)
        {
          if(res.status==true){
           $('#edit_course').val(res.data[0].course_id);
           $('#edit_subject').val(res.data[0].subject_id);
           $('#edit_topic').val(res.data[0].topic_id);
           $('#edit_text').text(res.data[0].comment);
           $('#edit_rating').val(res.data[0].rate);
           $('#edit_testimonial_id').val(res.data[0].testimonial_id);
           var rating = res.data[0].rate;
            var flag=0;
		  	for(var i=0;i<=5;i++)
		  	{
		  	  if(flag==0){
		  	   $('.star'+i).removeClass('fa fa-star-o').addClass('fa fa-star');
		  	  }
		  	  else{
		  	   $('.star'+i).removeClass('fa fa-star').addClass('fa fa-star-o');
		  	  }
		  	  if(i==rating){
		        flag=1;
		  	  }
		  	}
          }
          else{
             $(".alert").css('display','block');
             $(".alert").attr('class', 'alert alert-danger');
             $('#msg').html(res.message);
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $(".alert").css('display','block');
            $(".alert").attr('class', 'alert alert-danger');
            $('#msg').html('Error adding / update data!');
        }
    });
}

function updateRating(){
  var formData = new FormData( $("#edit_testi")[0]);
  res = update_valid();
    if(res){
		$.ajax({
	        url : base_url+"Api/Api/updateRating",
	        type: "POST",
	        data: formData, 
	        cache: false, 
	        async: false,
	        processData: false,
	        contentType: false,
	        
	        success: function(data)
	        {
	          if(data.status==true){
	            $(".alert").css('display','block').delay(3000).fadeOut();
	            $(".alert").attr('class', 'alert alert-success').delay(3000).fadeOut();
	            $('#msg').html(data.message).delay(3000).fadeOut();
	            $("#add_rating")[0].reset();
	            setTimeout(function(){
	               window.location.reload(1);
	            }, 3000);
	          }else{
	             $(".alert").css('display','block');
	             $(".alert").attr('class', 'alert alert-danger');
	             $('#msg').html(data.message);
	          }
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            $(".alert").css('display','block');
	            $(".alert").attr('class', 'alert alert-danger');
	            $('#msg').html('Error adding / update data!');
	        }
	    });
    }
}
function update_valid() {
	var flag = 1;
	var course = $('#edit_course option:selected').val();
	var subject = $('#edit_subject option:selected').val();
	var topic = $('#edit_topic option:selected').val();
	var rate = $('#edit_rating').val();
	var comment = $('#edit_text').val();
	if (course == "Select Course") {
		$(".err_ecourse").show().delay(3000).fadeOut();
		flag = 0;
	}
	if (subject == "Select Subject") {
		$(".err_esubject").show().delay(3000).fadeOut();
		flag = 0;
	}
	if (topic == "Select Topic") {
		$(".err_etopic").show().delay(3000).fadeOut();
		flag = 0;
	}
	if (rate == "") {
		$(".err_erate").show().delay(3000).fadeOut();
		flag = 0;
	}
	if (comment == "") {
		$(".err_ecomment").show().delay(3000).fadeOut();
		flag = 0;
	}
	return flag;
}