<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//----------- Admin -----------------
$route['admin'] = "admin/login";
$route['home'] = "admin/home";
$route['manage_course'] = "admin/manage_course";
$route['manage_user'] = "admin/manage_user";
$route['manage_session'] = "admin/manage_session";
$route['my_calender'] = "admin/my_calender";
$route['notification'] = "admin/notification";
$route['my_account'] = "admin/my_account";
$route['manage_content'] = "admin/manage_content";
$route['setting'] = "admin/setting";
$route['login'] = "login";
$route['add_new_course'] = "admin/manage_course/add_new_course";
$route['edit_course/(:num)'] = "admin/manage_course/edit_course/$1";
//$route['view_course_details'] = "admin/manage_user/view_course_details";
$route['view_course_details/(:num)'] = 'admin/manage_user/view_course_details/$1';
$route['testimonial'] = "admin/home/testimonial";
$route['view_notification_details'] = "admin/notification/view_notification_details";
$route['average_rating'] = "admin/my_account/average_rating";
$route['schedule_new_session'] = "admin/manage_session/schedule_new_session";
$route['edit_new_session'] = "admin/manage_session/edit_new_session";
$route['download-file/(:any)/(:any)'] = "admin/download_file/download/$1/$2";

$route['loginMe'] = "login/loginMe";








// -------------- Student ----------------
$route['student'] = "student/login";
$route['signup'] = "student/signup";
$route['student-home'] = "student/home";
$route['search-topic'] = "student/topic";
$route['search-select-topic'] = "student/topic/search_topic";
$route['my-account'] = "student/my_account";
$route['my-calender'] = "student/my_calender";
$route['view-course-details'] = "student/topic/view_course_details";
$route['view-quest-answers'] = "student/my_account/view_question_answers";
$route['test-overview'] = "student/my_account/test_overview";
$route['book-session'] = "student/my_calender/book_session";
$route['book-session-edit'] = "student/my_calender/book_session_edit";
$route['one-to-one-booking'] = "student/my_calender/one_to_one_booking";
$route['group-session-booking'] = "student/my_calender/group_session_booking";
$route['student-notification'] = "student/notification";
$route['student-notification-details'] = "student/notification/notification_details";
$route['my-profile'] = "student/profile";
$route['update-profile'] = "student/profile/update_profile";
$route['edit-profile'] = "student/profile/edit_profile";
$route['student-testimonial'] = "student/testimonial";
$route['about-us'] = "student/cms/about_us";
$route['contact-us'] = "student/cms/contact_us";
$route['faq'] = "student/cms/faq";
$route['privacy-policy'] = "student/cms/privacy_policy";
$route['terms-services'] = "student/cms/terms_services";
$route['change-password'] = "student/profile/change_password";
$route['logout'] = "student/login/logout";


$route['test'] = "admin/manage_session/test";
