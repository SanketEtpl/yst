<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

	if(!function_exists('sendEmail'))
    {
	    function sendEmail($email,$userName,$message=NULL,$subject=NULL)
	    {
	        $CI = & get_instance();
	        $CI->load->library("email");
	        $CI->load->helper('email_template_helper');         
	        $hostname = $CI->config->item('hostname');
	        $config['mailtype'] ='html';
	        $config['charset'] ='iso-8859-1';
	        $CI->email->initialize($config);
	        $from  = EMAIL_FROM; 
	        //$CI->messageBody  = email_header();
	        // if(!empty($message)) {
	        //     $CI->messageBody  .= $message;
	        // }  else {
	            $CI->messageBody  .= '<tr> 
	            <td style="font-size:16px;word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
	                <p>Hello '.$userName.',</p>
	                    <p> '.$message.'.</p>
	                    Tulii admin will review your profile and active your account within 24 hours.<br/></p>
	                                       
	                </td>
	            </tr>
	            ';      
	        // }              
	        if(empty($subject)) 
	        $subject =  'Forgot password';   
	        //$CI->messageBody  .= email_footer();
	        $CI->email->from($from, FROM_NAME);
	        $CI->email->to($email);
	        $CI->email->subject($subject);
	        $CI->email->message($CI->messageBody);
	        $send = $CI->email->send();
	        return $send;
	    }
    }
    
    //send password to user.
    if(!function_exists('sendPassword'))
    {
	    function sendPassword($email,$userName,$message=NULL,$subject=NULL)
	    {
	        $CI = & get_instance();
	        $CI->load->library("email");
	        $CI->load->helper('email_template_helper');         
	        $hostname = $CI->config->item('hostname');
	        $config['mailtype'] ='html';
	        $config['charset'] ='iso-8859-1';
	        $CI->email->initialize($config);
	        $from  = EMAIL_FROM; 
	        //$CI->messageBody  = email_header();
	        // if(!empty($message)) {
	        //     $CI->messageBody  .= $message;
	        // }  else {
	            $CI->messageBody  .= '<tr> 
	            <td style="font-size:16px;word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
	                <p>Hello '.$userName.',</p>
	                    <p> '.$message.'.</p>
	                    Tulii admin will review your profile and active your account within 24 hours.<br/></p>
	                                       
	                </td>
	            </tr>
	            ';      
	        // }              
	        if(empty($subject)) 
	        $subject =  'Forgot password';   
	        //$CI->messageBody  .= email_footer();
	        $CI->email->from($from, FROM_NAME);
	        $CI->email->to($email);
	        $CI->email->subject($subject);
	        $CI->email->message($CI->messageBody);
	        $send = $CI->email->send();
	        return $send;
	    }
    }

    function is_user_logged_in()
	{
	    $CI = & get_instance();
	    if($CI->session->userdata("isLoggedIn"))
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	}





?>

