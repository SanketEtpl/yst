<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

 class Api1 extends REST_Controller
{
	public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        ini_set('display_errors', '1');
        $this->load->model("topic_model");
        $this->load->model("session_model");
        $this->load->model("invite_model");
        $this->load->model("signup_model");
        $this->load->model("common_model");
        
    }

    /*This function used to logged in user*/
    public function loginMe_post()
    {
    	if (empty(trim($this->input->post('email_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter email Id.'
	        ));
	        exit;
        }
        if (empty(trim($this->input->post('password')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter password.'
	        ));
	        exit;
        }
        $email = $this->input->post('email_id');
        $password = $this->input->post('password');
        /*if($this->input->post('check1')){
          $remember_me = $this->input->post('check1');
		  $cookie_email = $this->input->post('email_id');
		  $cookie_pass = $this->input->post('password');
		  set_cookie("email_id",$cookie_email,time()+ (10*365*24*60*60));
		  set_cookie("password",$cookie_pass,time()+ (10*365*24*60*60));
        }
        else {
            if(isset($_COOKIE["email_id"])) {
                set_cookie ("email_id","");
            }
            if(isset($_COOKIE["password"])) {
                set_cookie ("password","");
            }
        }*/
        $this->load->model("login_model");
        $result = $this->login_model->loginMe($email, $password);
       // echo "<pre>";print_r($result[]);die;
        if($result)
        {
	        $sessionArray = array(
	            'userId'=>$result['tutor_id'],                    
	            'useremail'=>$result['tutor_email_id'],
	            'username'=>$result['tutor_name'],
	            'isLoggedIn' => TRUE
	        );
	        $this->session->set_userdata($sessionArray);
	        
	        $this->response(array(
	            'status' => true,
	            'message' => 'Login successfully',
	            'data' => $result
	        ));
	        exit;
        }
        else
        {
            $this->session->set_flashdata('error', 'Email or password mismatch');
            
            $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
        }
    }

    // ---- get all course list ----
    public function courseList_get()
    {
        $this->load->model("course_model");
	    $result    = $this->course_model->courseList();
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Course listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	// ---- get all subject list ----
    public function subjectList_get()
    {
    	$course_id = $this->get('id');
        $this->load->model("subject_model");
	    $result  = $this->subject_model->subjectList($course_id);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Subject listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}


	 // ---- get all student list ----
    public function studentList_get()
    {
        $this->load->model("student_model");
	    $result = $this->student_model->studentList();
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Student listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	// ---- get all session list ----
    public function sessionList_get()
    {
        $this->load->model("session_model");
	    $result = $this->session_model->sessionList();
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Session listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	 // ---- get all topic list ----
    public function topic_list_get()
    {
 		$this->load->model("topic_model");
	    $result = $this->topic_model->topicList();
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Topic listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	 // ---- view course details ----
    public function course_details_post()
    {//echo 'hiiii';exit;
        if (empty(trim($this->input->post('course_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter course.'
	        ));
	        exit;
        }
       
        $this->load->model("topic_model");
	    $result = $this->topic_model->courseDetails(trim($this->input->post('course_id')));
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Course Details',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	/*---------add multiple topic-------*/
	public function addTopic_post(){
		/*echo "<pre>";
		print_r($_FILES);
		//print_r($this->input->post());
		die;*/
		$postData = $_POST;
		$files = $_FILES;
		if(empty($postData['count'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please add the topic.'
	        ));
	        exit;
		}
		$topic_arr = array();
		$count = $postData['count'];
		$j=1;

		for($i=0;$i<$count;$i++){
			
	        $files = $_FILES;
			if(isset($_FILES['topic'.$j]) && !empty($_FILES['topic'.$j]['name']))
		    {
			    $config['upload_path'] = './uploads/topic/preview_video/';
			 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
		 		$this->load->library('upload');
			 	$_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['preview_video'];
		        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['preview_video'];
		        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['preview_video'];
		        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['preview_video'];
		        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['preview_video']; 

		    	$new_photo_name1 = rand('0','99999')."_".time();
			 	$config['file_name'] = $new_photo_name1;
		        $this->upload->initialize($config);
				if($this->upload->do_upload('topic'.$j)){
				    $uploadData = $this->upload->data();
					$preview_video = $uploadData['file_name'];
			 	}else{
					$file_err_msg = $this->upload->display_errors('<p>', '</p>');
					$this->response(array(
			            'status' => false,
			            'message' => $file_err_msg,
			        ));
		            exit;
				    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
					$picture = '';
				}
				$config['upload_path'] = './uploads/topic/question_bank/';
			 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
		 		$this->load->library('upload');
		        $_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['question_bank'];
		        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['question_bank'];
		        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['question_bank'];
		        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['question_bank'];
		        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['question_bank'];

				$new_photo_name2 = rand('0','99999')."_".time();
			 	$config['file_name'] = $new_photo_name2;
		        $this->upload->initialize($config);
				if($this->upload->do_upload('topic'.$j)){
				    $uploadData = $this->upload->data();
					$question_bank = $uploadData['file_name'];
			 	}else{
					$file_err_msg = $this->upload->display_errors('<p>', '</p>');
					$this->response(array(
			            'status' => false,
			            'message' => $file_err_msg,
			        ));
		            exit;
				    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
					$picture = '';
				}

				$config['upload_path'] = './uploads/topic/notes/';
			 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
		 		$this->load->library('upload');
		        $_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['notes'];
		        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['notes'];
		        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['notes'];
		        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['notes'];
		        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['notes']; 

				$new_photo_name3 = rand('0','99999')."_".time();
			 	$config['file_name'] = $new_photo_name3;
		        $this->upload->initialize($config);
				if($this->upload->do_upload('topic'.$j)){
				    $uploadData = $this->upload->data();
					$notes = $uploadData['file_name'];
			 	}else{
					$file_err_msg = $this->upload->display_errors('<p>', '</p>');
					$this->response(array(
			            'status' => false,
			            'message' => $file_err_msg,
			        ));
		            exit;
				    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
					$picture = '';
				}

				$config['upload_path'] = './uploads/topic/lession_video/';
			 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
		 		$this->load->library('upload');
		        $_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['lession_video'];
		        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['lession_video'];
		        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['lession_video'];
		        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['lession_video'];
		        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['lession_video'];

				$new_photo_name4 = rand('0','99999')."_".time();
			 	$config['file_name'] = $new_photo_name4;

		        $this->upload->initialize($config);
				if($this->upload->do_upload('topic'.$j)){
				    $uploadData = $this->upload->data();
					$lession_video = $uploadData['file_name'];
			 	}else{
					$file_err_msg = $this->upload->display_errors('<p>', '</p>');
					$this->response(array(
			            'status' => false,
			            'message' => $file_err_msg,
			        ));
		            exit;
				    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
					$picture = '';
				}
				
			}
			$img =array(
				'preview_video'=>$preview_video,
				'question_bank'=>$question_bank,
				'notes'=>$notes,
				'lession_video'=>$lession_video
			);
		   $topic_arr['topic'][$i]=$postData['topic'.$j];
		   $result = $this->topic_model->addTopic($topic_arr['topic'][$i],$postData['course_id'],$postData['subject_id'],$img);
		   $j++;
		}
		if (!empty($result)) {
			if($result['status'] == 'exists'){
				 $this->response(array(
		            'status' => false,
		            'message' => 'Topic allready exists!!',
		            'data' => $result
		        ));
	            exit;
			}
			else{
		        $this->response(array(
		            'status' => true,
		            'message' => 'Topic added successfully!!',
		            'data' => $result
		        ));
	            exit;
			}
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Topic fail to add.'
	        ));
	        exit;
	    }
	} 
	//add student by admin
	public function addUser_post()
	{
	    $this->load->model("student_model");
	    $postData = $_POST;
	    if(empty($postData['first_name'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter first name.'
	        ));
	        exit;
		}
		if(empty($postData['last_name'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter last name.'
	        ));
	        exit;
		}
		if(empty($postData['email_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter email id.'
	        ));
	        exit;
		}
		$password = rand(0,999999);
		$postData['password'] = $password;
		$result = $this->student_model->addStudent($postData);
		if (!empty($result)) {
			if($result['status'] == 'exists'){
				 $this->response(array(
		            'status' => false,
		            'message' => 'Student allready exists!!',
		            'data' => $result
		        ));
	            exit;
			}
			else{
				//send email to user for password
				/*$user_full_name = $postData['first_name'].' '.$postData['last_name'];
				$message  = "Congrats on signing up for Yst! You have registered successfully.  
		               Plase check your password : ".$password; 
				$send = sendPassword($postData['email_id'],$user_full_name,$message,"Yst registration from admin");*/

		        $this->response(array(
		            'status' => true,
		            'message' => 'Student added successfully!! Sent email to user for passsword.',
		            'data' => $result
		        ));
	            exit;
			}
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Student fail to add.'
	        ));
	        exit;
	    }
	}

	//get all topic by subject id
	public function topicList_get()
    {
    	$subject_id = $this->get('id');
	    $result  = $this->topic_model->topicBySubId($subject_id);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Topic listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	//add group session by admin
	public function addSession_post()
	{
		$postData = $_POST;
		if(empty($postData['course_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select course id.'
	        ));
	        exit;
		}
		if(empty($postData['subject_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select subject id.'
	        ));
	        exit;
		}
		if(empty($postData['topic_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select topic id.'
	        ));
	        exit;
		}
		if(empty($postData['date'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select date.'
	        ));
	        exit;
		}
		if(empty($postData['start_time'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select start time.'
	        ));
	        exit;
		}
		if(empty($postData['end_time'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select end time.'
	        ));
	        exit;
		}
		if(empty($postData['duration_in_minutes'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter duration.'
	        ));
	        exit;
		}
		if(empty($postData['cost'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter cost.'
	        ));
	        exit;
		}
		$result = $this->session_model->addSession($postData);
		//echo $this->db->last_query();die;
		if (!empty($result)) {
			if($result['status'] == 'exists'){
				 $this->response(array(
		            'status' => false,
		            'message' => 'Session allready exists!!',
		            'data' => $result
		        ));
	            exit;
			}
			else{
		        $this->response(array(
		            'status' => true,
		            'message' => 'Session added successfully!!.',
		            'data' => $result
		        ));
	            exit;
			}
	    } 
	    else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Session fail to add.'
	        ));
	        exit;
	    }

	}

	//------------get session details by session id -----------
	public function getSessiondetails_post(){
		$postData = $_POST;
		if(empty($postData['session_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please require session id.'
	        ));
	        exit;
		}
		$result = $this->session_model->getSessiondetails($postData);
		//echo "<pre>";print_r($result);die;
		if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Session added successfully!!.',
	            'data' => $result
	        ));
            exit;
	    } 
	    else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Session fail to add.'
	        ));
	        exit;
	    }
	}

	//-------------------update session details----------------------------------------
	public function updateSession_post()
	{
		$postData = $_POST;
		//echo "<pre>";print_r($postData);die;
		if(empty($postData['course_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select course id.'
	        ));
	        exit;
		}
		if(empty($postData['subject_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select subject id.'
	        ));
	        exit;
		}
		if(empty($postData['topic_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select topic id.'
	        ));
	        exit;
		}
		if(empty($postData['date'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select date.'
	        ));
	        exit;
		}
		if(empty($postData['start_time'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select start time.'
	        ));
	        exit;
		}
		if(empty($postData['end_time'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select end time.'
	        ));
	        exit;
		}
		if(empty($postData['duration_in_minutes'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter duration.'
	        ));
	        exit;
		}
		if(empty($postData['cost'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter cost.'
	        ));
	        exit;
		}
		$result = $this->session_model->updateSession($postData);
		if (!empty($result)) {
			if($result['status'] == 'exists'){
				 $this->response(array(
		            'status' => false,
		            'message' => 'Session allready exists!!',
		            'data' => $result
		        ));
	            exit;
			}
			else{
		        $this->response(array(
		            'status' => true,
		            'message' => 'Session update successfully!!.',
		            'data' => $result
		        ));
	            exit;
			}
	    } 
	    else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Session fail to add.'
	        ));
	        exit;
	    }
	}
	//------------delete session  -----------
	public function deleteSession_post(){
		$postData = $_POST;
		if(empty($postData['session_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please require session id.'
	        ));
	        exit;
		}
		$result = $this->session_model->deleteSession($postData);
		if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Session delete successfully!!.',
	            'data' => $result
	        ));
            exit;
	    } 
	    else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Session fail to add.'
	        ));
	        exit;
	    }
	}

	public function inviteList_get(){
	    $result    = $this->invite_model->inviteList();
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Invite listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}
	
	public function getTopicdetails_post(){
		$postData = $_POST;
		if(empty($postData['course_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please require course id.'
	        ));
	        exit;
		}
		if(empty($postData['subject_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please require course id.'
	        ));
	        exit;
		}
	    $result = $this->topic_model->getTopicdetails($postData);
	    //echo "<pre>";print_r($result);die;
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'topic details.',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	public function updateTopic_post(){
		/*echo "<pre>";
		print_r($_FILES);
		print_r($this->input->post());
		die;*/
		$postData = $_POST;
		$files = $_FILES;
		if(empty($postData['count'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please add the topic.'
	        ));
	        exit;
		}
		$topic_arr = array();
		$count = $postData['count'];
		$j=1;
		//$this->common_model->delete('topic_master_ut',array('course_id'=>$postData['course_id'],'subject_id'=>$postData['subject_id']));
		for($i=0;$i<$count;$i++){
			
	        $files = $_FILES;
			if(isset($_FILES['topic'.$j]) && !empty($_FILES['topic'.$j]['name']))
		    {
		    	if($files['topic'.$j]['name']['preview_video']){
				    $config['upload_path'] = './uploads/topic/preview_video/';
				 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
			 		$this->load->library('upload');
				 	$_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['preview_video'];
			        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['preview_video'];
			        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['preview_video'];
			        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['preview_video'];
			        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['preview_video']; 

			    	$new_photo_name1 = rand('0','99999')."_".time();
				 	$config['file_name'] = $new_photo_name1;
			        $this->upload->initialize($config);
					if($this->upload->do_upload('topic'.$j)){
					    $uploadData = $this->upload->data();
						$preview_video = $uploadData['file_name'];
				 	}else{
						$file_err_msg = $this->upload->display_errors('<p>', '</p>');
						$this->response(array(
				            'status' => false,
				            'message' => $file_err_msg,
				        ));
			            exit;
					    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
						$preview_video = '';
					}
		    	}else{
		    		$preview_video = '';
		    	}
		    	if($files['topic'.$j]['name']['question_bank']){
					$config['upload_path'] = './uploads/topic/question_bank/';
				 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
			 		$this->load->library('upload');
			        $_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['question_bank'];
			        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['question_bank'];
			        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['question_bank'];
			        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['question_bank'];
			        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['question_bank'];

					$new_photo_name2 = rand('0','99999')."_".time();
				 	$config['file_name'] = $new_photo_name2;
			        $this->upload->initialize($config);
					if($this->upload->do_upload('topic'.$j)){
					    $uploadData = $this->upload->data();
						$question_bank = $uploadData['file_name'];
				 	}else{
						$file_err_msg = $this->upload->display_errors('<p>', '</p>');
						$this->response(array(
				            'status' => false,
				            'message' => $file_err_msg,
				        ));
			            exit;
					    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
						$question_bank = '';
					}
		    	}else{
		    		$question_bank = '';
		    	}
		    	if($files['topic'.$j]['name']['notes']){
					$config['upload_path'] = './uploads/topic/notes/';
				 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
			 		$this->load->library('upload');
			        $_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['notes'];
			        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['notes'];
			        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['notes'];
			        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['notes'];
			        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['notes']; 

					$new_photo_name3 = rand('0','99999')."_".time();
				 	$config['file_name'] = $new_photo_name3;
			        $this->upload->initialize($config);
					if($this->upload->do_upload('topic'.$j)){
					    $uploadData = $this->upload->data();
						$notes = $uploadData['file_name'];
				 	}else{
						$file_err_msg = $this->upload->display_errors('<p>', '</p>');
						$this->response(array(
				            'status' => false,
				            'message' => $file_err_msg,
				        ));
			            exit;
					    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
						$notes = '';
					}
		    	}else{
		    		  $notes = '';
		    	}
		    	if($files['topic'.$j]['name']['lession_video']) {
					$config['upload_path'] = './uploads/topic/lession_video/';
				 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
			 		$this->load->library('upload');
			        $_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['lession_video'];
			        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['lession_video'];
			        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['lession_video'];
			        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['lession_video'];
			        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['lession_video'];

					$new_photo_name4 = rand('0','99999')."_".time();
				 	$config['file_name'] = $new_photo_name4;

			        $this->upload->initialize($config);
					if($this->upload->do_upload('topic'.$j)){
					    $uploadData = $this->upload->data();
						$lession_video = $uploadData['file_name'];
				 	}else{
						$file_err_msg = $this->upload->display_errors('<p>', '</p>');
						$this->response(array(
				            'status' => false,
				            'message' => $file_err_msg,
				        ));
			            exit;
					    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
						$lession_video = '';
					}
		    	}else{
		    		$lession_video = '';
		    	}
			}

			$img =array(
				'preview_video'=>$preview_video,
				'question_bank'=>$question_bank,
				'notes'=>$notes,
				'lession_video'=>$lession_video
			);
		   $topic_arr['topic'][$i]=$postData['topic'.$j];
		   //$result = $this->topic_model->updateTopic($topic_arr['topic'][$i],$postData['course_id'],$postData['subject_id'],$img);
		   $result = $this->topic_model->updateTopic($topic_arr['topic'][$i],$postData['course_id'],$postData['subject_id'],$img);
		   $j++;
		}
		if (!empty($result)) {
			if($result['status'] == 'exists'){
				 $this->response(array(
		            'status' => false,
		            'message' => 'Topic allready exists!!',
		            'data' => $result
		        ));
	            exit;
			}
			else{
		        $this->response(array(
		            'status' => true,
		            'message' => 'Topic updated successfully!!',
		            'data' => $result
		        ));
	            exit;
			}
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Topic fail to add.'
	        ));
	        exit;
	    }
	} 
	//-------------get country list----------------------------
	public function countryList_get()
	{
	    $result  = $this->signup_model->countryList();
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Country listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	//-------------get state list---------------------
	public function stateList_post()
	{
		$postData = $_POST;
	    $result  = $this->signup_model->stateList($postData['country_id']);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'State listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}
	//---------get city list----------
	public function cityList_post()
	{
		$postData = $_POST;
	    $result  = $this->signup_model->cityList($postData['state_id']);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'City listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	//--------------student sign up---------------------
	public function signUp_post()
	{
		/*echo "<pre>";print_r($_POST);die;*/
		$postData = $_POST;
		if(empty($postData['first_name'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter first name.'
	        ));
	        exit;
		}
		if(empty($postData['last_name'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter last name.'
	        ));
	        exit;
		}
		if(empty($postData['contact_no'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter contact no.'
	        ));
	        exit;
		}
		if(empty($postData['email_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter email id.'
	        ));
	        exit;
		}
		if(empty($postData['password'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter password.'
	        ));
	        exit;
		}
		if(empty($postData['address'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter address.'
	        ));
	        exit;
		}
		if(empty($postData['country_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select country.'
	        ));
	        exit;
		}
		if(empty($postData['state_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select state.'
	        ));
	        exit;
		}
		if(empty($postData['city_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select city.'
	        ));
	        exit;
		}
		if(empty($postData['zip_code'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter zip code.'
	        ));
	        exit;
		}
	    $result = $this->signup_model->signUp($postData);
	    if (!empty($result)) {
	    	if($result['status'] == 'exists'){
				 $this->response(array(
		            'status' => false,
		            'message' => 'Your email id allready exists!!',
		            'data' => $result
		        ));
	            exit;
			}
	        $this->response(array(
	            'status' => true,
	            'message' => 'You are signup successfully!!!',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}
	public function studentLogin_post()
	{
		if (empty(trim($this->input->post('email_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter email Id.'
	        ));
	        exit;
        }
        if (empty(trim($this->input->post('password')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter password.'
	        ));
	        exit;
        }
        $email = $this->input->post('email_id');
        $password = $this->input->post('password');
        if($this->input->post('check1')){
          $remember_me = $this->input->post('check1');
		  $cookie_email = $this->input->post('email_id');
		  $cookie_pass = $this->input->post('password');
		  set_cookie("email_id",$cookie_email,time()+ (10*365*24*60*60));
		  set_cookie("password",$cookie_pass,time()+ (10*365*24*60*60));
        }
        else {
            if(isset($_COOKIE["email_id"])) {
                set_cookie ("email_id","");
            }
            if(isset($_COOKIE["password"])) {
                set_cookie ("password","");
            }
        }
        $this->load->model("login_model");
        $result = $this->login_model->studentLogin($email, $password);
        if($result)
        {
	        $sessionArray = array(
	            'userId'=>$result['student_id'],                    
	            'useremail'=>$result['email_id'],
	            'username'=>$result['first_name'].' '.$result['last_name'],
	            'userimage'=>$result['student_image'],
	            'isLoggedIn' => TRUE
	        );
	        $this->session->set_userdata($sessionArray);

	        $this->response(array(
	            'status' => true,
	            'message' => 'Login successfully',
	            'data' => $result
	        ));
	        exit;
        }
        else
        {
            $this->session->set_flashdata('error', 'Email or password mismatch');
            $this->response(array(
	            'status' => false,
	            'message' => 'Email or password incorrect.'
	        ));
	        exit;
        }
	}

	//-----get student details--------- 
	public function studentProfile_post()
	{
		$studentId  = $this->input->post('student_id');

		$this->load->model("student_model");
		$result = $this->student_model->studentById($studentId);
	    //echo "<pre>";print_r($result);die;
		if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Student data',
	            'data' => $result
	        ));
            exit;
	    } 
	    else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}
	public function updateStudentProfile_post()
	{
		//echo "<pre>";print_r($_POST);die;
		$data = $this->input->post();

		/*print_r($data);
		exit();*/
		$this->load->model("student_model");
		$result = $this->student_model->updateStudentProfile($data);

		if('success' == $result['status'])
		{
			echo json_encode(array('status' => true,'message' => 'Profile Updated sucessfully'));
		}
		else
		{
			echo json_encode(array('status' => false,'message' => 'Something went wrong'));	
		}
	}
	


    public function paymentHistoryList_get()
    {
    	$this->load->model("my_account_model");
    	$result  = $this->my_account_model->paymentHistoryList();//print_r($result);exit;
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Payment History listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}
	public function deleteTopic_post(){
		//echo "<pre>";print_r($_POST);die;
		if (empty(trim($this->input->post('topic_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please require topic Id.'
	        ));
	        exit;
        }
        $this->load->model("common_model");
        $result = $this->common_model->update('topic_master_ut',array('topic_id'=>$_POST['topic_id']),array('is_deleted'=>'1'));
		if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Topic deleted successfully',
	            'data' => $result
	        ));
            exit;
	    } 
	    else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}









}


?>


	