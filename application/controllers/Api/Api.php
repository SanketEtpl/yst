<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

 class Api extends REST_Controller
{
	public function __construct()
    {
        parent::__construct();
        require_once APPPATH.'third_party/phpexcel/PHPExcel.php';
	    $this->excel = new PHPExcel();
        header('Access-Control-Allow-Origin: *');
        ini_set('display_errors', '1');
        $this->load->model("topic_model");
        $this->load->model("session_model");
        $this->load->model("manage_testimonials_model");

    }
    /*This function used to logged in user*/
    public function loginMe_post()
    {
    	if (empty(trim($this->input->post('email_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter email Id.'
	        ));
	        exit;
        }
        if (empty(trim($this->input->post('password')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter password.'
	        ));
	        exit;
        }
        $email = $this->input->post('email_id');
        $password = $this->input->post('password');
        /*if($this->input->post('check1')){
          $remember_me = $this->input->post('check1');
		  $cookie_email = $this->input->post('email_id');
		  $cookie_pass = $this->input->post('password');
		  set_cookie("email_id",$cookie_email,time()+ (10*365*24*60*60));
		  set_cookie("password",$cookie_pass,time()+ (10*365*24*60*60));
        }
        else {
            if(isset($_COOKIE["email_id"])) {
                set_cookie ("email_id","");
            }
            if(isset($_COOKIE["password"])) {
                set_cookie ("password","");
            }
        }*/
        $this->load->model("login_model");
        $result = $this->login_model->loginMe($email, $password);
       // echo "<pre>";print_r($result[]);die;
        if($result)
        {
	        $sessionArray = array(
	            'userId'=>$result['tutor_id'],                    
	            'useremail'=>$result['tutor_email_id'],
	            'username'=>$result['tutor_name'],
	            'type' => 'admin',
	            'isLoggedIn' => TRUE
	        );
	        $this->session->set_userdata($sessionArray);
	        
	        $this->response(array(
	            'status' => true,
	            'message' => 'Login successfully',
	            'data' => $result
	        ));
	        exit;
        }
        else
        {
            $this->session->set_flashdata('error', 'Email or password mismatch');
            
            $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
        }
    }

    // ---- get all course list ----
    public function courseList_get()
    {
        $this->load->model("course_model");
	    $result    = $this->course_model->courseList();
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Course listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	// ---- get all subject list ----
    public function subjectList_get()
    {
    	$course_id = $this->get('id');
        $this->load->model("subject_model");
	    $result  = $this->subject_model->subjectList($course_id);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Subject listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	// ---- get all topic list ----
    public function topicBySubId_get()
    {
 		$subject_id = $this->get('id');
 		$this->load->model("topic_model");
	    $result = $this->topic_model->topicBySubId($subject_id);//print_r($result);exit;
	//    echo $this->db->last_query();exit;
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Topic listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	// ---- get all topic list ----
    /*public function topicByTopicId_get()
    {
 		if (empty(trim($this->input->post('topic_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter topic.'
	        ));
	        exit;
        }
 		$this->load->model("topic_model");
	    $result = $this->topic_model->topicByTopicId(trim($this->input->post('topic_id')));//print_r($result);exit;
	//    echo $this->db->last_query();exit;
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Topic listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}
*/

	 // ---- get all student list ----
    public function studentList_get()
    {
        $this->load->model("student_model");
	    $result = $this->student_model->studentList();
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Student listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	// ---- get all session list ----
    public function sessionList_get()
    {
        $this->load->model("session_model");
	    $result = $this->session_model->sessionList();
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Session listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	 // ---- get all topic list ----
   /* public function topic_list_get()
    {
 		$this->load->model("topic_model");
	    $result = $this->topic_model->topicList();
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Topic listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}*/

	

	 // ---- view course details ----
  /*  public function course_details_post()
    {//echo 'hiiii';exit;
        if (empty(trim($this->input->post('course_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter course.'
	        ));
	        exit;
        }
       $topic_id=trim($this->input->post('topic_id'));
       $subject_id=trim($this->input->post('subject_id'));
       $course_id=trim($this->input->post('course_id'));

        $this->load->model("topic_model");
	    $result = $this->topic_model->courseDetails($topic_id,$subject_id,$course_id);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Course Details',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}*/

	 public function search_topic_by_topicId_post()
    {
		//echo "<pre>";print_r($_POST);die;

        if (empty(trim($this->input->post('topic_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter topic.'
	        ));
	        exit;
        }
        $topic_id=trim($this->input->post('topic_id'));
        $this->load->model("topic_model");
	    $result = $this->topic_model->courseDetailsByTopicId($topic_id);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Course Details',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	public function search_topic_by_subjectId_post()
    {
		//echo "<pre>";print_r($_POST);die;
        if (empty(trim($this->input->post('subject_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter subject.'
	        ));
	        exit;
        }
        $subject_id=trim($this->input->post('subject_id'));
        $this->load->model("topic_model");
	    $result = $this->topic_model->courseDetailsBySubjectId($subject_id);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Course Details',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	public function search_topic_by_courseId_post()
    {
        if (empty(trim($this->input->post('course_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter course.'
	        ));
	        exit;
        }
        $course_id=trim($this->input->post('course_id'));
        $this->load->model("topic_model");
	    $result = $this->topic_model->courseDetailsByCourseId($course_id);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Course Details',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}
	


	//----get manane content-------------
	
    public function manage_content_post()
    {
       // $key='my_approach';
    	 if (empty(trim($this->input->post('key_type')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter key type.'
	        ));
	        exit;
        }

        $key=trim($this->input->post('key_type'));//exit;

        $this->load->model("manage_content_model");
	    $result    = $this->manage_content_model->manage_content($key);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'My Approach conent',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	public function delete_manage_content_post()
    {
       // $key='my_approach';
    	 if (empty(trim($this->input->post('key_type')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter key type.'
	        ));
	        exit;
        }

        $key=trim($this->input->post('key_type'));//exit;

        $this->load->model("manage_content_model");
	    $result    = $this->manage_content_model->delete_manage_content($key);
		    if ($result['status'] == 'success') {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Record deleted successfully'
	        ));
	        exit;
		    } else if ($result['status'] == 'error') {
		        $this->response(array(
		            'status' => false,
		            'message' => 'Record is already deleted.'
		        ));
		        exit;
		    } else if ($result['status'] == 'not_exists') {
		        $this->response(array(
		            'status' => false,
		            'message' => 'Record does not exists.'
		        ));
		        exit;
		    }
	}

	public function update_manage_content_post()
    {
       // $key='my_approach';
    	 if (empty(trim($this->input->post('key_type')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter key type.'
	        ));
	        exit;
        }
         if (empty(trim($this->input->post('content')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter content.'
	        ));
	        exit;
        }

        $key=trim($this->input->post('key_type'));//exit;
        $updateArr = array(
            'content' => trim($this->input->post('content')),
            'key_type' => $key,
               );
//print_r($updateArr);exit;
        $this->load->model("manage_content_model");
	    $result    = $this->manage_content_model->update_manage_content($updateArr);
	    if ($result['status'] == 'success') {
	        $this->response(array(
		             'status' => true,
                'message' => 'Record updated successfully'
            ));
            exit;
        } else if ($result == 'error') {
            $this->response(array(
                'status' => false,
                'message' => 'Record is already up-to-date.'
            ));
            exit;
        }
	}

	//---manage offers--------
	 // ---- add offer ----
    public function save_offer_post()
    {
        $postData = $_POST;
        $this->load->model("manage_offer_model");
        if (empty(trim($this->input->post('title')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter title.'
	        ));
	        exit;
        }
         if (empty(trim($this->input->post('applicable_from_date')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter applicable from date.'
	        ));
	        exit;
        }

          if (empty(trim($this->input->post('applicable_to_date')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter applicable to date.'
	        ));
	        exit;
        }
          if (empty(trim($this->input->post('details')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter details'
	        ));
	        exit;
        }

        $insertArr = array(
            'title' => trim($this->input->post('title')),
            'applicable_from_date' => trim($this->input->post('applicable_from_date')),
            'applicable_to_date' => trim($this->input->post('applicable_to_date')),
            'details' => trim($this->input->post('details')),
            'is_deleted' =>  0,
             );

        $result    = $this->manage_offer_model->save_offer($insertArr);//print_r($result);exit;
        if ($result['status'] == 'exists') {
            $this->response(array(
                'status' => false,
                'message' => 'Offer already exists with same name.'
            ));
            exit;
        } else if ($result['status'] == 'success') {
            $this->response(array(
                'status' => true,
                'message' => 'Offer added successfully.'
            ));
            exit;
        } else if ($result['status'] == 'error') {
            $this->response(array(
                'status' => false,
                'message' => 'Oops!!! Something went wrong.'
            ));
            exit;
        }
    }

	//get all offers
	public function offerList_get()
    {
    	$this->load->model("manage_offer_model");
    	$result  = $this->manage_offer_model->offerList();
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Offer listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	//get offer byId
	public function get_offer_byId_post()
    {	
    	 if (empty(trim($this->input->post('id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter id.'
	        ));
	        exit;
        }
        $key=trim($this->input->post('id'));//exit;
    	$this->load->model("manage_offer_model");
    	$result  = $this->manage_offer_model->get_offer_byId($key);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Offer Details',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}
	//---delete offers
	public function delete_offer_post()
    {
       // $key='my_approach';
    	 if (empty(trim($this->input->post('id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter id.'
	        ));
	        exit;
        }

        $key=trim($this->input->post('id'));//exit;

        $this->load->model("manage_offer_model");
	    $result    = $this->manage_offer_model->delete_offer($key);
		    if ($result['status'] == 'success') {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Record deleted successfully'
	        ));
	        exit;
		    } else if ($result['status'] == 'error') {
		        $this->response(array(
		            'status' => false,
		            'message' => 'Record is already deleted.'
		        ));
		        exit;
		    } else if ($result['status'] == 'not_exists') {
		        $this->response(array(
		            'status' => false,
		            'message' => 'Record does not exists.'
		        ));
		        exit;
		    }
	}

	//----------update offer------------
	public function update_offer_post()
    {
       // $key='my_approach';
    	if (empty(trim($this->input->post('id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter id.'
	        ));
	        exit;
        }
         if (empty(trim($this->input->post('title')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter title.'
	        ));
	        exit;
        }
         if (empty(trim($this->input->post('applicable_from_date')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter applicable from date.'
	        ));
	        exit;
        }

          if (empty(trim($this->input->post('applicable_to_date')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter applicable to date.'
	        ));
	        exit;
        }
          if (empty(trim($this->input->post('details')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter details'
	        ));
	        exit;
        }

        $updateArr = array(
            'title' => trim($this->input->post('title')),
            'applicable_from_date' => trim($this->input->post('applicable_from_date')),
            'applicable_to_date' => trim($this->input->post('applicable_to_date')),
            'details' => trim($this->input->post('details')),
            'id' =>  trim($this->input->post('id')),
               );
//print_r($updateArr);exit;
        $this->load->model("manage_offer_model");
	    $result    = $this->manage_offer_model->update_offer($updateArr);
	    if ($result['status'] == 'success') {
	        $this->response(array(
		             'status' => true,
                'message' => 'Record updated successfully'
            ));
            exit;
        } else if ($result['status'] == 'error') {
            $this->response(array(
                'status' => false,
                'message' => 'Record is already up-to-date.'
            ));
            exit;
        }
	}


	 // ---- add pictures or clips----
    public function savePictureOrClip_post()
    {//print_r($_FILES['image']['name']);exit;
        $postData = $_POST;//print_r($postData);exit;
        $this->load->model("manage_pictures_model");
        if (empty(trim($this->input->post('name')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter name.'
	        ));
	        exit;
        }
       /*  if (empty(trim($this->input->post('image')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter image.'
	        ));
	        exit;
        }*/
         if (empty(trim($this->input->post('type')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter type.'
	        ));
	        exit;
        }
       $type=trim($this->input->post('type'));//exit;

        if ($type == 'picture') {//echo 'hii';exit;
	       //echo $type;exit;

        	$files = $_FILES;
			if(isset($_FILES['image']) && !empty($_FILES['image']['name']))
		    {//echo 'hii';exit;
 				$config['upload_path'] = './uploads/manage_content/picture/';
			 	$config['allowed_types'] = 'png|jpg|jpeg|PNG|JPG|JPEG';
		 		$this->load->library('upload');
			 	$_FILES['image']['name']= $files['image']['name'];
		        $_FILES['image']['type']= $files['image']['type'];
		        $_FILES['image']['tmp_name']= $files['image']['tmp_name'];
		        $_FILES['image']['error']= $files['image']['error'];
		        $_FILES['image']['size']= $files['image']['size']; 

		    	$new_photo_name1 = rand('0','99999')."_".time();
			 	$config['file_name'] = $new_photo_name1;
		        $this->upload->initialize($config);
				if($this->upload->do_upload('image')){
				    $uploadData = $this->upload->data();
					$picture_name= $uploadData['file_name'];
			 	}else{
					$file_err_msg = $this->upload->display_errors('<p>', '</p>');
					$this->response(array(
			            'status' => false,
			            'message' => $file_err_msg,
			        ));
		            exit;
				    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
					$picture = '';
				}
			}
			$insertArr = array(
	            'name' => trim($this->input->post('name')),
	            'image' =>$picture_name, //trim($this->input->post('image')),
	            'type' => trim($this->input->post('type')),
	            'is_deleted' =>  0,
	             );
	        //print_r($insertArr);exit;
	        $result = $this->manage_pictures_model->savePictureOrClip($insertArr);//print_r($result);exit;
	              
        }
        else if($type == 'clip' ) {
        	$files = $_FILES;
			if(isset($_FILES['image']) && !empty($_FILES['image']['name']))
		    {//echo 'hii';exit;
 				$config['upload_path'] = './uploads/manage_content/clip/';
			 	$config['allowed_types'] = 'png|jpg|jpeg|PNG|JPG|JPEG';
		 		$this->load->library('upload');
			 	$_FILES['image']['name']= $files['image']['name'];
		        $_FILES['image']['type']= $files['image']['type'];
		        $_FILES['image']['tmp_name']= $files['image']['tmp_name'];
		        $_FILES['image']['error']= $files['image']['error'];
		        $_FILES['image']['size']= $files['image']['size']; 

		    	$new_photo_name1 = rand('0','99999')."_".time();
			 	$config['file_name'] = $new_photo_name1;
		        $this->upload->initialize($config);
				if($this->upload->do_upload('image')){
				    $uploadData = $this->upload->data();
					$picture_name= $uploadData['file_name'];
			 	}else{
					$file_err_msg = $this->upload->display_errors('<p>', '</p>');
					$this->response(array(
			            'status' => false,
			            'message' => $file_err_msg,
			        ));
		            exit;
				    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
					$picture = '';
				}
			}
			$insertArr = array(
	            'name' => trim($this->input->post('name')),
	            'image' =>$picture_name, //trim($this->input->post('image')),
	            'type' => trim($this->input->post('type')),
	            'is_deleted' =>  0,
	             );
	        //print_r($insertArr);exit;
	        $result = $this->manage_pictures_model->savePictureOrClip($insertArr);//print_r($result);exit;
	              
        }else{
        	 $this->response(array(
	            'status' => false,
	            'message' => 'Type should be picture or clip.'
	        ));
	        exit;
        }

          if ($result['status'] == 'exists') {
	            $this->response(array(
	                'status' => false,
	                'message' => 'Picture already exists with same name.'
	            ));
	            exit;
	        } else if ($result['status'] == 'success') {
	            $this->response(array(
	                'status' => true,
	                'message' => 'Picture added successfully.'
	            ));
	            exit;
	        } else if ($result['status'] == 'error') {
	            $this->response(array(
	                'status' => false,
	                'message' => 'Oops!!! Something went wrong.'
	            ));
	            exit;
	        }

    }

    //get pictures byId
	public function get_pic_byId_post()
    {	
    	 if (empty(trim($this->input->post('id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter id.'
	        ));
	        exit;
        }
        $key=trim($this->input->post('id'));//exit;
    	$this->load->model("manage_pictures_model");
    	$result  = $this->manage_pictures_model->get_pic_byId($key);//print_r($result);exit;
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Picture Details',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

    //get all pictures
	public function picturesOrClipList_post()
    {
    	 if (empty(trim($this->input->post('type')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter type.'
	        ));
	        exit;
        }
        $type=trim($this->input->post('type'));//exit;

	    if ($type == 'picture' || $type == 'clip' ) {//echo 'hii';exit;
		     
	    	$this->load->model("manage_pictures_model");
	    	$result  = $this->manage_pictures_model->picturesOrClipList($type);
		    if (!empty($result)) {
		        $this->response(array(
		            'status' => true,
		            'message' => 'pictures listing',
		            'data' => $result
		        ));
	            exit;
		    } else if ($result == '') {
		        $this->response(array(
		            'status' => false,
		            'message' => 'No data found.'
		        ));
		        exit;
		    }
		}else{
        	 $this->response(array(
	            'status' => false,
	            'message' => 'Type should be picture or clip.'
	        ));
	        exit;
        }
    
	}

	//---delete pictures
	public function deletePictureOrClip_post()
    {
       // $key='my_approach';
    	 if (empty(trim($this->input->post('id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter id.'
	        ));
	        exit;
        }

        $key=trim($this->input->post('id'));//exit;

        $this->load->model("manage_pictures_model");
	    $result    = $this->manage_pictures_model->deletePictureOrClip($key);
		    if ($result['status'] == 'success') {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Record deleted successfully'
	        ));
	        exit;
		    } else if ($result['status'] == 'error') {
		        $this->response(array(
		            'status' => false,
		            'message' => 'Record is already deleted.'
		        ));
		        exit;
		    } else if ($result['status'] == 'not_exists') {
		        $this->response(array(
		            'status' => false,
		            'message' => 'Record does not exists.'
		        ));
		        exit;
		    }
	}

	//----------update pictures------------
	public function update_picturesOrCLip_post()
    {//echo $_FILES['image']['name'];
    //print_r($_POST);exit;
        $postData = $_POST;
        $this->load->model("manage_pictures_model");
       if (empty(trim($this->input->post('id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter id.'
	        ));
	        exit;
        }
        if (empty(trim($this->input->post('name')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter name.'
	        ));
	        exit;
        }
       /*  if (empty(trim($this->input->post('image')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter image.'
	        ));
	        exit;
        }
		*/

		$files = $_FILES;
		if(isset($_FILES['image']) && !empty($_FILES['image']['name']))
	    {//echo 'hii';exit;
			$config['upload_path'] = './uploads/manage_content/picture/';
		 	$config['allowed_types'] = 'png|jpg|jpeg|PNG|JPG|JPEG';
	 		$this->load->library('upload');
		 	$_FILES['image']['name']= $files['image']['name'];
	        $_FILES['image']['type']= $files['image']['type'];
	        $_FILES['image']['tmp_name']= $files['image']['tmp_name'];
	        $_FILES['image']['error']= $files['image']['error'];
	        $_FILES['image']['size']= $files['image']['size']; 

	    	$new_photo_name1 = rand('0','99999')."_".time();
		 	$config['file_name'] = $new_photo_name1;
	        $this->upload->initialize($config);
			if($this->upload->do_upload('image')){
			    $uploadData = $this->upload->data();
				$picture_name= $uploadData['file_name'];
		 	}else{
				$file_err_msg = $this->upload->display_errors('<p>', '</p>');
				$this->response(array(
		            'status' => false,
		            'message' => $file_err_msg,
		        ));
	            exit;
			    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
				$picture_name = '';
			}
		}else{
			$picture_name='';
		}
		//echo $picture_name;exit;

         $updateArr = array(
            'name' => trim($this->input->post('name')),
           // 'image' => trim($this->input->post('image')),
            'id' => trim($this->input->post('id')),
             );
         //if(!empty($picture_name)){
			$updateArr['image']=$picture_name;
		 //}


		//print_r($updateArr);exit;
        $result    = $this->manage_pictures_model->update_picturesOrCLip($updateArr);//print_r($result);exit;
	    if ($result['status'] == 'success') {
	        $this->response(array(
		             'status' => true,
                'message' => 'Record updated successfully'
            ));
            exit;
        } else if ($result['status']  == 'error') {
            $this->response(array(
                'status' => false,
                'message' => 'Record is already up-to-date.'
            ));
            exit;
        }
	}


	 //get FAQs---------------------
	public function faqList_get()
    {
    	$this->load->model("manage_faq_model");
	    $result  = $this->manage_faq_model->faqList();
		    if (!empty($result)) {
		        $this->response(array(
		            'status' => true,
		            'message' => 'FAQs listing',
		            'data' => $result
		        ));
	            exit;
		    } else if ($result == '') {
		        $this->response(array(
		            'status' => false,
		            'message' => 'No data found.'
		        ));
		        exit;
		    }

	}

	//get faq byId
	public function get_faq_byId_post()
    {	
    	 if (empty(trim($this->input->post('id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter id.'
	        ));
	        exit;
        }
        $key=trim($this->input->post('id'));//exit;
    	$this->load->model("manage_faq_model");
    	$result  = $this->manage_faq_model->get_faq_byId($key);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'FAQ Details',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	 // ---- add FAQs----
    public function saveFaq_post()
    {
        $postData = $_POST;
        $this->load->model("manage_faq_model");
        if (empty(trim($this->input->post('question')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter question.'
	        ));
	        exit;
        }
         if (empty(trim($this->input->post('answer')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter answer.'
	        ));
	        exit;
        }
          $insertArr = array(
	            'question' => trim($this->input->post('question')),
	            'answer' => trim($this->input->post('answer'))
	             );
	        //print_r($insertArr);exit;
	        $result    = $this->manage_faq_model->saveFaq($insertArr);//print_r($result);exit;
	        if ($result['status'] == 'success') {
	            $this->response(array(
	                'status' => true,
	                'message' => 'FAQ added successfully.'
	            ));
	            exit;
	        } else if ($result['status'] == 'error') {
	            $this->response(array(
	                'status' => false,
	                'message' => 'Oops!!! Something went wrong.'
	            ));
	            exit;
	        }
      
    }

    //---delete FAQ
	public function deleteFaq_post()
    {
       // $key='my_approach';
    	 if (empty(trim($this->input->post('id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter id.'
	        ));
	        exit;
        }

        $key=trim($this->input->post('id'));//exit;

        $this->load->model("manage_faq_model");
	    $result    = $this->manage_faq_model->deleteFaq($key);
		    if ($result['status'] == 'success') {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Record deleted successfully'
	        ));
	        exit;
		    } else if ($result['status'] == 'error') {
		        $this->response(array(
		            'status' => false,
		            'message' => 'Record is already deleted.'
		        ));
		        exit;
		    } else if ($result['status'] == 'not_exists') {
		        $this->response(array(
		            'status' => false,
		            'message' => 'Record does not exists.'
		        ));
		        exit;
		    }
	}

	//----------update faq------------
	public function updateFaq_post()
    {
        $postData = $_POST;
        $this->load->model("manage_faq_model");
       if (empty(trim($this->input->post('id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter id.'
	        ));
	        exit;
        }
        if (empty(trim($this->input->post('question')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter question.'
	        ));
	        exit;
        }
         if (empty(trim($this->input->post('answer')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter answer.'
	        ));
	        exit;
        }

         $updateArr = array(
            'question' => trim($this->input->post('question')),
            'answer' => trim($this->input->post('answer')),
            'id' => trim($this->input->post('id')),
             );
//print_r($updateArr);exit;
        $result    = $this->manage_faq_model->updateFaq($updateArr);//print_r($result);exit;
	    if ($result['status'] == 'success') {
	        $this->response(array(
		             'status' => true,
                'message' => 'Record updated successfully'
            ));
            exit;
        } else if ($result['status']  == 'error') {
            $this->response(array(
                'status' => false,
                'message' => 'Record is already up-to-date.'
            ));
            exit;
        }
	}


 //get testimonials---------------------
	public function testimonialsList_get()
    {
    	$this->load->model("manage_testimonials_model");
	    $result  = $this->manage_testimonials_model->testimonialsList();
		    if (!empty($result)) {
		        $this->response(array(
		            'status' => true,
		            'message' => 'Testimonials listing',
		            'data' => $result
		        ));
	            exit;
		    } else if ($result == '') {
		        $this->response(array(
		            'status' => false,
		            'message' => 'No data found.'
		        ));
		        exit;
		    }

	}

//get Testimonial byId
	public function get_Testim_byId_post()
    {	
    	 if (empty(trim($this->input->post('id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter id.'
	        ));
	        exit;
        }
        $key=trim($this->input->post('id'));//exit;
    	$this->load->model("manage_testimonials_model");
    	$result  = $this->manage_testimonials_model->get_Testim_byId($key);//print_r($result);exit;
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Testimonials Details',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}
	 // ---- add testimonials----
    public function saveTestimonials_post()
    {
        $postData = $_POST;//print_r($postData);exit;
        $this->load->model("manage_testimonials_model");
        if (empty(trim($this->input->post('name')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter name.'
	        ));
	        exit;
        }
         if (empty(trim($this->input->post('rating')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter rating.'
	        ));
	        exit;
        }
        if (empty(trim($this->input->post('comments')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter comments.'
	        ));
	        exit;
        }
        if (empty($_FILES['image']['name'])) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please select Image.'
	        ));
	        exit;
        }

            $files = $_FILES;
			if(isset($_FILES['image']) && !empty($_FILES['image']['name']))
		    {//echo 'hii';exit;
 				$config['upload_path'] = './uploads/manage_content/testimonials/';
			 	$config['allowed_types'] = 'png|jpg|jpeg|PNG|JPG|JPEG';
		 		$this->load->library('upload');
			 	$_FILES['image']['name']= $files['image']['name'];
		        $_FILES['image']['type']= $files['image']['type'];
		        $_FILES['image']['tmp_name']= $files['image']['tmp_name'];
		        $_FILES['image']['error']= $files['image']['error'];
		        $_FILES['image']['size']= $files['image']['size']; 

		    	$new_photo_name1 = rand('0','99999')."_".time();
			 	$config['file_name'] = $new_photo_name1;
		        $this->upload->initialize($config);
				if($this->upload->do_upload('image')){
				    $uploadData = $this->upload->data();
					$picture_name= $uploadData['file_name'];
			 	}else{
					$file_err_msg = $this->upload->display_errors('<p>', '</p>');
					$this->response(array(
			            'status' => false,
			            'message' => $file_err_msg,
			        ));
		            exit;
				    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
					$picture_name = '';
				}

                $insertArr = array(
	            'name' => trim($this->input->post('name')),
	            'image' => $picture_name,
	            'rating' =>trim($this->input->post('rating')),
	            'comments' => trim($this->input->post('comments'))
	             );
	        	//print_r($insertArr);exit;
	        	$result    = $this->manage_testimonials_model->saveTestimonials($insertArr);//print_r($result);exit;
			
			//echo $result['status'];exit;
	        if ($result['status'] == 'success') {
	            $this->response(array(
	                'status' => true,
	                'message' => 'Testimonials added successfully.'
	            ));
	            exit;
	        } else if ($result['status'] == 'error') {
	            $this->response(array(
	                'status' => false,
	                'message' => 'Oops!!! Something went wrong.'
	            ));
	            exit;
	        }
        }
    }

    //---delete Testimonials
	public function deleteTestimonials_post()
    {
       // $key='my_approach';
    	 if (empty(trim($this->input->post('id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter id.'
	        ));
	        exit;
        }

        $key=trim($this->input->post('id'));//exit;

        $this->load->model("manage_testimonials_model");
	    $result    = $this->manage_testimonials_model->deleteTestimonials($key);
		    if ($result['status'] == 'success') {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Record deleted successfully'
	        ));
	        exit;
		    } else if ($result['status'] == 'error') {
		        $this->response(array(
		            'status' => false,
		            'message' => 'Record is already deleted.'
		        ));
		        exit;
		    } else if ($result['status'] == 'not_exists') {
		        $this->response(array(
		            'status' => false,
		            'message' => 'Record does not exists.'
		        ));
		        exit;
		    }
	}

	//----------update Testimonials------------
	public function updateTestimonials_post()
    {
        $postData = $_POST;
        $this->load->model("manage_testimonials_model");
       if (empty(trim($this->input->post('id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter id.'
	        ));
	        exit;
        }
      if (empty(trim($this->input->post('name')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter name.'
	        ));
	        exit;
        }
         if (empty(trim($this->input->post('rating')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter rating.'
	        ));
	        exit;
        }
        if (empty(trim($this->input->post('comments')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter comments.'
	        ));
	        exit;
        }

        $files = $_FILES;
		if(isset($_FILES['image']) && !empty($_FILES['image']['name']))
	    {//echo 'hii';exit;
				$config['upload_path'] = './uploads/manage_content/testimonials/';
		 	$config['allowed_types'] = 'png|jpg|jpeg|PNG|JPG|JPEG';
	 		$this->load->library('upload');
		 	$_FILES['image']['name']= $files['image']['name'];
	        $_FILES['image']['type']= $files['image']['type'];
	        $_FILES['image']['tmp_name']= $files['image']['tmp_name'];
	        $_FILES['image']['error']= $files['image']['error'];
	        $_FILES['image']['size']= $files['image']['size']; 

	    	$new_photo_name1 = rand('0','99999')."_".time();
		 	$config['file_name'] = $new_photo_name1;
	        $this->upload->initialize($config);
			if($this->upload->do_upload('image')){
			    $uploadData = $this->upload->data();
				$testim_name= $uploadData['file_name'];
		 	}else{
				$file_err_msg = $this->upload->display_errors('<p>', '</p>');
				$this->response(array(
		            'status' => false,
		            'message' => $file_err_msg,
		        ));
	            exit;
			    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
				$testim_name = '';
			}
		}else{
			  $testim_name = '';
		}

          $updateArr = array(
         	    'name' => trim($this->input->post('name')),
	            'rating' => trim($this->input->post('rating')),
	            'comments' => trim($this->input->post('comments')),
           	    'id' => trim($this->input->post('id')),
             );
         $updateArr['image']=$testim_name;
		//print_r($updateArr);exit;
        $result    = $this->manage_testimonials_model->updateTestimonials($updateArr);//print_r($result);exit;
	    if ($result['status'] == 'success') {
	        $this->response(array(
		             'status' => true,
                'message' => 'Record updated successfully'
            ));
            exit;
        } else if ($result['status']  == 'error') {
            $this->response(array(
                'status' => false,
                'message' => 'Record is already up-to-date.'
            ));
            exit;
        }
	}

	//add student by admin
	public function addUser_post()
	{
	    $this->load->model("student_model");
	    $postData = $_POST;
	    if(empty($postData['first_name'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter first name.'
	        ));
	        exit;
		}
		if(empty($postData['last_name'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter last name.'
	        ));
	        exit;
		}
		if(empty($postData['email_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter email id.'
	        ));
	        exit;
		}
		$password = rand(0,999999);
		$postData['password'] = $password;
		$result = $this->student_model->addStudent($postData);
		
		if (!empty($result)) {
			if($result['status'] == 'exists'){
				 $this->response(array(
		            'status' => false,
		            'message' => 'Student allready exists!!',
		            'data' => $result
		        ));
	            exit;
			}
			else{
				//send email to user for password
				/*$user_full_name = $postData['first_name'].' '.$postData['last_name'];
				$message  = "Congrats on signing up for Yst! You have registered successfully.  
		               Plase check your password : ".$password; 
				$send = sendPassword($postData['email_id'],$user_full_name,$message,"Yst registration from admin");*/

		        $this->response(array(
		            'status' => true,
		            'message' => 'Student added successfully!! Sent email to user for passsword.',
		            'data' => $result
		        ));
	            exit;
			}
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Student fail to add.'
	        ));
	        exit;
	    }
	}

	//get all topic by subject id
	public function topicList_get()
    {
    	$subject_id = $this->get('id');
	    $result  = $this->topic_model->topicBySubId($subject_id);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Topic listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	//add group session by admin
	public function addSession_post()
	{
		//echo "<pre>";print_r($_POST);die;
		$postData = $_POST;
		if(empty($postData['course_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select course id.'
	        ));
	        exit;
		}
		if(empty($postData['subject_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select subject id.'
	        ));
	        exit;
		}
		if(empty($postData['topic_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select topic id.'
	        ));
	        exit;
		}
		if(empty($postData['title'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter title.'
	        ));
	        exit;
		}
		if(empty($postData['date'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select date.'
	        ));
	        exit;
		}
		if(empty($postData['start_time'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select start time.'
	        ));
	        exit;
		}
		if(empty($postData['end_time'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select end time.'
	        ));
	        exit;
		}
		if(empty($postData['duration_in_minutes'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter duration.'
	        ));
	        exit;
		}
		if(empty($postData['cost'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter cost.'
	        ));
	        exit;
		}

		$result = $this->session_model->addSession($postData);

		if (!empty($result)) {
			if($result['status'] == 'exists'){
				 $this->response(array(
		            'status' => false,
		            'message' => 'Session allready exists!!',
		            'data' => $result
		        ));
	            exit;
			}
			else{
		        $this->response(array(
		            'status' => true,
		            'message' => 'Session added successfully!!.',
		            'data' => $result
		        ));
	            exit;
			}
	    } 
	    else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Session fail to add.'
	        ));
	        exit;
	    }

	}

	//------------get session details by session id -----------
	public function getSessiondetails_post(){
		$postData = $_POST;
		if(empty($postData['session_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please require session id.'
	        ));
	        exit;
		}
		$result = $this->session_model->getSessiondetails($postData);
		//echo "<pre>";print_r($result);die;
		if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Session added successfully!!.',
	            'data' => $result
	        ));
            exit;
	    } 
	    else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Session fail to add.'
	        ));
	        exit;
	    }
	}

	//-------------------update session details----------------------------------------
	public function updateSession_post()
	{
		$postData = $_POST;
		//echo "<pre>";print_r($postData);die;
		if(empty($postData['course_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select course id.'
	        ));
	        exit;
		}
		if(empty($postData['subject_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select subject id.'
	        ));
	        exit;
		}
		if(empty($postData['topic_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select topic id.'
	        ));
	        exit;
		}
		if(empty($postData['date'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select date.'
	        ));
	        exit;
		}
		if(empty($postData['start_time'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select start time.'
	        ));
	        exit;
		}
		if(empty($postData['end_time'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select end time.'
	        ));
	        exit;
		}
		if(empty($postData['duration_in_minutes'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter duration.'
	        ));
	        exit;
		}
		if(empty($postData['cost'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter cost.'
	        ));
	        exit;
		}
		$result = $this->session_model->updateSession($postData);
		if (!empty($result)) {
			if($result['status'] == 'exists'){
				 $this->response(array(
		            'status' => false,
		            'message' => 'Session allready exists!!',
		            'data' => $result
		        ));
	            exit;
			}
			else{
		        $this->response(array(
		            'status' => true,
		            'message' => 'Session update successfully!!.',
		            'data' => $result
		        ));
	            exit;
			}
	    } 
	    else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Session fail to add.'
	        ));
	        exit;
	    }
	}
	//------------delete session  -----------
	public function deleteSession_post(){
		$postData = $_POST;
		if(empty($postData['session_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please require session id.'
	        ));
	        exit;
		}
		$result = $this->session_model->deleteSession($postData);
		if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Session delete successfully!!.',
	            'data' => $result
	        ));
            exit;
	    } 
	    else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Session fail to add.'
	        ));
	        exit;
	    }
	}

	public function inviteList_get(){
	    $result    = $this->invite_model->inviteList();
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Invite listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}
	/*---------add multiple topic-------*/
	public function addTopic_post(){
		$postData = $_POST;
		$files = $_FILES;
		if(empty($postData['count'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please add the topic.'
	        ));
	        exit;
		}
		$topic_arr = array();
		$count = $postData['count'];
		$j=1;

		for($i=0;$i<$count;$i++){
			
	        $files = $_FILES;
			if(isset($_FILES['topic'.$j]) && !empty($_FILES['topic'.$j]['name']))
		    {
			    $config['upload_path'] = './uploads/topic/preview_video/';
			 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
		 		$this->load->library('upload');
			 	$_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['preview_video'];
		        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['preview_video'];
		        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['preview_video'];
		        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['preview_video'];
		        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['preview_video']; 

		    	$new_photo_name1 = rand('0','99999')."_".time();
			 	$config['file_name'] = $new_photo_name1;
		        $this->upload->initialize($config);
				if($this->upload->do_upload('topic'.$j)){
				    $uploadData = $this->upload->data();
					$preview_video = $uploadData['file_name'];
			 	}else{
					$file_err_msg = $this->upload->display_errors('<p>', '</p>');
					$this->response(array(
			            'status' => false,
			            'message' => $file_err_msg,
			        ));
		            exit;
				    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
					$picture = '';
				}
				$config['upload_path'] = './uploads/topic/question_bank/';
			 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
		 		$this->load->library('upload');
		        $_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['question_bank'];
		        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['question_bank'];
		        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['question_bank'];
		        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['question_bank'];
		        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['question_bank'];

				$new_photo_name2 = rand('0','99999')."_".time();
			 	$config['file_name'] = $new_photo_name2;
		        $this->upload->initialize($config);
				if($this->upload->do_upload('topic'.$j)){
				    $uploadData = $this->upload->data();
					$question_bank = $uploadData['file_name'];

					$question_excel_data = $this->excelExport($uploadData['file_name']);
					// print_r($question_excel_data);
					// exit();
			 	}else{
					$file_err_msg = $this->upload->display_errors('<p>', '</p>');
					$this->response(array(
			            'status' => false,
			            'message' => $file_err_msg,
			        ));
		            exit;
				    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
					$picture = '';
				}

				$config['upload_path'] = './uploads/topic/notes/';
			 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
		 		$this->load->library('upload');
		        $_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['notes'];
		        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['notes'];
		        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['notes'];
		        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['notes'];
		        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['notes']; 

				$new_photo_name3 = rand('0','99999')."_".time();
			 	$config['file_name'] = $new_photo_name3;
		        $this->upload->initialize($config);
				if($this->upload->do_upload('topic'.$j)){
				    $uploadData = $this->upload->data();
					$notes = $uploadData['file_name'];
			 	}else{
					$file_err_msg = $this->upload->display_errors('<p>', '</p>');
					$this->response(array(
			            'status' => false,
			            'message' => $file_err_msg,
			        ));
		            exit;
				    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
					$picture = '';
				}

				$config['upload_path'] = './uploads/topic/lession_video/';
			 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
		 		$this->load->library('upload');
		        $_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['lession_video'];
		        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['lession_video'];
		        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['lession_video'];
		        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['lession_video'];
		        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['lession_video'];

				$new_photo_name4 = rand('0','99999')."_".time();
			 	$config['file_name'] = $new_photo_name4;

		        $this->upload->initialize($config);
				if($this->upload->do_upload('topic'.$j)){
				    $uploadData = $this->upload->data();
					$lession_video = $uploadData['file_name'];
			 	}else{
					$file_err_msg = $this->upload->display_errors('<p>', '</p>');
					$this->response(array(
			            'status' => false,
			            'message' => $file_err_msg,
			        ));
		            exit;
				    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
					$picture = '';
				}
				
			}
			$img =array(
				'preview_video'=>$preview_video,
				'question_bank'=>$question_bank,
				'notes'=>$notes,
				'lession_video'=>$lession_video
			);
		   $topic_arr['topic'][$i]=$postData['topic'.$j];
		  
		   $result = $this->topic_model->addTopic($topic_arr['topic'][$i],$postData['course_id'],$postData['subject_id'],$img);
		   echo "<pre>";print_r($result);die;  
		   foreach($question_excel_data as $question){

		   }
		   $j++;
		}
		if (!empty($result)) {
			if($result['status'] == 'exists'){
				 $this->response(array(
		            'status' => false,
		            'message' => 'Topic allready exists!!',
		            'data' => $result
		        ));
	            exit;
			}
			else{
		        $this->response(array(
		            'status' => true,
		            'message' => 'Topic added successfully!!',
		            'data' => $result
		        ));
	            exit;
			}
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Topic fail to add.'
	        ));
	        exit;
	    }
	} 
	//-----get student details--------- 
	public function studentProfile_post()
	{
		$studentId  = $this->input->post('student_id');

		$this->load->model("student_model");
		$result = $this->student_model->studentById($studentId);
		if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Student data',
	            'data' => $result
	        ));
            exit;
	    } 
	    else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}
	//update student profile
	public function updateStudentProfile_post()
	{
		$data = $this->input->post();
		$this->load->model("student_model");
		$result = $this->student_model->updateStudentProfile($data);

		if('success' == $result['status'])
		{
			echo json_encode(array('status' => true,'message' => 'Profile Updated sucessfully'));
		}
		else
		{
			echo json_encode(array('status' => false,'message' => 'Something went wrong'));	
		}
	}
    //student login
	public function studentLogin_post()
	{
		if (empty(trim($this->input->post('email_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter email Id.'
	        ));
	        exit;
        }
        if (empty(trim($this->input->post('password')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please enter password.'
	        ));
	        exit;
        }
        $email = $this->input->post('email_id');
        $password = $this->input->post('password');
        if($this->input->post('check1')){
          $remember_me = $this->input->post('check1');
		  $cookie_email = $this->input->post('email_id');
		  $cookie_pass = $this->input->post('password');
		  set_cookie("email_id",$cookie_email,time()+ (10*365*24*60*60));
		  set_cookie("password",$cookie_pass,time()+ (10*365*24*60*60));
        }
        else {
            if(isset($_COOKIE["email_id"])) {
                set_cookie ("email_id","");
            }
            if(isset($_COOKIE["password"])) {
                set_cookie ("password","");
            }
        }
        $this->load->model("login_model");
        $result = $this->login_model->studentLogin($email, $password);
        if($result)
        {
	        $sessionArray = array(
	            'userId'=>$result['student_id'],                    
	            'useremail'=>$result['email_id'],
	            'username'=>$result['first_name'].' '.$result['last_name'],
	            'userimage'=>$result['student_image'],
	            'type' => 'student',
	            'isLoggedIn' => TRUE
	        );
	        $this->session->set_userdata($sessionArray);
	        $this->response(array(
	            'status' => true,
	            'message' => 'Login successfully',
	            'data' => $result
	        ));
	        exit;
        }
        else
        {
            $this->session->set_flashdata('error', 'Email or password mismatch');
            
            $this->response(array(
	            'status' => false,
	            'message' => 'Email or password incorrect.'
	        ));
	        exit;
        }
	}
	//--------------student sign up---------------------
	public function signUp_post()
	{
		$postData = $_POST;
		if(empty($postData['first_name'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter first name.'
	        ));
	        exit;
		}
		if(empty($postData['last_name'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter last name.'
	        ));
	        exit;
		}
		if(empty($postData['contact_no'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter contact no.'
	        ));
	        exit;
		}
		if(empty($postData['email_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter email id.'
	        ));
	        exit;
		}
		if(empty($postData['password'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter password.'
	        ));
	        exit;
		}
		if(empty($postData['address'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter address.'
	        ));
	        exit;
		}
		if(empty($postData['country_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select country.'
	        ));
	        exit;
		}
		if(empty($postData['state_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select state.'
	        ));
	        exit;
		}
		if(empty($postData['city_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select city.'
	        ));
	        exit;
		}
		if(empty($postData['zip_code'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter zip code.'
	        ));
	        exit;
		}
	    $result = $this->signup_model->signUp($postData);
	    if (!empty($result)) {
	    	if($result['status'] == 'exists'){
				 $this->response(array(
		            'status' => false,
		            'message' => 'Your email id allready exists!!',
		            'data' => $result
		        ));
	            exit;
			}
	        $this->response(array(
	            'status' => true,
	            'message' => 'You are signup successfully!!!',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}
	//---------get city list----------
	public function cityList_post()
	{
		$postData = $_POST;
	    $result  = $this->signup_model->cityList($postData['state_id']);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'City listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}
	//-------------get state list---------------------
	public function stateList_post()
	{
		$postData = $_POST;
	    $result  = $this->signup_model->stateList($postData['country_id']);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'State listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}
	//-------------get country list----------------------------
	public function countryList_get()
	{
	    $result  = $this->signup_model->countryList();
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Country listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}
	public function updateTopic_post(){
		
		$postData = $_POST;
		$files = $_FILES;
		if(empty($postData['count'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please add the topic.'
	        ));
	        exit;
		}
		$topic_arr = array();
		$count = $postData['count'];
		$j=1;

		for($i=0;$i<$count;$i++){
			
	        $files = $_FILES;
			if(isset($_FILES['topic'.$j]) && !empty($_FILES['topic'.$j]['name']))
		    {
		    	if($files['topic'.$j]['name']['preview_video']){
				    $config['upload_path'] = './uploads/topic/preview_video/';
				 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
			 		$this->load->library('upload');
				 	$_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['preview_video'];
			        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['preview_video'];
			        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['preview_video'];
			        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['preview_video'];
			        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['preview_video']; 

			    	$new_photo_name1 = rand('0','99999')."_".time();
				 	$config['file_name'] = $new_photo_name1;
			        $this->upload->initialize($config);
					if($this->upload->do_upload('topic'.$j)){
					    $uploadData = $this->upload->data();
						$preview_video = $uploadData['file_name'];
				 	}else{
						$file_err_msg = $this->upload->display_errors('<p>', '</p>');
						$this->response(array(
				            'status' => false,
				            'message' => $file_err_msg,
				        ));
			            exit;
					    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
						$preview_video = '';
					}
		    	}else{
		    		$preview_video = '';
		    	}
		    	if($files['topic'.$j]['name']['question_bank']){
					$config['upload_path'] = './uploads/topic/question_bank/';
				 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
			 		$this->load->library('upload');
			        $_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['question_bank'];
			        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['question_bank'];
			        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['question_bank'];
			        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['question_bank'];
			        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['question_bank'];

					$new_photo_name2 = rand('0','99999')."_".time();
				 	$config['file_name'] = $new_photo_name2;
			        $this->upload->initialize($config);
					if($this->upload->do_upload('topic'.$j)){
					    $uploadData = $this->upload->data();
						$question_bank = $uploadData['file_name'];
				 	}else{
						$file_err_msg = $this->upload->display_errors('<p>', '</p>');
						$this->response(array(
				            'status' => false,
				            'message' => $file_err_msg,
				        ));
			            exit;
					    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
						$question_bank = '';
					}
		    	}else{
		    		$question_bank = '';
		    	}
		    	if($files['topic'.$j]['name']['notes']){
					$config['upload_path'] = './uploads/topic/notes/';
				 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
			 		$this->load->library('upload');
			        $_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['notes'];
			        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['notes'];
			        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['notes'];
			        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['notes'];
			        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['notes']; 

					$new_photo_name3 = rand('0','99999')."_".time();
				 	$config['file_name'] = $new_photo_name3;
			        $this->upload->initialize($config);
					if($this->upload->do_upload('topic'.$j)){
					    $uploadData = $this->upload->data();
						$notes = $uploadData['file_name'];
				 	}else{
						$file_err_msg = $this->upload->display_errors('<p>', '</p>');
						$this->response(array(
				            'status' => false,
				            'message' => $file_err_msg,
				        ));
			            exit;
					    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
						$notes = '';
					}
		    	}else{
		    		  $notes = '';
		    	}
		    	if($files['topic'.$j]['name']['lession_video']) {
					$config['upload_path'] = './uploads/topic/lession_video/';
				 	$config['allowed_types'] = 'xlsx|csv|xls|mp4|pdf';
			 		$this->load->library('upload');
			        $_FILES['topic'.$j]['name']= $files['topic'.$j]['name']['lession_video'];
			        $_FILES['topic'.$j]['type']= $files['topic'.$j]['type']['lession_video'];
			        $_FILES['topic'.$j]['tmp_name']= $files['topic'.$j]['tmp_name']['lession_video'];
			        $_FILES['topic'.$j]['error']= $files['topic'.$j]['error']['lession_video'];
			        $_FILES['topic'.$j]['size']= $files['topic'.$j]['size']['lession_video'];

					$new_photo_name4 = rand('0','99999')."_".time();
				 	$config['file_name'] = $new_photo_name4;

			        $this->upload->initialize($config);
					if($this->upload->do_upload('topic'.$j)){
					    $uploadData = $this->upload->data();
						$lession_video = $uploadData['file_name'];
				 	}else{
						$file_err_msg = $this->upload->display_errors('<p>', '</p>');
						$this->response(array(
				            'status' => false,
				            'message' => $file_err_msg,
				        ));
			            exit;
					    //echo json_encode(array("status"=>"error","action"=>"add","msg"=>$file_err_msg)); exit;
						$lession_video = '';
					}
		    	}else{
		    		$lession_video = '';
		    	}
			}

			$img =array(
				'preview_video'=>$preview_video,
				'question_bank'=>$question_bank,
				'notes'=>$notes,
				'lession_video'=>$lession_video
			);
		   $topic_arr['topic'][$i]=$postData['topic'.$j];
		   $result = $this->topic_model->updateTopic($topic_arr['topic'][$i],$postData['course_id'],$postData['subject_id'],$img);
		   $j++;
		}
		if (!empty($result)) {
			if($result['status'] == 'exists'){
				 $this->response(array(
		            'status' => false,
		            'message' => 'Topic allready exists!!',
		            'data' => $result
		        ));
	            exit;
			}
			else{
		        $this->response(array(
		            'status' => true,
		            'message' => 'Topic updated successfully!!',
		            'data' => $result
		        ));
	            exit;
			}
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Topic fail to add.'
	        ));
	        exit;
	    }
	} 
	//get topic details
	public function getTopicdetails_post(){
		$postData = $_POST;
		if(empty($postData['course_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please require course id.'
	        ));
	        exit;
		}
		if(empty($postData['subject_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please require course id.'
	        ));
	        exit;
		}
	    $result = $this->topic_model->getTopicdetails($postData);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'topic details.',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	//add student testimonial
	public function addRating_post() { 
		$postData = $_POST;
		if(empty($postData['topic_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please require topic id.'
	        ));
	        exit;
		}
		if(empty($postData['rating'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please require rating.'
	        ));
	        exit;
		}
		if(empty($postData['comment'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please require comment.'
	        ));
	        exit;
		}
		$result = $this->manage_testimonials_model->addTestimonial($postData);
	    if (!empty($result)) {
	    	if($result['status'] == 'exists'){
				 $this->response(array(
		            'status' => false,
		            'message' => 'Testimonials for this topic allready exist!!',
		            'data' => $result
		        ));
	            exit;
			}
			else{
		        $this->response(array(
		            'status' => true,
		            'message' => 'Testimonials add successfully.',
		            'data' => $result
		        ));
	            exit;
            }
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Fail to add testimonials.'
	        ));
	        exit;
	    }
	}

	//update student testimonial
	public function updateRating_post()
	{
		$postData = $_POST;
		if(empty($postData['topic_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please require topic id.'
	        ));
	        exit;
		}
		if(empty($postData['rating'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please require rating.'
	        ));
	        exit;
		}
		if(empty($postData['comment'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please require comment.'
	        ));
	        exit;
		}
		$result = $this->manage_testimonials_model->updateTestimonial($postData);
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Testimonials add successfully.',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Fail to add testimonials.'
	        ));
	        exit;
	    }
	}
	//get testimonial details by id
	public function getRating_post(){
		$postData = $_POST;
		if(empty($postData['testimonial_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please require testimonial id.'
	        ));
	        exit;
		}
		$result = $this->manage_testimonials_model->getTestimonial($postData);
	    if (!empty($result)) {
	    	
		        $this->response(array(
		            'status' => true,
		            'message' => 'Testimonials list',
		            'data' => $result
		        ));
	            exit;
            
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}



	//add one to one session
	public function addindividualSession_post(){
		//echo "<pre>";print_r($_POST);die;
		$postData = $_POST;
		if(empty($postData['course_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select course id.'
	        ));
	        exit;
		}
		if(empty($postData['subject_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select subject id.'
	        ));
	        exit;
		}
		if(empty($postData['topic_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select topic id.'
	        ));
	        exit;
		}
		if(empty($postData['title'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter title.'
	        ));
	        exit;
		}
		if(empty($postData['date'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select date.'
	        ));
	        exit;
		}
		if(empty($postData['start_time'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select start time.'
	        ));
	        exit;
		}
		if(empty($postData['end_time'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select end time.'
	        ));
	        exit;
		}
		if(empty($postData['duration_in_minutes'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please enter duration.'
	        ));
	        exit;
		}
		$result = $this->session_model->addSession($postData);
		if (!empty($result)) {
			if($result['status'] == 'exists'){
				 $this->response(array(
		            'status' => false,
		            'message' => 'Session allready exists!!',
		            'data' => $result
		        ));
	            exit;
			}
			else{
		        $this->session->userdata['session_id']=$result['id'];
		        // echo "<pre>";print_r($_SESSION);die;
		        $this->response(array(
		            'status' => true,
		            'message' => 'Session added successfully!!.',
		            'data' => $result
		        ));
	            exit;
			}
	    } 
	    else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Session fail to add.'
	        ));
	        exit;
	    }
	}

public function paymentHistoryList_get()
    {
    	$this->load->model("my_account_model");
    	$result  = $this->my_account_model->paymentHistoryList();//print_r($result);exit;
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Payment History listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	//view user details----
	public function userDetailsList_post()
    {	
    	$postData = $_POST;
		if(empty($postData['student_id'])){
			$this->response(array(
	            'status' => false,
	            'message' => 'Please select student id.'
	        ));
	        exit;
		}
    	$this->load->model("student_model");
    	$result  = $this->student_model->userDetailsList(trim($postData['student_id']));
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'User Details',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	public function deleteTopic_post(){
		if (empty(trim($this->input->post('topic_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please require topic Id.'
	        ));
	        exit;
        }
        $this->load->model("common_model");
        $result = $this->common_model->update('topic_master_ut',array('topic_id'=>$_POST['topic_id']),array('is_deleted'=>'1'));
		if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'Topic deleted successfully',
	            'data' => $result
	        ));
            exit;
	    } 
	    else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

//-----get all testimonials
	public function allTestimonialStudList_get()
    {
    	$this->load->model("manage_testimonials_model");
    	$result  = $this->manage_testimonials_model->allTestimonialStudList();//print_r($result);exit;
	    if (!empty($result)) {
	        $this->response(array(
	            'status' => true,
	            'message' => 'All testimonials listing',
	            'data' => $result
	        ));
            exit;
	    } else if ($result == '') {
	        $this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
	    }
	}

	public function excelExport($file)
	{
		//echo "<pre>";print_r($file);die;
		//$file = $_POST['file'];
		$file_directory = 'uploads/topic/question_bank/';
		$new_file_name = $file;
		$file_type	= PHPExcel_IOFactory::identify($file_directory . $new_file_name);
	    $objReader	= PHPExcel_IOFactory::createReader($file_type);
	    $objPHPExcel = $objReader->load($file_directory . $new_file_name);
	    // $sheet_data	= $objPHPExcel->getActiveSheet()->toArray();
	    // $sheet	= $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow(); 
		//echo "<pre>";print_r($sheet);die;
        $highestColumn = $sheet->getHighestColumn();
	    $res = array();
		//for($i=2; $i<=$sheet_data;$i++)
		$i=0;
		for ($row = 2; $row <= $highestRow; $row++)
	    {
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	    	// $obj = $objPHPExcel->setActiveSheetIndex(0);
			// $res[] = $obj->getCellByColumnAndRow(0, $i)->getValue();
			//echo "<pre>";print_r($rowData[0]);
			$res[$i]['question_text'] =$rowData[0][0];
			$res[$i]['option1'] = $rowData[0][1];
			$res[$i]['option2'] = $rowData[0][2];
			$res[$i]['option3'] = $rowData[0][3];
			$res[$i]['option4'] = $rowData[0][4];
			$res[$i]['correct_option'] = $rowData[0][5];
			//$this->common_model->insert('question_master_ut',$res);
			$i++;
		}
		//die;
		//echo "<pre>";print_r($rowData);die;
	    return $res;
	}

	public function upcomingSessionList_post(){
		if (empty(trim($this->input->post('student_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please required student id.'
	        ));
	        exit;
		}
		$student_id = $this->input->post('student_id');
		$result = $this->session_model->upcomingSessionList($student_id);
		if(!empty($result)){
			$this->response(array(
				'status' => true,
				'message' => 'upcoming session list',
				'data' => $result
			));
			exit;
		}
		else{
			$this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
		}
	}
	public function pastSessionList_post(){
		if (empty(trim($this->input->post('student_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please required student id.'
	        ));
	        exit;
		}
		$student_id = $this->input->post('student_id');
		$result = $this->session_model->pastSessionList($student_id);
		if(!empty($result)){
			$this->response(array(
				'status' => true,
				'message' => 'upcoming session list',
				'data' => $result
			));
			exit;
		}
		else{
			$this->response(array(
	            'status' => false,
	            'message' => 'No data found.'
	        ));
	        exit;
		}
	}
	public function setAvailability_post(){
		//echo "<pre>";print_r($_POST);die;
		if (empty(trim($this->input->post('course_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please required course id.'
	        ));
	        exit;
		}
		if (empty(trim($this->input->post('subject_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please required subject id.'
	        ));
	        exit;
		}
		if (empty(trim($this->input->post('topic_id')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please required topic id.'
	        ));
	        exit;
		}
		if (empty(trim($this->input->post('cost')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please required cost.'
	        ));
	        exit;
		}
		if (empty(trim($this->input->post('date')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please required date.'
	        ));
	        exit;
		}
		if (empty(trim($this->input->post('from_time')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please required from time.'
	        ));
	        exit;
		}
		if (empty(trim($this->input->post('to_time')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please required to time.'
	        ));
	        exit;
		}
		if (empty(trim($this->input->post('duration')))) {
	        $this->response(array(
	            'status' => false,
	            'message' => 'Please required duration.'
	        ));
	        exit;
		}
        if($_POST['date']){
			$dates = explode(",", $_POST['date']);
		    //echo "<pre>";print_r($_SESSION);die;
			foreach($dates as $date){
				$checkExist = $this->common_model->select('*','admin_availability_ut',array('date'=>$date,'from_time'=>$_POST['from_time'],'to_time'=>$_POST['to_time']));
				if(empty($checkExist)){
					$data=array(
						'user_id'=>$_SESSION['userId'],
						'date'=>$date,
						'from_time'=>$_POST['from_time'],
						'to_time'=>$_POST['to_time'],
						'topic_id'=>$_POST['topic_id'],
						'cost'=>$_POST['cost'],
						'total_duration'=>$_POST['duration'],
						'status'=>'1',
						'is_deleted'=>'0',
						'created_at'=>date('Y-m-d'),
						'created_user_id'=>$_SESSION['userId']
					);
					$this->common_model->insert('admin_availability_ut',$data);
				}
				else{
					$this->response(array(
						'status' => true,
						'message' => $date.' '.'allready exist',
					));
					exit;
				}
			}
			$this->response(array(
				'status' => true,
				'message' => 'Availability set successfully.',
			));
			exit;
		}


	}


	














}


?>