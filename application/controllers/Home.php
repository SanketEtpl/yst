<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('common_model');	
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data = array(
            'pageTitle' => 'YST : Login',
            'isActive' => 'active'         
        );
		$this->load->view('includes/header_home',$data);
        $this->load->view('home',$data);
		$this->load->view('includes/student_footer',$data);
	}


}
