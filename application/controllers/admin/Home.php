<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct() {
		parent::__construct();

		$this->load->model('common_model');	
		 $this->load->helper('url');
	}

	public function index()
	{
		//if(is_user_logged_in()){
		$data = array(
            'pageTitle' => 'YST : Home',
        );
		$this->load->view('includes/header',$data);
		//---get all testimonial listing--
		$alltestim = json_decode(file_get_contents(base_url()."Api/Api/allTestimonialStudList"),true);
		$data['alltestim'] = $alltestim['status'] == 1 ? $alltestim['data'] : '';
    
        $this->load->view('admin/home',$data);
		$this->load->view('includes/footer',$data);
        // } else {
        //     $this->session->sess_destroy();
        //     redirect('login');
        // }
        
	}
	public function testimonial()
	{
		$data = array(
            'pageTitle' => 'YST : Testimonial',
            'isActive' => 'active',         
        );
		$this->load->view('includes/header',$data);
		
		 //---get all testimonial listing--
		$alltestim = json_decode(file_get_contents(base_url()."Api/Api/allTestimonialStudList"),true);
		$data['alltestim'] = $alltestim['status'] == 1 ? $alltestim['data'] : '';
    
        $this->load->view('admin/testimonial',$data);
		$this->load->view('includes/footer',$data);
	}

	

}