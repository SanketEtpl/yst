<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('common_model');	
	}

	public function index()
	{
		$data = array(
            'pageTitle' => 'YST : Notification',
        );
		$this->load->view('includes/header',$data);
        $this->load->view('admin/notification',$data);
		$this->load->view('includes/footer',$data);
	}

	public function view_notification_details()
	{
		$data = array(
            'pageTitle' => 'YST : View notification details',
            'isActive' => 'active',         
        );
		$this->load->view('includes/header',$data);
        $this->load->view('admin/view_notification_details',$data);
		$this->load->view('includes/footer',$data);
	}

	

}