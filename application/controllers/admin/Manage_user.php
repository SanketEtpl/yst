<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_user extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('common_model');	
	}

	public function index()
	{
		$data = array(
            'pageTitle' => 'YST : Manage user',
        );
		$this->load->view('includes/header',$data);

		$student_list = json_decode(file_get_contents(base_url()."Api/Api/studentList"),true);
		$data['student_list'] = $student_list['status'] == 1 ? $student_list['data'] : '';

        $this->load->view('admin/manage_user',$data);
		$this->load->view('includes/footer',$data);
	}
	public function view_course_details(){
		//echo 'hiiiiiii';
		$data = array(
			'pageTitle'=>'YST : View course details',
			'isActive'=>'active'
		);
		$this->load->view('includes/header',$data);

		//print_r($_GET);exit;
		//echo $this->uri->segment(2);exit;
		if(!empty($this->uri->segment(2))){
				$postdata = http_build_query(
            		array("student_id"=>$this->uri->segment(2))
            		);
			}
		$opts = array('http' =>array(
		  'method' => 'POST',
		  'header' => 'Content-type: application/x-www-form-urlencoded',
		  'content' => $postdata));
		$context = stream_context_create($opts);
		$user_details = json_decode(file_get_contents(base_url().'Api/Api/userDetailsList', false, $context),true);
		$data['user_details'] = $user_details['status'] == 1 ? $user_details['data'] : '';

		$this->load->view('admin/view_course_details',$data);
		$this->load->view('includes/footer',$data);
	}

	

}