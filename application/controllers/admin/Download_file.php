<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download_file extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('download');
	}

	public function index()
	{
		

	}
	public function download($file,$type)
	{
		//echo $file;
		$folder = '';
		if('question_bank' == $type)
		{
			$folder = 'question_bank';
		}

		$str =  base_url()."/uploads/topic/".$folder."/8685_1541164772.csv";
		//echo $str;
		$data = file_get_contents(base_url()."/uploads/topic/question_bank/$file"); // Read the file's contents
		$name = $file.'.csv';
		force_download($name,$data);
	}
}