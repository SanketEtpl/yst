<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_content extends CI_Controller {
	function __construct() {
		parent::__construct();

		$this->load->model('common_model');	
	}

	public function index()
	{
		$data = array(
            'pageTitle' => 'YST : Manage content',
        );

		//----about me----------------------------
		$postdata = http_build_query(array("key_type"=>'about_me'));
		$opts = array('http' =>array(
			'method' => 'POST','header' => 'Content-type: application/x-www-form-urlencoded',
			'content' => $postdata));
		$context = stream_context_create($opts);

		$about_me = json_decode(file_get_contents(base_url().'Api/Api/manage_content', false, $context),true);
		//echo "<pre>";print_r($about_me);//exit;

		$data['about_me'] = $about_me['status'] == 1 ? $about_me['data'] : '';
       

       //----contact us ----------------------------
		$postdata = http_build_query(array("key_type"=>'contact_us'));
		$opts = array('http' =>array(
			'method' => 'POST','header' => 'Content-type: application/x-www-form-urlencoded',
			'content' => $postdata));
		$context = stream_context_create($opts);
		$contact_us =json_decode(file_get_contents(base_url().'Api/Api/manage_content', false, $context),true);
		$data['contact_us'] = $contact_us['status'] == 1 ? $contact_us['data'] : '';
       
       //----terms_condition ----------------------------
		$postdata = http_build_query(array("key_type"=>'terms_condition'));
		$opts = array('http' =>array(
			'method' => 'POST','header' => 'Content-type: application/x-www-form-urlencoded',
			'content' => $postdata));
		$context = stream_context_create($opts);
		$terms_condition =json_decode(file_get_contents(base_url().'Api/Api/manage_content', false, $context),true);
		$data['terms_condition'] = $terms_condition['status'] == 1 ? $terms_condition['data'] : '';
       
        //----privacy_policy ----------------------------
		$postdata = http_build_query(array("key_type"=>'privacy_policy'));
		$opts = array('http' =>array(
			'method' => 'POST','header' => 'Content-type: application/x-www-form-urlencoded',
			'content' => $postdata));
		$context = stream_context_create($opts);
		$privacy_policy =json_decode(file_get_contents(base_url().'Api/Api/manage_content', false, $context),true);
		$data['privacy_policy'] = $privacy_policy['status'] == 1 ? $privacy_policy['data'] : '';


		 //----my_approach ----------------------------
		$postdata = http_build_query(array("key_type"=>'my_approach'));
		$opts = array('http' =>array(
			'method' => 'POST','header' => 'Content-type: application/x-www-form-urlencoded',
			'content' => $postdata));
		$context = stream_context_create($opts);
		$my_approach =json_decode(file_get_contents(base_url().'Api/Api/manage_content', false, $context),true);
		$data['my_approach'] = $my_approach['status'] == 1 ? $my_approach['data'] : '';
		
		//-------manage offers--------------
		$offerList = json_decode(file_get_contents(base_url()."Api/Api/offerList"),true);
		$data['offerList'] = $offerList['status'] == 1 ? $offerList['data'] : '';
        
        //-------manage FAQs----------------
        $faqList = json_decode(file_get_contents(base_url()."Api/Api/faqList"),true);
		$data['faqList'] = $faqList['status'] == 1 ? $faqList['data'] : '';

		//-------manage pictures----------------
   		$postdata = http_build_query(array("type"=>'picture'));
		$opts = array('http' =>array(
			'method' => 'POST','header' => 'Content-type: application/x-www-form-urlencoded',
			'content' => $postdata));
		$context = stream_context_create($opts);

		$picture = json_decode(file_get_contents(base_url().'Api/Api/picturesOrClipList', false, $context),true);
		//echo "<pre>";print_r($about_me);exit;

		$data['picture'] = $picture['status'] == 1 ? $picture['data'] : '';

		//-------manage clips----------------
   		$postdata = http_build_query(array("type"=>'clip'));
		$opts = array('http' =>array(
			'method' => 'POST','header' => 'Content-type: application/x-www-form-urlencoded',
			'content' => $postdata));
		$context = stream_context_create($opts);

		$clip = json_decode(file_get_contents(base_url().'Api/Api/picturesOrClipList', false, $context),true);
		//echo "<pre>";print_r($about_me);exit;
		$data['clip'] = $clip['status'] == 1 ? $clip['data'] : '';

		 //-------manage testimonial----------------
        $testimonial = json_decode(file_get_contents(base_url()."Api/Api/testimonialsList"),true);
		$data['testimonial'] = $testimonial['status'] == 1 ? $testimonial['data'] : '';

        
       //print_r($picture);exit;
		$this->load->view('includes/header',$data);
        $this->load->view('admin/manage_content',$data);
		$this->load->view('includes/footer',$data);
	}

	

}