<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_session extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('common_model');	
	}

	public function index()
	{
		$data = array(
            'pageTitle' => 'YST : Manage session',
        );
		$this->load->view('includes/header',$data);

		$session_list = json_decode(file_get_contents(base_url()."Api/Api/sessionList"),true);
		$data['session_list'] = $session_list['status'] == 1 ? $session_list['data'] : '';

        $this->load->view('admin/manage_session',$data);
		$this->load->view('includes/footer',$data);
	}

	public function schedule_new_session()
	{
		$data = array(
            'pageTitle' => 'YST : Schedule new session',
            'isActive' => 'active',         
        );
		$this->load->view('includes/header',$data);

		$course_list = json_decode(file_get_contents(base_url()."Api/Api/courseList"),true);
		$data['course_list'] = $course_list['status'] == 1 ? $course_list['data'] : '';

		//get availability date list
		$availability_date = $this->common_model->select('*','admin_availability_ut');
		$data['availability_date'] = isset($availability_date) ? $availability_date : '';

		//get current month session
		$group_by = '';
		$cond_dr = "";
		$jointype_dr  = array('topic_master_ut'=>"LEFT",'course_master_ut'=>"LEFT",'subject_master_ut'=>"LEFT");
        $join_dr = array('topic_master_ut'=>"topic_master_ut.topic_id=session_ut.topic_id",
			'course_master_ut'=>"course_master_ut.course_id=topic_master_ut.course_id",
                'subject_master_ut'=>"subject_master_ut.subject_id=topic_master_ut.subject_id");
		$session_list = $this->common_model->getMasterQuery("session_id,title,date,conducted_flag,course_name,subject_name,topic_name,CONCAT(date,' ',start_time) as start_datetime,CONCAT(date,' ',end_time) as end_datetime,start_time,end_time",'session_ut', $cond_dr, array(), $join_dr, $jointype_dr,$group_by);
		
		$upcoming_session = array();
		$past_session = array();
		$session_data = array();
		foreach($session_list as $session){
			if($session['start_datetime'] >= date('Y-m-d H:i:s')){
				$upcoming_session[]=$session;
			}
			else if($session['end_datetime'] <= date('Y-m-d H:i:s')){
				$past_session[]=$session;
			}
		}
		//echo "<pre>";print_r($upcoming_session);die;
		$data['upcoming_session'] = isset($upcoming_session) ? $upcoming_session : '';
		$data['past_session'] = isset($past_session) ? $past_session : '';

        $this->load->view('admin/schedule_new_session',$data);
		$this->load->view('includes/footer',$data);
	}
	public function edit_new_session()
	{
		$session_id = base64_decode($_GET['id']);
		$data = array(
            'pageTitle' => 'YST : Edit new session',
            'isActive' => 'active',         
        );
		$this->load->view('includes/header',$data);

		//get session details
		$postdata = http_build_query(
		    array("session_id"=>$session_id)
		);
		$opts = array('http' =>array(
		  'method' => 'POST',
		  'header' => 'Content-type: application/x-www-form-urlencoded',
		  'content' => $postdata));
		$context = stream_context_create($opts);
		// echo "<pre>";print_r($context);die;
		$session_details = json_decode(file_get_contents(base_url().'Api/Api/getSessiondetails', false, $context),true);
		$data['session_details'] = $session_details['status'] == 1 ? $session_details['data'] : '';

		//get course list
		$course_list = json_decode(file_get_contents(base_url()."Api/Api/courseList"),true);
		$data['course_list'] = $course_list['status'] == 1 ? $course_list['data'] : '';

		//get invite list
		$invite_list = json_decode(file_get_contents(base_url()."Api/Api1/inviteList"),true);
		$data['invite_list'] = $invite_list['status'] == 1 ? $invite_list['data'] : '';

		//get availability date list
		$availability_date = $this->common_model->select('*','admin_availability_ut');
		$data['availability_date'] = isset($availability_date) ? $availability_date : '';
		
		//get current month session
		$group_by = '';
		$cond_dr = "";
		$jointype_dr  = array('topic_master_ut'=>"LEFT",'course_master_ut'=>"LEFT",'subject_master_ut'=>"LEFT");
        $join_dr = array('topic_master_ut'=>"topic_master_ut.topic_id=session_ut.topic_id",
			'course_master_ut'=>"course_master_ut.course_id=topic_master_ut.course_id",
                'subject_master_ut'=>"subject_master_ut.subject_id=topic_master_ut.subject_id");
		$session_list = $this->common_model->getMasterQuery("session_id,title,date,conducted_flag,course_name,subject_name,topic_name,CONCAT(date,' ',start_time) as start_datetime,CONCAT(date,' ',end_time) as end_datetime,start_time,end_time",'session_ut', $cond_dr, array(), $join_dr, $jointype_dr,$group_by);
		
		$upcoming_session = array();
		$past_session = array();
		$session_data = array();
		foreach($session_list as $session){
			if($session['start_datetime'] >= date('Y-m-d H:i:s')){
				$upcoming_session[]=$session;
			}
			else if($session['end_datetime'] <= date('Y-m-d H:i:s')){
				$past_session[]=$session;
			}
		}
		$data['upcoming_session'] = isset($upcoming_session) ? $upcoming_session : '';
		$data['past_session'] = isset($past_session) ? $past_session : '';

        $this->load->view('admin/edit_new_session',$data);
		$this->load->view('includes/footer',$data);
	}

	public function test(){
		//echo 'hi';die;
		$this->load->view('admin/test');

	}

	

}