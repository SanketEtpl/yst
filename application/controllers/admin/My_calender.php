<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_calender extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('common_model');	
	}

	public function index()
	{
		$data = array(
            'pageTitle' => 'YST : My calender',
        );
		$this->load->view('includes/header',$data);
		//get course list
		$course_list = $this->common_model->select('*','course_master_ut');
		$data['course_list'] = isset($course_list) ? $course_list : '';

		//get availability date list
		$availability_date = $this->common_model->select('*','admin_availability_ut');
		$data['availability_date'] = isset($availability_date) ? $availability_date : '';
		//	echo "<pre>";print_r($availability_date);die;
		//get current month session
		$group_by = '';
		$cond_dr = "";
		$jointype_dr  = array('topic_master_ut'=>"LEFT",'course_master_ut'=>"LEFT",'subject_master_ut'=>"LEFT");
        $join_dr = array('topic_master_ut'=>"topic_master_ut.topic_id=session_ut.topic_id",
			'course_master_ut'=>"course_master_ut.course_id=topic_master_ut.course_id",
                'subject_master_ut'=>"subject_master_ut.subject_id=topic_master_ut.subject_id");
		$session_list = $this->common_model->getMasterQuery("session_id,title,date,conducted_flag,course_name,subject_name,topic_name,CONCAT(date,' ',start_time) as start_datetime,CONCAT(date,' ',end_time) as end_datetime,start_time,end_time",'session_ut', $cond_dr, array(), $join_dr, $jointype_dr,$group_by);
		
		$upcoming_session = array();
		$past_session = array();
		$session_data = array();
		foreach($session_list as $session){
			if($session['start_datetime'] >= date('Y-m-d H:i:s')){
				$upcoming_session[]=$session;
			}
			else if($session['end_datetime'] <= date('Y-m-d H:i:s')){
				$past_session[]=$session;
			}
			if(date('m',strtotime($session['date'])) == date('m') ){
				$session_data[]=$session;
			}
		}
		$data['upcoming_session'] = isset($upcoming_session) ? $upcoming_session : '';
		$data['past_session'] = isset($past_session) ? $past_session : '';
		$data['session_data'] = isset($session_data) ? $session_data : '';
		//echo "<pre>";print_r($session_data);die;
        $this->load->view('admin/my_calender',$data);
		$this->load->view('includes/footer',$data);
	}

	

}