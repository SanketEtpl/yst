<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_account extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('common_model');	
	}

	public function index()
	{
		$data = array(
            'pageTitle' => 'YST : My account',
        );
		$this->load->view('includes/header',$data);
		$purchase_history = json_decode(file_get_contents(base_url()."Api/Api/paymentHistoryList"),true);
		//print_r($purchase_history);exit;
		$data['purchase_history'] = $purchase_history['status'] == 1 ? $purchase_history['data'] : '';
        
        $this->load->view('admin/my_account',$data);
		$this->load->view('includes/footer',$data);
	}
	public function average_rating()
	{
		$data = array(
            'pageTitle' => 'YST : Average rating',
            'isActive' => 'active',         
        );
		$this->load->view('includes/header',$data);
        $this->load->view('admin/average_rating',$data);
		$this->load->view('includes/footer',$data);
	}

	

}