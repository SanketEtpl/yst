<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_course extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('common_model');	

		//print_r($this->session->userdata());

		if("" == $this->session->userdata())
		{
			redirect('/login');
		}
	}

	public function index()
	{
		$data = array(
            'pageTitle' => 'YST : Manage course',
        );
		$this->load->view('includes/header',$data);

		$course_list = json_decode(file_get_contents(base_url()."Api/Api/courseList"),true);
		$data['course_list'] = $course_list['status'] == 1 ? $course_list['data'] : '';
       
        $this->load->view('admin/manage_course',$data);
		$this->load->view('includes/footer',$data);
	}
	public function add_new_course(){
	 	$data = array(
            'pageTitle' => 'YST : Add course',
            'isActive' => 'active',         
        );
		$this->load->view('includes/header',$data);
		
		/*get all course list*/
		$course_list = json_decode(file_get_contents(base_url()."Api/Api/courseList"),true);
		$data['course_list'] = $course_list['status'] == 1 ? $course_list['data'] : '';
		

        $this->load->view('admin/add_new_course',$data);
		$this->load->view('includes/footer',$data);
	 }
	 public function edit_course(){
	 	//echo $this->uri->segment(2);die;
	 	$data = array(
            'pageTitle' => 'YST : Edit course',
            'isActive' => 'active',  
            'course_id'=>$this->uri->segment(2)       
        );
		$this->load->view('includes/header',$data);

		/*$course_list = json_decode(file_get_contents(base_url()."Api/Api/courseList"),true);
		$data['course_list'] = $course_list['status'] == 1 ? $course_list['data'] : '';*/
		
		/*get course name*/
		$data['course_list']= $this->common_model->select('*','course_master_ut',array('course_id'=>$data['course_id']));
		
		//get subject list   
		$data['subject_list']= $this->common_model->select('*','subject_master_ut',array('course_id'=>$data['course_id']));
        
        //echo "<pre>";print_r($data['course_list']);die;
        $this->load->view('admin/edit_course',$data);
		$this->load->view('includes/footer',$data);
	 }
	
	

}