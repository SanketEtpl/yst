<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('common_model');	
		$this->load->helper('url');
	}

	public function index()
	{
		//if(is_user_logged_in()){
			$data = array('pageTitle' => 'YST : Home');
			$this->load->view('includes/student_header',$data);
	        $this->load->view('student/home',$data);
			$this->load->view('includes/student_footer',$data);
        // } else {
        //     $this->session->sess_destroy();
        //     redirect('login');
        // }
	}
	
}