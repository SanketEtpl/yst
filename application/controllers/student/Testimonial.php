<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonial extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('common_model');	
	}

	public function index()
	{
		$data = array('pageTitle' => 'Testimonials');
		$this->load->view('includes/student_header',$data);
		
		//get course list
	    $data['course_list'] = $this->common_model->select('course_id,course_name','course_master_ut');
	    $data['subject_list'] = $this->common_model->select('subject_id,subject_name','subject_master_ut');
	    $data['topic_list'] = $this->common_model->select('topic_id,topic_name','topic_master_ut');
	    
	    //get testimonials list
	    $cond=array('student_id'=>$this->session->userdata['userId'],'student_testimonial_ut.status'=>'1','student_testimonial_ut.is_deleted'=>'0');

	    $jointype_dr  = array('topic_master_ut'=>'LEFT','course_master_ut'=>"LEFT",'subject_master_ut'=>"LEFT");

        $join_dr = array('topic_master_ut'=>"topic_master_ut.topic_id=student_testimonial_ut.topic_id",
                'course_master_ut'=>"course_master_ut.course_id=topic_master_ut.course_id",
                'subject_master_ut'=>"subject_master_ut.subject_id=topic_master_ut.subject_id");
	    $data['testimonial_list'] = $this->common_model->getMasters('*','student_testimonial_ut',$cond,array(), $join_dr, $jointype_dr);
        
        $this->load->view('student/testimonial',$data);
		$this->load->view('includes/student_footer',$data);
	}	
}
?>