<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('common_model');	
	}

	public function index()
	{
		$data = array('pageTitle' => 'Notification');
		$this->load->view('includes/student_header',$data);
        $this->load->view('student/notification',$data);
		$this->load->view('includes/student_footer',$data);
	}

	public function notification_details()
	{
		$data = array('pageTitle' => 'Notification');
		$this->load->view('includes/student_header',$data);
        $this->load->view('student/notification_details',$data);
		$this->load->view('includes/student_footer',$data);
	}
}
?>