<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Topic extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('common_model');	
	}

	public function index()
	{
		$data = array('pageTitle' => 'Search Topic');
		// $topiclist = json_decode(file_get_contents(base_url()."Api/Api/topic_list"),true);
		// $data['topic_list'] = $topiclist['status'] == 1 ? $topiclist['data'] : '';

		//---get course listing--
		$courseList = json_decode(file_get_contents(base_url()."Api/Api/courseList"),true);
		$data['course_list'] = $courseList['status'] == 1 ? $courseList['data'] : '';
        
		$this->load->view('includes/student_header',$data);
        $this->load->view('student/search_topic',$data);
		$this->load->view('includes/student_footer',$data);
	}

	public function search_topic()
	{
		$data = array('pageTitle' => 'Search Topic');
		$courseList = json_decode(file_get_contents(base_url()."Api/Api/courseList"),true);
		$data['course_list'] = $courseList['status'] == 1 ? $courseList['data'] : '';
		if(!empty($_POST)){

			//-----search by topic ID--------------
			if(!empty($_POST['topic'])){
				$postdata = http_build_query(
            		array("topic_id"=>$_POST['topic'])
            		);

				$opts = array('http' =>array(
					'method' => 'POST','header' => 'Content-type: application/x-www-form-urlencoded',
					'content' => $postdata));
				$context = stream_context_create($opts);

				$course_details = json_decode(file_get_contents(base_url().'Api/Api/search_topic_by_topicId', false, $context),true);
			}
			else if(!empty($_POST['subject'])){
				$postdata = http_build_query(
            		array("subject_id"=>$_POST['subject'])
            		);
				$opts = array('http' =>array(
					'method' => 'POST',
					'header' => 'Content-type: application/x-www-form-urlencoded',
					'content' => $postdata));
				$context = stream_context_create($opts);

				$course_details = json_decode(file_get_contents(base_url().'Api/Api/search_topic_by_subjectId', false, $context),true);
			}
			else if(!empty($_POST['course'])){
				$postdata = http_build_query(
            		array("course_id"=>$_POST['course'])
            		);

				$opts = array('http' =>array(
					'method' => 'POST','header' => 'Content-type: application/x-www-form-urlencoded',
					'content' => $postdata));
				$context = stream_context_create($opts);

				$course_details = json_decode(file_get_contents(base_url().'Api/Api/search_topic_by_courseId', false, $context),true);
			}
			$course_list = array_unique(array_column($course_details['data'], 'course_name'));
			$subject_list = array_unique(array_column($course_details['data'], 'subject_name'));
			$serch = array();
			foreach($course_list as $keyc=>$course){
				$serch[$keyc]['course']=$course;
				foreach($subject_list as $key=>$value){
					$serch[$keyc]['subject'][$key]['subject_name']=$value;
				    foreach($course_details['data'] as $keyt=>$topic)
				    {
				    	if($topic['subject_name']==$serch[$keyc]['subject'][$key]['subject_name']){
				          $serch[$keyc]['course_cost']=$topic['course_cost'];
				          $serch[$keyc]['subject'][$key]['subject_cost']= $topic['subject_cost'];
				          $serch[$keyc]['subject'][$key]['topic'][$keyt]=$topic;
				    	}
				    }
				}
			}
		    $data['course_details'] = $course_details['status'] == 1 ? $serch : '';
		   // echo "<pre>";print_r($data['course_details']);die;
			$this->load->view('includes/student_header',$data);
        	$this->load->view('student/search_topic',$data);
			$this->load->view('includes/student_footer',$data);	
		}

		

	}

	/*public function view_course_details()
	{
		//echo base64_decode($_GET['course_id']);exit;
		$data = array('pageTitle' => 'View Details');

		$postdata = http_build_query(array("course_id"=>base64_decode($_GET['course_id'])));

		$opts = array('http' =>array(
			'method' => 'POST','header' => 'Content-type: application/x-www-form-urlencoded',
			'content' => $postdata));

		$context = stream_context_create($opts);

		$course_details = json_decode(file_get_contents(base_url().'Api/Api/course_details', false, $context),true);
		//print_r($course_details);//exit;

		$data['course_details'] = $course_details['status'] == 1 ? $course_details['data'] : '';
		
													
		$this->load->view('includes/student_header',$data);
        $this->load->view('student/view_course_details',$data);
		$this->load->view('includes/student_footer',$data);	
	}	*/

	public function view_course_details()
	{	$data=array();
		$this->load->view('includes/student_header',$data);
        $this->load->view('student/view_course_details',$data);
		$this->load->view('includes/student_footer',$data);	
	}
	
}