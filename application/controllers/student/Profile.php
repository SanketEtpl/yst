<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('common_model');	
		$this->load->helper("api"); 
	}

	public function index()
	{
		$data = array('pageTitle' => 'My Profile');
		$stud_id = $this->session->userdata()['userId'];
		$postdata = http_build_query(
		    array("student_id"=>$stud_id)
		);
		$opts = array('http' =>array(
		  'method' => 'POST',
		  'header' => 'Content-type: application/x-www-form-urlencoded',
		  'content' => $postdata));
		$context = stream_context_create($opts);
		$profile = json_decode(file_get_contents(base_url().'Api/Api/studentProfile', false, $context),true);
		$data['student_profile'] = $profile['status'] == 1 ? $profile['data'] : '';
		$this->load->view('includes/student_header',$data);
        $this->load->view('student/my_profile',$data);
		$this->load->view('includes/student_footer',$data);
	}
	
	public function edit_profile()
	{
		if("" != $this->session->userdata('userId'))
		{
			$data = array('pageTitle' => 'Edit Profile');
			$student_id = $this->session->userdata('userId');
			$url = base_url()."Api/Api/studentProfile";
			$postData = array('student_id' => $student_id);
			$data['profile'] = callApi($url,$postData);
			$data['country'] = $this->common_model->select('cnt_id,country_name','countries_ut');
			$data['state'] = $this->common_model->select('*','states_ut',array('country_id' => $data['profile']->data[0]->country_id));
			$data['city'] = $this->common_model->select('*','cities_ut',array('state_id' => $data['profile']->data[0]->state_id));
			$data['level'] = $this->common_model->select('level_id,level_name','student_level_ut');
			
			$this->load->view('includes/student_header',$data);
	        $this->load->view('student/edit_profile',$data);
			$this->load->view('includes/student_footer',$data);
		}
		else
		{
			redirect('student');
		}
	}

	public function getState()
	{
		$country_id = $this->input->post('country_id');

		$data = $this->common_model->select('*','states_ut',array('country_id' => $country_id));
		if(count($data) > 0)
		{
			echo json_encode(array('status' => true,'data' => $data));
		}
	}
	public function getCity()
	{
		$state_id = $this->input->post('state_id');

		$data = $this->common_model->select('*','cities_ut',array('state_id' => $state_id));

		
		if(count($data) > 0)
		{
			echo json_encode(array('status' => true,'data' => $data));
		}
	}

	public function update_profile()
	{
		//print_r($this->input->post());die;
		$url = base_url()."Api/Api1/updateStudentProfile";
		$postData = array('student_id' => 1);
		$res = callApi($url,$this->input->post());

		if(1 == $res->status)
		{
			echo json_encode(array('status' => true,'message' =>$res->message));
		}
		else
		{
			echo json_encode(array('status' => false,'message' =>$res->message));	
		}
		
	}

	public function uploadProfileImage()
	{
		if (isset($_FILES['file']['name'])) 
		{
		    if (0 < $_FILES['file']['error']) {
		       echo json_encode(array('status' => false,'message' =>'Error during file upload' . $_FILES['file']['error']));

		    } else {
		    	if($_FILES['file']['size'] > 1000000)
		    	{
		            echo json_encode(array('status' => false,'message' =>'File max size is 1 MB'));
		    		exit();
		    	}

		        if (file_exists('uploads/' . $_FILES['file']['name'])) {
		            echo json_encode(array('status' => false,'message' =>'File already exists : uploads/' . $_FILES['file']['name']));

		        } else 
		        {
		            if(move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/' . $_FILES['file']['name']))
		            {
		            	$data=array(
		            		'student_image'=>$_FILES['file']['name']
		            	);
		            	$where=array('student_id'=>$_POST['student_id']);
		            	$this->common_model->update('student_master_ut',$where,$data);
		            	echo json_encode(array('status' => true,'message' =>'File upload successfully!'));
		            }
		            else
		            {
		            	echo json_encode(array('status' => false,'message' =>'Fail to upload file.'));
		            }
		            
		        }
		    }
		} else {
		    echo json_encode(array('status' => false,'message' =>'Please choose a file..'));
		}
	}

	public function change_password()
	{
		if("" != $this->session->userdata()['userId'])
		{	
			$data = $this->input->post();
			$id = $this->session->userdata()['userId'];
			$oldPass  = $this->common_model->select('password',TB_STUDENT,array('password' => $data['old_password'],'student_id' => $id));
			if(count($oldPass) > 0)
			{
				$result = $this->common_model->update(TB_STUDENT,array('student_id' => $id),array('password' => $data['new_password']));

				if($result)
				{
					echo json_encode(array('status' => true,'message' => 'Password changed successfully'));
				}
				else
				{
					echo json_encode(array('status' => false,'message' => 'Something went wrong'));	
				}
			}
			else
			{
				echo json_encode(array('status' => false,'message' => 'Old password not match'));
			}
	
		}
		else
		{
			echo json_encode(array('status' => false,'message' => 'logout'));
			exit;
		}
	} 

			
}
?>