<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_calender extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('common_model');	
	}

	public function index()
	{
		$data = array('pageTitle' => 'My Calender');
		$this->load->view('includes/student_header',$data);
		//get course list
		$course_list = $this->common_model->select('*','course_master_ut');
		$data['course_list'] = isset($course_list) ? $course_list : '';
		
		//get availability date list
		$availability_date = $this->common_model->select('*','admin_availability_ut');
		$data['availability_date'] = isset($availability_date) ? $availability_date : '';
		//	echo "<pre>";print_r($availability_date);die;
		//get current month session
		$group_by = '';
		$cond_dr = "";
		$jointype_dr  = array('topic_master_ut'=>"LEFT",'course_master_ut'=>"LEFT",'subject_master_ut'=>"LEFT");
        $join_dr = array('topic_master_ut'=>"topic_master_ut.topic_id=session_ut.topic_id",
			'course_master_ut'=>"course_master_ut.course_id=topic_master_ut.course_id",
                'subject_master_ut'=>"subject_master_ut.subject_id=topic_master_ut.subject_id");
		$session_list = $this->common_model->getMasterQuery("session_id,title,date,conducted_flag,course_name,subject_name,topic_name,CONCAT(date,' ',start_time) as start_datetime,CONCAT(date,' ',end_time) as end_datetime,start_time,end_time",'session_ut', $cond_dr, array(), $join_dr, $jointype_dr,$group_by);
		
		$upcoming_session = array();
		$past_session = array();
		$session_data = array();
		foreach($session_list as $session){
			if($session['start_datetime'] >= date('Y-m-d H:i:s')){
				$upcoming_session[]=$session;
			}
			else if($session['end_datetime'] <= date('Y-m-d H:i:s')){
				$past_session[]=$session;
			}
			if(date('m',strtotime($session['date'])) == date('m')){
				$session_data[]=$session;
			}
		}
		$data['upcoming_session'] = isset($upcoming_session) ? $upcoming_session : '';
		$data['past_session'] = isset($past_session) ? $past_session : '';
		$data['session_data'] = isset($session_data) ? $session_data : '';

        $this->load->view('student/my_calender',$data);
		$this->load->view('includes/student_footer',$data);
	}

	public function book_session()
	{
		$data = array('pageTitle' => 'Book Session');
		$this->load->view('includes/student_header',$data);
        $this->load->view('student/book_session',$data);
		$this->load->view('includes/student_footer',$data);
	}

	public function book_session_edit()
	{
		$data = array('pageTitle' => 'Book Session');
		$this->load->view('includes/student_header',$data);
		//get course list
	    $data['course_list'] = $this->common_model->select('course_id,course_name','course_master_ut');
        
        $this->load->view('student/book_session_edit',$data);
		$this->load->view('includes/student_footer',$data);
	}

	public function one_to_one_booking()
	{
		$data = array('pageTitle' => 'Book Session');
		$this->load->view('includes/student_header',$data);
		$session_id = $this->session->userdata['session_id'];

		$postdata = http_build_query(
		    array("session_id"=>$session_id)
		);
		$opts = array('http' =>array(
		  'method' => 'POST',
		  'header' => 'Content-type: application/x-www-form-urlencoded',
		  'content' => $postdata));
		$context = stream_context_create($opts);
		$sessiondetails = json_decode(file_get_contents(base_url().'Api/Api/getSessiondetails', false, $context),true);
		$data['session_details'] = $sessiondetails['status'] == 1 ? $sessiondetails['data'][0] : '';
		//echo "<pre>";print_r($data['session_details']);die;
        $this->load->view('student/one_to_one_booking',$data);
		$this->load->view('includes/student_footer',$data);
	}

	public function group_session_booking()
	{
		$data = array('pageTitle' => 'Book Session');
		$this->load->view('includes/student_header',$data);
        $this->load->view('student/group_booking',$data);
		$this->load->view('includes/student_footer',$data);
	}
	
}