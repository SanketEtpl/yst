<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_account extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('common_model');	
		$this->load->model('topic_model');	
	}

	public function index()
	{
		$data = array('pageTitle' => 'My Account');
		$this->load->view('includes/student_header',$data);

		//get upcomimg session data
		if(!empty($_SESSION['userId'])){
			$postdata = http_build_query(
				array("student_id"=>$_SESSION['userId'])
			);
		}
		$opts = array('http' =>array(
	    	'method' => 'POST',
		    'header' => 'Content-type: application/x-www-form-urlencoded',
			'content' => $postdata));
		$context = stream_context_create($opts);
		$user_details = json_decode(file_get_contents(base_url().'Api/Api/upcomingSessionList', false, $context),true);
		$data['user_details'] = $user_details['status'] == 1 ? $user_details['data'] : '';
		
		//get past session data
		if(!empty($_SESSION['userId'])){
			$postdata = http_build_query(
				array("student_id"=>$_SESSION['userId'])
			);
		}
		$opts = array('http' =>array(
	    	'method' => 'POST',
		    'header' => 'Content-type: application/x-www-form-urlencoded',
			'content' => $postdata));
		$context = stream_context_create($opts);
		$past_session = json_decode(file_get_contents(base_url().'Api/Api/pastSessionList', false, $context),true);
		$i=0;
		if(!empty($past_session['data'])){
			foreach($past_session['data'] as $past) { 
				$rating_data = $this->common_model->select('*','student_testimonial_ut',array('topic_id'=>$past['topic_id']));
				$sum = array_sum(array_column($rating_data,'rate'));
				$rate = $sum / count($rating_data);
				$past_session['data'][$i]['rating'] = $rate;
				$i++;
			}
		}
		$data['past_session'] = $past_session['status'] == 1 ? $past_session['data'] : '';
		
		//get course list
		$topic_list = $this->topic_model->getPurchaseList();
	    $data['course_data'] = isset($topic_list) ? $topic_list : '';
		
        $this->load->view('student/my_account',$data);
		$this->load->view('includes/student_footer',$data);
	}

	public function view_question_answers()
	{
		$data = array('pageTitle' => 'View Details');
		$this->load->view('includes/student_header',$data);
        $this->load->view('student/view_question_answer',$data);
		$this->load->view('includes/student_footer',$data);	
	}

	public function test_overview()
	{
		$data = array('pageTitle' => 'Test Overview');
		$this->load->view('includes/student_header',$data);
        $this->load->view('student/test_overview',$data);
		$this->load->view('includes/student_footer',$data);	
	}	
}