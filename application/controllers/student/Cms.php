<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('common_model');	
	}

	public function about_us()
	{
		$data = array('pageTitle' => 'About Us');
		// $this->load->view('includes/header',$data);
		$this->load->view('includes/header_home',$data);
        $this->load->view('student/about_us',$data);
		$this->load->view('includes/student_footer',$data);
	}

	public function contact_us()
	{
		$data = array('pageTitle' => 'Contact Us');
		// $this->load->view('includes/student_header',$data);
		$this->load->view('includes/header_home',$data);
        $this->load->view('student/contact_us',$data);
		$this->load->view('includes/student_footer',$data);
	}

	public function faq()
	{
		$data = array('pageTitle' => 'FAQ');
		$this->load->view('includes/student_header',$data);
        $this->load->view('student/faq',$data);
		$this->load->view('includes/student_footer',$data);
	}

	public function privacy_policy()
	{
		$data = array('pageTitle' => 'Privacy Policy');
		$this->load->view('includes/student_header',$data);
        $this->load->view('student/privacy_policy',$data);
		$this->load->view('includes/student_footer',$data);
	}

	public function terms_services()
	{
		$data = array('pageTitle' => 'Terms and Services');
		$this->load->view('includes/student_header',$data);
        $this->load->view('student/terms_services',$data);
		$this->load->view('includes/student_footer',$data);
	}	
}
?>