<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('common_model');	
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data = array(
            'pageTitle' => 'YST : SignUp',
        );
        
		//-------get country list----------
        $country_list = json_decode(file_get_contents(base_url()."Api/Api1/countryList"),true);
		$data['country_list'] = $country_list['status'] == 1 ? $country_list['data'] : '';
		$this->load->view('includes/header_home',$data);
        $this->load->view('student/signup',$data);
	}

	
    

	

}
