<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	function __construct() {
		parent::__construct();

		$this->load->model('common_model');	
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data = array(
            'pageTitle' => 'YST : Login',
            'isActive' => 'active'         
        );
		// $this->load->view('includes/header',$data);
		$this->load->view('includes/header_home',$data);
        $this->load->view('student/login',$data);
		$this->load->view('includes/footer',$data);
	}

	public function logout(){
		$type = $this->session->userdata()['type'];
		$this->session->sess_destroy();
		
		if('admin' == $type)
		{
			redirect('admin/login');
		}
		else if('student' == $type)
		{
			redirect('student/login');
		}
		
	}
	
    

	

}
