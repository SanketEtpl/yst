<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class React extends CI_Controller
{
	function __construct() {
		parent::__construct();

		$this->load->model('common_model');	
		$this->load->library('form_validation');
	}
    public function index()
    {
        $this->load->view('react');
    }
}