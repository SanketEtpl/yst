<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url(); ?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>BOOK A SESSION</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- profile details section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="profile_info">
					<div class="section_heading_sec">
						<h4>GROUP LIVE SESSION DETAILS</h4>
						<a href="<?php echo base_url(); ?>book-session-edit" class="back_page"><img src="<?php echo base_url(); ?>assets/img/back_arrow.png" class="img-responsive" alt="back"></a>
					</div>
					<div class="session_live_booking_details_data">
						<div class="row">
							<div class="col-md-6">
								<div class="profile_data_list">
									<div class="profil_data">
										<label>Course</label>
										<div class="data_deatils">
											Calculus
										</div>
									</div>
									<div class="profil_data">
										<label>Subject</label>
										<div class="data_deatils">
											Math
										</div>
									</div>
									<div class="profil_data">
										<label>Topic</label>
										<div class="data_deatils">
											Intigration
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="profile_data_list">
									<div class="profil_data">
										<label>Date</label>
										<div class="data_deatils">
											05-08-2018  10:30 AM
										</div>
									</div>
									<div class="profil_data">
										<label>Cost</label>
										<div class="book_seesion_price_set">
											$150
										</div>
									</div>
									<div class="profil_data">
										<label>Total Duration</label>
										<div class="data_deatils">
											4 hrs
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="add_topic_btn_new">
						<a href="#." class="btn save_cancel_btn" data-toggle="modal" data-target="#payment_gateway_popup">Proceed</a>
						<a href="#." class="btn save_cancel_btn">Cancel</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- profile details section end here-->
<!--popup start here-->

<!-- paymetn getway popup Start Here -->
<div class="modal fade" id="payment_gateway_popup" role="dialog">
	<div class="modal-dialog delet_cours_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url(); ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="moel_details">
					<div class="section_heading_sec">
						<h4>PAYMENT GETWAY</h4>
					</div>
					<div class="ammoun_popup_content">
						<h4>Amount</h4>
						<span class="ammout_rs">
							$150
						</span>
					</div>
					<div class="ammount_popup_btn_new">
						<a href="#." class="btn save_cancel_btn" data-dismiss="modal" data-toggle="modal" data-target="#success_fully_done_popup">Proceed</a>
						<a href="#." class="btn save_cancel_btn" data-dismiss="modal">Cancel</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- paymetn getway popup end Here -->

<!--succesfully changes popup Start Here -->
<div class="modal fade" id="success_fully_done_popup" role="dialog">
	<div class="modal-dialog delet_cours_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url(); ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="moel_details">
					<span class="model_header">
						<img src="<?php echo base_url(); ?>assets/student/img/correct-icon.png" class="img-responsive" alt="forgotpassword"/>
						<h5>Successfully Done</h5>
					</span>
					<div class="model_content">
						<p>The payment was succesful,<br>
						$150 has been deducted from your account.</p>
						<div class="pop_btn_sect_new">
							<button type="button" class="btn popup_coom_btn">OK</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- succesfully changes popup end Here -->
<!--popup end here-->