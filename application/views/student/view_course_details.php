<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url();?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>VIEW DETAILS</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- manage course section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">									
				<div class="section_details">
					<div class="view_details_collapse">
						<div class="fancy-collapse-panel">
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingOne">
										<h4 class="panel-title">
											<div class="cheak_box_here">
												<div class="round">
													<input type="checkbox" id="checkbox" />
													<label for="checkbox"></label>
												</div>
											</div>
											<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
												<div class="coyes_view_detilas_panel">
													<div class="cours_name">COURSE NAME - 
													<?php /*foreach($course_details  as $val) {
															$course= $val['course_name'];
													}echo $course;*/
													?></div>
													<div class="cours_cost">
														COST-$345
													</div>
												</div>
											</a>
										</h4>
									</div>
									<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
										<div class="view_detils_subject">
											<div class="panel-group" id="accordion02" role="tablist" aria-multiselectable="true">
												<div class="panel panel-default">
													<div class="panel-heading" role="tab" id="headingtwo">
														<h4 class="panel-title">
															<div class="cheak_box_here">
																<div class="round">
																	<input type="checkbox" id="checkbox1" />
																	<label for="checkbox1"></label>
																</div>
															</div>
															<a data-toggle="collapse" data-parent="#accordion02" href="#collapsetwo" aria-expanded="true" aria-controls="collapsetwo">
																<div class="coyes_view_detilas_panel">																				
																	<div class="cours_name">BIOLOGY</div>
																	<div class="cours_cost">
																		COST-$345
																	</div>
																</div>
															</a>
														</h4>
													</div>
													<div id="collapsetwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingtwo">
														<div class="panel-body">
															<div class="cours_table">
																<div class="table-responsive">
																	<table class="table cours_table_set">
																		<thead>
																			<tr>
																				<th>
																					<div class="table_checkbox">
																						<div class="round">
																							<input type="checkbox" id="checkbox5" />
																							<label for="checkbox5"></label>
																						</div>
																					</div>
																					Select All
																				</th>
																				<th>Topic</th>
																				<th>Cost</th>
																				<th>Ratings</th>
																				<th>Preview</th>
																			</tr>
																		</thead>
																		<tbody>
																			<tr>
																				<td>
																					<div class="table_checkbox">
																						<div class="round">
																							<input type="checkbox" id="checkbox6" />
																							<label for="checkbox6"></label>
																						</div>
																					</div>
																					10
																				</td>
																				<td>Botany</td>
																				<td>$45</td>
																				<td>
																					<div class="star_ratting_table_views">
																						<ul>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																						</ul>
																					</div>
																				</td>
																				<td>
																					<div class="delet_edit_table_link_new">
																						<a href="#." data-toggle="modal" data-target="#view_course_video_set_popup">
																							<i class="fa fa-video-camera" aria-hidden="true"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<div class="table_checkbox">
																						<div class="round">
																							<input type="checkbox" id="checkbox7" />
																							<label for="checkbox7"></label>
																						</div>
																					</div>
																					11
																				</td>
																				<td>Living Cells</td>
																				<td>$30</td>
																				<td>
																					<div class="star_ratting_table_views">
																						<ul>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																						</ul>
																					</div>
																				</td>
																				<td>
																					<div class="delet_edit_table_link_new">
																						<a href="#." data-toggle="modal" data-target="#view_course_video_set_popup">
																							<i class="fa fa-video-camera" aria-hidden="true"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<div class="table_checkbox">
																						<div class="round">
																							<input type="checkbox" id="checkbox8" />
																							<label for="checkbox8"></label>
																						</div>
																					</div>
																					12
																				</td>
																				<td>Anatomy</td>
																				<td>$25</td>
																				<td>
																					<div class="star_ratting_table_views">
																						<ul>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																						</ul>
																					</div>
																				</td>
																				<td>
																					<div class="delet_edit_table_link_new">
																						<a href="#." data-toggle="modal" data-target="#view_course_video_set_popup">
																							<i class="fa fa-video-camera" aria-hidden="true"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="panel panel-default">
													<div class="panel-heading" role="tab" id="headingthree">
														<h4 class="panel-title">
															<div class="cheak_box_here">
																<div class="round">
																	<input type="checkbox" id="checkbox2" />
																	<label for="checkbox2"></label>
																</div>
															</div>
															<a data-toggle="collapse" data-parent="#accordion02" href="#collapsethree" aria-expanded="true" aria-controls="headingthree">
																<div class="coyes_view_detilas_panel">
																	<div class="cours_name">CHEMISTRY</div>
																	<div class="cours_cost">
																		COST-$345
																	</div>
																</div>
															</a>
														</h4>
													</div>
													<div id="collapsethree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingthree">
														<div class="panel-body">
															<div class="cours_table">
																<div class="table-responsive">
																	<table class="table cours_table_set">
																		<thead>
																			<tr>
																				<th>
																					<div class="table_checkbox">
																						<div class="round">
																							<input type="checkbox" id="checkbox9" />
																							<label for="checkbox9"></label>
																						</div>
																					</div>
																					Select All
																				</th>
																				<th>Topic</th>
																				<th>Cost</th>
																				<th>Ratings</th>
																				<th>Preview</th>
																			</tr>
																		</thead>
																		<tbody>
																			<tr>
																				<td>
																					<div class="table_checkbox">
																						<div class="round">
																							<input type="checkbox" id="checkbox10" />
																							<label for="checkbox10"></label>
																						</div>
																					</div>
																					10
																				</td>
																				<td>Botany</td>
																				<td>$45</td>
																				<td>
																					<div class="star_ratting_table_views">
																						<ul>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																						</ul>
																					</div>
																				</td>
																				<td>
																					<div class="delet_edit_table_link_new">
																						<a href="#." data-toggle="modal" data-target="#view_course_video_set_popup">
																							<i class="fa fa-video-camera" aria-hidden="true"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<div class="table_checkbox">
																						<div class="round">
																							<input type="checkbox" id="checkbox11" />
																							<label for="checkbox11"></label>
																						</div>
																					</div>
																					11
																				</td>
																				<td>Living Cells</td>
																				<td>$30</td>
																				<td>
																					<div class="star_ratting_table_views">
																						<ul>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																						</ul>
																					</div>
																				</td>
																				<td>
																					<div class="delet_edit_table_link_new">
																						<a href="#." data-toggle="modal" data-target="#view_course_video_set_popup">
																							<i class="fa fa-video-camera" aria-hidden="true"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<div class="table_checkbox">
																						<div class="round">
																							<input type="checkbox" id="checkbox12" />
																							<label for="checkbox12"></label>
																						</div>
																					</div>
																					12
																				</td>
																				<td>Anatomy</td>
																				<td>$25</td>
																				<td>
																					<div class="star_ratting_table_views">
																						<ul>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																						</ul>
																					</div>
																				</td>
																				<td>
																					<div class="delet_edit_table_link_new">
																						<a href="#." data-toggle="modal" data-target="#view_course_video_set_popup">
																							<i class="fa fa-video-camera" aria-hidden="true"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</div> 
														</div>
													</div>
												</div>
												<div class="panel panel-default">
													<div class="panel-heading" role="tab" id="headingfour">
														<h4 class="panel-title">
															<div class="cheak_box_here">
																<div class="round">
																	<input type="checkbox" id="checkbox3" />
																	<label for="checkbox3"></label>
																</div>
															</div>
															<a data-toggle="collapse" data-parent="#accordion02" href="#collapsefour" aria-expanded="true" aria-controls="collapsetwo">
																<div class="coyes_view_detilas_panel">																				
																	<div class="cours_name">PHYSICS</div>
																	<div class="cours_cost">
																		COST-$345
																	</div>
																</div>
															</a>
														</h4>
													</div>
													<div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
														<div class="panel-body">
															<div class="cours_table">
																<div class="table-responsive">
																	<table class="table cours_table_set">
																		<thead>
																			<tr>
																				<th>
																					<div class="table_checkbox">
																						<div class="round">
																							<input type="checkbox" id="checkbox13" />
																							<label for="checkbox13"></label>
																						</div>
																					</div>
																					Select All
																				</th>
																				<th>Topic</th>
																				<th>Cost</th>
																				<th>Ratings</th>
																				<th>Preview</th>
																			</tr>
																		</thead>
																		<tbody>
																			<tr>
																				<td>
																					<div class="table_checkbox">
																						<div class="round">
																							<input type="checkbox" id="checkbox14" />
																							<label for="checkbox14"></label>
																						</div>
																					</div>
																					10
																				</td>
																				<td>Botany</td>
																				<td>$45</td>
																				<td>
																					<div class="star_ratting_table_views">
																						<ul>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																						</ul>
																					</div>
																				</td>
																				<td>
																					<div class="delet_edit_table_link_new">
																						<a href="#." data-toggle="modal" data-target="#view_course_video_set_popup">
																							<i class="fa fa-video-camera" aria-hidden="true"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<div class="table_checkbox">
																						<div class="round">
																							<input type="checkbox" id="checkbox15" />
																							<label for="checkbox15"></label>
																						</div>
																					</div>
																					11
																				</td>
																				<td>Living Cells</td>
																				<td>$30</td>
																				<td>
																					<div class="star_ratting_table_views">
																						<ul>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																						</ul>
																					</div>
																				</td>
																				<td>
																					<div class="delet_edit_table_link_new">
																						<a href="#." data-toggle="modal" data-target="#view_course_video_set_popup">
																							<i class="fa fa-video-camera" aria-hidden="true"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<div class="table_checkbox">
																						<div class="round">
																							<input type="checkbox" id="checkbox16" />
																							<label for="checkbox16"></label>
																						</div>
																					</div>
																					12
																				</td>
																				<td>Anatomy</td>
																				<td>$25</td>
																				<td>
																					<div class="star_ratting_table_views">
																						<ul>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																							<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																						</ul>
																					</div>
																				</td>
																				<td>
																					<div class="delet_edit_table_link_new">
																						<a href="#." data-toggle="modal" data-target="#view_course_video_set_popup">
																							<i class="fa fa-video-camera" aria-hidden="true"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- manage course section end here-->
<!-- offer section start here-->
<section class="offer_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="offer_details_section">
					<div class="offier_blog_here">
						<div class="row">
							<div class="col-md-3">
								<div class="offer_blog">
									<div class="offer_name_detils">
										<h4>OFFER 1</h4>
										<p>Purchese 3 topic - $300 and get a topic free</p>
									</div>
									<div class="offer_price">
										$300
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="offer_blog">
									<div class="offer_name_detils">
										<h4>OFFER 2</h4>
										<p>Purchese a Complete COURSE - $400</p>
									</div>
									<div class="offer_price">
										$300
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="offer_blog">
									<div class="offer_name_detils">
										<h4>OFFER 3</h4>
										<p>Purchese 2 Topics - $200</p>
									</div>
									<div class="offer_price">
										$200
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="offer_blog">
									<div class="offer_name_detils">
										<h4>OFFER 4</h4>
										<p>Purchese any 3 topic - $250</p>
									</div>
									<div class="offer_price">
										$250
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="offer_total_cost">
						<div class="row">
							<div class="col-md-3">
								<div class="form_set">
									<label>Total Course Cost</label>
									<input type="text" class="form-control" placeholder="Search" id="course_cost"/>
								</div>
							</div>
						</div>
						<div class="schedul_btn_group">
							<a href="#." class="btn save_cancel_btn" data-toggle="modal" data-target="#payment_gateway_popup">Purchese</a>
							<a href="#." class="btn save_cancel_btn">Cancel</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- offer section end here-->

<!-- popup start Here -->
		
		<!--view course details video popup start here-->
		<div class="modal fade" id="view_course_video_set_popup" role="dialog">
			<div class="modal-dialog ">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<img src="<?php echo base_url() ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
						</button>
					</div>
					<div class="modal-body">
						<div class="moel_details_new">
							<div class="section_heading_sec">
								<h4>LASSION VIDEO</h4>
							</div>
							<div class="model_content">
								<div class="video_set_pop_main">
									<video id="video" poster="img/video_pop_img.jpg" controls="">
										<source src="<?php echo base_url() ?>assets/student/video/how_it_work.mp4" type="video/mp4">
										</video>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--view course details video popup end here-->
			
			<!-- paymetn getway popup Start Here -->
			<div class="modal fade" id="payment_gateway_popup" role="dialog">
				<div class="modal-dialog delet_cours_popup_width">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<img src="<?php echo base_url() ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
							</button>
						</div>
						<div class="modal-body">
							<div class="moel_details">
								<div class="section_heading_sec">
									<h4>PAYMENT GETWAY</h4>
								</div>
								<div class="ammoun_popup_content">
									<h4>Amount</h4>
									<span class="ammout_rs">
										$450
									</span>
								</div>
								<div class="ammount_popup_btn_new">
									<a href="#." class="btn save_cancel_btn" data-dismiss="modal" data-toggle="modal" data-target="#success_fully_done_popup">Proceed</a>
									<a href="#." class="btn save_cancel_btn" data-dismiss="modal">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- paymetn getway popup end Here -->
			
			<!--succesfully changes popup Start Here -->
			<div class="modal fade" id="success_fully_done_popup" role="dialog">
				<div class="modal-dialog delet_cours_popup_width">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<img src="<?php echo base_url() ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
							</button>
						</div>
						<div class="modal-body">
							<div class="moel_details">
								<span class="model_header">
									<img src="<?php echo base_url() ?>assets/student/img/correct-icon.png" class="img-responsive" alt="forgotpassword"/>
									<h5>Successfully Done</h5>
								</span>
								<div class="model_content">
									<p>The payment was succesful,<br>
									$450 has been deducted from your account.</p>
									<div class="pop_btn_sect_new">
										<button type="button" class="btn popup_coom_btn">OK</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- succesfully changes popup end Here -->
			
			<!-- popup end Here -->
		</div>