<!-- banner section start here-->
<?php 
    $userProfile = $student_profile[0];
 ?>
<section class="banner_section_new" style="background-image:url('<?php echo base_url(); ?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>MY PROFILE</h4>
				</div>
				<div class="my_profile_user">
					<div class="profile_picture">
						<span><img src="<?php echo base_url(); ?>uploads/<?php echo $userProfile['student_image']?>" class="img-responsive" alt="profile"/></span>
						<h3><?php echo $userProfile['first_name'].' '.$userProfile['last_name']?></h3>
						<!-- <a href="#." class="profile_edit"><img src="<?php echo base_url(); ?>assets/student/img/edit-profile.png" class="img-responsive" alt="edit-profile"/></a> -->
					</div>
					<div class="my_pr_buttons">
						<button type="button" class="btn common_btn_sec margin-right-10px" data-toggle="modal" data-target="#change_password_popup">Change Password</button>
						<a href="<?php echo base_url(); ?>edit-profile" class="btn common_btn_sec">Edit Profile</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->

<!-- profile details section start here-->
<section class="profile_details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="profile_info">
					<div class="row">
						<div class="col-md-6">
							<div class="profile_data_list">
								<div class="profil_data">
									<label>FIRST NAME</label>
									<div class="data_deatils">
										<?php echo $userProfile['first_name'];?>
									</div>
								</div>
								<div class="profil_data">
									<label>LAST NAME</label>
									<div class="data_deatils">
										<?php echo $userProfile['last_name'];?>
									</div>
								</div>
								<div class="profil_data">
									<label>DATE OF BIRTH</label>
									<div class="data_deatils">
										<?php echo $userProfile['dob'];?>
									</div>
								</div>
								<div class="profil_data">
									<label>GENDER</label>
									<div class="data_deatils">
										<?php echo $userProfile['gender'];?>
									</div>
								</div>
								<div class="profil_data">
									<label>CONTACT NUMBER</label>
									<div class="data_deatils">
										<?php echo $userProfile['contact_no'];?>
									</div>
								</div>
								<div class="profil_data">
									<label>SCHOOL NAME</label>
									<div class="data_deatils">
										<?php echo $userProfile['school_name'];?>
									</div>
								</div>
								<div class="profil_data">
									<label>LEVEL</label>
									<div class="data_deatils">
										<?php echo $userProfile['level'];?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="profile_data_list">
								<div class="profil_data">
									<label>EMAIL</label>
									<div class="data_deatils">
										<?php echo $userProfile['email_id'];?>
									</div>
								</div>
								<!-- <div class="profil_data">
									<label>PASSWORD</label>
									<div class="data_deatils">
										<?php echo $userProfile['password'];?>
									</div>
								</div> -->
								<div class="profil_data">
									<label>ADDRESS</label>
									<div class="data_deatils">
										<?php echo $userProfile['address'];?>
									</div>
								</div>
								<div class="profil_data">
									<label>COUNTRY</label>
									<div class="data_deatils">
										<?php echo $userProfile['country'];?>
									</div>
								</div>
								<div class="profil_data">
									<label>STATE</label>
									<div class="data_deatils">
										<?php echo $userProfile['state_name'];?>
									</div>
								</div>
								<div class="profil_data">
									<label>CITY</label>
									<div class="data_deatils">
										<?php echo $userProfile['city'];?>
									</div>
								</div>
								<div class="profil_data">
									<label>ZIP CODE</label>
									<div class="data_deatils">
										<?php echo $userProfile['zip_code'];?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- profile details section end here-->

<!--popup start here-->

<!-- change password popup Start Here -->
<div class="modal fade" id="change_password_popup" role="dialog">
	<div class="modal-dialog change_password_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url(); ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="model_details">
					<div class="section_heading_sec">
						<h4>CHANGE PASSWORD</h4>
					</div>
					<div class="model_content">
						<div class="add_topic_form">
							<div class="row">
								<form id="change_password">
									<div class="col-md-12">
										<div class="form_set">
											<label>Old Password</label>
											<input type="password" class="form-control" name="old_password" id="old_password"/>
											<span class="err_opass" style="color: red; display: none;">Please enter old password.</span>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form_set">
											<label>New Password</label>
											<input type="password" class="form-control" name="new_password" id="new_password"/>
											<span class="err_npass" style="color: red; display: none;">Please enter new password.</span>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form_set">
											<label>Re-type Password</label>
											<input type="password" class="form-control" name="retype_password" id="retype_password"/>
											<span class="err_rpass" style="color: red; display: none;">Please enter re-type password.</span>
										</div>
									</div>
									<div class="col-md-12">
										<div class="main_profil_btn">
											<!-- <a href="#." class="btn save_cancel_btn margin-right-10px" data-dismiss="modal" data-toggle="modal" data-target="#success_changes_popup">Update</a> -->
											<button type="button" class="btn save_cancel_btn margin-right-10px" id="changePassword">Update</button>
											<a href="javascript:void(0)" class="btn save_cancel_btn" data-dismiss="modal">Cancel</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- change password popup end Here -->

<!--succesfully changes popup Start Here -->
<div class="modal fade" id="success_changes_popup" role="dialog">
	<div class="modal-dialog delet_cours_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url(); ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="moel_details">
					<span class="model_header">
						<img src="<?php echo base_url(); ?>assets/student/img/correct-icon.png" class="img-responsive" alt="forgotpassword"/>
						<h5>Successfully Changed</h5>
					</span>
					<div class="model_content">
						<p>Password has been succesfully changed.</p>
						<div class="pop_btn_sect_new">
							<button type="button" class="btn popup_coom_btn">OK</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- succesfully changes popup end Here -->
<!--popup end here-->

<script src="<?php echo base_url();?>assets/js/student_profile.js"></script>