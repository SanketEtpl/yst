<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $pageTitle; ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<!--css link start here-->
	<link rel="icon" type="imge/png" sizes="16x16" href="<?php echo base_url();?>assets/img/favicon.png"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/student_style.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css"/>		
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/media.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/stylesheet.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.scrollbar.css"/>
	<!--css link end here-->
	 <script type="text/javascript">
	    var base_url = '<?php echo base_url();?>';//alert(base_url);
	 </script>
</head>
<body>
	<!-- Page wrapper Start here-->
	<div class="wrapper">
		<!-- Login Page Start Here -->
		<section class="loginpage">
			<div class="inner-sect">

				<div class="bgshow">
					<div class="container">
						<div class="col-md-6">
					       <div class="alert alert-success" style="display:none">
					        <strong id="msg" ></strong> 
					       </div>
					    </div>
						<div class="lftlog_sec">
							<form class="logform" id="signup_form">
								<div class="commsect">
									<!-- Row Start Here -->
									<div class="row nomarg">
										<h1>Sign Up</h1>
									</div>
									<!-- Row end here -->
									<!-- Row Start Here -->
									<div class="sign_up_page_form">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<input id="first_name" type="text" name="first_name" class="form-control" placeholder="First Name"/>
												    <span class="err_fname" style="color: red; display: none;">Please enter first name.</span>
												</div> 
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input id="last_name" type="text" name="last_name" class="form-control" placeholder="Last Name"/>
												    <span class="err_lname" style="color: red; display: none;">Please enter last name.</span>
												</div> 
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input id="contact_no" type="number" name="contact_no" class="form-control" placeholder="Contact Number"/>
												    <span class="err_contact" style="color: red; display: none;">Please enter contact no.</span>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input id="email_id" type="text" name="email_id" class="form-control" placeholder="Email ID"/>
												    <span class="err_email" style="color: red; display: none;">Please enter valid email id.</span>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input id="password" type="password" name="password" class="form-control" placeholder="e.g: Password@123"/>
												    <span class="err_pass" style="color: red; display: none;">Please enter password.</span>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input id="confirm_password" type="password" name="confirm_password" class="form-control" placeholder="Confirm Password"/>
												    <span class="err_cpass" style="color: red; display: none;">Please enter confirm password.</span>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<input id="address" type="text" name="address" class="form-control" placeholder="Address"/>
												    <span class="err_add" style="color: red; display: none;">Please enter address.</span>
												</div>
											</div>
											<div class="col-md-12 sign_up_select_field">
												<div class="row">
													<div class="col-md-3">
														<div class="form_set_sec">
															<select class="form-control select_country" onchange="stateList();" id="select_country" name="country_id">
																<option value="select">Select Country</option>
																<?php if($country_list) { 
																	foreach($country_list as $country) {
																?>
																<option value="<?php echo $country['cnt_id']?>"><?php echo $country['country_name']?></option>
																<?php } } ?>
															</select>
												            <span class="err_country" style="color: red; display: none;">Please select country.</span>
														</div>
													</div>
													<div class="col-md-3">
														<div class="form_set_sec">
															<select class="form-control select_state" onchange="cityList()" id="select_state" name="state_id">
																<option value="select">Select State</option>
															</select>
												            <span class="err_state" style="color: red; display: none;">Please select state.</span>
														</div>
													</div>
													<div class="col-md-3">
														<div class="form_set_sec">
															<select class="form-control select_city" name="city_id" id="select_city">
																<option value="select">Select City</option>
															</select>
												            <span class="err_city" style="color: red; display: none;">Please select city.</span>
														</div>
													</div>
													<div class="col-md-3">
														<div class="form_set_sec">
															<input id="zip_code" type="text" name="zip_code" class="form-control" placeholder="Zip Code"/>
												            <span class="err_zip" style="color: red; display: none;">Please enter zip code.</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- Row end here -->
									<div class="row nomarg">
										<div class="check_inner check_inner_new">
											<input class="filled-in" id="check1" name="check1" type="checkbox" value="off">
											<label for="check1">I agree to the Terms & Conditions.</label>
										</div>
											<span class="err_term" style="color: red; display: none;">Please check this field.</span>
									</div>
									<!-- Row Start Here -->
									<div class="row nomarg">
										<div class="sign_up_btns_set">
											<!-- <a onclick="signUp();" class="btn comm-btn" data-toggle="modal" data-target="#success_register_popup">Sign Up</a> -->
											<a onclick="signUp();" class="btn comm-btn">Sign Up</a>
										</div>
									</div>
							</form>
									<div class="row nomarg">
										<div class="hav_acc_link">Already Have an account?<a href="<?php echo base_url()?>student">Sign In</a></div>
									</div>
									<!-- Row end here -->
								</div>
						</div>
						<div class="website_right_details_sec">
							<span class="web_logo_log_sign">
								<a href="homepage.html"><img src="<?php echo base_url();?>assets/img/loginlogo.png" class="img-responsive" alt="logo"/></a>
							</span>
							<div class="web_details">
								<h4>WELCOME <br> YOUR SCIENCE TUTOR</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br><br>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
							</div>
						</div>
					</div>
				</div> 
			</div> 
		</section> 
		<!-- Login Page end Here -->
		<!-- footer section start Here -->
		<footer class="footer-section" style="background-image:url(./assets/student/img/footer-img.jpg);">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="footer_details">
							<div class="row">
								<div class="col-md-4">
									<div class="foot_blogs">
										<span class="foot-logo">
											<a href="index.html">
												<img src="<?php echo base_url();?>assets/img/loginlogo.png" class="img-responsive" alt="logo"/>
											</a>
										</span>
										<div class="foot_web_info">
											<p>
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br><br> when an unknown printer took a galley of type and scrambled it to make a type specimen book.
											</p>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="foot_blogs">
										<div class="fot_blo_head">
											<h4>Contact</h4>
										</div>
										<div class="foot_web_info_sec">
											<div class="cont_us_link">
												<span>Email : </span>
												<a href="mailto:info@yoursciencetutor.com">info@yoursciencetutor.com</a>
											</div>
											<p>
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br><br> when an unknown printer took a galley of type and scrambled it to make a type specimen book.
											</p>
										</div>
									</div>
								</div>
								<div class="col-md-3 pull-right">
									<div class="foot_blogs">
										<div class="fot_blo_head">
											<h4>Useful Link</h4>
										</div>
										<div class="foot_web_info_sec">
											<ul>
												<li><a href="index.html">Home</a></li>
												<li><a href="about_us.html">About Us</a></li>
												<li><a href="contact_us.html">Contact Us</a></li>
												<li><a href="faq.html">FAQs</a></li>
												<li><a href="privacy_policy.html">Privacy Policy</a></li>
												<li><a href="terms_and_service.html">Terms and Services</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="soacial-footer-links">
							<ul>
								<li><a href="#." class="tooltip"><span class="tooltiptext">Facebook</span><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#." class="tooltip"><span class="tooltiptext">Google-plus</span><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
								<li><a href="#." class="tooltip"><span class="tooltiptext">Twitter</span><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<!--<li><a href="#." class="tooltip"><span class="tooltiptext">Snapchat</span><i class="fa fa-snapchat" aria-hidden="true"></i></a></li>
								<li><a href="#." class="tooltip"><span class="tooltiptext">Instagram</span><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="foot_not_info">
				<p>Copyright &copy; 2018 Your Science Tutor. All rights reserved.</p>
			</div>
		</footer>
		<!-- footer section end Here -->

		<!-- popup Start Here -->

		<!-- success Register popup Start Here -->
		<div class="modal fade" id="success_register_popup" role="dialog">
			<div class="modal-dialog model_forgot_pass_popup">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<img src="<?php echo base_url();?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
						</button>
					</div>
					<div class="modal-body">
						<div class="moel_details">
							<span class="model_header">
								<img src="<?php echo base_url();?>assets/student/img/correct-icon.png" class="img-responsive" alt="forgotpassword"/>
								<h5>Successfully registered</h5>
							</span>
							<div class="model_content">
								<p>Welcome to Your Science Tutor.<br>
									You are successfully registered in the system<br>
								Do you want to proceed? </p>
								<div class="pop_btn_sect">
									<a href="Signin.html" class="btn popup_coom_btn">OK</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- success Register popup end Here -->

		<!-- popup end Here -->

	</div>
	<!--js link start here-->
	<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/student_custom.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.scrollbar.min.js"></script>
	<script src="<?php echo base_url();?>assets/student/js/signup.js"></script>
	<!--js link end here-->
</body>
</html>
