<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url();?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>TESTIMONIALS</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- schedule new session section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
		       <div class="alert alert-success" style="display:none">
		        <strong id="msg" ></strong> 
		       </div>
		    </div> 
			<div class="col-md-12">
				<div class="section_details">
					<form id="add_rating">
					<div class="testimonial_form">
						<div class="row">
							<div class="col-md-4">
								<div class="form_set">
									<label>Course</label>
									<select class="form-control select_course" onchange="getSubjectlist();">
										<option select>Select Course</option>
										<?php if(!empty($course_list)) { 
											foreach($course_list as $course) { 
										?>
										 <option value="<?php echo $course['course_id']?>"><?php echo $course['course_name']?></option>
									    <?php } } ?>
									</select>
									<span class="err_course" style="color: red; display: none;">Please select course.</span>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form_set">
									<label>Subject</label>
									<select class="form-control select_subject" onchange="getTopiclist();">
										<option select>Select Subject</option>
									</select>
									<span class="err_subject" style="color: red; display: none;">Please select subject.</span>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form_set">
									<label>Topic</label>
									<select class="form-control select_topic" name="topic_id">
										<option select>Select Topic</option>
									</select>
									<span class="err_topic" style="color: red; display: none;">Please select topic.</span>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form_set">
									<label>Rating</label>
									<div class="star_ratting_testimonial_views">
										<ul>
											<li class="rating_star" value="1"><a href="#."><i class="star1 fa fa-star-o" aria-hidden="true"></i></a></li>
											<li class="rating_star" value="2"><a href="#."><i class="star2 fa fa-star-o" aria-hidden="true"></i></a></li>
											<li class="rating_star" value="3"><a href="#."><i class="star3 fa fa-star-o" aria-hidden="true"></i></a></li>
											<li class="rating_star" value="4"><a href="#."><i class="star4 fa fa-star-o" aria-hidden="true"></i></a></li>
											<li class="rating_star" value="5"><a href="#."><i class="star5 fa fa-star-o" aria-hidden="true"></i></a></li>
										</ul>
									</div>
									<input type="hidden" name="rating" value="" id="rating_star">
									<input type="hidden" name="student_id" value="<?php echo $this->session->userdata['userId']?>" >
								</div>
									<span class="err_rate" style="color: red; display: none;">Please select rate.</span>
							</div>
							<div class="col-md-8">
								<div class="form_set">
									<label>Add Comments</label>
									<textarea class="form-control comment_textarea" id="text_comment" name="comment" placeholder="Add comment"></textarea>
								</div>
								<span class="err_comment" style="color: red; display: none;">Please enter comment.</span>
							</div>
						</div>
						<div class="add_topic_btn_new">
							<!-- <a href="#." class="btn save_cancel_btn" data-toggle="modal" data-target="#saved_testimonil_popup">Save</a> -->
							<a href="#." class="btn save_cancel_btn" onclick="addRating();">Save</a>
							<a href="#." class="btn save_cancel_btn">Cancel</a>
						</div>
					</div>
				</form>
					<div class="my_testimonial_table">
						<div class="section_heading_sec">
							<h4>MY TESTIMONIALS</h4>
						</div>
						<div class="cours_table">
							<div class="table-responsive">
								<table class="table cours_table_set " id="rating-table">
									<thead>
										<tr>
											<th>SN</th>
											<th>Date</th>
											<th>Course</th>
											<th>Subject</th>
											<th>Topic</th>
											<th>Rating</th>
											<th>Comments</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$i=1;
										  if(!empty($testimonial_list)) { 
										  	foreach($testimonial_list as $testimonial) { 
										?>
										<tr>
											<td><?php echo $i;?></td>
											<td><?php echo date('Y-m-d',strtotime($testimonial['created_at']));?></td>
											<td><?php echo $testimonial['course_name'];?></td>
											<td><?php echo $testimonial['subject_name'];?></td>
											<!-- <td class="topic_table_width_new"><?php echo $testimonial['topic_name'];?></td> -->
											<td><?php echo $testimonial['topic_name'];?></td>
											<td><?php echo $testimonial['rate'];?>/5</td>
											<td class="topic_table_width_new"><?php echo $testimonial['comment'];?></td>
											<td>
												<div class="delet_edit_table_link">
													<a href="#." onclick="getTestiminial('<?php echo $testimonial['testimonial_id']?>');"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
												</div>
											</td>
										</tr>
									    <?php $i++ ; } } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="testimonail_blg_set">
						<div class="section_heading_sec">
							<h4>OTHER TESTIMONIALS</h4>
						</div>
						<div class="testmonial_profiles_blogs_set">
							<div class="row">
								<div class="col-md-4">
									<div class="testominail_blogs_new">
										<div class="testi_m_profile">
											<span><img src="<?php echo base_url();?>assets/student/img/image-1.png" class="img-responsive img-circle img-thumbnail" alt="testimonial"></span>
											<div class="testi_m_profile_name">
												<h4>PETEY CRUISER</h4>
												<div class="star_rat">
													<ul>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="delet_testimonila_blog">
												<a href="#."><i class="fa fa-trash-o" aria-hidden="true"></i></a>
											</div>
										</div>
										<div class="test_profil_info">
											<ul>
												<li><span>COURSE :</span>Biology</li>
												<li><span>SUBJECT :</span>Botany</li>
												<li><span>TOPIC :</span>Lorem Ipsum is simply dummy</li>
												<li><span>COMMENTS :</span><div class="testm_blog_commt">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="testominail_blogs_new">
										<div class="testi_m_profile">
											<span><img src="<?php echo base_url();?>assets/student/img/image-2.png" class="img-responsive img-circle img-thumbnail" alt="testimonial"></span>
											<div class="testi_m_profile_name">
												<h4>BOB FRAPPLESS</h4>
												<div class="star_rat">
													<ul>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="delet_testimonila_blog">
												<a href="#."><i class="fa fa-trash-o" aria-hidden="true"></i></a>
											</div>
										</div>
										<div class="test_profil_info">
											<ul>
												<li><span>COURSE :</span>Biology</li>
												<li><span>SUBJECT :</span>Botany</li>
												<li><span>TOPIC :</span>Lorem Ipsum is simply dummy</li>
												<li><span>COMMENTS :</span><div class="testm_blog_commt">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="testominail_blogs_new">
										<div class="testi_m_profile">
											<span><img src="<?php echo base_url();?>assets/student/img/image-3.png" class="img-responsive img-circle img-thumbnail" alt="testimonial"></span>
											<div class="testi_m_profile_name">
												<h4>WALTER MELLON</h4>
												<div class="star_rat">
													<ul>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
													</ul>
												</div>
											</div>
											<div class="delet_testimonila_blog">
												<a href="#."><i class="fa fa-trash-o" aria-hidden="true"></i></a>
											</div>
										</div>
										<div class="test_profil_info">
											<ul>
												<li><span>COURSE :</span>Biology</li>
												<li><span>SUBJECT :</span>Botany</li>
												<li><span>TOPIC :</span>Lorem Ipsum is simply dummy</li>
												<li><span>COMMENTS :</span><div class="testm_blog_commt">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div></li>
											</ul>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- schedule new session section end here-->

<!-- popup start Here -->
<!--update testimonial popup start here -->
<div class="modal fade" id="update_testimonials_popup" role="dialog">
	<div class="modal-dialog update_testimonials_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url(); ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="model_details">
					<div class="section_heading_sec">
						<h4>Edit Testimonials</h4>
					</div>
					<div class="model_content">
						<div class="add_topic_form">
							<div class="row">
								<form id="edit_testi">
									<div class="col-md-12">
										<div class="form_set">
											<label>Course</label>
											<select class="form-control select_course" id="edit_course" onchange="getSubjectlist('edit');">
												<option select>Select Course</option>
												<?php if(!empty($course_list)) { 
													foreach($course_list as $course) { 
												?>
												 <option value="<?php echo $course['course_id']?>"><?php echo $course['course_name']?></option>
											    <?php } } ?>
											</select>
										</div>
										<span class="err_ecourse" style="color: red; display: none;">Please select course.</span>
									</div>
									<div class="col-md-12">
										<div class="form_set">
											<label>Subject</label>
											<select class="form-control select_subject" id="edit_subject" onchange="getTopiclist('edit');">
												<option select>Select Subject</option>
												<?php if(!empty($subject_list)) { 
													foreach($subject_list as $subject) { 
												?>
												 <option value="<?php echo $subject['subject_id']?>"><?php echo $subject['subject_name']?></option>
											    <?php } } ?>
											</select>
										</div>
									    <span class="err_esubject" style="color: red; display: none;">Please select subject.</span>
									</div>
									<div class="col-md-12">
										<div class="form_set">
											<label>Topic</label>
											<select class="form-control select_topic" name="topic_id" id="edit_topic">
												<option select>Select Topic</option>
												<?php if(!empty($topic_list)) { 
													foreach($topic_list as $topic) { 
												?>
												 <option value="<?php echo $topic['topic_id']?>"><?php echo $topic['topic_name']?></option>
											    <?php } } ?>
											</select>
										</div>
										<span class="err_etopic" style="color: red; display: none;">Please select topic.</span>
									</div>
									<div class="col-md-12">
										<div class="form_set">
											<label>Rate</label>
											<div class="star_ratting_testimonial_views">
												<ul>
													<li class="edit_rating_star" value="1"><a href="#."><i class="star1 fa fa-star-o" aria-hidden="true"></i></a></li>
													<li class="edit_rating_star" value="2"><a href="#."><i class="star2 fa fa-star-o" aria-hidden="true"></i></a></li>
													<li class="edit_rating_star" value="3"><a href="#."><i class="star3 fa fa-star-o" aria-hidden="true"></i></a></li>
													<li class="edit_rating_star" value="4"><a href="#."><i class="star4 fa fa-star-o" aria-hidden="true"></i></a></li>
													<li class="edit_rating_star" value="5"><a href="#."><i class="star5 fa fa-star-o" aria-hidden="true"></i></a></li>
												</ul>
											</div>
											<input type="hidden" name="rating" value="" id="edit_rating">
											<input type="hidden" name="testimonial_id" value="" id="edit_testimonial_id">
											<input type="hidden" name="student_id" value="<?php echo $this->session->userdata['userId']?>" >
										</div>
										<span class="err_erate" style="color: red; display: none;">Please select rate.</span>
									</div>
									<div class="col-md-12">
										<div class="form_set">
											<label>Add Comments</label>
											<textarea class="form-control comment_textarea" id="edit_text" name="comment" placeholder="Add comment"></textarea>
										</div>
										<span class="err_ecomment" style="color: red; display: none;">Please enter comment.</span>
									</div>
									<div class="col-md-12">
										<div class="main_profil_btn">
											<button type="button" class="btn save_cancel_btn margin-right-10px" id="updateTestimonials" onclick="updateRating();">Update</button>
											<a href="javascript:void(0)" class="btn save_cancel_btn" data-dismiss="modal">Cancel</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- update testimonial popup end here -->
<!-- popup end Here -->
<script src="<?php echo base_url();?>assets/student/js/student_testimonial.js"></script>