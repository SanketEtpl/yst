<style type="text/css">
	.privacy_policy_content_heading h4::after {
		background-image: url('<?php echo base_url();?>assets/student/img/verification.png');
	}
	.privacy_policy_content h4::after{
		background-image: url('<?php echo base_url();?>assets/student/img/correct-icon.png');
	}
</style>
<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url();?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>TERMS AND SERVICE</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- pravacy policy section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details_se_new">
					<div class="privacy_policy_content_heading">
						<h4>Important information about the usage of this website</h4>
						<p>Please read the following terms of service ("terms of service") carefully before participating in, or accessing, any of the services made available by YourScienceTutor, a business of YourScienceTutor Ltd. ("YourScienceTutor", "our", "we", "the Company"). By using and participating in the site, you acknowledge that you have read the terms of service and agree that the terms of service constitute a binding legal agreement between the user ("you") and YourScienceTutor. If you do not agree with these terms of service, you must not use this website or any other service provided by YourScienceTutor.
							<br>
							The terms of service apply to the services from any of the access points (YourScienceTutor.com) and its related subdomains ("Site" or "Sites") as well as any other service ("Service") operated, owned, managed and administered by YourScienceTutor.
						</p>
					</div>
					<div class="privacy_policy_main_group">
						<div class="privacy_policy_content">
							<h4>1 Restrictions</h4>
						</div>
						<div class="privacy_policy_content_sec">
							<h4>1.1 Age</h4>
							<p>If you are under 13, please do not use any of the services provided on the site by YourScienceTutor. Also, please do not attempt to register for membership or send any information about yourself, including your name, address, telephone number, or email address. If in the event we unknowingly gather information from a child under the age of 13, we will delete that information immediately.</p>
						</div>
					</div>
					<div class="privacy_policy_main_group">
						<div class="privacy_policy_content">
							<h4>2 Use of this Website</h4>
						</div>
						<div class="privacy_policy_content_sec">
							<h4>2.1 Available Files</h4>
							<p>YourScienceTutor does not guarantee that videos or files available for viewing through the site will be free of contaminating or damaging code such as viruses, trap doors and the like. YourScienceTutor does not endorse content, nor warrant the accuracy, completeness, correctness, timeliness or usefulness of any opinions, advice, content, services, or merchandise provided through the service or on the internet generally.</p>
						</div>
						<div class="privacy_policy_content_sec">
							<h4>2.2 Accuracy and Purpose of Educational Content</h4>
							<p>The content is not intended to be a substitute for professional lecturers and educators. The content is created by A qualified tutor, not a qualified teacher, and should not be considered to be professionally sanctioned material. Rather it should be used as an aid to study. Reliance on any information appearing on the YourScienceTutor site is strictly at your own risk and should always be checked with the information provided by exam board specifications and requirements by schools to deem its accurateness and validity. YourScienceTutor is not liable or responsible in any way, if you incorrectly answer a question in an assignment, exam or any form of formal testing administered by a school or exam board by using a method or procedure taught on this site which does not correspond to the method or procedure taught at school. It is the student's responsibility to ensure that the methods and procedures for answering questions taught on the site correspond to that which is taught at the school you attend. In no way does YourScienceTutor guarantee any outcome in formal academic testing, or that the material which appears on the website is exhaustive in the relevant subject matter. The content of the YourScienceTutor site is intended for educational and entertainment purposes only. Such content is not intended to, and do not, constitute legal, professional, medical, psychological, therapeutic or healthcare advice or diagnosis or counselling, and may not be used for such purposes.</p>
						</div>
						<div class="privacy_policy_content_sec">
							<h4>2.3 Prohibitions</h4>
							<p>When using this website you agree not to:</p>
							<ul>
								<li>Modify, download, reproduce, copy or resell the YourScienceTutor site, the site's content or any other service offered by YourScienceTutor.</li>
								<li>Commercially use this site, the content or any portion derivative thereof.</li>
								<li>Bypass any YourScienceTutor measure used to prevent or restrict access to any portion of the YourScienceTutor site or any other service operated, owned and managed by YourScienceTutor.</li>
								<li>Interfere with or damage in any way the operation of the site, by any means, including uploading viruses, adware, spyware, worms, or other malicious code.</li>
								<li>Use any robot, spider, data miner, crawler, scraper or other automated means to access the site to collect information from or otherwise interact with the site.</li>
								<li>Reverse engineer, dissemble, decompile or otherwise attempt to discover the source code or underlying algorithms of all or any part of the site, Facebook or any related webhosted service administered, managed and owned by YourScienceTutor. The sole exception to this prohibition is where such activity is expressly permitted by applicable law.</li>
								<li>Use the site, Facebook or any other related service unlawfully, which could damage, disable or impair the site.</li>
								<li>Use the website for any illegal purpose, or in violation of any local, state, national, or international law, including, without limitation, laws governing intellectual property and other proprietary rights, and data protection and privacy.</li>
								<li>Collect contact information from the site, Facebook or any other related service administered, managed and owned by YourScienceTutor for the purpose of sending emails, unwarranted messages or for the benefit of any third party.</li>
								<li>Modify or create derivatives of any part of the site, Facebook or any other service administered, managed and owned by YourScienceTutor.</li>
								<li>Post, upload or distribute any defamatory, slanderous or inaccurate user content or any other form of content.</li>
								<li>Post, upload or distribute any user content that is unlawful, offensive, pornographic, harassing, threating, racist, hateful or otherwise inappropriate.</li>
								<li>Impersonate any person or entity, falsely state or otherwise misrepresent yourself and your age, or create a false identity on the site, Facebook page or any other service.</li>
								<li>Use or attempt to use another member's account. Under no circumstance will this be permitted.</li>
								<li>Delete, remove or obscure any copyright or other proprietary notices on the site, Facebook or any other service.</li>
								<li>Upload, post, transmit or otherwise make available any unsolicited offers, advertisements, proposals or send junk mail or spam to other users of the website. This includes, but is not limited to, unsolicited advertising, promotional materials, or other solicitation material, bulk mailing of commercial advertising, chain mail, informational announcements, charity requests, and petitions for signatures.</li>
								<li>Upload, post, transmit, share, store or otherwise make publicly available any private information of any third party, including addresses, phone number, email addresses, credit card numbers etc.</li>
								<li>Solicit personal information from anyone under 18 or solicit passwords or personal information for commercial or unlawful purposes.</li>
								<li>Upload, post, transmit, share or otherwise make available any material that contains software viruses or any other computer code, viruses, malware, bots, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment.</li>
								<li>Upload, post, transmit, share, store or otherwise make available content that would constitute, encourage or provide instructions for a criminal offence, violate the rights of any party, or that would otherwise create liability or violate any local, state, national or international law.</li>
								<li>Upload, post, transmit, share, store or otherwise make available content that, in the sole judgment of the Company, is objectionable or which restricts or inhibits any other person from using or enjoying the site, or the Facebook application, or which may expose the Company or its users to any harm or liability of any type.</li>
							</ul>
							<p>Due to the global nature of our company, YourScienceTutor may process information in UK, or in another country. We may process information for our own purposes or occasionally, we may process information for one of our partners.</p>
						</div>
					</div>
					<div class="privacy_policy_main_group">
						<div class="privacy_policy_content">
							<h4>3 User Accounts (Members)</h4>
						</div>
						<div class="privacy_policy_content_sec">
							<h4>3.1 Registration</h4>
							<p>For every subject offered on the site, a promo video of the class content will be available for free viewing without requiring any form of membership.
								<br>
								To be able to access Full videos you will be required to register to become a member ("member") of the site. In order to register you will be required to provide valid and correct information for the following:
							</p>
							<ul>
								<li>Full name</li>
								<li>Email address</li>
								<li>Password</li>
							</ul>
							<p>The information you provide must be true, accurate, current and complete. If the original information provided upon initial sign up changes after this time, it is your obligation to update and correct it. If any information provided by you is not true, accurate, current or complete, YourScienceTutor maintains the right to terminate your membership and refuse any current or future access or use of this site. When selecting a password, for your own security you should choose a combination of lower case, uppercase, number and symbols (for example, Br1dGe$). Please ensure you select a password unique for your YourScienceTutor account. Avoid choosing obvious words or dates such as a nickname or your birth date. Please exercise maximum caution to keep your user name and password confidential and log-off from the website when your session is complete to prevent unauthorised access to your information. If your user name or password is subject to unauthorised access, you should immediately inform YourScienceTutor.</p>
						</div>
						<div class="privacy_policy_content_sec">
							<h4>3.2 Member Profile</h4>
							<p>Once YourScienceTutor has received your information you will be sent a confirmation email confirming your membership of the site. You will have your own member's profile and page, which will contain any subjects you have purchased, your account settings as well as a list of suggested subjects which will be generated based on the subjects you already own. You will also have access to book classes and one-to-one sessions with the tutor as an additional cost to you. You will be able to monitor your progress through the areas within the profile pages of your account.</p>
						</div>
						<div class="privacy_policy_content_sec">
							<h4>3.3 Charges and Payment</h4>
							<p>Charges are set as a fixed price and are displayed on each Course or Subject. Once you have purchased the subject, it will appear in your member page under the heading ‘My Account’. Your ownership of the subject will be for a period of 12 months from the time of purchase. If, for example, you purchase the subject on 21st October 2018, you will be able to access it until the 21st October 2019. After this time, your access to the subject will be restricted again and if you wish to view the subject in its entirety you will be required to purchase it again. After the 12 month period you will be given an option to continue your access for a top up fee, as stated for the Course or Subject. If you accept and pay the top up fee, your access will continue for a further 12 months. You will be offered this access 1 month prior to the end of your initial 12 month period. 
								<br><br>
								YourScienceTutor accepts payment by Mastercard, Visa using Paypal as a secure checkout. Your payment details will be encrypted to minimize the possibility of unauthorized access or disclosure. At the time of registration, you will be asked to provide credit card/debit card/Paypal details. In the event that the Company is unable at any time to obtain payment of any charges using such payment details, the Company may freeze your account until it has received settlement in full. 
							</p>
						</div>
						<div class="privacy_policy_content_sec">
							<h4>3.4 Termination</h4>
							<p>YourScienceTutor may terminate or suspend with immediate effect and without notice your access to and use of this website and your membership if we:</p>
							<ul>
								<li>Do not receive payment; or</li>
								<li>Believe you have shared your account with another student, person or entity. YourScienceTutor is able to monitor your usage of the service including your IP address and any video activity, and will use this information to draw a conclusion as to whether you have shared your account; or</li>
								<li>Reasonably believe that you have breached any of these Terms of Service; or</li>
								<li>Are unable to verify the accuracy or validity of any information provided by you; or</li>
								<li>Suspects fraudulent, abusive or illegal activity by you.
								Furthermore, YourScienceTutor may come to a conclusion which will result in termination even if it is based upon our opinion or mere suspicion or belief, without any duty to prove that our opinion or suspicion is well-founded and even if our opinion or suspicion is proven not to be well-founded. At any point if YourScienceTutor deems user conduct to be unsuitable, the company may, of its own volition, choose to terminate any licence or permission previously granted to a user. You may cancel your membership at any time but under no circumstances will YourScienceTutor provide a refund of any payment(s).</li>
							</ul>
						</div>
					</div>
					<div class="privacy_policy_main_group">
						<div class="privacy_policy_content">
							<h4>4 Dealing with Third Parties</h4>
						</div>
						<div class="privacy_policy_content_sec">
							<h4>4.1 Dealing with Third Parties</h4>
							<p>YourScienceTutor is not associated with or an agent of any third party or party named or linked on the site and therefore does not have any authority for such third parties. YourScienceTutor maintains no jurisdiction, endorsement, responsibility or liability for any content, products, advertising or other resources available from such third parties. By agreeing to these terms of service, you agree that YourScienceTutor (and our officers, directors and employees) have no liability to you in relation to you in relation to any dispute you may have with a third party.</p>
						</div>
						<div class="privacy_policy_content_sec">
							<h4>4.2 Relationship of YourScienceTutor and Exam Boards</h4>
							<p>YourScienceTutor is a private company and acts solely and entirely independently from any Exam Board mentioned on the site. YourScienceTutor and the exam boards mentioned on the site share no formal contract, affiliation, endorsement or association with each other. In no way do the exam boards mentioned on this site engage in quality control, manage or check any of the accurateness of any of the content available. As previously mentioned in Clause 2.2, reliance on any information appearing on the YourScienceTutor site is strictly at your own risk.</p>
						</div>
					</div>
					<div class="privacy_policy_main_group">
						<div class="privacy_policy_content">
							<h4>5 Copyright</h4>
							<p>By agreeing to these terms of service, you acknowledge that all the content and information provided on the site or through any of the services, contained in any advertisements, sponsorships, or presented to you by YourScienceTutor is protected by copyrights, trademarks, service marks, patents, or other proprietary rights and laws. This applies to all the content and information, including but not limited to text, software, music, sound, photographs, graphics, video or other material defined as content. Except as expressly authorised by YourScienceTutor, you agree not to copy, reproduce, download, modify, translate, publish, broadcast, transmit, distribute, publish, edit, adapt, upload, share, display, license, sell or otherwise exploit for any purposes whatsoever any content or third party submissions or other proprietary rights owned by you. YourScienceTutor reserves all rights to the content not expressly granted in these terms of service.</p>
						</div>
					</div>
					<div class="privacy_policy_main_group">
						<div class="privacy_policy_content">
							<h4>6 Warranty Disclaimer</h4>
							<p>
								YourScienceTutor services and related documentation are provided "as is" and without any warranty of any kind either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. No information, whether oral or written, obtained by you from us through the service shall create any warranty, representation or guarantee not expressly stated in these terms. YourScienceTutor does not represent or warrant that the service will be uninterrupted or error-free, that defects will be corrected, or that the service or the server that makes it available, are free of viruses or other harmful components. 
								<br>
								YourScienceTutor does not warrant or represent that the use or the results of the use of the materials available through the service, from third parties or a linked site will be correct, accurate, timely, reliable or otherwise. In jurisdictions that do not allow the exclusion of implied warranties and representations, YourScienceTutor's liability shall be limited to the greatest extent permitted by law. Under no circumstances will YourScienceTutor be liable for any loss or damage caused by a user's reliance on information obtained through the service, from third parties (such as experts or others) or a linked site, or user's reliance on any product or service obtained from a third party or a linked site. Use of this service is at the users' sole risk.
							</p>
						</div>
					</div>
					<div class="privacy_policy_main_group">
						<div class="privacy_policy_content">
							<h4>7 Limitation of Liabilities</h4>
							<p>In no event shall YourScienceTutor, its parents, subsidiaries, affiliates, officers, directors, shareholders, employees or our suppliers be liable for any indirect, special, incidental, consequential, punitive or exemplary damages (including but not limited to loss of business, profits, data, use, revenue or other economic advantage),arising out of or in connection with our service, our services or the terms based on any theory, even if advised of the possibility of such damages. The negation of damages set forth above is fundamental for the basis of the bargain between us and you. The service and the information would not be provided without such limitations. In no event will our liability, and the liability of our parents, subsidiaries, officers, directors, employees, and suppliers, to you or any third parties in any circumstance exceed the amount of fees you pay to YourScienceTutor in the 12 months prior to the action giving rise to liability. You and YourScienceTutor agree that any cause of action arising out of or related to this service must commence within six (6) months after the cause of action arose (except for causes of action arising out of users' responsibilities in the indemnification section below); otherwise, such cause of action is permanently barred.</p>
						</div>
					</div>
					<div class="privacy_policy_main_group">
						<div class="privacy_policy_content">
							<h4>8 Amendment to these Terms of Service</h4>
							<p>YourScienceTutor reserves the right, at our discretion, to change, modify, add or remove portions of the terms of service at any time. In the event these terms are altered, YourScienceTutor will post the amended terms of service on the site and changes will apply from the date of posting. If after the terms of service and/or the Privacy Policy have been altered and you do not agree to their terms, you must stop using the site.</p>
						</div>
					</div>
					<div class="privacy_policy_main_group">
						<div class="privacy_policy_content">
							<h4>9 Site Maintenance</h4>
							<p>Whilst YourScienceTutor will try to provide you with uninterrupted access to this Website and its Services, YourScienceTutor may need to withdraw, modify, discontinue or temporarily or permanently suspend one or more aspects of this Website where YourScienceTutor has a legal, technical or other good reason to do so (including technical difficulties experienced by the Company or any Internet infrastructure). However, YourScienceTutor will try, wherever possible, to give reasonable notice of its intention to do so. YourScienceTutor reserves the right to withhold, remove and/or discard any content available as part of your account, with or without notice at any time.</p>
						</div>
					</div>
					<div class="privacy_policy_main_group">
						<div class="privacy_policy_content">
							<h4>10 Indemnity</h4>
							<p>You agree to indemnify and hold YourScienceTutor, its subsidiaries, and affiliates, and their respective officers, agents, partners and employees, harmless from any loss, liability, claim, or demand, including reasonable attorneys' fees, made by any third party due to or arising out of your use of the Service in violation of this Agreement and/or arising from a breach of this Agreement and/or any breach of your representations and warranties set forth above and/or if any Content that you post on or through the Service causes YourScienceTutor to be liable to another.</p>
						</div>
					</div>
					<div class="privacy_policy_main_group">
						<div class="privacy_policy_content">
							<h4>11 Refund Policy</h4>
							<p>Unfortunately, we do not offer refunds on purchases. We provide several free promo videos before you purchase a subject as well as a comprehensive list of all the content which is covered in order to help inform your decision to purchase or not. If you purchase the wrong subject we cannot transfer you to another course of the same value at no cost.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- pravacy policy section end here-->