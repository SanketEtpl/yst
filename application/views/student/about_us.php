<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url();?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>ABOUT US</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- our story section start here-->
<section class="web_section_common_latest">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					<div class="section_heading_new_temp">
						<h4>OUR STORY</h4>
						<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
					</div>
					<div class="about_details">
						<div class="row">
							<div class="col-md-5">
								<div class="about_us">
									<img src="<?php echo base_url();?>assets/student/img/about_u_01.jpg" class="img-responsive" alt="our_story"/>
								</div>
							</div>
							<div class="col-md-7">
								<div class="about_info_here">
									<div class="about_first_info">
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
											<br><br>
											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
										</p>
										<div class="our_strory_list">
											<ul>
												<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
												<li>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </li>
												<li>Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</li>
												<li>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</li>
												<li>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- our story section end here-->
<!-- our team section start here-->
<section class="ou_team_section_latest" style="background-image:url('<?php echo base_url();?>assets/student/img/team_mebmers.jpg');">
	<div class="our_team_overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					<div class="section_heading_new_temp">
						<h4>OUR TEAMS</h4>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>
					</div>
					<div class="our_team_details">
						<div class="row">
							<div class="col-md-3">
								<div class="our_team_blog">
									<div class="our_team_img">
										<img src="<?php echo base_url();?>assets/student/img/image-1.png" class="img-responsive" alt="team"/>
									</div>
									<div class="tem_memb_det">
										<h4>Co-Founder</h4>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
									</div>
									<div class="secial_links_our_tems">
										<ul>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Facebook</span><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Google-Plus</span><span><i class="fa fa-google-plus" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Twitter</span><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Snapchat</span><span><i class="fa fa-snapchat" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Instagram</span><span><i class="fa fa-instagram" aria-hidden="true"></i></span></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="our_team_blog">
									<div class="our_team_img">
										<img src="<?php echo base_url();?>assets/student/img/image-2.png" class="img-responsive" alt="team"/>
									</div>
									<div class="tem_memb_det">
										<h4>Former COO</h4>
										<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
									</div>
									<div class="secial_links_our_tems">
										<ul>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Facebook</span><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Google-Plus</span><span><i class="fa fa-google-plus" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Twitter</span><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Snapchat</span><span><i class="fa fa-snapchat" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Instagram</span><span><i class="fa fa-instagram" aria-hidden="true"></i></span></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="our_team_blog">
									<div class="our_team_img">
										<img src="<?php echo base_url();?>assets/student/img/image-3.png" class="img-responsive" alt="team"/>
									</div>
									<div class="tem_memb_det">
										<h4>Director</h4>
										<p>when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
									</div>
									<div class="secial_links_our_tems">
										<ul>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Facebook</span><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Google-Plus</span><span><i class="fa fa-google-plus" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Twitter</span><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Snapchat</span><span><i class="fa fa-snapchat" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Instagram</span><span><i class="fa fa-instagram" aria-hidden="true"></i></span></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="our_team_blog">
									<div class="our_team_img">
										<img src="<?php echo base_url();?>assets/student/img/image-4.png" class="img-responsive" alt="team"/>
									</div>
									<div class="tem_memb_det">
										<h4>Art Director</h4>
										<p>the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
									</div>
									<div class="secial_links_our_tems">
										<ul>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Facebook</span><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Google-Plus</span><span><i class="fa fa-google-plus" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Twitter</span><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Snapchat</span><span><i class="fa fa-snapchat" aria-hidden="true"></i></span></a></li>
											<li><a href="#." class="tooltip"><span class="tooltiptext">Instagram</span><span><i class="fa fa-instagram" aria-hidden="true"></i></span></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- our team section end here-->