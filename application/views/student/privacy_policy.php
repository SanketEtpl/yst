<style type="text/css">
	.privacy_policy_content_heading h4::after {
		background-image: url('<?php echo base_url();?>assets/student/img/verification.png');
	}
	.privacy_policy_content h4::after{
		background-image: url('<?php echo base_url();?>assets/student/img/correct-icon.png');
	}
</style>
<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url();?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>PRIVACY POLICY</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- pravacy policy section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details_se_new">
					<div class="privacy_details_container">
						<div class="privacy_policy_content_heading">
							<h4>We care about protecting your personal information</h4>
							<p>This Privacy Policy ("Privacy Policy”) explains how YourScienceTutor ("YourScienceTutor", "we", "us", "Company") collect, use and share personally identifiable information of visitors using YourScienceTutor.com ("Website”) or any related services offered by the Company ("Services”). By using the website and services you consent to the collection, use and disclosure of your personally identifiable information as set out in this Privacy Policy. The Privacy Policy shall be interpreted under the laws of UK and applies to information that you may provide to us, or that we may obtain, through means other than our Website, such as by phone, through postal mail and through other live and print media, including without limitation, information obtained from or provided to us by third parties.</p>
						</div>
						<div class="privacy_policy_main_group">
							<div class="privacy_policy_content">
								<h4>1. Types of Information Collected by Us</h4>
								<p>We gather various forms of personal information from our users, as explained in detail below. We may use this personal information to personalize and improve our services, to allow our users to set up a user account and profile, to contact users, to fulfil your requests for certain products and services, to analyse how users utilize the Website, and as otherwise set forth in this Privacy Policy. We may share certain types of personal information with third parties (described in this section and the sections below). We collect the following types of information:</p>
							</div>
							<div class="privacy_policy_content_sec">
								<h4>a) Information You Provide to Us</h4>
								<p>We receive and store any information you knowingly provide to us. This includes personal information such as your name, email address, school you attend and details of transactions you carry out through the Website. You can choose not to provide us with certain information, but then you may not be able to register with us or take advantage of some of our features.</p>
							</div>
							<div class="privacy_policy_content_sec">
								<h4>b) Information Collected Automatically</h4>
								<p>Whenever you interact with our Website, we automatically receive and record information on our server logs from your browser including your IP address, "cookie” information, links accessed and the page you requested. "Cookies” are identifiers we transfer to your computer or mobile device that allow us to recognize your browser or mobile device and tell us how and when pages in our Website are visited and by how many people.
									<br><br>
									When we collect usage information (such as the numbers and frequency of visitors to the Website, the length of stay on the Website, the types of pages visited and other general information), we only use this data in aggregate form, and not in a manner that would identify you personally. For example, this aggregate data tells us how often users use parts of the Website, so that we can make the Website appealing to as many users as possible. We may also provide this aggregate information to our partners and our partners may use such information to understand how often and in what ways people use our Website, so that they, too, can provide you with an optimal experience. We never disclose aggregate information to a partner in a manner that would identify you personally.
								</p>
							</div>
							<div class="privacy_policy_content_sec">
								<h4>c) Email and Other Communications</h4>
								<p>We may contact you, by email or other means; for example, we may send you promotional offers on behalf of other businesses, or communicate with you about your use of the Website. If you do not want to receive email or other mail from us, please email us. Please note that if you do not want to receive legal notices from us, those legal notices will still govern your use of the Website, and you are responsible for reviewing such legal notices for changes.</p>
							</div>
						</div>
						<div class="privacy_policy_main_group">
							<div class="privacy_policy_content">
								<h4>2. Use of Your Personal Information</h4>
								<p>The purposes for which we use your information, in addition to any reasons stated above include:</p>
								<ul>
									<li>Providing our services, including advertising</li>
									<li>Research, analysis and auditing, publish best practice reports and/or other documents that will assist in improving our services</li>
									<li>Maintaining and ensuring the viability and integrity of our network</li>
									<li>Protecting the rights or property of YourScienceTutor, or our users or partners</li>
									<li>Creating, researching and developing new products and services</li>
								</ul>
								<p>Due to the global nature of our company, YourScienceTutor may process information in UK, or in another country. We may process information for our own purposes or occasionally, we may process information for one of our partners.</p>
							</div>
						</div>
						<div class="privacy_policy_main_group">
							<div class="privacy_policy_content">
								<h4>3. Sharing of Your Personal Information</h4>
								<p>We neither rent nor sell your personal information in a personally identifiable form to anyone. We may occasionally hire other companies to provide services on our behalf, including but not limited to handling customer support enquires, processing transactions, marketing campaigns and customer freight shipping. Those companies will be permitted to obtain only the personal information they need to deliver the service. YourScienceTutor takes reasonable steps to ensure that these organizations are bound by confidentiality and privacy obligations in relation to the protection of your personal information.</p>
							</div>
						</div>
						<div class="privacy_policy_main_group">
							<div class="privacy_policy_content">
								<h4>4. Accessing Your Personal Information</h4>
								<p>Through your account settings, you may access, and, in some cases, edit or delete the following information you have provided to us: including but not limited to your name, email, the school you attend. The information you can view, update, and delete may change as the Website changes. If you have any questions about viewing or updating information we have on file about you, please contact us.</p>
							</div>
						</div>
						<div class="privacy_policy_main_group">
							<div class="privacy_policy_content">
								<h4>5. Security</h4>
								<p>Your account is protected by a password for your privacy and security. You must prevent unauthorized access to your account and personal information by selecting and protecting your password appropriately and limiting access to your computer or device and browser by signing off after you have finished accessing your account We endeavour to protect the privacy of your account and other personal information we hold in our records, but we cannot guarantee complete security. Unauthorized entry or use, hardware or software failure and other factors, may compromise the security of user information at any time. Transmission of information via the Internet can never be completely secure, and all such transactions are at your risk.</p>
							</div>
						</div>
						<div class="privacy_policy_main_group">
							<div class="privacy_policy_content">
								<h4>6. Changes to This Privacy Policy</h4>
								<p>We may amend this Privacy Policy from time to time. Use of information we collect now is subject to the Privacy Policy in effect at the time such information is used. If we make changes in the way we use personal information, we will notify you by posting an announcement on our Website or sending you an email. You are bound by any changes to the Privacy Policy when you use the Website after such changes have been posted.</p>
							</div>
						</div>
						<div class="privacy_policy_main_group">
							<div class="privacy_policy_content">
								<h4>7. Questions and Concerns</h4>
								<p>If you have any questions or concerns regarding our privacy policies, please send us a detailed message. We will make every effort to resolve your concerns.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- pravacy policy section end here-->