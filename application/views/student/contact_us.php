<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url();?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>CONTACT US</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- pravacy policy section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					<div class="section_heading_sec">
						<h4>THE CONTACT US PAGE IS ONE OF THE MOST VISITED PAGES ON ANY WEBSITE. :-</h4>
					</div>
					<div class="inn_cols_main_div">
						<div class="row">
							<div class="col-md-5">
								<div class="faq_set_info">
									<span>
										<img src="<?php echo base_url();?>assets/student/img/contact_details_back.jpg" class="img-responsive" alt="faq"/>
									</span>
									<div class="faq_details">
										<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
									</div>
								</div>
							</div>
							<div class="col-md-7">
								<div class="contact_us_info">
									<div class="contact_us_add_web_details">
										<div class="row">
											<div class="col-md-4">
												<div class="cont_links_set">
													<div class="set_contact_data">
														<span><img src="<?php echo base_url();?>assets/student/img/address.png" class="img-responsive" alt="address"/></span>
														<h5>Akshya Nagar 1st Block 1st Cross, 
														Rammurthy nagar, Bangalore-560016</h5>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="cont_links_set">
													<div class="set_contact_data">
														<a href="mailto:info@yoursciencetutor.com;">
															<span><img src="<?php echo base_url();?>assets/student/img/mail.png" class="img-responsive" alt="mail"/></span>
															<h5>Info@yoursciencetutor.com</h5>
														</a>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="cont_links_set">
													<div class="set_contact_data">
														<a href="tel:9876543210;">
															<span><img src="<?php echo base_url();?>assets/student/img/contact.png" class="img-responsive" alt="contact"/></span>
															<h5>987 - 654 - 3210</h5>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="contact_us_form_set">
										<div class="row">
											<div class="col-md-4">
												<div class="form_set">
													<label>Enter Your First Name</label>
													<input type="text" class="form-control" id="first_name">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form_set">
													<label>Enter Your Last Name</label>
													<input type="text" class="form-control" id="first_name">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form_set">
													<label>Enter Your Email</label>
													<input type="text" class="form-control" id="first_name">
												</div>
											</div>
											<div class="col-md-12">
												<div class="conatct_form_set">
													<label>Enter Your Massage</label>
													<textarea type="text" class="form-control" id="first_name"></textarea>
												</div>
											</div>
											<div class="col-md-12">
												<div class="contact_btn_div">
													<button type="button" class="btn contact_us_send_msg_btn">
														<i class="fa fa-paper-plane-o" aria-hidden="true"></i>
														SEND MASSAGE
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- pravacy policy section end here-->