<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url(); ?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>NOTIFICATION</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- edit schedule section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					<div class="notification_blog">
						<div class="notifiaction_info_list_active">
							<span class="date_of_notification_active">Today 02-03-2018 04:00 pm</span>
							<div class="noti_list_pass">
								<h4>NEW VIDEO ON BOTANY HAS BEEN ADDED</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<a href="<?php echo base_url(); ?>student-notification-details" class="btn notif_link"><img src="<?php echo base_url(); ?>assets/img/right-arrow.png" class="img-responsive" alt="noti_arrow"></a>
							</div>
						</div>
						<div class="notifiaction_info_list">
							<span class="date_of_notification">02-03-2018 04:00 pm</span>
							<div class="noti_list_pass">
								<h4>NEW USER HAS BEEN ADDED</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<a href="<?php echo base_url(); ?>student-notification-details" class="btn notif_link"><img src="<?php echo base_url(); ?>assets/img/right-arrow.png" class="img-responsive" alt="noti_arrow"></a>
							</div>
						</div>
						<div class="notifiaction_info_list">
							<span class="date_of_notification">02-03-2018 04:00 pm</span>
							<div class="noti_list_pass">
								<h4>NEW REQUEST FOR ONE SESSION HAS BEEN ACCEPTED</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<a href="<?php echo base_url(); ?>student-notification-details" class="btn notif_link"><img src="<?php echo base_url(); ?>assets/img/right-arrow.png" class="img-responsive" alt="noti_arrow"></a>
							</div>
						</div>
						<div class="notifiaction_info_list">
							<span class="date_of_notification">10-03-2018 04:00 pm</span>
							<div class="noti_list_pass">
								<h4>NEW VIDEO ON CHEMISTRY GRADE 11 HAS BEEN ADDED.</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<a href="<?php echo base_url(); ?>student-notification-details" class="btn notif_link"><img src="<?php echo base_url(); ?>assets/img/right-arrow.png" class="img-responsive" alt="noti_arrow"></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
				<!-- edit schedule section end here-->