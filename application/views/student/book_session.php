<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url(); ?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>BOOK SESSION</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- book a session section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					<div class="book_sesion_buttons_set">
						<a href="<?php echo base_url(); ?>book-session-edit" class="btn book_session_btn">One To One Live Session</a>
						<a href="<?php echo base_url(); ?>book-session-edit" class="btn book_session_btn">Group Live Session</a>
					</div>
					<div class="session_book_section">
						<div class="row">
							<div class="col-md-6">
								<div class="session_blog_here graident_border_left">
									<div class="session_blog_head">
										<h4>ONE TO ONE LIVE SESSION</h4>
									</div>
									<div class="indivdual_session_details">
										<span class="indivdual_session_img">
											<img src="<?php echo base_url(); ?>assets/student/img/book-session-img-01.jpg" class="img-responsive" alt="session"/>
										</span>
										<div class="individual_details_info">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
										</div>
										<a href="<?php echo base_url(); ?>book-session-edit" class="btn book_session_btn">One To One Live Session</a>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="session_blog_here">
									<div class="session_blog_head">
										<h4>GROUP LIVE SESSION</h4>
									</div>
									<div class="indivdual_session_details">
										<span class="indivdual_session_img">
											<img src="<?php echo base_url(); ?>assets/student/img/book-session-img-02.jpg" class="img-responsive" alt="session"/>
										</span>
										<div class="individual_details_info">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
										</div>
										<a href="<?php echo base_url(); ?>book-session-edit" class="btn book_session_btn">Group Live Session</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- book a session section end here-->