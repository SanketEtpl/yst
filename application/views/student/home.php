
<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url();?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>HOME</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- pie chart section start here-->
<section class="piechart_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="piechart_details">
					<div class="piechart-main-wrapper margin-bottom-30px">
						<div class="row">
							<div class="col-md-5 custom_width_user_det">
								<div class="user_details_home">
									<span class="user_details_photo">
										<img src="<?php echo base_url();?>assets/student/img/image-13.png" class="img-responsive img-circle img-thumbnail" alt="userimage"/>
									</span>
									<div class="user_details_home_info">
										<h4>GRAHAM CRACKER</h4>
										<div class="star_user_deta_home">
											<ul>
												<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
												<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
												<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
												<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
												<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
											</ul>
										</div>
										<div class="cours_level">
											<span>Course Level - </span>
											<div>GCEG</div>
										</div>
									</div>
									<div class="user_details_home_links">
										<div><a href="<?php echo base_url(); ?>my-account"><span><img src="<?php echo base_url();?>assets/student/img/user-icon.png" class="img-responsive my_account_img" alt="myaccount"/></span>My Account</a></div>
										<div><a href="#." data-toggle="modal" data-target="#refer_friend_set_popup"><span>
											<img src="<?php echo base_url();?>assets/student/img/frien-refer.png" class="img-responsive refer_img" alt="refer"/></span>Refer a Friend!</a></div>
										</div>
									</div>
									<div class="my_scoreboard">
										<div class="blog_heading">
											<h4>MY SCOREBOARD</h4>
											<a href="javascript:void(0)" class="view_all_link">View All</a>
										</div>
										<div class="score_set_here">
											<div class="row">
												<div class="col-md-4 score_blogs">
													<div class="score_study">
														<h3 class="score_no">15</h3>
														<div class="score_name">STUDY LEVEL</div>
													</div>
												</div>
												<div class="col-md-4 score_blogs">
													<div class="score_target">
														<h3 class="score_no">30</h3>
														<div class="score_name">TARGET SCORE</div>
													</div>
												</div>
												<div class="col-md-4 score_blogs">
													<div class="score_working_score">
														<h3 class="score_no">50</h3>
														<div class="score_name">WORKING AT SCORE</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-7 custom_width_user_det_sec">
									<div class="pie_chart_blogs">
										<div class="pie_chart_head_sec">
											<h4>PHYISICS</h4>
											<div class="new_selectpiechart">
												<select class="form-control">
													<option selected="">Recent</option>
													<option>option 1</option>
													<option>option 2</option>
													<option>option 3</option>
													<option>option 4</option>
												</select>
											</div>
										</div>
										<div class="pioe_chart_set_here">
											<img src="<?php echo base_url();?>assets/student/img/graph-physics.png" class="img-responsive" alt="piechart"/>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="piechart-main-wrapper">
							<div class="row">
								<div class="col-md-5 custom_width_user_det">
									<div class="pie_chart_blogs">
										<div class="pie_chart_head_sec">
											<h4>BIOLOGY</h4>
											<div class="new_selectpiechart">
												<select class="form-control">
													<option selected="">Recent</option>
													<option>option 1</option>
													<option>option 2</option>
													<option>option 3</option>
													<option>option 4</option>
												</select>
											</div>
										</div>
										<div class="pioe_chart_set_here">
											<img src="<?php echo base_url();?>assets/student/img/graph-biology.jpg" class="img-responsive" alt="piechart"/>
										</div>
									</div>
								</div>
								<div class="col-md-7 custom_width_user_det_sec">
									<div class="pie_chart_blogs">
										<div class="pie_chart_head_sec">
											<h4>CHEMISTRY</h4>
											<div class="new_selectpiechart">
												<select class="form-control">
													<option selected="">Recent</option>
													<option>option 1</option>
													<option>option 2</option>
													<option>option 3</option>
													<option>option 4</option>
												</select>
											</div>
										</div>
										<div class="pioe_chart_set_here">
											<img src="<?php echo base_url();?>assets/student/img/graph-chemistry.jpg" class="img-responsive" alt="piechart"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- pie chart section end here-->
	<!-- testimonial section start here-->
	<section class="testimonial_section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="testmonial_details">
						<div class="section_heading">
							<h4>TESTIMONIAL</h4>
						</div>
						<span class="view_all"><a href="javascript:void(0)">View All</a></span>
						<div class="testimonail_all_member">
							<div class="row">
								<div class="col-md-4">
									<div class="testominail_blogs">
										<div class="testi_m_profile">
											<span><img src="<?php echo base_url();?>assets/student/img/image-1.png" class="img-responsive img-circle img-thumbnail" alt="testimonial"/></span>
											<div class="testi_m_profile_name">
												<h4>HOLLY GRAHAM</h4>
												<div class="star_rat">
													<ul>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="test_profil_info">
											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="testominail_blogs">
										<div class="testi_m_profile">
											<span><img src="<?php echo base_url();?>assets/student/img/image-2.png" class="img-responsive img-circle img-thumbnail" alt="testimonial"/></span>
											<div class="testi_m_profile_name">
												<h4>DONATELLA NOBATTI</h4>
												<div class="star_rat">
													<ul>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="test_profil_info">
											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="testominail_blogs">
										<div class="testi_m_profile">
											<span><img src="<?php echo base_url();?>assets/student/img/image-3.png" class="img-responsive img-circle img-thumbnail" alt="testimonial"/></span>
											<div class="testi_m_profile_name">
												<h4>MANUEL LABOR</h4>
												<div class="star_rat">
													<ul>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
														<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="test_profil_info">
											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley .
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- testimonial section end here-->


	<!--popup start here-->

	<!--refer friend popup start here-->
	<div class="modal fade" id="refer_friend_set_popup" role="dialog">
		<div class="modal-dialog refer_friend_width_popup">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="<?php echo base_url();?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
					</button>
				</div>
				<div class="modal-body">
					<div class="moel_details_new">
						<div class="section_heading_sec">
							<h4>REFER A FRIEND</h4>
						</div>
						<div class="model_content">
							<div class="refer_friend_popup_details">
								<ul class="nav nav-tabs">
									<li class="active">
										<a data-toggle="tab" href="#refer_frd_01">
											<span>
												<img src="<?php echo base_url();?>assets/student/img/popup-call-icon.png" class="img-responsive" alt="call"/>
											</span>
											<div>PHONE NUMBER</div>
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#refer_frd_02">
											<span>
												<img src="<?php echo base_url();?>assets/student/img/popup-email-icon.png" class="img-responsive" alt="call"/>
											</span>
											<div>EMAIL ID</div>
										</a>
									</li>
									<li>
										<a data-toggle="tab" href="#refer_frd_03">
											<span>
												<img src="<?php echo base_url();?>assets/student/img/popup-facebook-icon.png" class="img-responsive" alt="call"/>
											</span>
											<div>FACEBOOK</div>
										</a>
									</li>
								</ul>

								<div class="tab-content">
									<div id="refer_frd_01" class="tab-pane fade in active">
										<div class="refe_frd_tab_content">
											<div class="col-md-8 center_div">
												<div class="form_set_n_h">
													<label>Enter Phone Number</label>
													<div class="input-group">
														<input class="form-control" id="phone_number"/>
														<button type="button" class="btn comm-btn" data-dismiss="modal" data-toggle="modal" data-target="#sent_success_popup">Share</button>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="refer_frd_02" class="tab-pane fade">
										<div class="refe_frd_tab_content">
											<div class="col-md-8 center_div">
												<div class="form_set_n_h">
													<label>Enter Email ID</label>
													<div class="input-group">
														<input class="form-control" id="email_id"/>
														<button type="button" class="btn comm-btn" data-dismiss="modal" data-toggle="modal" data-target="#sent_success_popup">Share</button>
													</div>
												</div>
											</div>
										</div>											 
									</div>
									<div id="refer_frd_03" class="tab-pane fade">
										<div class="refe_frd_tab_content">
											<div class="col-md-8 center_div">
												<div class="form_set_n_h">
													<label>Enter Email ID</label>
													<div class="input-group">
														<input class="form-control" id="email_id"/>
														<button type="button" class="btn comm-btn" data-dismiss="modal" data-toggle="modal" data-target="#sent_success_popup">Share</button>
													</div>
												</div>
											</div>
										</div>												  
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--refer friend popup end here-->


	<!--popup start here-->  
	<!-- sent succesfully popup Start Here -->
	<div class="modal fade" id="sent_success_popup" role="dialog">
		<div class="modal-dialog delet_cours_popup_width">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="<?php echo base_url();?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
					</button>
				</div>
				<div class="modal-body">
					<div class="moel_details">
						<span class="model_header">
							<img src="<?php echo base_url();?>assets/student/img/correct-icon.png" class="img-responsive" alt="forgotpassword"/>
							<h5>Sent Successfully</h5>
						</span>
						<div class="model_content">
							<p>Link has been sent to the phone number</p>
							<div class="pop_btn_sect_new">
								<button type="button" class="btn popup_coom_btn">OK</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- sent succesfully popup end Here -->					  
	<!--popup end here-->
</div>