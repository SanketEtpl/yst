<!-- Page wrapper Start here-->
 <div class="wrapper">
  <section class="loginpage">
    <div class="inner-sect">
    <div class="col-md-6">
       <div class="alert alert-success" style="display:none">
        <strong id="msg" ></strong> 
       </div>
    </div> 
      <div class="bgshow">
        <div class="container">
          <div class="lftlog">
            <form class="logform" id="student_form">
              <div class="commsect">
                <div class="row nomarg">
                  <h1>Sign in</h1>
                </div>
                <input type="hidden" name="" class="usertype" value="student">
                <div class="row nomarg">
                  <div class="form-group">
                    <input id="username" type="text" name="email_id" class="form-control username" placeholder="Email Address" >
                    <span class="err_username" style="color: red; display: none;">Please enter valid email address.</span>
                  </div> 
                </div>
                <div class="row nomarg">
                  <div class="form-group">
                    <input id="password" type="password" name="password" class="form-control password" placeholder="Password (e.g : Password@123)" value="">
                    <span class="err_password" style="color: red; display: none;">Please enter valid password.</span>
                  </div>
                </div>
                <div class="row nomarg">
                  <div class="form-group">
                    <div class="check_inner">
                      <input class="filled-in" id="check1" name="check1" type="checkbox" value="1">
                      <label for="check1">Remember Me</label>
                      <span class="forgotpass"> <a href="#." class="" data-toggle="modal" data-target="#forgot_password_popup">Forgot Password ?</a></span>
                    </div>
                  </div>
                </div>
                <div class="row nomarg">
                  <div class="form-group">
                     <!-- <button type="button" id="btnLogin" class="btn comm-btn " onclick="loginMe()">Sign In</button> -->
                     <button type="button" id="login" class="btn comm-btn" onclick="studentLogin()">Sign In</button>
                    <a href="<?php base_url();?>signup" class="btn comm-btn">Sign Up</a>
                  </div>
                </div>
                <div class="soacila_links_sign_in">
                  <span>CONTINUE WITH</span>
                  <ul>
                    <li><a href="#."><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                    <li><a href="#."><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
                    <li><a href="#."><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                    <!--<li><a href="#."><i class="fa fa-snapchat-square" aria-hidden="true"></i></a></li>
                    <li><a href="#."><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
                  </ul>
                </div>
                <div class="row nomarg lastrow">
                  <div class="form-group">
                    <p class="instrct">By signing in, I Accept <a href="#.">Terms and Conditions</a></p>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="website_right_details ">
            <span class="web_logo_log_sign">
              <a href="#"><img src="<?php echo base_url();?>assets/admin/img/loginlogo.png" class="img-responsive" alt="logo"/></a>
            </span>
            <div class="web_details">
              <h4>WELCOME <br> YOUR SCIENCE TUTOR</h4>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
          </div>
        </div>
      </div> 
    </div> 
  </section> 
  <!-- Login Page end Here -->
  <!-- popup Start Here -->
  <!-- forgot password popup Start Here -->
  <div class="modal fade" id="forgot_password_popup" role="dialog">
    <div class="modal-dialog model_forgot_pass_popup"> 
      <!-- Modal content -->
       <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
           <img src="<?php echo base_url();?>assets/admin/img/pop_up_close.png" class="img-responsive" alt="close"/>
         </button>
       </div>
       <div class="modal-body">
        <div class="moel_details">
          <span class="model_header">
            <img src="<?php echo base_url();?>assets/admin/img/forgot_password.png" class="img-responsive" alt="forgotpassword"/>
            <h5>Forgot Password</h5>
          </span>
          <form id="forgot_pass">
            <div class="model_content">
              <p>Please enter the email address you signed up with and <br>we'll send you a password reset Link.</p>
              <div class="pop_form">
               <input id="email_address" type="text" name="email" class="form-control forgot_ps_input" placeholder="Email Address"/>
               <span class="err_email" style="color: red; display: none;">Please enter valid email address.</span>
              </div>
              <div class="pop_btn_sect">
              <!-- <button type="button" class="btn popup_coom_btn" data-dismiss="modal" data-toggle="modal" data-target="#sent_success_popup">RESET PASSWORD</button> -->
              <button type="button" class="btn popup_coom_btn" onclick="forgot_password()";>RESET PASSWORD</button>
              </div>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
</div> 
<!-- forgot password popup end Here -->
<!-- sent succesfully popup Start Here -->
 <div class="modal fade" id="sent_success_popup" role="dialog">
  <div class="modal-dialog model_forgot_pass_popup"> 
    <!-- Modal content -->
    <div class="modal-content">
   <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">
         <img src="<?php echo base_url();?>assets/admin/img/pop_up_close.png" class="img-responsive" alt="close"/>
       </button>
     </div>
     <div class="modal-body">
      <div class="moel_details">
        <span class="model_header">
          <img src="<?php echo base_url();?>assets/admin/img/sent.png" class="img-responsive" alt="forgotpassword"/>
          <h5>Sent Successfully</h5>
        </span>
        <div class="model_content">
          <p>Your request has been proceeded.Check your registered<br>Email ID to update your password.</p>
          <div class="pop_btn_sect">
            <button type="button" class="btn popup_coom_btn">OK</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div> 
<!-- sent succesfully popup end Here -->
<!-- popup end Here -->

 <script src="<?php echo base_url();?>assets/admin/js/login.js"></script>


