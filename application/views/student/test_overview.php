<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url(); ?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>MY ACCOUNT</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- edit schedule section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					<div class="section_heading_sec">
						<h4>TEST OVERVIEW</h4>
						<a href="<?php echo base_url(); ?>my-account" class="back_page"><img src="<?php echo base_url();?>assets/img/back_arrow.png" class="img-responsive" alt="back"></a>
					</div>
					<div class="que_ans_level">
						<div class="level_details">
							<div class="video_topic_details">
								<div class="test_overview">
									<div class="form_set_new">
										<label>Test:</label>
										<select class="form-control">
											<option selected="">Select Test:</option>
											<option>Test 1</option>
											<option>Test 2</option>
											<option>Test 3</option>
										</select>
									</div>
								</div>
								<div class="topic_name">
									<label>Course Name:</label>
									<span>Mathematics</span>
								</div>
								<div class="tutor">
									<label>Subject Name:</label>
									<span>Algebra</span>
								</div>
								<div class="tutor">
									<label>Topic Name:</label>
									<span>Trignometry</span>
								</div>
								<div class="total_score">
									<label>TOTAL SCORE:</label>
									<span>8/10</span>
								</div>
							</div>
						</div>
					</div>
					<div class="session_all_order_list_here">
						<div class="scrollbar-inner custom_session_que_ans_height_custom">
							<div class="session_order_lists">
								<div class="session_questions">
									Q.1 Water in plants is trans-ported by.
								</div>
								<div class="ans_here">
									<div class="ans_status_multiole_qustion">
										<div class="answer_heading">Answer:</div>
										<div class="ans_multiple_here">
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test1" name="radio-group" checked="">
															<label for="test1">	01) Xylem</label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test2" name="radio-group" checked="">
															<label for="test2">	02) Phloem</label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test3" name="radio-group" checked="">
															<label for="test3">	03) Epidermis</label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test5" name="radio-group" checked="">
															<label for="test5">	04) Cambium</label>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="answer_status">
										<h4>Correct Answer</h4>
										<img src="<?php echo base_url(); ?>assets/student/img/double-correct.png" class="img-responsive" alt="correct"/>
									</div>
								</div>
							</div>
							<div class="session_order_lists">
								<div class="session_questions">
									Q.2 Taxicology is related to the study of
								</div>
								<div class="ans_here">
									<div class="ans_status_multiole_qustion">
										<div class="answer_heading">Answer:</div>
										<div class="ans_multiple_here">
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test6" name="radio-group" checked="">
															<label for="test6">	01) Poisons</label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test7" name="radio-group" checked="">
															<label for="test7">	02) Diseases</label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test8" name="radio-group" checked="">
															<label for="test8">	03) Bacteria</label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test9" name="radio-group" checked="">
															<label for="test9">	04) Viruses</label>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="incorrectanswer_status">
										<h4>Incorrect Answer</h4>
										<img src="<?php echo base_url(); ?>assets/student/img/duble-close.png" class="img-responsive" alt="correct"/>
									</div>
								</div>
							</div>
							<div class="session_order_lists">
								<div class="session_questions">
									Q.3 Lorem Ipsum is simply dummy text of the printing and typesetting industry.
								</div>
								<div class="ans_here">
									<div class="ans_status_multiole_qustion">
										<div class="answer_heading">Answer:</div>
										<div class="ans_multiple_here">
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test10" name="radio-group" checked="">
															<label for="test10">	01) Lorem </label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test11" name="radio-group" checked="">
															<label for="test11">	02) industry</label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test12" name="radio-group" checked="">
															<label for="test12">	03) packages </label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test13" name="radio-group" checked="">
															<label for="test13">	04) Ipsum </label>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="answer_status">
										<h4>Correct Answer</h4>
										<img src="<?php echo base_url(); ?>assets/student/img/double-correct.png" class="img-responsive" alt="correct"/>
									</div>
								</div>
							</div>
							<div class="session_order_lists">
								<div class="session_questions">
									Q.4 There are many variations of passages of Lorem Ipsum available.
								</div>
								<div class="ans_here">
									<div class="ans_status_multiole_qustion">
										<div class="answer_heading">Answer:</div>
										<div class="ans_multiple_here">
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test14" name="radio-group" checked="">
															<label for="test14">	01) injected</label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test15" name="radio-group" checked="">
															<label for="test15">	02) Lorem </label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test16" name="radio-group" checked="">
															<label for="test16">	03) suffered </label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test17" name="radio-group" checked="">
															<label for="test17">	04) alteration </label>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="answer_status">
										<h4>Correct Answer</h4>
										<img src="<?php echo base_url(); ?>assets/student/img/double-correct.png" class="img-responsive" alt="correct"/>
									</div>
								</div>
							</div>
							<div class="session_order_lists">
								<div class="session_questions">
									Q.5 Lorem Ipsum has been the industry's standard dummy.
								</div>
								<div class="ans_here">
									<div class="ans_status_multiole_qustion">
										<div class="answer_heading">Answer:</div>
										<div class="ans_multiple_here">
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test18" name="radio-group" checked="">
															<label for="test18">	01) random </label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test19" name="radio-group" checked="">
															<label for="test19">	02) Virginia</label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test20" name="radio-group" checked="">
															<label for="test20">	03) literature </label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test21" name="radio-group" checked="">
															<label for="test21">	04) Hampden</label>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="incorrectanswer_status">
										<h4>Incorrect Answer</h4>
										<img src="<?php echo base_url(); ?>assets/student/img/duble-close.png" class="img-responsive" alt="correct"/>
									</div>
								</div>
							</div>
							<div class="session_order_lists">
								<div class="session_questions">
									Q.6 There are many variations of passages of Lorem Ipsum available.
								</div>
								<div class="ans_here">
									<div class="ans_status_multiole_qustion">
										<div class="answer_heading">Answer:</div>
										<div class="ans_multiple_here">
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test22" name="radio-group" checked="">
															<label for="test22">	01) passage </label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test23" name="radio-group" checked="">
															<label for="test23">	02) embarrassing </label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test24" name="radio-group" checked="">
															<label for="test24">	03) Ipsum </label>
														</span>
													</div>
												</div>
											</div>
											<div class="answer_width_set_sec">
												<div class="ans_option">
													<div class="radio_btn_group">
														<span>
															<input type="radio" id="test25" name="radio-group" checked="">
															<label for="test25">	04) repetition</label>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="answer_status">
										<h4>Correct Answer</h4>
										<img src="<?php echo base_url(); ?>assets/student/img/double-correct.png" class="img-responsive" alt="correct"/>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="que_ans_btn">
						<a href="#." class="btn save_cancel_btn" data-toggle="modal" data-target="#success_fully_submit_popup">Submit</a>
						<a href="#." class="btn save_cancel_btn">Cancel</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- edit schedule section end here-->

<!-- popup start Here -->

<!--succesfully changes popup Start Here -->
<div class="modal fade" id="success_fully_submit_popup" role="dialog">
	<div class="modal-dialog delet_cours_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url(); ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="moel_details">
					<span class="model_header">
						<img src="<?php echo base_url(); ?>assets/student/img/correct-icon.png" class="img-responsive" alt="forgotpassword"/>
						<h5>Successfully Submit</h5>
					</span>
					<div class="model_content">
						<p>Do you really want to submit your test?</p>
						<div class="btn_group_popup">
							<a href="#." class="btn yes_no_btn" data-dismiss="modal" data-toggle="modal" data-target="#congratulations_popup">Yes</a>
							<a href="#." class="btn yes_no_btn" data-dismiss="modal">No</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- succesfully changes popup end Here -->


<!--congratulations popup Start Here -->
<div class="modal fade" id="congratulations_popup" role="dialog">
	<div class="modal-dialog congratulation_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url(); ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="moel_details">
					<span class="model_header">
						<img src="<?php echo base_url(); ?>assets/student/img/congratulation.png" class="img-responsive" alt="forgotpassword"/>
						<h5>Congratulations !</h5>
					</span>
					<div class="model_content">
						<div class="your_score_popup">
							<p>You have Successfully submitted your test.</p>
							<div class="your_scor_here">
								<h5>Your Score:</h5>
								<span class="score_popup">30%</span>
								<p>Remark:Your perfomance is not satisfactory.<br> You have to view the video<br> again . All the best</p>
							</div>
						</div>
						<div class="pop_btn_sect_new">
							<button type="button" class="btn popup_coom_btn">OK</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- congratulations popup end Here -->
<!-- popup end Here -->