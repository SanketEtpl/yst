
<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url();?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>SEARCH TOPICS</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- manage course section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<form action="<?php base_url()?>search-select-topic" method="post" id="content-form" class="form-horizontal" enctype="multipart/form-data">
					<div class="section_details">
						<div class="section_heading_sec">
							<h4>SEARCH BY</h4>
						</div>
						<div class="saerch_user_form">
							<div class="row">
								<div class="col-md-3">
									<div class="form_set">
										<label>Search By</label>
										<select id="search_by" class="form-control">
											<option value="">---Select---</option>
											<option value="1">Course</option>
											<option value="2">Subject</option>
											<option value="3">Topic</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row serach_div">
								<div class="col-md-3">
									<div class="form_set" id="course_div">
										<label>Course</label>
										<select id="course" name="course" class="form-control">
											<option value="0">--Select Course--</option>
											<?php foreach ($course_list as $key => $value) {?>
												<option value="<?php echo $value['course_id'];?>" <?php if(isset($_POST['course']) && $_POST['course']==$value['course_id']) 
												echo ' selected';?>><?php echo $value['course_name'];?></option>
											<?php }?>
										</select>
									</div>
								</div>
								<div class="col-md-3" id="subjects_div" style="display:none">
									<div class="form_set">
										<label>Subjects</label>
										<select id="subjects" name="subject" class="form-control">
										</select>
									</div>
								</div>
								<div class="col-md-3" id="topic_div" style="display:none">
									<div class="form_set">
										<label>Topic</label>
										<select id="topic" name="topic" class="form-control">
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="btn_group_set">
										<button type="submit" class="btn search_cancel_btn margin-right-10px">Search</button>
										<button type="button" class="btn search_cancel_btn">Cancel</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
	<!--	<div class="section_details">
			<div class="section_heading_sec">
				<h4>RESULT LIST</h4>
			</div>
			<div class="cours_table">
				<div class="table-responsive">
				<form id="topiclist">
				  <table class="table cours_table_set">
					<thead>
					  <tr>
						<th>SN</th>
						<th>Course</th>
						<th>Subject</th>
						<th>Action</th>
					  </tr>
					</thead>
					<tbody>
					<?php /*$i=1; foreach ($topic_list as $key => $value) {?>
					  <tr>
						<td><?php echo $i;?></td>
						<td><?php echo $value['course_name'];?></td>
						<td><?php echo $value['subject_name'];?></td>
						<td>
						<?php $cid=$value['course_id'];?>
							<div class="delet_edit_table_link_new">
								<a href="<?php echo base_url(); ?>student/Topic/view_course_details?course_id=<?php echo base64_encode($cid) ;?>">
									<i class="fa fa-eye" aria-hidden="true"></i>
								</a>

							<!--<i class="fa fa-eye" onclick="view_course_details(<?php echo $value['course_id'];?>)" aria-hidden="true"></i>
							</div>-->
						</td>
					  </tr>
					   <?php $i++;}*/?>
					</tbody>
				 </table>
				 </form>
				</div>
				<div class="tables_pagination">
					<ul class="pagination">
						<li><a href="#." class="pagination_arrow"><i class="fa fa-arrow-left" aria-hidden="true"></i></a></li>
						<li class="active"><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#." class="pagination_arrow"><i class="fa fa-arrow-right" aria-hidden="true"></i></a></li>
					</ul> 
				</div>
			</div>
		</div>-->
        <?php if(!empty($course_details)) { ?>
			<!-- banner section start here-->
			<section class="banner_section" style="background-image:url('<?php echo base_url();?>assets/img/mid-bg.jpg');">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="banner_content">
								<h4>VIEW DETAILS</h4>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- banner section end here-->
			<!-- manage course section start here-->
		
		<section class="web_section_common">
			<div class="container">
				<div class="row">
					<div class="col-md-12">									
						<div class="section_details">
							<div class="view_details_collapse">
								<div class="fancy-collapse-panel">
									<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingOne">
												<h4 class="panel-title">
													<div class="cheak_box_here">
														<div class="round">
															<input type="checkbox" id="checkbox" />
															<label for="checkbox"></label>
														</div>
													</div>
													<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
														<div class="coyes_view_detilas_panel">
															<div class="cours_name">COURSE NAME - <?php echo $course_details[0]['course']?></div>
															<div class="cours_cost">
																COST-$<?php echo $course_details[0]['course_cost']?>
															</div>
														</div>
													</a>
												</h4>
											</div>
											<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
												<div class="view_detils_subject">
													<div class="panel-group" id="accordion02" role="tablist" aria-multiselectable="true">
														<?php foreach($course_details[0]['subject'] as $subject) { ?>
														<div class="panel panel-default">
															<div class="panel-heading" role="tab" id="headingtwo">
																<h4 class="panel-title">
																	<div class="cheak_box_here">
																		<div class="round">
																			<input type="checkbox" id="checkbox1" />
																			<label for="checkbox1"></label>
																		</div>
																	</div>
																	<a data-toggle="collapse" data-parent="#accordion02" href="#collapsetwo" aria-expanded="true" aria-controls="collapsetwo">
																		<div class="coyes_view_detilas_panel">																				
																			<div class="cours_name"><?php echo $subject['subject_name']?></div>
																			<div class="cours_cost">
																				COST-$<?php echo $subject['subject_cost']?>
																			</div>
																		</div>
																	</a>
																</h4>
															</div>
															<div id="collapsetwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingtwo">
																<div class="panel-body">
																	<div class="cours_table">
																		<div class="table-responsive">
																			<table class="table cours_table_set">
																				<thead>
																					<tr>
																						<th>
																							<div class="table_checkbox">
																								<div class="round">
																									<input type="checkbox" class="select_check" id="checkbox5" value="false" />
																									<label for="checkbox5"></label>
																								</div>
																							</div>
																							Select All
																						</th>
																						<th>Topic</th>
																						<th>Cost</th>
																						<th>Ratings</th>
																						<th>Preview</th>
																					</tr>
																				</thead>
																				<tbody>
																					<?php 
																					$i=1;
																					foreach($subject['topic'] as $topic) { ?>
																					<tr>
																						<td>
																							<div class="table_checkbox">
																								<div class="round">
																									<input type="checkbox" class="topic_check" id="checkbox6" value="false" />
																									<label for="checkbox6"></label>
																								</div>
																							</div>
																							<?php echo $i;?>
																						</td>
																						<td><?php echo $topic['topic_name']?></td>
																						<td>$<?php echo $topic['topic_cost']?></td>
																						<td>
																							<div class="star_ratting_table_views">
																								<ul>
																									<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																									<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																									<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																									<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																									<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																								</ul>
																							</div>
																						</td>
																						<td>
																							<div class="delet_edit_table_link_new">
																								<a href="#." data-toggle="modal" data-target="#view_course_video_set_popup">
																									<i class="fa fa-video-camera" aria-hidden="true"></i>
																								</a>
																							</div>
																						</td>
																					</tr>
																				<?php $i++ ; } ?>
																				</tbody>
																			</table>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													    <?php } ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>					
			<!-- manage course section end here-->
		<!-- offer section start here-->
		<section class="offer_section">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="offer_details_section">
							<div class="offier_blog_here">
								<div class="row">
									<div class="col-md-3">
										<div class="offer_blog">
											<div class="offer_name_detils">
												<h4>OFFER 1</h4>
												<p>Purchese 3 topic - $300 and get a topic free</p>
											</div>
											<div class="offer_price">
												$300
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="offer_blog">
											<div class="offer_name_detils">
												<h4>OFFER 2</h4>
												<p>Purchese a Complete COURSE - $400</p>
											</div>
											<div class="offer_price">
												$300
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="offer_blog">
											<div class="offer_name_detils">
												<h4>OFFER 3</h4>
												<p>Purchese 2 Topics - $200</p>
											</div>
											<div class="offer_price">
												$200
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="offer_blog">
											<div class="offer_name_detils">
												<h4>OFFER 4</h4>
												<p>Purchese any 3 topic - $250</p>
											</div>
											<div class="offer_price">
												$250
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="offer_total_cost">
								<div class="row">
									<div class="col-md-3">
										<div class="form_set">
											<label>Total Course Cost</label>
											<input type="text" class="form-control" placeholder="cost" id="course_cost"/>
										</div>
									</div>
								</div>
								<div class="schedul_btn_group">
									<a href="#." class="btn save_cancel_btn" data-toggle="modal" data-target="#payment_gateway_popup">Purchase</a>
									<a href="#." class="btn save_cancel_btn">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php } else { ?>
			<h2>No data found.</h2>
		<?php } ?>
		<!-- offer section end here-->

		<!-- popup start Here -->
		
		<!--view course details video popup start here-->
		<div class="modal fade" id="view_course_video_set_popup" role="dialog">
			<div class="modal-dialog ">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<img src="<?php echo base_url() ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
						</button>
					</div>
					<div class="modal-body">
						<div class="moel_details_new">
							<div class="section_heading_sec">
								<h4>LASSION VIDEO</h4>
							</div>
							<div class="model_content">
								<div class="video_set_pop_main">
									<video id="video" poster="img/video_pop_img.jpg" controls="">
										<source src="<?php echo base_url() ?>assets/student/video/how_it_work.mp4" type="video/mp4">
										</video>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--view course details video popup end here-->
			
			<!-- paymetn getway popup Start Here -->
			<div class="modal fade" id="payment_gateway_popup" role="dialog">
				<div class="modal-dialog delet_cours_popup_width">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<img src="<?php echo base_url() ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
							</button>
						</div>
						<div class="modal-body">
							<div class="moel_details">
								<div class="section_heading_sec">
									<h4>PAYMENT GETWAY</h4>
								</div>
								<div class="ammoun_popup_content">
									<h4>Amount</h4>
									<span class="ammout_rs">
										$450
									</span>
								</div>
								<div class="ammount_popup_btn_new">
									<a href="#." class="btn save_cancel_btn" data-dismiss="modal" data-toggle="modal" data-target="#success_fully_done_popup">Proceed</a>
									<a href="#." class="btn save_cancel_btn" data-dismiss="modal">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- paymetn getway popup end Here -->
			
			<!--succesfully changes popup Start Here -->
			<div class="modal fade" id="success_fully_done_popup" role="dialog">
				<div class="modal-dialog delet_cours_popup_width">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<img src="<?php echo base_url() ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
							</button>
						</div>
						<div class="modal-body">
							<div class="moel_details">
								<span class="model_header">
									<img src="<?php echo base_url() ?>assets/student/img/correct-icon.png" class="img-responsive" alt="forgotpassword"/>
									<h5>Successfully Done</h5>
								</span>
								<div class="model_content">
									<p>The payment was succesful,<br>
									$450 has been deducted from your account.</p>
									<div class="pop_btn_sect_new">
										<button type="button" class="btn popup_coom_btn">OK</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- succesfully changes popup end Here -->
			
			<!-- popup end Here -->
		</div>


	</div>
</div>
</div>
</section>
<!-- manage course section end here-->
<script type="text/javascript">
    var base_url = '<?php echo base_url();?>';//alert(base_url);
</script>
<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/search_topic.js"></script>