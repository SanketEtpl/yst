<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url();?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>MY ACCOUNT</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- edit schedule section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					<div class="section_heading_sec">
						<h4>VIEW QUESTIONS AND ANSWERS</h4>
						<a href="<?php echo base_url(); ?>my-account" class="back_page"><img src="<?php echo base_url();?>assets/img/back_arrow.png" class="img-responsive" alt="back"></a>
					</div>
					<div class="que_ans_level">
						<div class="level_head">LEVEL:11</div>
						<div class="level_details">
							<div class="video_topic_details">
								<div class="topic_name">
									<label>Course Name:</label>
									<span>Mathematics</span>
								</div>
								<div class="tutor">
									<label>Subject Name:</label>
									<span>Algebra</span>
								</div>
								<div class="tutor">
									<label>Topic Name:</label>
									<span>Trignometry</span>
								</div>
							</div>
						</div>
					</div>
					<div class="session_all_order_list_here">
						<div class="scrollbar-inner custom_session_que_ans_height_custom">
							<div class="session_order_lists">
								<div class="session_questions">
									Q.1 The difference between two numbers is 4 and the difference between squares is 128. Which is the larger number?
								</div>
								<div class="ans_here">
									<div class="answer_heading">Answer:</div>
									<div class="answer_width_set">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test1" name="radio-group" checked="">
													<label for="test1">	01) 14</label>
												</span>
											</div>
										</div>
									</div>
									<div class="answer_width_set">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test2" name="radio-group" checked="">
													<label for="test2">	02) 16</label>
												</span>
											</div>
										</div>
									</div>
									<div class="answer_width_set">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test3" name="radio-group" checked="">
													<label for="test3">	03) 12</label>
												</span>
											</div>
										</div>
									</div>
									<div class="answer_width_set_new">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test5" name="radio-group" checked="">
													<label for="test5">	04) None of these Regular Method</label>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="session_order_lists">
								<div class="session_questions">
									Q.1 The difference between two numbers is 4 and the difference between squares is 128. Which is the larger number?
								</div>
								<div class="ans_here">
									<div class="answer_heading">Answer:</div>
									<div class="answer_width_set">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test1" name="radio-group" checked="">
													<label for="test1">	01) 14</label>
												</span>
											</div>
										</div>
									</div>
									<div class="answer_width_set">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test2" name="radio-group" checked="">
													<label for="test2">	02) 16</label>
												</span>
											</div>
										</div>
									</div>
									<div class="answer_width_set">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test3" name="radio-group" checked="">
													<label for="test3">	03) 12</label>
												</span>
											</div>
										</div>
									</div>
									<div class="answer_width_set_new">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test5" name="radio-group" checked="">
													<label for="test5">	04) None of these Regular Method</label>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="session_order_lists">
								<div class="session_questions">
									Q.1 The difference between two numbers is 4 and the difference between squares is 128. Which is the larger number?
								</div>
								<div class="ans_here">
									<div class="answer_heading">Answer:</div>
									<div class="answer_width_set">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test1" name="radio-group" checked="">
													<label for="test1">	01) 14</label>
												</span>
											</div>
										</div>
									</div>
									<div class="answer_width_set">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test2" name="radio-group" checked="">
													<label for="test2">	02) 16</label>
												</span>
											</div>
										</div>
									</div>
									<div class="answer_width_set">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test3" name="radio-group" checked="">
													<label for="test3">	03) 12</label>
												</span>
											</div>
										</div>
									</div>
									<div class="answer_width_set_new">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test5" name="radio-group" checked="">
													<label for="test5">	04) None of these Regular Method</label>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="session_order_lists">
								<div class="session_questions">
									Q.1 The difference between two numbers is 4 and the difference between squares is 128. Which is the larger number?
								</div>
								<div class="ans_here">
									<div class="answer_heading">Answer:</div>
									<div class="answer_width_set">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test1" name="radio-group" checked="">
													<label for="test1">	01) 14</label>
												</span>
											</div>
										</div>
									</div>
									<div class="answer_width_set">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test2" name="radio-group" checked="">
													<label for="test2">	02) 16</label>
												</span>
											</div>
										</div>
									</div>
									<div class="answer_width_set">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test3" name="radio-group" checked="">
													<label for="test3">	03) 12</label>
												</span>
											</div>
										</div>
									</div>
									<div class="answer_width_set_new">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test5" name="radio-group" checked="">
													<label for="test5">	04) None of these Regular Method</label>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="session_order_lists">
								<div class="session_questions">
									Q.1 The difference between two numbers is 4 and the difference between squares is 128. Which is the larger number?
								</div>
								<div class="ans_here">
									<div class="answer_heading">Answer:</div>
									<div class="answer_width_set">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test1" name="radio-group" checked="">
													<label for="test1">	01) 14</label>
												</span>
											</div>
										</div>
									</div>
									<div class="answer_width_set">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test2" name="radio-group" checked="">
													<label for="test2">	02) 16</label>
												</span>
											</div>
										</div>
									</div>
									<div class="answer_width_set">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test3" name="radio-group" checked="">
													<label for="test3">	03) 12</label>
												</span>
											</div>
										</div>
									</div>
									<div class="answer_width_set_new">
										<div class="ans_option">
											<div class="radio_btn_group">
												<span>
													<input type="radio" id="test5" name="radio-group" checked="">
													<label for="test5">	04) None of these Regular Method</label>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="que_ans_btn">
						<a href="#." class="btn save_cancel_btn" data-toggle="modal" data-target="#success_fully_submit_popup">Submit</a>
						<a href="#." class="btn save_cancel_btn">Cancel</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- edit schedule section end here-->

<!-- popup start Here -->

<!--succesfully changes popup Start Here -->
<div class="modal fade" id="success_fully_submit_popup" role="dialog">
	<div class="modal-dialog delet_cours_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url() ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="moel_details">
					<span class="model_header">
						<img src="<?php echo base_url() ?>assets/student/img/correct-icon.png" class="img-responsive" alt="forgotpassword"/>
						<h5>Successfully Submit</h5>
					</span>
					<div class="model_content">
						<p>Do you really want to submit your test?</p>
						<div class="btn_group_popup">
							<a href="#." class="btn yes_no_btn" data-dismiss="modal" data-toggle="modal" data-target="#congratulations_popup">Yes</a>
							<a href="#." class="btn yes_no_btn" data-dismiss="modal">No</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- succesfully changes popup end Here -->


<!--congratulations popup Start Here -->
<div class="modal fade" id="congratulations_popup" role="dialog">
	<div class="modal-dialog congratulation_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url() ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="moel_details">
					<span class="model_header">
						<img src="<?php echo base_url() ?>assets/student/img/congratulation.png" class="img-responsive" alt="forgotpassword"/>
						<h5>Congratulations !</h5>
					</span>
					<div class="model_content">
						<div class="your_score_popup">
							<p>You have Successfully submitted your test.</p>
							<div class="your_scor_here">
								<h5>Your Score:</h5>
								<span class="score_popup">30%</span>
								<p>Remark:Your perfomance is not satisfactory.<br> You have to view the video<br> again . All the best</p>
							</div>
						</div>
						<div class="pop_btn_sect_new">
							<button type="button" class="btn popup_coom_btn">OK</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- congratulations popup end Here -->
<!-- popup end Here -->  