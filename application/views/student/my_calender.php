<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url(); ?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>MY CALENDER</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- schedule new session section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					<div class="row">
						<div class="col-md-9">
							<div class="calend_set_new">
								<div class="calender_set_form">
									<div class="calend_btn_group">
										<a href="#." class="btn calender_btn">Today</a>
										<a href="<?php echo base_url(); ?>book-session" class="btn calender_btn">Book a Session</a>
									</div>
									<div class="calender_form_set">
										<div class="row">
											<div class="col-md-6">
												<div class="form_set">
													<select class="form-control">
														<option selected>Select Status:</option>
														<option>Daily</option>
														<option>Weekly</option>
														<option>Yearly</option>
													</select>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form_set">
													<input class="form-control" id="search" placeholder="Search"/>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- <div class="calender_page_calender_set">
									<a href="#."><image src="<?php echo base_url(); ?>assets/student/img/calender.jpg" class="img-responsive" alt="calender"/></a>
								</div> -->
								<div class="calender">
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="session_details_list">
								<div class="session_list_heading">
									<h4>SESSION BOOKING DETAILS</h4>
								</div>
								<div class="session_list_blog">
									<?php if(!empty($session_data)) { 
										foreach($session_data as $session) { 	
									?>
									<div class="session_list_blog_list_here">
										<h4 class="session_heading_date">
											<?php echo date('d F Y',strtotime($session['date']))?>
										</h4>
										<div class="session_blog_list">
											<ul>
												<li><span>Date:</span><?php echo date('d-m-Y',strtotime($session['date']))?></li>
												<li><span>Session Type:</span><?php echo ucfirst($session['conducted_flag']);?></li>
												<li><span>Course:</span><?php echo $session['course_name'];?></li>
												<li><span>Subject:</span><?php echo $session['subject_name'];?></li>
												<li><span>Topic:</span><?php echo $session['topic_name'];?></li>
											</ul>
										</div>
									</div>
									<?php } } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- schedule new session section end here-->

<!--event popup start here-->
<div class="modal fade" id="event_content_popup" role="dialog">
	<div class="modal-dialog add_topic_width_popup">
	<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal">
			      <img src="<?php echo base_url();?>assets/admin/img/pop_up_close.png" class="img-responsive" alt="close"/>
			    </button>
			</div>
			<div class="modal-body">
			<div class="moel_details_new">
				<div class="section_heading_sec dotted_bottom_border">
				<h4>SESSION DETAILS</h4>
				</div>
				<div class="model_content">
					<div class="add_topic_form">
					    <div class="session_list_blog_list_here">
							<h4 class="session_heading_date heading_sec">
								
							</h4>
							<div class="session_blog_list">
								<ul>
									<li><span>Date:</span><span class="stud_date"></span></li>
									<li><span>Session Type:</span><span class="type"></span></li>
									<li><span>Course:</span><span class="course"></span></li>
									<li><span>Subject:</span><span class="subject"></span></li>
									<li><span>Topic:</span><span class="topic"></span></li>
								</ul>
							</div>
					    </div>
					</div>
				</div>
			 </div>
			</div>
		</div>
	</div>
</div>
<!--set availability popup end here-->
<!--Schedule session popup start here-->
<div class="modal fade" id="schedule_session_popup" role="dialog">
	<div class="modal-dialog add_topic_width_popup">
	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">
			<img src="<?php echo base_url();?>assets/admin/img/pop_up_close.png" class="img-responsive" alt="close"/>
		</button>
		</div>
		<div class="modal-body">
			<form id="addSessionfrom">
				<div class="moel_details_new">
					<div class="section_heading_sec dotted_bottom_border">
						<h4>SCHEDULE SESSION</h4>
					</div>
					<div class="model_content">
						<div class="col-md-12">
							<div class="alert alert-success" style="display:none">
								<strong id="msg1" ></strong> 
							</div>
						</div>
						<div class="add_topic_form">
								<div class="row">
									<div class="col-md-4">
										<div class="form_set">
											<label>Course <sup class="star_class">*</sup></label>
											<select class="form-control select_course" onchange="getSubjectList();" name='course_id'>
												<option>Select Course</option>
												<?php if(!empty($course_list)) {
													foreach($course_list as $course) { 
												?>
												<option value="<?php echo $course['course_id']?>"><?php echo $course['course_name']?></option>
												<?php }  } ?>
											</select>
											<span class="err_course1" style="display:none;color:red">Please Select course</span>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form_set">
											<label>Subject <sup class="star_class">*</sup></label>
											<select class="form-control select_subjects" onchange="getTopicList();" name="subject_id">
												<option>Select Subject</option>
											</select>
											<span class="err_subject1" style="display:none;color:red">Please Select subject</span>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form_set">
											<label>Topic <sup class="star_class">*</sup></label>
											<select class="form-control select_topics" name="topic_id">
												<option>Select Topic</option>
											</select>
											<span class="err_topic1" style="display:none;color:red">Please Select topic</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="add_topic_form">
							<div class="row"> 
								<div class="col-md-4">
									<div class="form_set">
										<label>Session Title <sup class="star_class">*</sup></label>
										<input class="form-control" id="title" name="title" placeholder="Title">
										<span class="err_title1" style="display:none;color:red">Please enter title</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form_set nomarg">
										<label>Date <sup class="star_class">*</sup></label>
										<div class="form-group calend_div">
											<div class="input-group date form_date" data-date="" data-date-format="yyyy-m-d" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
												<input class="form-control calender_set" value="" name = "date" readonly="" type="text" id="date" placeholder="Date">
												<span class="input-group-addon custom_input_calend fa_img_color"><i class="fa fa-calendar" aria-hidden="true"></i></span>
											</div>
											<input id="dtp_input2" value="" type="hidden"/>
											<span class="err_date1" style="display:none;color:red">Please select date</span>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form_set">
										<label>Start time <sup class="star_class">*</sup></label>
										<!-- <input class="form-control start_time" id="start_time" name="start_time"> -->
										<div class="input_boxes_set">
											<div class="form-group calend_div_new">
												<div class="input-group date form_time" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
													<input class="form-control calender_set required_check" size="16" type="text"  name="start_time" id="start_time" placeholder="00:00">
													<div class="input-group-addon custom_input_calend"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
												</div>
												<input type="hidden" id="dtp_input3" value="">
											</div>
										</div>
										<span class="err_sdate1" style="display:none;color:red">Please select start time</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form_set">
										<label>End time<sup class="star_class">*</sup></label>
										<!-- <input class="form-control end_time" id="end_time" name="end_time"> -->
										<div class="input_boxes_set">
											<div class="form-group calend_div_new">
												<div class="input-group date form_time" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
													<input class="form-control calender_set required_check" size="16" type="text"  name="end_time" id="end_time" placeholder="00:00">
													<div class="input-group-addon custom_input_calend"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
												</div>
												<input type="hidden" id="dtp_input3" value="">
											</div>
										</div>
										<span class="err_edate1" style="display:none;color:red">Please select end time</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form_set">
										<label>Total Duration <sup class="star_class">*</sup></label>
										<input class="form-control" id="tot_duration" name="duration_in_minutes" placeholder="Duration" readonly>
										<span class="err_duration1" style="display:none;color:red">Please enter duration</span>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form_set">
										<label>Cost <sup class="star_class">*</sup></label>
										<input class="form-control cost" id="cost" name="cost" placeholder="100">
										<span class="err_cost1" style="display:none;color:red">Please enter cost</span>
									</div>
								</div>
								<input type="hidden" name="conducted_flag" value="one-to-one">
							</div>
							
						</div>
					</div>
				</div>
				<div class="add_topic_btn_new">
					<a href="#." class="btn save_cancel_btn" onclick="add_session();">SAVE</a>
					<a href="#." class="btn save_cancel_btn" data-dismiss="modal">Cancel</a>
				</div>
			</form>
		</div>
	</div>
	</div>
</div>
<!--Schedule session popup end here-->
<script src="<?php echo base_url();?>assets/js/calender.js"></script>
<script>
 $(".calender").fullCalendar({  
        header: {
            left: 'prev,next today',
            center: 'title',
            right:  'prevYear,nextYear, agendaDay,agendaWeek,month'
		},
		dayRender: function (date, cell) {
			var date    = new Date(date);
			// console.log(date);
			yr      = date.getFullYear();
			month   = date.getMonth();
			day     = date.getDate();
			check = yr + '-' + month + '-' + day;
			
			var today = new Date();
			yr1      = today.getFullYear();
			month1  = today.getMonth();
			day1  = today.getDate();
			//today = yr1 + '-' + month1 + '-' + day1;
			var today = ((today.getDate().length+1) === 1)? (today.getDate()+1) :(today.getDate()+1);
		    console.log(today);
			if (check < today) {
				cell.css("background-color", "red");
			}
		},
		events :
		[
			<?php 
			 if(!empty($upcoming_session)){
		     	for ($i=0; $i <count($upcoming_session); $i++) { 
			?> 
				{
					title : '<?php echo $upcoming_session[$i]['title']; ?>',
					start : '<?php echo $upcoming_session[$i]['date'].'T'.$upcoming_session[$i]['start_time']; ?>',
					link : '<?php echo $upcoming_session[$i]['session_id']; ?>',
					course_name : '<?php echo $upcoming_session[$i]['course_name']; ?>',
					subject_name : '<?php echo $upcoming_session[$i]['subject_name']; ?>',
					topic_name : '<?php echo $upcoming_session[$i]['topic_name']; ?>',
					date : '<?php echo $upcoming_session[$i]['date']; ?>',
					conducted_flag : '<?php echo $upcoming_session[$i]['conducted_flag']; ?>',
					backgroundColor: '#4c9cff',
					eventType : 'UpcomingSession'
				},
			<?php }  } ?>
			<?php
			if(!empty($past_session)) {  
				for ($i=0; $i <count($past_session); $i++) { ?> 
				{
					title : '<?php echo $past_session[$i]['title']; ?>',
					start : '<?php echo $past_session[$i]['date'].'T'.$past_session[$i]['start_time']; ?>',
					link : '<?php echo $past_session[$i]['session_id']; ?>',
					course_name : '<?php echo $past_session[$i]['course_name']; ?>',
					subject_name : '<?php echo $past_session[$i]['subject_name']; ?>',
					topic_name : '<?php echo $past_session[$i]['topic_name']; ?>',
					date : '<?php echo $past_session[$i]['date']; ?>',
					conducted_flag : '<?php echo $past_session[$i]['conducted_flag']; ?>',
					backgroundColor: '#3c763d',
					eventType : 'PastSession'
				},
			<?php } } ?>
			<?php 
			if(!empty($availability_date)) { 
				for ($i=0; $i <count($availability_date); $i++) { ?> 
				{
					start : '<?php echo $availability_date[$i]['date'].'T'.$availability_date[$i]['from_time']; ?>',
					end : '<?php echo $availability_date[$i]['date'].'T'.$availability_date[$i]['to_time']; ?>',
					link : '<?php echo $availability_date[$i]['availability_id']; ?>',
					backgroundColor: '#FFA500',
					eventType : 'Available'
				},
			<?php } } ?>
		],
		eventClick: function(event){
			if(event.eventType == 'PastSession'){
				$('.heading_sec').html(event.date);
				$('.stud_date').html(event.date);
				$('.type').html(event.conducted_flag);
				$('.course').html(event.course_name);
				$('.subject').html(event.subject_name);
				$('.topic').html(event.topic_name);
				$('#event_content_popup').modal('show');
			}
			else if(event.eventType == 'UpcomingSession'){
				$('.heading_sec').html(event.date);
				$('.stud_date').html(event.date);
				$('.type').html(event.conducted_flag);
				$('.course').html(event.course_name);
				$('.subject').html(event.subject_name);
				$('.topic').html(event.topic_name);
				$('#event_content_popup').modal('show');
			}
			else if(event.eventType == 'Available'){
				$('#schedule_session_popup').modal('show');
			}
		}
		
	});
</script>