<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url(); ?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>BOOK SESSION</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- book a session edit section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
		       <div class="alert alert-success" style="display:none">
		        <strong id="msg" ></strong> 
		       </div>
		    </div>
			<div class="col-md-12">
				<div class="section_details">
					<div class="my_account_tabbing book_session_tab">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#one_one_live_session">One To One Live Session</a></li>
							<li><a data-toggle="tab" href="#group_live_session">Group Live Session</a></li>
						</ul>
						<div class="tab-content">
							<form id="addindividualSession">
							<div id="one_one_live_session" class="tab-pane fade in active">
								<div class="book_session_edit_section">
									<div class="row">
										<div class="col-md-4">
											<div class="form_set">
												<label>Course Name</label>
												<select class="form-control course-id" name="course_id" onclick="getSubjectlist();">
													<option selected="">Select Course</option>
													<?php if(!empty($course_list)) { 
														foreach($course_list as $course) { 
													?>
													    <option value="<?php echo $course['course_id']?>"><?php echo $course['course_name']?></option>
												    <?php } } ?>
												</select>
												<span class="err_ecourse" style="color: red; display: none;">Please select course.</span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form_set">
												<label>Subject</label>
												<select class="form-control select_subject" name="subject_id" onclick="getTopiclist();">
													<option selected="">Select Subject</option>
												</select>
												<span class="err_esubject" style="color: red; display: none;">Please select subject.</span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form_set">
												<label>Topic</label>
												<select class="form-control select_topic" name="topic_id">
													<option selected="">Select Topic</option>
												</select>
												<span class="err_etopic" style="color: red; display: none;">Please select topic.</span>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form_set">
												<label>Session Title</label>
												<input type="text" name="title" id="title" placeholder="Title" class="form-control">
											</div>
											<span class="err_etitle" style="color: red; display: none;">Please enter title.</span>
										</div>
										<div class="col-md-4">
											<div class="form_set">
												<label>Date</label>
												<div class="form-group calend_div">
													<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
														<input class="form-control calender_set" type="text" value="" name="date" id="select_date" readonly="">
														<span class="input-group-addon custom_input_calend"><i class="fa fa-calendar" aria-hidden="true"></i></span>
													</div>
													<input type="hidden" id="dtp_input2" value="" />
												</div>
											    <span class="err_edate" style="color: red; display: none;">Please select date.</span>
											</div>
										</div>
										<!-- <div class="col-md-4">
											<div class="form_set">
												<label>Date</label>
												<div class="form-group calend_div">
													<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
														<input class="form-control calender_set" type="text" value="" name="date" id="select_date" readonly="">
														<span class="input-group-addon custom_input_calend"><i class="fa fa-calendar" aria-hidden="true"></i></span>
													</div>
													<input type="hidden" id="dtp_input2" value="" />
												</div>
												<span class="err_date" style="color: red; display: none;">Please select date.</span>
											</div>
										</div> -->
										<div class="col-md-4">
											<div class="form_set">
												<label>Start Time</label>
												<input type="text" name="start_time" id="start_time" placeholder="hh:mm:ss" class="form-control">
											</div>
											<span class="err_estime" style="color: red; display: none;">Please select start time.</span>
										</div>
										<div class="col-md-4">
											<div class="form_set">
												<label>End Time</label>
												<input type="text" name="end_time" id="end_time" placeholder="hh:mm:ss" class="form-control">
											</div>
											<span class="err_etime" style="color: red; display: none;">Please select end time.</span>
										</div>
										<div class="col-md-4">
											<div class="form_set">
												<label>Total Duration (In minutes)</label>
												<input type="text" name="duration_in_minutes" id="eduration" placeholder="Duration in minutes" class="form-control">
											</div>
											<span class="err_eduration" style="color: red; display: none;">Please select end time.</span>
										</div>
									</div>
									<input type="hidden" name="conducted_flag" value="one-to-one">
									<div class="session_cost_price">
										<span>SESSION COST</span>
										<input type="hidden" name="cost" value="500">
										<div class="session_price_here">
											$500
										</div>
									</div>
								</div>
								<div class="add_topic_btn_new">
									<!-- <a href="<?php echo base_url(); ?>one-to-one-booking" class="btn save_cancel_btn">Book</a> -->
									<a href="#." onclick="addindividualSession();" class="btn save_cancel_btn">Book</a>
									<a href="#." class="btn save_cancel_btn">Cancel</a>
								</div>

							</div>
						</form>
							<div id="group_live_session" class="tab-pane fade">
								<div class="tabs_set_my_account">
									<div class="section_details">
										<div class="group_live_search_foem">
											<div class="row">
												<div class="col-md-3 group_live_width_cols">
													<div class="form_set">
														<label>Course</label>
														<select class="form-control">
															<option selected="">Select Course</option>
															<option>Course 1</option>
															<option>Course 2</option>
															<option>Course 3</option>
														</select>
													</div>
												</div>
												<div class="col-md-3 group_live_width_cols">
													<div class="form_set">
														<label>Subject</label>
														<select class="form-control">
															<option selected="">Select Subject</option>
															<option>Subject 1</option>
															<option>Subject 2</option>
															<option>Subject 3</option>
														</select>
													</div>
												</div>
												<div class="col-md-3 group_live_width_cols">
													<div class="form_set">
														<label>Topic</label>
														<input type="text" class="form-control" id="time"/>
													</div>
												</div>
												<div class="col-md-3 group_live_width_cols">
													<div class="form_set">
														<label>Duration</label>
														<input type="text" class="form-control" id="time"/>
													</div>
												</div>
												<div class="col-md-3 group_live_width_cols">
													<div class="tabing_form_set_btns">
														<button type="button" class="btn search_cancel_btn ">Search</button>
													</div>
												</div>
											</div>
										</div>
										<div class="session_all_order_list_here">
											<div class="session_order_lists">
												<div class="row">
													<div class="col-md-12">
														<div class="order_list_details">
															<div class="order_list_heading">
																<h4>TOPIC:- INTEGRATION</h4>
																<span class="order_list_date">5 Sept 2018 09:32 AM</span>
															</div>
															<div class="order_list_info">
																<div class="order_list_info_heading">
																	<h4>COURSE : MATHEMATICS</h4>
																</div>
																<div class="order_listr_content">
																	<div class="row">
																		<div class="col-md-2">
																			<div class="order_content">
																				<span>Topic</span>
																				<div>Equations</div>
																			</div>
																		</div>
																		<div class="col-md-2">
																			<div class="order_content">
																				<span>Subject</span>
																				<div>Algebra</div>
																			</div>
																		</div>
																		<div class="col-md-2">
																			<div class="order_content">
																				<span>Duration</span>
																				<div>4hrs</div>
																			</div>
																		</div>
																		<div class="col-md-2">
																			<div class="order_content">
																				<span>No Attendees</span>
																				<div>50</div>
																			</div>
																		</div>
																		<div class="col-md-2">
																			<div class="order_content">
																				<span>Cost</span>
																				<div class="cost_bold_color">$160</div>
																			</div>
																		</div>
																		<div class="col-md-2">
																			<div class="session_list_book_btn_groups">
																				<a href="<?php echo base_url(); ?>group-session-booking" class="btn book_btn">BOOK</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="session_order_lists">
												<div class="row">
													<div class="col-md-12">
														<div class="order_list_details">
															<div class="order_list_heading">
																<h4>TOPIC:- ALGEBRA</h4>
																<span class="order_list_date">5 Sept 2018 09:32 AM</span>
															</div>
															<div class="order_list_info">
																<div class="order_list_info_heading">
																	<h4>COURSE: ARITHMETIC</h4>
																</div>
																<div class="order_listr_content">
																	<div class="row">
																		<div class="col-md-2">
																			<div class="order_content">
																				<span>Topic</span>
																				<div>Algebra</div>
																			</div>
																		</div>
																		<div class="col-md-2">
																			<div class="order_content">
																				<span>Subject</span>
																				<div>Maths</div>
																			</div>
																		</div>
																		<div class="col-md-2">
																			<div class="order_content">
																				<span>Duration</span>
																				<div>2hrs</div>
																			</div>
																		</div>
																		<div class="col-md-2">
																			<div class="order_content">
																				<span>No Attendees</span>
																				<div>45</div>
																			</div>
																		</div>
																		<div class="col-md-2">
																			<div class="order_content">
																				<span>Cost</span>
																				<div class="cost_bold_color">$240</div>
																			</div>
																		</div>
																		<div class="col-md-2">
																			<div class="session_list_book_btn_groups">
																				<a href="<?php echo base_url(); ?>group-session-booking" class="btn book_btn">BOOK</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="session_order_lists">
												<div class="row">
													<div class="col-md-12">
														<div class="order_list_details">
															<div class="order_list_heading">
																<h4>TOPIC:- TRIGNOMETRIC</h4>
																<span class="order_list_date">5 Sept 2018 09:32 AM</span>
															</div>
															<div class="order_list_info">
																<div class="order_list_info_heading">
																	<h4>COURSE: GEOMETRY</h4>
																</div>
																<div class="order_listr_content">
																	<div class="row">
																		<div class="col-md-2">
																			<div class="order_content">
																				<span>Topic</span>
																				<div>Trignometric</div>
																			</div>
																		</div>
																		<div class="col-md-2">
																			<div class="order_content">
																				<span>Subject</span>
																				<div>Maths</div>
																			</div>
																		</div>
																		<div class="col-md-2">
																			<div class="order_content">
																				<span>Duration</span>
																				<div>5hrs</div>
																			</div>
																		</div>
																		<div class="col-md-2">
																			<div class="order_content">
																				<span>No Attendees</span>
																				<div>35</div>
																			</div>
																		</div>
																		<div class="col-md-2">
																			<div class="order_content">
																				<span>Cost</span>
																				<div class="cost_bold_color">$370</div>
																			</div>
																		</div>
																		<div class="col-md-2">
																			<div class="session_list_book_btn_groups">
																				<a href="<?php echo base_url(); ?>group-session-booking" class="btn book_btn">BOOK</a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- book a session edit section end here-->
<script src="<?php echo base_url();?>assets/js/session.js"></script>