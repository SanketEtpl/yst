<!-- banner section start here-->
<section class="banner_section" style="background-image:url('<?php echo base_url(); ?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>MY ACCOUNT</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- edit schedule section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					<div class="my_account_tabbing">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#my_sessions">My Sessions</a></li>
							<li><a data-toggle="tab" href="#my_courses">My Courses</a></li>
						</ul>
						<div class="tab-content">
							<div id="my_sessions" class="tab-pane fade in active">
								<div class="tabs_set_my_account">
									<div class="my_account_session_tabs">
										<ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab" href="#upcoming">UPCOMING</a></li>
											<li><a data-toggle="tab" href="#cancel">CANCELLED</a></li>
											<li><a data-toggle="tab" href="#past">PAST</a></li>
										</ul>
									</div>
									<div class="my_session_content">
										<div class="tab-content">
											<div id="upcoming" class="tab-pane fade in active">
												<div class="my_session_content">
													<div class="section_heading_sec">
														<h4>UPCOMING</h4>
													</div>
													<div class="session_all_order_list_here">
														<?php if(!empty($user_details)) { 
															foreach($user_details as $user) { 	
														?>
															<div class="session_order_lists">
																<div class="row">
																	<!-- <div class="col-md-2">
																		<div class="session_order_img" style="background-image:url('<?php echo base_url(); ?>assets/student/img/trignometry_img.jpg');"></div>
																	</div> -->
																	<div class="col-md-10">
																		<div class="order_list_details">
																			<div class="order_list_heading">
																				<h4>ORDER NO <?php echo isset($user['order_id']) ?  $user['order_id'] : '';?></h4>
																				<span class="order_list_date"><?php echo isset($user['date']) ? date('d F Y',strtotime($user['date'])).' ' : ''?><?php echo isset($user['start_time']) ? date('H:i',strtotime($user['start_time'])) : ''?></span>
																			</div>
																			<div class="order_list_info">
																				<div class="order_list_info_heading">
																					<h4><?php echo isset($user['topic_name']) ? $user['topic_name'] : ''?></h4>
																				</div>
																				<div class="order_listr_content">
																					<div class="row">
																						<div class="col-md-2">
																							<div class="order_content">
																								<span>Course</span>
																								<div><?php echo isset($user['course_name']) ? $user['course_name'] : ''?></div>
																							</div>
																						</div>
																						<div class="col-md-2">
																							<div class="order_content">
																								<span>Subject</span>
																								<div><?php echo isset($user['subject_name']) ? $user['subject_name'] : ''?></div>
																							</div>
																						</div>
																						<div class="col-md-2">
																							<div class="order_content">
																								<span>Cost</span>
																								<div>$<?php echo isset($user['amount']) ? $user['amount'] : ''?></div>
																							</div>
																						</div>
																						<div class="col-md-2">
																							<div class="order_content">
																								<span>Total Duration</span>
																								<div><?php 
																								       $hours = (int)($user['duration_in_minutes'] / 60);
																								       $minutes = $user['duration_in_minutes'] - ($hours * 60);
																										if($hours == '0'){
																											echo $minutes.' '.'min';
																										}
																										else if($minutes == '0'){
																											echo $hours.' '.'hr';
																										}
																										else if($hours != '0' && $minutes != '0'){
																											echo $hours.' '.'hr'.' '.$minutes.' '.'min';
																										}
																								?></div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<?php } } else { ?>
																<div class="session_order_lists">
																  <h4>Data not found...</h4>
																</div>
															<?php  } ?>
														<!-- <div class="session_order_lists">
															<div class="row">
																<div class="col-md-2">
																	<div class="session_order_img" style="background-image:url('<?php echo base_url(); ?>assets/student/img/organic_chemistry_img.jpg');"></div>
																</div>
																<div class="col-md-10">
																	<div class="order_list_details">
																		<div class="order_list_heading">
																			<h4>ORDER NO 8562314</h4>
																			<span class="order_list_date">12 April 2018 09:32 AM</span>
																		</div>
																		<div class="order_list_info">
																			<div class="order_list_info_heading">
																				<h4>ORGANIC CHEMISTRY LIVE SESSION</h4>
																			</div>
																			<div class="order_listr_content">
																				<div class="row">
																					<div class="col-md-2">
																						<div class="order_content">
																							<span>Course</span>
																							<div>Algebra</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="order_content">
																							<span>Subject</span>
																							<div>Maths</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="order_content">
																							<span>Cost</span>
																							<div>$40</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="order_content">
																							<span>Total Duration</span>
																							<div>1 hrs</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div> -->
													</div>
												</div>
											</div>
											<div id="cancel" class="tab-pane fade">
												<div class="my_session_content">
													<div class="section_heading_sec">
														<h4>CANCELLED</h4>
													</div>
													<div class="session_all_order_list_here">
														<div class="session_order_lists">
															<div class="row">
																<div class="col-md-2">
																	<div class="session_order_img" style="background-image:url('<?php echo base_url(); ?>assets/student/img/session_biology.jpg');"></div>
																</div>
																<div class="col-md-10">
																	<div class="order_list_details">
																		<div class="order_list_heading">
																			<h4>ORDER NO 854623</h4>
																			<span class="order_list_date">2 sept 2018 10:40 AM</span>
																		</div>
																		<div class="order_list_info">
																			<div class="order_list_info_heading">
																				<h4>ONE TO ONE SESSION FOR BIOLOGY</h4>
																			</div>
																			<div class="order_listr_content">
																				<div class="row">
																					<div class="col-md-2">
																						<div class="order_content">
																							<span>Course</span>
																							<div>Algebra</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="order_content">
																							<span>Subject</span>
																							<div>Maths</div>
																						</div>
																					</div>
																								<!--<div class="col-md-2">
																									<div class="order_content">
																										<span>Cost</span>
																										<div>$40</div>
																									</div>
																								</div>-->
																								<div class="col-md-2">
																									<div class="order_content">
																										<span>Total Duration</span>
																										<div>1 hrs</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="session_order_lists">
																		<div class="row">
																			<div class="col-md-2">
																				<div class="session_order_img" style="background-image:url('<?php echo base_url(); ?>assets/student/img/group_session.jpg');"></div>
																			</div>
																			<div class="col-md-10">
																				<div class="order_list_details">
																					<div class="order_list_heading">
																						<h4>ORDER NO 985623</h4>
																						<span class="order_list_date">12 May 2018 09:32 AM</span>
																					</div>
																					<div class="order_list_info">
																						<div class="order_list_info_heading">
																							<h4>GROUP SESSION</h4>
																						</div>
																						<div class="order_listr_content">
																							<div class="row">
																								<div class="col-md-2">
																									<div class="order_content">
																										<span>Course</span>
																										<div>Algebra</div>
																									</div>
																								</div>
																								<div class="col-md-2">
																									<div class="order_content">
																										<span>Subject</span>
																										<div>Maths</div>
																									</div>
																								</div>
																								<!--<div class="col-md-2">
																									<div class="order_content">
																										<span>Cost</span>
																										<div>$40</div>
																									</div>
																								</div>-->
																								<div class="col-md-2">
																									<div class="order_content">
																										<span>Total Duration</span>
																										<div>1 hrs</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>													  
														</div>
														<div id="past" class="tab-pane fade">
															<div class="my_session_content">
																<div class="section_heading_sec">
																	<h4>PAST</h4>
																</div>
																<div class="session_all_order_list_here">
																   <?php if(!empty($past_session)) { 
																	   foreach($past_session as $key=>$past) { 
																	?>
																	<div class="session_order_lists">
																		<div class="row">
																			<!-- <div class="col-md-2">
																				<div class="session_order_img" style="background-image:url('<?php echo base_url(); ?>assets/student/img/session_biology.jpg');"></div>
																			</div> -->
																			<div class="col-md-10">
																				<div class="order_list_details">
																					<div class="order_list_heading">
																						<h4>ORDER NO <?php echo isset($past['order_id']) ? $past['order_id'] : '';?></h4>
																						<span class="order_list_date"><?php echo isset($past['date']) ? date('d F Y',strtotime($past['date'])).' ' : '';?><?php echo isset($past['start_time']) ? date('H:i',strtotime($past['start_time'])) : '';?></span>
																					</div>
																					<div class="order_list_info">
																						<div class="order_list_info_heading">
																							<h4><?php echo isset($past['topic_name']) ? $past['topic_name'] : '';?></h4>
																						</div>
																						<div class="order_listr_content">
																							<div class="row">
																								<div class="col-md-1">
																									<div class="order_content">
																										<span>Course</span>
																										<div><?php echo isset($past['course_name']) ? $past['course_name'] : '';?></div>
																									</div>
																								</div>
																								<div class="col-md-1">
																									<div class="order_content">
																										<span>Subject</span>
																										<div><?php echo isset($past['topic_name']) ? $past['topic_name'] : '';?></div>
																									</div>
																								</div>
																								<div class="col-md-2">
																									<div class="order_content">
																										<span>Total Duration</span>
																										<div>
																										<?php 
																											$hours = (int)($past['duration_in_minutes'] / 60);
																											$minutes = $past['duration_in_minutes'] - ($hours * 60);
																												if($hours == '0'){
																													echo $minutes.' '.'min';
																												}
																												else if($minutes == '0'){
																													echo $hours.' '.'hr';
																												}
																												else if($hours != '0' && $minutes != '0'){
																													echo $hours.' '.'hr'.' '.$minutes.' '.'min';
																												}
																										?>
																										</div>
																									</div>
																								</div>
																								<div class="col-md-2">
																									<div class="order_content">
																										<span>Ratings</span>
																										<div class="star_ratting_table_views">
																											<ul>
																											   <?php 
																											   $flag = '0';
																											   for($i=1;$i<=5;$i++) { 
																												   if($flag == '0'){
																												?>
																													<li><a href="#." ><i class="fa fa-star" aria-hidden="true"></i></a></li>
																											   <?php  } else { ?>
																												    <li><a href="#." ><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																											   <?php } 
																													if($i==$past['rating']){
																														$flag = '1';	
																													}
																											    } ?>
																												<!-- <li><a href="#." value='1'><i class="fa fa-star" aria-hidden="true"></i></a></li>
																												<li><a href="#." value='2'><i class="fa fa-star" aria-hidden="true"></i></a></li>
																												<li><a href="#." value='3'><i class="fa fa-star" aria-hidden="true"></i></a></li>
																												<li><a href="#." value='4'><i class="fa sssssfa-star-o" aria-hidden="true"></i></a></li>
																												<li><a href="#." value='5'><i class="fa fa-star-o" aria-hidden="true"></i></a></li> -->
																											</ul>
																										</div>
																										<input type="hidden" value="<?php echo isset($past['rating']) ? $past['rating'] : '';?>" class="rating<?php echo $key;?>">
																									</div>
																								</div>
																								<div class="col-md-2">
																									<div class="order_content_video">
																										<!-- <span class="video_info_img">
																											<img src="<?php echo base_url(); ?>assets/student/img/session_biology.jpg" class="img-responsive"/>
																										</span> -->
																										<a href="<?php echo base_url() ?>view-quest-answers"><img src="<?php echo base_url(); ?>assets/student/img/video_record.png" class="img-responsive" alt="video"/></a>
																									</div>
																								</div>
																								<div class="col-md-3">
																									<div class="rebook_buttons">
																										<button type="btn" class="btn rebook_comm-btn">Re-Book Tutor</button>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<?php } } ?>
																	<!-- <div class="session_order_lists">
																		<div class="row">
																			<div class="col-md-2">
																				<div class="session_order_img" style="background-image:url('<?php echo base_url(); ?>assets/student/img/group_session.jpg');"></div>
																			</div>
																			<div class="col-md-10">
																				<div class="order_list_details">
																					<div class="order_list_heading">
																						<h4>ORDER NO 985623</h4>
																						<span class="order_list_date">12 May 2018 09:32 AM</span>
																					</div>
																					<div class="order_list_info">
																						<div class="order_list_info_heading">
																							<h4>GROUP SESSION</h4>
																						</div>
																						<div class="order_listr_content">
																							<div class="row">
																								<div class="col-md-1">
																									<div class="order_content">
																										<span>Course</span>
																										<div>Algebra</div>
																									</div>
																								</div>
																								<div class="col-md-1">
																									<div class="order_content">
																										<span>Subject</span>
																										<div>Maths</div>
																									</div>
																								</div>
																								<div class="col-md-2">
																									<div class="order_content">
																										<span>Total Duration</span>
																										<div>1 hrs</div>
																									</div>
																								</div>
																								<div class="col-md-2">
																									<div class="order_content">
																										<span>Ratings</span>
																										<div class="star_ratting_table_views">
																											<ul>
																												<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																												<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																												<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																												<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																												<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																											</ul>
																										</div>
																									</div>
																								</div>
																								<div class="col-md-2">
																									<div class="order_content_video">
																										<span class="video_info_img">
																											<img src="<?php echo base_url(); ?>assets/student/img/session_biology.jpg" class="img-responsive"/>
																										</span>
																										<a href="<?php echo base_url() ?>view-quest-answers"><img src="<?php echo base_url(); ?>assets/student/img/video_record.png" class="img-responsive" alt="video"/></a>
																									</div>
																								</div>
																								<div class="col-md-3">
																									<div class="rebook_buttons">
																										<button type="btn" class="btn rebook_comm-btn">Re-Book Tutor</button>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div> -->
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="my_courses" class="tab-pane fade">
											<div class="tabs_set_my_account">
												<div class="section_details">
													<div class="course_page_heading">
														<h4>MY COURSES</h4>
														<div class="my_course_input_form">
															<input type="text" class="form-control" id="my_course"/>
															<a href="#." class="btn filter-comm-btn">
																<img src="<?php echo base_url(); ?>assets/student/img/filter.png" class="img-responsive" alt="filter"/>
															</a>
														</div>
													</div>
													<div class="session_all_order_list_here">
													    <?php if(!empty($course_data)) { 
															foreach($course_data as $course) { 	
													    ?>
														<div class="session_order_lists">
															<div class="row">
																<div class="col-md-12">
																	<div class="order_list_details">
																		<div class="order_list_heading">
																			<h4>ORDER NO <?php echo $course['order_id']?></h4>
																			<span class="order_list_date"><?php echo isset($course['purchase_date']) ? date('d F Y',strtotime($course['purchase_date'])).' ' : ''?> </span>
																		</div>
																		<div class="order_list_info">
																			<div class="order_list_info_heading">
																				<h4>COURSE:<?php echo $course['course_name']?></h4>
																			</div>
																			<div class="order_listr_content">
																				<div class="row">
																					<div class="col-md-2">
																						<div class="order_content">
																							<span>Topic</span>
																							<div><?php echo $course['topic_name']?></div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="order_content">
																							<span>Subject</span>
																							<div><?php echo $course['subject_name']?></div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="order_content">
																							<span>Cost</span>
																							<div class="cost_bold_color">$<?php echo $course['amount']?></div>
																						</div>
																					</div>
																					<div class="col-md-6">
																						<div class="course_btn_groups">
																							<a href="#." class="btn course_btnset_btn">Play Video Again</a>
																							<a href="#." class="btn course_btnset_btn">Notes</a>
																							<a href="<?php echo base_url(); ?>test-overview" class="btn course_btnset_btn">Test Overview</a>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													    <?php } }?>
														<!-- <div class="session_order_lists">
															<div class="row">
																<div class="col-md-12">
																	<div class="order_list_details">
																		<div class="order_list_heading">
																			<h4>ORDER NO 8563214</h4>
																			<span class="order_list_date">4 Sept 2018 10:32 AM</span>
																		</div>
																		<div class="order_list_info">
																			<div class="order_list_info_heading">
																				<h4>COURSE:MATHEMATICS</h4>
																			</div>
																			<div class="order_listr_content">
																				<div class="row">
																					<div class="col-md-2">
																						<div class="order_content">
																							<span>Topic</span>
																							<div>Equations</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="order_content">
																							<span>Subject</span>
																							<div>Algebra</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="order_content">
																							<span>Cost</span>
																							<div class="cost_bold_color">$35</div>
																						</div>
																					</div>
																					<div class="col-md-6">
																						<div class="course_btn_groups">
																							<a href="#." class="btn course_btnset_btn">Play Video Again</a>
																							<a href="#." class="btn course_btnset_btn">Notes</a>
																							<a href="<?php echo base_url(); ?>test-overview" class="btn course_btnset_btn">Test Overview</a>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="session_order_lists">
															<div class="row">
																<div class="col-md-12">
																	<div class="order_list_details">
																		<div class="order_list_heading">
																			<h4>ORDER NO 8563214</h4>
																			<span class="order_list_date">3 Sept 2018 10:32 AM</span>
																		</div>
																		<div class="order_list_info">
																			<div class="order_list_info_heading">
																				<h4>COURSE:MATHEMATICS</h4>
																			</div>
																			<div class="order_listr_content">
																				<div class="row">
																					<div class="col-md-2">
																						<div class="order_content">
																							<span>Topic</span>
																							<div>Equations</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="order_content">
																							<span>Subject</span>
																							<div>Algebra</div>
																						</div>
																					</div>
																					<div class="col-md-2">
																						<div class="order_content">
																							<span>Cost</span>
																							<div class="cost_bold_color">$87</div>
																						</div>
																					</div>
																					<div class="col-md-6">
																						<div class="course_btn_groups">
																							<a href="#." class="btn course_btnset_btn">Play Video Again</a>
																							<a href="#." class="btn course_btnset_btn">Notes</a>
																							<a href="<?php echo base_url(); ?>test-overview" class="btn course_btnset_btn">Test Overview</a>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div> -->
													</div>
												</div>
											</div>
										</div>
										<div id="session_history" class="tab-pane fade">
											<div class="tabs_set_my_account">
												<div class="section_details">
													<div class="section_heading_sec">
														<h4>SESSION HISTORY</h4>
													</div>
													<div class="cours_table">
														<div class="table-responsive">
															<table class="table cours_table_set">
																<thead>
																	<tr>
																		<th>SN</th>
																		<th>Date</th>
																		<th>Session Type</th>
																		<th>No.of Attendees</th>
																		<th>Avg.Ratings</th>
																		<th>Level</th>
																		<th>Course</th>
																		<th>Subject</th>
																		<th>Topic</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>01</td>
																		<td>01/07/2018</td>
																		<td>Group</td>
																		<td>45</td>
																		<td>1/5 <span class="ratting_show"><a href="average_rating.html"><i class="fa fa-eye" aria-hidden="true"></i></a></span></td>
																		<td>10</td>
																		<td>Biology</td>
																		<td>Botany</td>
																		<td>Lorem Ipsum is simply dummy text</td>
																	</tr>
																	<tr>
																		<td>02</td>
																		<td>01/07/2018</td>
																		<td>One to One</td>
																		<td>-</td>
																		<td>3/5 <span class="ratting_show"><a href="average_rating.html"><i class="fa fa-eye" aria-hidden="true"></i></a></span></td>
																		<td>12</td>
																		<td>Biology</td>
																		<td>Anthology</td>
																		<td>Lorem Ipsum is simply dummy text</td>
																	</tr>
																	<tr>
																		<td>03</td>
																		<td>01/07/2018</td>
																		<td>Group</td>
																		<td>52</td>
																		<td>3/5 <span class="ratting_show"><a href="average_rating.html"><i class="fa fa-eye" aria-hidden="true"></i></a></span></td>
																		<td>10</td>
																		<td>Biology</td>
																		<td>Plants</td>
																		<td>Lorem Ipsum is simply dummy text</td>
																	</tr>
																	<tr>
																		<td>04</td>
																		<td>01/07/2018</td>
																		<td>One to One</td>
																		<td>22</td>
																		<td>2/5 <span class="ratting_show"><a href="average_rating.html"><i class="fa fa-eye" aria-hidden="true"></i></a></span></td>
																		<td>13</td>
																		<td>Physics</td>
																		<td>Quantum Physics</td>
																		<td>Lorem Ipsum is simply dummy text</td>
																	</tr>
																	<tr>
																		<td>05</td>
																		<td>01/07/2018</td>
																		<td>Group</td>
																		<td>45</td>
																		<td>3/5 <span class="ratting_show"><a href="average_rating.html"><i class="fa fa-eye" aria-hidden="true"></i></a></span></td>
																		<td>10</td>
																		<td>Biology</td>
																		<td>Circlular Motion</td>
																		<td>Lorem Ipsum is simply dummy text</td>
																	</tr>
																	<tr>
																		<td>06</td>
																		<td>01/07/2018</td>
																		<td>One to One</td>
																		<td>45</td>
																		<td>2/5 <span class="ratting_show"><a href="average_rating.html"><i class="fa fa-eye" aria-hidden="true"></i></a></span></td>
																		<td>11</td>
																		<td>Biology</td>
																		<td>Botany</td>
																		<td>Lorem Ipsum is simply dummy text</td>
																	</tr>
																	<tr>
																		<td>07</td>
																		<td>01/07/2018</td>
																		<td>Group</td>
																		<td>-</td>
																		<td>3/5 <span class="ratting_show"><a href="average_rating.html"><i class="fa fa-eye" aria-hidden="true"></i></a></span></td>
																		<td>10</td>
																		<td>Physics</td>
																		<td>Anthology</td>
																		<td>Lorem Ipsum is simply dummy text</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
				<!-- edit schedule section end here-->

	<script>
	$(document).ready(function(){
		// var past_session = <?php echo json_encode($past_session)?>;
		// alert(past_session.length);
		// var rating = ;
		// var flag=0;
		// for(var i=0;i<=5;i++)
		// {
		// 	if(flag==0){
		// 	$('.star'+i).removeClass('fa fa-star-o').addClass('fa fa-star');
		// 	}
		// 	else{
		// 	$('.star'+i).removeClass('fa fa-star').addClass('fa fa-star-o');
		// 	}
		// 	if(i==rating){
		// 		flag=1;
		// 	}
		// }


	})
	 
	</script>