<?php 
	$userdata = $profile->data[0];

	//print_r($userdata);
?>
<!-- banner section start here-->
<section class="banner_section_new" style="background-image:url('<?php echo base_url(); ?>assets/img/mid-bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>EDIT MY PROFILE</h4>
				</div>
				<div class="my_profile_user">
					<div class="profile_picture">
						<span><img src="<?php echo base_url(); ?>uploads/<?php echo $userdata->student_image?>" class="img-responsive" alt="profile"/></span>
						<h3><?php echo $userdata->first_name.' '.$userdata->last_name; ?></h3>
						<!-- <a href="#." class="profile_edit"><img src="<?php echo base_url(); ?>assets/student/img/edit-profile.png" class="img-responsive" alt="edit-profile"/></a> -->
						<form id="upload_file">
						    <input type="hidden" name="student_id" value="<?php echo $userdata->student_id;?>">
							<div class="profile_edit_btn_custom profile_edit">
								<label id="#bb">
									 <img src="http://localhost/yst/assets/student/img/edit-profile.png" class="img-responsive" alt="edit"/>
							   		 <input type="file" id="File" name="file"  size="60" >
							    </label> 
							    <a id="upload" class="btn edit_profilcomm-btn">Upload</a>
							</div>
							
						</form>
					</div>
					<div class="my_pr_buttons">
						<a href="<?php echo base_url()?>my-profile" class="btn common_btn_sec margin-right-10px">Back</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- profile details section start here-->
<section class="profile_details">
	<div class="container">
		<div class="row">
			 <div class="col-md-6">
		       <div class="alert alert-success" style="display:none">
		        <strong id="msg" ></strong> 
		       </div>
		    </div> 
			<form id="update_profile">
			<div class="col-md-12">
				<div class="profile_info">
					<div class="row">
						<input type="hidden" name="student_id" value="<?php echo $userdata->student_id;?>">
						<div class="col-md-3">
							<div class="form_set">
								<label>First Name</label>
								<input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $userdata->first_name; ?>">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form_set">
								<label>Last Name</label>
								<input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $userdata->last_name; ?>">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form_set">
								<label>Date Of Birth</label>
								<div class="form-group calend_div_new">
									<div class="input-group date form_date">
										<input class="form-control calender_set" type="datetime" name="bday" value="<?php echo $userdata->dob;?>">
										<span class="input-group-addon custom_input_calend"><i class="fa fa-calendar" aria-hidden="true"></i></span>
									</div>
									<input type="hidden" id="dtp_input2" value="" />
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form_set">
								<label>Gender</label>
								<select class="form-control" id="gender" name="gender">
									<option selected>Select Gender</option>
									<option value="male">Male</option>
									<option value="female">Female</option>
								</select>
								<script type="text/javascript">
									$('#gender').val('<?php echo $userdata->gender;?>');
										
								</script>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form_set">
								<label>Contact Number</label>
								<input type="text" class="form-control" name="contact_no" id="conatct_no" value="<?php echo $userdata->contact_no;?>">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form_set">
								<label>School Name</label>
								<input type="text" class="form-control" name="school_name" id="school_name" value="<?php echo $userdata->school_name;?>">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form_set">
								<label>Level</label>
								<select class="form-control" name="level_id" id="level">
									<option selected>Select Lavel</option>
									<?php if(!empty($level)) { 
										foreach($level as $levels) { 
										if($userdata->level == $levels['level_name']){
									?>
									<option selected value="<?php echo $levels['level_id']?>"><?php echo $levels['level_name']?></option> 
								    <?php }
								    else
								    {?>

								    	<option  value="<?php echo $levels['level_id']?>"><?php echo $levels['level_name']?></option> 
								    <?php }

								     }  } ?>
								</select>

								
							</div>
						</div>
						<div class="col-md-3">
							<div class="form_set">
								<label>Email ID</label>
								<input type="text" class="form-control" name="email_id" id="email_id" value="<?php echo $userdata->email_id?>">
							</div>
						</div>
					    <input type="hidden" class="form-control" id="password" name="password" value="<?php echo $userdata->password?>">

						<!-- <div class="col-md-3">
							<div class="form_set">
								<label>Password</label>
								<input type="password" class="form-control" id="password" value="<?php echo $userdata->password?>">
							</div>
						</div> -->
						<div class="col-md-12">
							<div class="form_set">
								<label>Address</label>
								<input type="text" class="form-control" name="address" id="address" value="<?php echo $userdata->address?>">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form_set">
								<label>Country</label>
								<select class="form-control select_country" onchange="getState();" name="country_id">
									<option selected>Select Country</option>
									<?php if(!empty($country)) { 
									foreach ($country as $counties) {
									?>
									<option value="<?php echo $counties['cnt_id']?>"><?php echo $counties['country_name']?></option>
								    <?php } } ?>
								</select>
								<script type="text/javascript">
									$('.select_country').val('<?php echo $userdata->country_id;?>');
								</script>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form_set">
								<label>State</label>
								<select class="form-control select_state" onchange="getCity();" name="state_id">
									<option selected>Select State</option>
									<?php if(!empty($state)) { 
										foreach($state as $states) { 
									?>
									  <option value="<?php echo $states['st_id']?>"><?php echo $states['state_name']?></option>
								    <?php } } ?>
								</select>
								<script type="text/javascript">
									$('.select_state').val('<?php echo $userdata->state_id;?>');
								</script>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form_set">
								<label>City</label>
								<select class="form-control select_city" name="city_id">
									<option selected>Select City</option>
									<?php if(!empty($city)) { 
										foreach($city as $cities) { 
									?>
									  <option value="<?php echo $cities['cty_id']?>"><?php echo $cities['city_name']?></option>
								    <?php } } ?>
									
								</select>
								<script type="text/javascript">
									$('.select_city').val('<?php echo $userdata->city_id;?>');
								</script>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form_set">
								<label>Zip Code</label>
								<input type="text" class="form-control" name="zip_code" id="zip_code" value="<?php echo $userdata->zip_code?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="main_profil_btn">
								<a href="#." class="btn save_cancel_btn margin-right-10px" id="updateProfile" >Update</a>
								<a href="#." class="btn save_cancel_btn">Cancel</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		</div>
	</div>
</section>
<!-- profile details section end here-->

<!--popup start here-->

<!-- sent succesfully popup Start Here -->
<div class="modal fade" id="success_update_popup" role="dialog">
	<div class="modal-dialog delet_cours_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url(); ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="moel_details">
					<span class="model_header">
						<img src="<?php echo base_url(); ?>assets/student/img/correct-icon.png" class="img-responsive" alt="forgotpassword"/>
						<h5>Successfully Update</h5>
					</span>
					<div class="model_content">
						<p>Your Profile has been Update.</p>
						<div class="pop_btn_sect_new">
							<button type="button"  data-dismiss="modal" class="btn popup_coom_btn">OK</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- sent succesfully popup end Here -->

<!-- change password popup Start Here -->
<div class="modal fade" id="change_password_popup" role="dialog">
	<div class="modal-dialog change_password_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url(); ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="model_details">
					<div class="section_heading_sec">
						<h4>CHANGE PASSWORD</h4>
					</div>
					<div class="alert alert-danger hide" id="errMsg" style="float:left;width:100%;">
						New Password & Re-type Password must be same
					</div>
					<div class="alert alert-success hide" id="successMsg" style="float:left;width:100%;">
						Password changed successfully
					</div>
					<div class="model_content">
						<div class="add_topic_form">
							<div class="row">
								<form id="change_password">
									<div class="col-md-12">
										<div class="form_set">
											<label>Old Password</label>
											<input type="password" class="form-control" name="old_password" id="old_password"/>
											<span class="err_opass" style="color: red; display: none;">Please enter old password.</span>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form_set">
											<label>New Password</label>
											<input type="password" class="form-control" name="new_password" id="new_password"/>
											<span class="err_npass" style="color: red; display: none;">Please enter new password.</span>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form_set">
											<label>Re-type Password</label>
											<input type="password" class="form-control" name="retype_password" id="retype_password"/>
											<span class="err_rpass" style="color: red; display: none;">Please enter re-type password.</span>
										</div>
									</div>
									<div class="col-md-12">
										<div class="main_profil_btn">
											<!-- <a href="#." class="btn save_cancel_btn margin-right-10px" data-dismiss="modal" data-toggle="modal" data-target="#success_changes_popup">Update</a> -->
											<button type="button" class="btn save_cancel_btn margin-right-10px" id="changePassword1">Update</button>
											<a href="javascript:void(0)" class="btn save_cancel_btn" data-dismiss="modal">Cancel</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- change password popup end Here -->

<!--succesfully changes popup Start Here -->
<div class="modal fade" id="success_changes_popup" role="dialog">
	<div class="modal-dialog delet_cours_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url(); ?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="moel_details">
					<span class="model_header">
						<img src="<?php echo base_url(); ?>assets/student/img/correct-icon.png" class="img-responsive" alt="forgotpassword"/>
						<h5>Successfully Changed</h5>
					</span>
					<div class="model_content">
						<p>Password has been succesfully changed.</p>
						<div class="pop_btn_sect_new">
							<button type="button" class="btn popup_coom_btn">OK</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- succesfully changes popup end Here -->
<!--popup end here-->
<script src="<?php echo base_url();?>assets/js/student_profile.js"></script>