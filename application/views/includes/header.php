<!DOCTYPE html>
<html lang="en">
<head>
  <title><?php echo $pageTitle;?></title>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!--css link start here-->
  <link rel="icon" type="imge/png" sizes="16x16" href="<?php echo base_url();?>assets/img/favicon.png"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/admin_media.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/stylesheet.css"/>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.min.css"/>
  <!-- <link rel="stylesheet"  href="assets/admin/css/owl.carousel.css"/> -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css"/> 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css"/> 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css"/> 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style_home.css"/> 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fullcalender/fullcalendar.min.css"/> 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fullcalender/fullcalendar.print.css" media="print"/> 
  
  <!--css link end here-->

  <!--js link start here-->
  <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url();?>assets/fullcalender/moment.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/fullcalender/fullcalendar.min.js" type="text/javascript"></script>
  <!-- <script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/datepicker.css" > -->



  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>  -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script> 
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet"/>


  <script type="text/javascript">
    var base_url = '<?php echo base_url();?>';//alert(base_url);
  </script>
  <!--js link end here-->
</head>
<body>
  <div class="wrapper">
    <!-- header section Start here-->
    <?php if($pageTitle != 'YST : Login'){?>
      <header class="header_section">
        <div class="col-md-12">
          <div class="head_navbar">
           <nav class="navbar">

            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand nav_logo" href="<?php echo base_url();?>home">
               <img src="<?php echo base_url();?>assets/admin/img/logo_header.png" class="img-responsive" alt="logo"/>
             </a>
           </div>

           <?php
           $current_page = $this->uri->segment(1);
           ?>

           <div class="navbar_set">
            <ul class="nav navbar-nav collapse navbar-collapse first_navbar" id="myNavbar">
              <li class="<?php if($current_page == 'home'){ echo "active"; } ?>"><a href="<?php echo base_url();?>home">Home</a></li>
              <li class="<?php if($current_page == 'manage_course'){ echo "active"; } ?>"><a href="<?php echo base_url();?>manage_course">Manage Course</a></li>
              <li class="<?php if($current_page == 'manage_user'){ echo "active"; } ?>"><a href="<?php echo base_url();?>manage_user">Manage User</a></li>
              <li class="<?php if($current_page == 'manage_session'){ echo "active"; } ?>"><a href="<?php echo base_url();?>manage_session">Manage Session</a></li>
            </ul>
            <ul class="nav navbar-nav second_navbar">
              <li>
                <a href="#." class="dropdown">
                  <img src="<?php echo base_url();?>assets/admin/img/search.png" class="img-responsive" data-toggle="dropdown" alt="search"/>
                  <div class="dropdown-menu search_box_set">  
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Search">
                      <div class="input-group-btn">
                        <button class="btn search_btn_btn" type="submit">
                          <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </a>
              </li>
              <li>
                <a href="<?php echo base_url();?>my_calender"><img src="<?php echo base_url();?>assets/admin/img/calander.png" class="img-responsive" alt="calander"/></a>
              </li>
              <li>
                <a href="<?php echo base_url();?>notification">
                  <div class="notifiaction_set">
                    <img src="<?php echo base_url();?>assets/admin/img/nitification.png" class="img-responsive" alt="nitification"/>
                    <span class="badge">2</span>
                  </div>
                </a>
              </li>
              <li class="dropdown navbar_pro_dropdown_set">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#.">
                  <div class="header_profile_drop_down">
                    <img src="<?php echo base_url();?>assets/admin/img/image-2.png" class="img-responsive img-circle profil_header_img" alt="profile"/>
                    <div class="profil_details">
                      <small>Welcome</small>
                    
                      <span> <?php echo $this->session->userdata('username');//exit;?></span>
                    </div>
                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                  </div>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url();?>my_account">My Account</a></li>
                  <li><a href="<?php echo base_url();?>manage_content">Manage Content</a></li>
                  <li><a href="<?php echo base_url();?>setting">Settings</a></li>
                  <li class="logout_list"><a href="<?php echo base_url();?>logout" class="btn logout_btn">LOGOUT</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </nav> 
      </div>
    </div>
  </header>
  <?php } ?>