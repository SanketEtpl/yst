<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $pageTitle; ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<!--css link start here-->
	<link rel="icon" type="imge/png" sizes="16x16" href="<?php echo base_url();?>assets/img/favicon.png"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/student_style.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css"/>		
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/media.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/stylesheet.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.scrollbar.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css"/> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fullcalender/fullcalendar.min.css"/> 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fullcalender/fullcalendar.print.css" media="print"/> 
	<!--css link end here-->


	<!--js link start here-->
	<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url();?>assets/fullcalender/moment.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/fullcalender/fullcalendar.min.js" type="text/javascript"></script>
	<!--js link end here-->
	 <script type="text/javascript">
	    var base_url = '<?php echo base_url();?>';//alert(base_url);
	  </script>
</head>
<body>
	<?php
	$current_page = $this->uri->segment(1);
	?>
	<!-- Page wrapper Start here-->
	<div class="wrapper">
		<!-- header section Start here-->
		<header class="header_section">
			<div class="col-md-12">
				<div class="head_navbar">
					<nav class="navbar">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand nav_logo" href="<?php echo base_url(); ?>student-home">
								<img src="<?php echo base_url();?>assets/img/logo_header.png" class="img-responsive" alt="logo"/>
							</a>
						</div>
						<div class="navbar_set">
							<?php
							$topic_arr = array('search-topic','view-course-details'); 
							$acc_arr = array('my-account','view-quest-answers','test-overview'); 
							$cal_arr = array('my-calender','book-session','book-session-edit','one-to-one-booking','group-session-booking'); 
							?>
							<ul class="nav navbar-nav collapse navbar-collapse first_navbar" id="myNavbar">
								<li class="<?php if($current_page == 'student-home'){ echo "active"; } ?>"><a href="<?php echo base_url(); ?>student-home">Home</a></li>
								<li class="<?php if(in_array($current_page, $topic_arr)){ echo "active"; } ?>"><a href="<?php echo base_url(); ?>search-topic">Search Topic</a></li>
								<li class="<?php if(in_array($current_page, $acc_arr)){ echo "active"; } ?>"><a href="<?php echo base_url(); ?>my-account">My Account</a></li>
								<li class="<?php if(in_array($current_page, $cal_arr)){ echo "active"; } ?>"><a href="<?php echo base_url(); ?>my-calender">My Calender</a></li>
							</ul>
							<ul class="nav navbar-nav second_navbar">
								<li>
									<a href="#." class="dropdown">
										<img src="<?php echo base_url();?>assets/img/search.png" class="img-responsive" data-toggle="dropdown" alt="search"/>
										<div class="dropdown-menu search_box_set">  
											<div class="input-group">
												<input type="text" class="form-control" placeholder="Search">
												<div class="input-group-btn">
													<button class="btn search_btn_btn" type="submit">
														<i class="fa fa-search" aria-hidden="true"></i>
													</button>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url(); ?>book-session"><img src="<?php echo base_url();?>assets/img/calander.png" class="img-responsive" alt="calander"/></a>
								</li>

								<?php
								$noti_arr = array('student-notification','student-notification-details'); 
								?>
								<li class="<?php if(in_array($current_page, $noti_arr)){ echo "active"; } ?> ?>">
									<a href="<?php echo base_url(); ?>student-notification">
										<div class="notifiaction_set">
											<?php 
											if(!in_array($current_page, $noti_arr)){
												?>
												<img src="<?php echo base_url();?>assets/img/nitification.png" class="img-responsive icon_set" alt="nitification"/>
											<?php } else { ?>
												<img src="<?php echo base_url();?>assets/img/notifiation-active.png" class="img-responsive active_icon_set" alt="nitification"/>
											<?php } ?>

											<span class="badge">2</span>
										</div>
									</a>
								</li>
								<li class="dropdown navbar_pro_dropdown_set">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#.">
										<div class="header_profile_drop_down">
											<img src="<?php echo base_url();?>uploads/<?php echo $this->session->userdata('userimage')?>" class="img-responsive img-circle profil_header_img" alt="profile"/>
											<div class="profil_details">
												<small>Welcome</small>
												<span><?php echo $this->session->userdata('username')?></span>
											</div>
											<i class="fa fa-caret-down" aria-hidden="true"></i>
										</div>
									</a>
									<ul class="dropdown-menu">
										<li><a href="<?php echo base_url(); ?>my-profile">My Profile</a></li>
										<li><a href="<?php echo base_url(); ?>student-testimonial">Testimonial</a></li>
										<li class="logout_list"><a href="<?php echo base_url(); ?>logout" class="btn logout_btn">LOGOUT</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</nav> 
				</div>
			</div>
		</header>
				<!-- header section end here-->