<!-- footer section start here-->
<?php if($pageTitle != 'YST : Login') { ?>
<footer class="footer-section footer_padding" style="background-image:url('<?php echo base_url()?>/assets/img/footer-img.jpg');">
		<div class="container">
		  <div class="row">
		   <div class="col-md-12">
			<div class="footer_social_link">
				<ul class="nav nav-pills">
					<li>
						<a href="#." class="footer_social_link_list"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					</li>
					<li>
						<a href="#." class="footer_social_link_list"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					</li>
					<li>
						<a href="#." class="footer_social_link_list"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					</li>
					<li>
						<a href="#." class="footer_social_link_list"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
					</li>
					<!--<li>
						<a href="#." class="footer_social_link_list"><i class="fa fa-instagram" aria-hidden="true"></i></a>
					</li>-->
				</ul>
			</div>
		   </div>
		  </div>
		</div>
		 <div class="foot_not_info">
		  <p>Copyright © 2018 Your Science Tutor. All rights reserved.</p>
		 </div>
	</footer>
	<?php } ?>
	<!-- footer section end here-->
</div>


<!--js link start here-->
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.js"></script>
<!--js link end here-->
</body>
</html>