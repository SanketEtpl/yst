<!-- footer section start Here -->
				<footer class="footer-section" style="background-image:url('<?php echo base_url();?>assets/img/footer-img.jpg');">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="footer_details">
									<div class="row">
										<div class="col-md-4">
											<div class="foot_blogs">
												<span class="foot-logo">
													<a href="index.html">
														<img src="<?php echo base_url();?>assets/img/loginlogo.png" class="img-responsive" alt="logo"/>
													</a>
												</span>
												<div class="foot_web_info">
													<p>
														Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br><br> when an unknown printer took a galley of type and scrambled it to make a type specimen book.
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="foot_blogs">
												<div class="fot_blo_head">
													<h4>Contact</h4>
												</div>
												<div class="foot_web_info_sec">
													<div class="cont_us_link">
														<span>Email : </span>
														<a href="mailto:info@yoursciencetutor.com">info@yoursciencetutor.com</a>
													</div>
													<p>
														Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br><br> when an unknown printer took a galley of type and scrambled it to make a type specimen book.
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-3 pull-right">
											<div class="foot_blogs">
												<div class="fot_blo_head">
													<h4>Useful Link</h4>
												</div>
												<div class="foot_web_info_sec">
													<ul>
														<li><a href="<?php echo base_url(); ?>home">Home</a></li>
														<li><a href="<?php echo base_url(); ?>about-us">About Us</a></li>
														<li><a href="<?php echo base_url(); ?>contact-us">Contact Us</a></li>
														<li><a href="<?php echo base_url(); ?>faq">FAQs</a></li>
														<li><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a></li>
														<li><a href="<?php echo base_url(); ?>terms-services">Terms and Services</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="soacial-footer-links">
									<ul>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Facebook</span><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Google-plus</span><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Twitter</span><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
										<!--<li><a href="#." class="tooltip"><span class="tooltiptext">Snapchat</span><i class="fa fa-snapchat" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Instagram</span><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
									 </ul>
								</div>
							</div>
						</div>
					</div>
					<div class="foot_not_info">
						<p>Copyright &copy; 2018 Your Science Tutor. All rights reserved.</p>
					</div>
				</footer>
				<!-- footer section end Here -->
<!--js link start here-->
			
			<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
			<script src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker.min.js"></script>	
			<script src="<?php echo base_url();?>assets/js/jquery.scrollbar.min.js"></script>
			<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
			<script src="<?php echo base_url();?>assets/js/student_custom.js"></script>
			<!-- <script src="<?php echo base_url();?>assets/js/materialize.min.js"></script> -->
			<script>
					$('.score_no').each(function () {
						$(this).prop('Counter',0).animate({
							Counter: $(this).text()
						}, {
							duration: 4000,
							easing: 'swing',
							step: function (now) {
								$(this).text(Math.ceil(now));
							}
						});
					});

					 
					var base_carousel = $('.features_owl_multiple_img_slider');
					  if (base_carousel.length) {
					    base_carousel.owlCarousel({
					      loop:true,
					      margin:50,
					      autoplay:true,
					      autoplayTimeout:2000,
					      autoplayHoverPause:true,
					      nav:false,
					      dots: true,
					      responsive:{
					        0:{
					          items:1
					        },
					        640:{
					          items:2
					        },
					        767:{
					          items:2
					        },
					        1000:{
					          items:3
					        }
					      }
					    });
					  } 
			</script>
			<!--js link end here-->
		</body>
</html>