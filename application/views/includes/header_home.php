<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Homepage</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<!--css link start here-->
		<link rel="icon" type="imge/png" sizes="16x16" href="<?php echo base_url()?>assets/img/favicon.png"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/style.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/media.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/fonts/stylesheet.css"/>
		<link rel="stylesheet"  href="<?php echo base_url()?>assets/css/owl.carousel.min.css"/>
		<!--css link end here-->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css"/> 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css"/> 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css"/> 
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style_home.css"/> 
  <!--css link end here-->

  <!--js link start here-->
  <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>

  <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
  <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script> -->
  
  <script type="text/javascript">
    var base_url = '<?php echo base_url();?>';//alert(base_url);
  </script>
	</head>
		<body>
			<!-- Page wrapper Start here-->
			<div class="wrapper">
				<!-- header section Start here-->
				<header id="header_section_new" class="header_section_new">
					<div class="col-md-12">
						<div class="head_navbar">
							 <nav class="navbar">
								<div class="navbar-header">
								  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								  </button>
								  <a class="navbar-brand nav_logo" href="<?php echo base_url();?>">
									 <img src="<?php echo base_url();?>/assets/img/logo_header.png" class="img-responsive" alt="logo"/>
								  </a>
								</div>
								<div class="navbar_set">
								  <ul class="nav navbar-nav collapse navbar-collapse first_navbar" id="myNavbar">
									<li class="active"><a href="<?php echo base_url();?>">Home</a></li>
									<li><a href="<?php echo base_url();?>about-us">About Us</a></li>
									<li><a href="<?php echo base_url();?>contact-us">Contact Us</a></li>
									<li><a href="<?php echo base_url();?>student">Sign In</a></li>
									<li><a href="<?php echo base_url();?>signup">Sign Up</a></li>
								  </ul>
								</div>
							 </nav> 
						</div>
					</div>
				</header>
				<!-- header section end here-->