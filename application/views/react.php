

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Add React in One Minute</title>
  </head>
  <body>

    <h2>Add React in One Minute</h2>
    <p>This page demonstrates using React with no build tooling.</p>
    <p>React is loaded as a script tag.</p>

    <p>
      This is the first comment.
      <!-- We will put our React component inside this div. -->
      <div class="like_button_container" data-commentid="1"></div>
    </p>

    <p>
      This is the second comment.
      <!-- We will put our React component inside this div. -->
      <div class="like_button_container" data-commentid="2"></div>
    </p>

    <p>
      This is the third comment.
      <!-- We will put our React component inside this div. -->
      <div class="like_button_container" data-commentid="3"></div>
    </p>
    <div class="modal fade" id="add_new_new_popup" role="dialog">
			<div class="modal-dialog add_topic_width_popup add_new_user_pop_width">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<img src="<?php echo base_url();?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
						</button>
					</div>
					<div class="modal-body">
						<div class="moel_details_new">
							<div class="section_heading_sec dotted_bottom_border">
								<h4>ADD NEW USER</h4>
							</div>
							<div class="model_content">
								<div class="add_topic_form">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="form_set">
												<label>First Name</label>
												<input type="text" class="form-control" name = "first_name" id="first_name" onChange={this.onChange}>
											</div>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="form_set">
												<label>Last Name</label>
												<input type="text" class="form-control" name="last_name" id="last_name" onChange={this.onChange}>
											</div>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="form_set">
												<label>Email ID</label>
												<input type="text" class="form-control" name="email_id" id="email_id" placeholder="montycarlo@yst.com" onChange={this.onChange}>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="moel_details_new_sec">
							<div class="add_topic_btn_new">
								<!-- <a href="#." onClick={this.addStudent} class="btn save_cancel_btn add_user_pop_btn_width" data-dismiss="modal" data-toggle="modal" data-target="#added_user_popup">ADD</a> -->
								<a href="#." onClick={this.addStudent} class="btn save_cancel_btn add_user_pop_btn_width" >ADD</a>
								<a href="#." class="btn save_cancel_btn add_user_pop_btn_width" data-dismiss="modal">Cancel</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    <!-- Load React. -->
    <!-- Note: when deploying, replace "development.js" with "production.min.js". -->
    <script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script>

    <!-- Load our React component. -->
    <script src="assets/js/like_button.js"></script>
    
  </body>
</html>