
				<!-- header section end here-->
				<!-- banner section start here-->
				<section class="banner_new_section">
				    <div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
						  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						  <li data-target="#myCarousel" data-slide-to="1"></li>
						  <li data-target="#myCarousel" data-slide-to="2"></li>
						  <li data-target="#myCarousel" data-slide-to="3"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner">
						  <div class="item active">
								<div class="banner_back_img_set" style="background-image:url(assets/img/banner-img-new.jpg);">
									<div class="container">
										<div class="row">
											<div class="col-md-12">
												<div class="banner_new_set_new">
													<div class="banner_content_new">
														<div class="banner_logo">
															<img src="<?php echo base_url() ?>assets/img/banner_icon.png" class="img-responsive" alt="banner"/>
														</div>
														<div class="banner_details_new">
															<h2>DEVELOPING YOUR KNOWLEDGE TOWARDS SUCCESS</h2>
															<span>Find Your Preferred Courses & Improve Your Skills</span>
															<div class="banner_serach_box">
																<input type="text" class="form-control" placeholder="Search" id="search"/>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
						  </div>
						  <div class="item">
								<div class="banner_back_img_set" style="background-image:url(assets/img/banner-img-new.jpg);">
									<div class="container">
										<div class="row">
											<div class="col-md-12">
												<div class="banner_new_set_new">
													<div class="banner_content_new">
														<div class="banner_logo">
															<img src="<?php echo base_url() ?>assets/img/banner_icon.png" class="img-responsive" alt="banner"/>
														</div>
														<div class="banner_details_new">
															<h2>DEVELOPING YOUR KNOWLEDGE TOWARDS SUCCESS</h2>
															<span>Find Your Preferred Courses & Improve Your Skills</span>
															<div class="banner_serach_box">
																<input type="text" class="form-control" placeholder="Search" id="search"/>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
						  </div>
						  <div class="item">
								<div class="banner_back_img_set" style="background-image:url(assets/img/banner-img-new.jpg);">
									<div class="container">
										<div class="row">
											<div class="col-md-12">
												<div class="banner_new_set_new">
													<div class="banner_content_new">
														<div class="banner_logo">
															<img src="<?php echo base_url() ?>assets/img/banner_icon.png" class="img-responsive" alt="banner"/>
														</div>
														<div class="banner_details_new">
															<h2>DEVELOPING YOUR KNOWLEDGE TOWARDS SUCCESS</h2>
															<span>Find Your Preferred Courses & Improve Your Skills</span>
															<div class="banner_serach_box">
																<input type="text" class="form-control" placeholder="Search" id="search"/>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
						  </div>
						  <div class="item">
								<div class="banner_back_img_set" style="background-image:url(assets/img/banner-img-new.jpg);">
									<div class="container">
										<div class="row">
											<div class="col-md-12">
												<div class="banner_new_set_new">
													<div class="banner_content_new">
														<div class="banner_logo">
															<img src="<?php echo base_url() ?>assets/img/banner_icon.png" class="img-responsive" alt="banner"/>
														</div>
														<div class="banner_details_new">
															<h2>DEVELOPING YOUR KNOWLEDGE TOWARDS SUCCESS</h2>
															<span>Find Your Preferred Courses & Improve Your Skills</span>
															<div class="banner_serach_box">
																<input type="text" class="form-control" placeholder="Search" id="search"/>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
						  </div>
						</div>
					</div>
				</section>
				<!-- banner section end here-->
				<!-- about us section start here-->
				<section class="about_us_section_new">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="about_us_details_new">
									<div class="section_heading_home">
										<div class="main_section_head">
											<div><img src="<?php echo base_url() ?>assets/img/lep-02.png" class="img-responsive" alt="head"/></div>
											<h3>ABOUT US</h3>
											<div><img src="<?php echo base_url() ?>assets/img/lep-02.png" class="img-responsive" alt="head"/></div>
										</div>
									</div>
									<div class="section_heading_home_info">
										<span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span>
										<p>It is a long established fact that a reader will be distracted by the readable content <br>of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
										<div class="btn_wrap">
											<button type="button" class="btn home-cmm-btn">Read More</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- about us section end here-->
				<!-- features section start here-->
				<section class="features_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="featurs_details_set">
									<div class="section_heading_home">
										<div class="main_section_head">
											<div><img src="<?php echo base_url() ?>assets/img/lep-02.png" class="img-responsive" alt="head"/></div>
											<h3>FEATURED COURSES</h3>
											<div><img src="<?php echo base_url() ?>assets/img/lep-02.png" class="img-responsive" alt="head"/></div>
										</div>
										<h4>We're Here to help you building <br>your confidence</h4>
									</div>
									<div class="featurs_details_data">
										<div id="owl-demo" class="js-base-carousel owl-carousel features_owl_multiple_img_slider">
											<div class="item">
												<div class="features_blog">
													<div class="features_blog_pric_label">$150</div>
													<div class="featurse_banner" style="background-image:url(assets/img/features_img.jpg);"></div>
													<div class="fetaures_info_set">
														<div class="fetures_user_name">
															<span><img src="<?php echo base_url() ?>assets/img/image-3.png" class="img-responsive" alt="features"/></span>
															<div class="features_name_set">
																<h4>Mario Speedwagon</h4>
																<ul>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																</ul>
															</div>
														</div>
														<div class="features_pass">
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
														</div>
														<!--<div class="features_time_date">
															<div class="row">
																<div class="col-md-3 col-sm-3">
																	<div class="set_dat_tim">
																		<span>3 Year</span>
																		<div>Course</div>
																	</div>
																</div>
																<div class="col-md-4 col-sm-4">
																	<div class="set_dat_tim">
																		<span>18</span>
																		<div>Class Size</div>
																	</div>
																</div>
																<div class="col-md-5 col-sm-5">
																	<div class="set_dat_tim">
																		<span>1 hour 50 min</span>
																		<div>Time Duration</div>
																	</div>
																</div>
															</div>
														</div>-->
													</div>
												</div>
											</div>
											<div class="item">
												<div class="features_blog">
													<div class="features_blog_pric_label">$350</div>
													<div class="featurse_banner" style="background-image:url(assets/img/features_img.jpg);"></div>
													<div class="fetaures_info_set">
														<div class="fetures_user_name">
															<span><img src="<?php echo base_url() ?>assets/img/image-2.png" class="img-responsive" alt="features"/></span>
															<div class="features_name_set">
																<h4>Mario Speedwagon</h4>
																<ul>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																</ul>
															</div>
														</div>
														<div class="features_pass">
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
														</div>
														<!--<div class="features_time_date">
															<div class="row">
																<div class="col-md-3 col-sm-3">
																	<div class="set_dat_tim">
																		<span>3 Year</span>
																		<div>Course</div>
																	</div>
																</div>
																<div class="col-md-4 col-sm-4">
																	<div class="set_dat_tim">
																		<span>18</span>
																		<div>Class Size</div>
																	</div>
																</div>
																<div class="col-md-5 col-sm-5">
																	<div class="set_dat_tim">
																		<span>1 hour 50 min</span>
																		<div>Time Duration</div>
																	</div>
																</div>
															</div>
														</div>-->
													</div>
												</div>
											</div>
											<div class="item">
												<div class="features_blog">
													<div class="features_blog_pric_label">$185</div>
													<div class="featurse_banner" style="background-image:url(assets/img/features_img.jpg);"></div>
													<div class="fetaures_info_set">
														<div class="fetures_user_name">
															<span><img src="<?php echo base_url() ?>assets/img/image-1.png" class="img-responsive" alt="features"/></span>
															<div class="features_name_set">
																<h4>Mario Speedwagon</h4>
																<ul>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																</ul>
															</div>
														</div>
														<div class="features_pass">
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
														</div>
														<!--<div class="features_time_date">
															<div class="row">
																<div class="col-md-3 col-sm-3">
																	<div class="set_dat_tim">
																		<span>3 Year</span>
																		<div>Course</div>
																	</div>
																</div>
																<div class="col-md-4 col-sm-4">
																	<div class="set_dat_tim">
																		<span>18</span>
																		<div>Class Size</div>
																	</div>
																</div>
																<div class="col-md-5 col-sm-5">
																	<div class="set_dat_tim">
																		<span>1 hour 50 min</span>
																		<div>Time Duration</div>
																	</div>
																</div>
															</div>
														</div>-->
													</div>
												</div>
											</div>
											<div class="item">
												<div class="features_blog">
													<div class="features_blog_pric_label">$150</div>
													<div class="featurse_banner" style="background-image:url(assets/img/features_img.jpg);"></div>
													<div class="fetaures_info_set">
														<div class="fetures_user_name">
															<span><img src="<?php echo base_url() ?>assets/img/image-3.png" class="img-responsive" alt="features"/></span>
															<div class="features_name_set">
																<h4>Mario Speedwagon</h4>
																<ul>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																</ul>
															</div>
														</div>
														<div class="features_pass">
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
														</div>
														<!--<div class="features_time_date">
															<div class="row">
																<div class="col-md-3 col-sm-3">
																	<div class="set_dat_tim">
																		<span>3 Year</span>
																		<div>Course</div>
																	</div>
																</div>
																<div class="col-md-4 col-sm-4">
																	<div class="set_dat_tim">
																		<span>18</span>
																		<div>Class Size</div>
																	</div>
																</div>
																<div class="col-md-5 col-sm-5">
																	<div class="set_dat_tim">
																		<span>1 hour 50 min</span>
																		<div>Time Duration</div>
																	</div>
																</div>
															</div>
														</div>-->
													</div>
												</div>
											</div>
											<div class="item">
												<div class="features_blog">
													<div class="features_blog_pric_label">$350</div>
													<div class="featurse_banner" style="background-image:url(assets/img/features_img.jpg);"></div>
													<div class="fetaures_info_set">
														<div class="fetures_user_name">
															<span><img src="<?php echo base_url() ?>assets/img/image-2.png" class="img-responsive" alt="features"/></span>
															<div class="features_name_set">
																<h4>Mario Speedwagon</h4>
																<ul>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																</ul>
															</div>
														</div>
														<div class="features_pass">
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
														</div>
														<!--<div class="features_time_date">
															<div class="row">
																<div class="col-md-3 col-sm-3">
																	<div class="set_dat_tim">
																		<span>3 Year</span>
																		<div>Course</div>
																	</div>
																</div>
																<div class="col-md-4 col-sm-4">
																	<div class="set_dat_tim">
																		<span>18</span>
																		<div>Class Size</div>
																	</div>
																</div>
																<div class="col-md-5 col-sm-5">
																	<div class="set_dat_tim">
																		<span>1 hour 50 min</span>
																		<div>Time Duration</div>
																	</div>
																</div>
															</div>
														</div>-->
													</div>
												</div>
											</div>
											<div class="item">
												<div class="features_blog">
													<div class="features_blog_pric_label">$180</div>
													<div class="featurse_banner" style="background-image:url(assets/img/features_img.jpg);"></div>
													<div class="fetaures_info_set">
														<div class="fetures_user_name">
															<span><img src="<?php echo base_url() ?>assets/img/image-1.png" class="img-responsive" alt="features"/></span>
															<div class="features_name_set">
																<h4>Mario Speedwagon</h4>
																<ul>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																</ul>
															</div>
														</div>
														<div class="features_pass">
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
														</div>
														<!--<div class="features_time_date">
															<div class="row">
																<div class="col-md-3 col-sm-3">
																	<div class="set_dat_tim">
																		<span>3 Year</span>
																		<div>Course</div>
																	</div>
																</div>
																<div class="col-md-4 col-sm-4">
																	<div class="set_dat_tim">
																		<span>18</span>
																		<div>Class Size</div>
																	</div>
																</div>
																<div class="col-md-5 col-sm-5">
																	<div class="set_dat_tim">
																		<span>1 hour 50 min</span>
																		<div>Time Duration</div>
																	</div>
																</div>
															</div>
														</div>-->
													</div>
												</div>
											</div>
											<div class="item">
												<div class="features_blog">
													<div class="features_blog_pric_label">$150</div>
													<div class="featurse_banner" style="background-image:url(assets/img/features_img.jpg);"></div>
													<div class="fetaures_info_set">
														<div class="fetures_user_name">
															<span><img src="<?php echo base_url() ?>assets/img/image-3.png" class="img-responsive" alt="features"/></span>
															<div class="features_name_set">
																<h4>Mario Speedwagon</h4>
																<ul>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																</ul>
															</div>
														</div>
														<div class="features_pass">
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
														</div>
														<!--<div class="features_time_date">
															<div class="row">
																<div class="col-md-3 col-sm-3">
																	<div class="set_dat_tim">
																		<span>3 Year</span>
																		<div>Course</div>
																	</div>
																</div>
																<div class="col-md-4 col-sm-4">
																	<div class="set_dat_tim">
																		<span>18</span>
																		<div>Class Size</div>
																	</div>
																</div>
																<div class="col-md-5 col-sm-5">
																	<div class="set_dat_tim">
																		<span>1 hour 50 min</span>
																		<div>Time Duration</div>
																	</div>
																</div>
															</div>
														</div>-->
													</div>
												</div>
											</div>
											<div class="item">
												<div class="features_blog">
													<div class="features_blog_pric_label">$350</div>
													<div class="featurse_banner" style="background-image:url(assets/img/features_img.jpg);"></div>
													<div class="fetaures_info_set">
														<div class="fetures_user_name">
															<span><img src="<?php echo base_url() ?>assets/img/image-2.png" class="img-responsive" alt="features"/></span>
															<div class="features_name_set">
																<h4>Mario Speedwagon</h4>
																<ul>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																</ul>
															</div>
														</div>
														<div class="features_pass">
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
														</div>
														<!--<div class="features_time_date">
															<div class="row">
																<div class="col-md-3 col-sm-3">
																	<div class="set_dat_tim">
																		<span>3 Year</span>
																		<div>Course</div>
																	</div>
																</div>
																<div class="col-md-4 col-sm-4">
																	<div class="set_dat_tim">
																		<span>18</span>
																		<div>Class Size</div>
																	</div>
																</div>
																<div class="col-md-5 col-sm-5">
																	<div class="set_dat_tim">
																		<span>1 hour 50 min</span>
																		<div>Time Duration</div>
																	</div>
																</div>
															</div>
														</div>-->
													</div>
												</div>
											</div>
											<div class="item">
												<div class="features_blog">
													<div class="features_blog_pric_label">$185</div>
													<div class="featurse_banner" style="background-image:url(assets/img/features_img.jpg);"></div>
													<div class="fetaures_info_set">
														<div class="fetures_user_name">
															<span><img src="<?php echo base_url() ?>assets/img/image-1.png" class="img-responsive" alt="features"/></span>
															<div class="features_name_set">
																<h4>Mario Speedwagon</h4>
																<ul>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																</ul>
															</div>
														</div>
														<div class="features_pass">
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
														</div>
														<!--<div class="features_time_date">
															<div class="row">
																<div class="col-md-3 col-sm-3">
																	<div class="set_dat_tim">
																		<span>3 Year</span>
																		<div>Course</div>
																	</div>
																</div>
																<div class="col-md-4 col-sm-4">
																	<div class="set_dat_tim">
																		<span>18</span>
																		<div>Class Size</div>
																	</div>
																</div>
																<div class="col-md-5 col-sm-5">
																	<div class="set_dat_tim">
																		<span>1 hour 50 min</span>
																		<div>Time Duration</div>
																	</div>
																</div>
															</div>
														</div>-->
													</div>
												</div>
											</div>
											<div class="item">
												<div class="features_blog">
													<div class="features_blog_pric_label">$150</div>
													<div class="featurse_banner" style="background-image:url(assets/img/features_img.jpg);"></div>
													<div class="fetaures_info_set">
														<div class="fetures_user_name">
															<span><img src="<?php echo base_url() ?>assets/img/image-1.png" class="img-responsive" alt="features"/></span>
															<div class="features_name_set">
																<h4>Mario Speedwagon</h4>
																<ul>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																</ul>
															</div>
														</div>
														<div class="features_pass">
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
														</div>
														<!--<div class="features_time_date">
															<div class="row">
																<div class="col-md-3 col-sm-3">
																	<div class="set_dat_tim">
																		<span>3 Year</span>
																		<div>Course</div>
																	</div>
																</div>
																<div class="col-md-4 col-sm-4">
																	<div class="set_dat_tim">
																		<span>18</span>
																		<div>Class Size</div>
																	</div>
																</div>
																<div class="col-md-5 col-sm-5">
																	<div class="set_dat_tim">
																		<span>1 hour 50 min</span>
																		<div>Time Duration</div>
																	</div>
																</div>
															</div>
														</div>-->
													</div>
												</div>
											</div>
											<div class="item">
												<div class="features_blog">
													<div class="features_blog_pric_label">$350</div>
													<div class="featurse_banner" style="background-image:url(assets/img/features_img.jpg);"></div>
													<div class="fetaures_info_set">
														<div class="fetures_user_name">
															<span><img src="<?php echo base_url() ?>assets/img/image-1.png" class="img-responsive" alt="features"/></span>
															<div class="features_name_set">
																<h4>Mario Speedwagon</h4>
																<ul>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																</ul>
															</div>
														</div>
														<div class="features_pass">
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
														</div>
														<!--<div class="features_time_date">
															<div class="row">
																<div class="col-md-3 col-sm-3">
																	<div class="set_dat_tim">
																		<span>3 Year</span>
																		<div>Course</div>
																	</div>
																</div>
																<div class="col-md-4 col-sm-4">
																	<div class="set_dat_tim">
																		<span>18</span>
																		<div>Class Size</div>
																	</div>
																</div>
																<div class="col-md-5 col-sm-5">
																	<div class="set_dat_tim">
																		<span>1 hour 50 min</span>
																		<div>Time Duration</div>
																	</div>
																</div>
															</div>
														</div>-->
													</div>
												</div>
											</div>
											<div class="item">
												<div class="features_blog">
													<div class="features_blog_pric_label">$185</div>
													<div class="featurse_banner" style="background-image:url(assets/img/features_img.jpg);"></div>
													<div class="fetaures_info_set">
														<div class="fetures_user_name">
															<span><img src="<?php echo base_url() ?>assets/img/image-1.png" class="img-responsive" alt="features"/></span>
															<div class="features_name_set">
																<h4>Mario Speedwagon</h4>
																<ul>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																</ul>
															</div>
														</div>
														<div class="features_pass">
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
														</div>
														<!--<div class="features_time_date">
															<div class="row">
																<div class="col-md-3 col-sm-3">
																	<div class="set_dat_tim">
																		<span>3 Year</span>
																		<div>Course</div>
																	</div>
																</div>
																<div class="col-md-4 col-sm-4">
																	<div class="set_dat_tim">
																		<span>18</span>
																		<div>Class Size</div>
																	</div>
																</div>
																<div class="col-md-5 col-sm-5">
																	<div class="set_dat_tim">
																		<span>1 hour 50 min</span>
																		<div>Time Duration</div>
																	</div>
																</div>
															</div>
														</div>-->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- features section end here-->
				<!-- Take toure section start here-->
				<section class="tak_tour_section" style="background-image:url(assets/img/tak_toure.jpg);">
					<div class="blue_ovarlay"></div>
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="tak_tour_deta">
									<div class="section_heading_home_new">
										<div class="main_section_head">
											<div><img src="<?php echo base_url() ?>assets/img/lep-01.png" class="img-responsive" alt="head"/></div>
											<h3>TAKE A TOUR</h3>
											<div><img src="<?php echo base_url() ?>assets/img/lep-01.png" class="img-responsive" alt="head"/></div>
										</div>
									</div>
									<div class="take_tour_video_player">
										<video controls poster="<?php echo base_url() ?>assets/img/features_img.jpg">
											<source src="<?php echo base_url() ?>assets/video/how_it_work.mp4" type="video/mp4">
											<source src="<?php echo base_url() ?>assets/video/how_it_work.ogg" type="video/ogg">
											Your browser does not support HTML5 video.
										</video>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- Take toure section end here-->
				<!-- Take toure section start here-->
				<section class="our_teachers_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="tak_tour_deta">
									<div class="section_heading_home_new">
										<div class="main_section_head">
											<div><img src="<?php echo base_url() ?>assets/img/lep-01.png" class="img-responsive" alt="head"/></div>
											<h3>YOUR TEACHERS</h3>
											<div><img src="<?php echo base_url() ?>assets/img/lep-01.png" class="img-responsive" alt="head"/></div>
										</div>
										<h4>Learn from the best to acheive<br>your goal</h4>
									</div>
									<div class="our_teachers_slider">
										<div class="our_teacher_blogs_item">
												<div class="col-sm-4 teachers_new_blog_cols">
													<div class="teacher_info_blogs">
														<div class="teachers_name_img_star_rat">
															<div class="teachers_img">
																<span>
																	<img src="<?php echo base_url() ?>assets/img/teachers-01.png" class="img-responsive" alt="teachers"/>
																</span>
															</div>
															<div class="teachers_name_star_rat">
																<h4>Maya Didas</h4>
																<ul>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																	<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
																</ul>
															</div>
														</div>
														<div class="teachers_subject_details">
															<h4>Chemistry</h4>
															<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
														</div>
														<div class="teachers_socail_link">
															<ul>
																<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
																<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
																<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
																<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
																<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
																<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="owl-demo" class="js-base-carousel owl-carousel our_teachers_owl_multiple_img_slider">
											<!--<div class="item">
												<div class="teacher_info_blogs">
													<div class="teachers_name_img_star_rat">
														<div class="teachers_img">
															<span>
																<img src="img/teachers-01.png" class="img-responsive" alt="teachers"/>
															</span>
														</div>
														<div class="teachers_name_star_rat">
															<h4>Maya Didas</h4>
															<ul>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															</ul>
														</div>
													</div>
													<div class="teachers_subject_details">
														<h4>Chemistry</h4>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
													</div>
													<div class="teachers_socail_link">
														<ul>
															<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="teacher_info_blogs">
													<div class="teachers_name_img_star_rat">
														<div class="teachers_img">
															<span>
																<img src="img/teachers-02.png" class="img-responsive" alt="teachers"/>
															</span>
														</div>
														<div class="teachers_name_star_rat">
															<h4>Buck Kinnear</h4>
															<ul>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															</ul>
														</div>
													</div>
													<div class="teachers_subject_details">
														<h4>Physics</h4>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
													</div>
													<div class="teachers_socail_link">
														<ul>
															<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="teacher_info_blogs">
													<div class="teachers_name_img_star_rat">
														<div class="teachers_img">
															<span>
																<img src="img/teachers-03.png" class="img-responsive" alt="teachers"/>
															</span>
														</div>
														<div class="teachers_name_star_rat">
															<h4>Graham Cracker</h4>
															<ul>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															</ul>
														</div>
													</div>
													<div class="teachers_subject_details">
														<h4>Biology</h4>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
													</div>
													<div class="teachers_socail_link">
														<ul>
															<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="teacher_info_blogs">
													<div class="teachers_name_img_star_rat">
														<div class="teachers_img">
															<span>
																<img src="img/teachers-01.png" class="img-responsive" alt="teachers"/>
															</span>
														</div>
														<div class="teachers_name_star_rat">
															<h4>Maya Didas</h4>
															<ul>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															</ul>
														</div>
													</div>
													<div class="teachers_subject_details">
														<h4>Chemistry</h4>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
													</div>
													<div class="teachers_socail_link">
														<ul>
															<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="teacher_info_blogs">
													<div class="teachers_name_img_star_rat">
														<div class="teachers_img">
															<span>
																<img src="img/teachers-02.png" class="img-responsive" alt="teachers"/>
															</span>
														</div>
														<div class="teachers_name_star_rat">
															<h4>Buck Kinnear</h4>
															<ul>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															</ul>
														</div>
													</div>
													<div class="teachers_subject_details">
														<h4>Physics</h4>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
													</div>
													<div class="teachers_socail_link">
														<ul>
															<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="teacher_info_blogs">
													<div class="teachers_name_img_star_rat">
														<div class="teachers_img">
															<span>
																<img src="img/teachers-03.png" class="img-responsive" alt="teachers"/>
															</span>
														</div>
														<div class="teachers_name_star_rat">
															<h4>Graham Cracker</h4>
															<ul>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															</ul>
														</div>
													</div>
													<div class="teachers_subject_details">
														<h4>Biology</h4>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
													</div>
													<div class="teachers_socail_link">
														<ul>
															<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="teacher_info_blogs">
													<div class="teachers_name_img_star_rat">
														<div class="teachers_img">
															<span>
																<img src="img/teachers-01.png" class="img-responsive" alt="teachers"/>
															</span>
														</div>
														<div class="teachers_name_star_rat">
															<h4>Maya Didas</h4>
															<ul>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															</ul>
														</div>
													</div>
													<div class="teachers_subject_details">
														<h4>Chemistry</h4>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
													</div>
													<div class="teachers_socail_link">
														<ul>
															<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="teacher_info_blogs">
													<div class="teachers_name_img_star_rat">
														<div class="teachers_img">
															<span>
																<img src="img/teachers-02.png" class="img-responsive" alt="teachers"/>
															</span>
														</div>
														<div class="teachers_name_star_rat">
															<h4>Buck Kinnear</h4>
															<ul>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															</ul>
														</div>
													</div>
													<div class="teachers_subject_details">
														<h4>Physics</h4>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
													</div>
													<div class="teachers_socail_link">
														<ul>
															<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="teacher_info_blogs">
													<div class="teachers_name_img_star_rat">
														<div class="teachers_img">
															<span>
																<img src="img/teachers-03.png" class="img-responsive" alt="teachers"/>
															</span>
														</div>
														<div class="teachers_name_star_rat">
															<h4>Graham Cracker</h4>
															<ul>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															</ul>
														</div>
													</div>
													<div class="teachers_subject_details">
														<h4>Biology</h4>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
													</div>
													<div class="teachers_socail_link">
														<ul>
															<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="teacher_info_blogs">
													<div class="teachers_name_img_star_rat">
														<div class="teachers_img">
															<span>
																<img src="img/teachers-01.png" class="img-responsive" alt="teachers"/>
															</span>
														</div>
														<div class="teachers_name_star_rat">
															<h4>Maya Didas</h4>
															<ul>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															</ul>
														</div>
													</div>
													<div class="teachers_subject_details">
														<h4>Chemistry</h4>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
													</div>
													<div class="teachers_socail_link">
														<ul>
															<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="teacher_info_blogs">
													<div class="teachers_name_img_star_rat">
														<div class="teachers_img">
															<span>
																<img src="img/teachers-02.png" class="img-responsive" alt="teachers"/>
															</span>
														</div>
														<div class="teachers_name_star_rat">
															<h4>Buck Kinnear</h4>
															<ul>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															</ul>
														</div>
													</div>
													<div class="teachers_subject_details">
														<h4>Physics</h4>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
													</div>
													<div class="teachers_socail_link">
														<ul>
															<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="teacher_info_blogs">
													<div class="teachers_name_img_star_rat">
														<div class="teachers_img">
															<span>
																<img src="img/teachers-03.png" class="img-responsive" alt="teachers"/>
															</span>
														</div>
														<div class="teachers_name_star_rat">
															<h4>Graham Cracker</h4>
															<ul>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															</ul>
														</div>
													</div>
													<div class="teachers_subject_details">
														<h4>Biology</h4>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
													</div>
													<div class="teachers_socail_link">
														<ul>
															<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
											<div class="item">
												<div class="teacher_info_blogs">
													<div class="teachers_name_img_star_rat">
														<div class="teachers_img">
															<span>
																<img src="img/teachers-03.png" class="img-responsive" alt="teachers"/>
															</span>
														</div>
														<div class="teachers_name_star_rat">
															<h4>Graham Cracker</h4>
															<ul>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
																<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
															</ul>
														</div>
													</div>
													<div class="teachers_subject_details">
														<h4>Biology</h4>
														<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
													</div>
													<div class="teachers_socail_link">
														<ul>
															<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
															<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
														</ul>
													</div>
												</div>
											</div>-->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- Take toure section end here-->
				<!-- Testimonail section start here-->
				<section class="testimonail_section">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="testimonail_data_info">
									<div class="section_heading_home">
										<div class="main_section_head">
											<div><img src="<?php echo base_url() ?>assets/img/lep-02.png" class="img-responsive" alt="head"/></div>
											<h3>TESTIMONAIL</h3>
											<div><img src="<?php echo base_url() ?>assets/img/lep-02.png" class="img-responsive" alt="head"/></div>
										</div>
										<h4>Our happy students</h4>
									</div>
									<div class="section_testimaonail_datals_slider">
										<div class="carousel slide" data-ride="carousel" id="quote-carousel">
											<!-- Carousel Slides / Quotes -->
											<div class="carousel-inner text-center">
												<!-- Quote 1 -->
												<div class="item active">
													<blockquote>
														<div class="row">
															<div class="col-sm-6 width_auto_testimonial">
																<div class="testimonial_video">
																	<video controls="" poster="<?php echo base_url() ?>assets/img/features_img.jpg">
																		<source src="<?php echo base_url() ?>assets/video/how_it_work.mp4" type="video/mp4">
																		<source src="<?php echo base_url() ?>assets/video/how_it_work.ogg" type="video/ogg">
																		Your browser does not support HTML5 video.
																	</video>
																</div>
																<div class="testim_blog_info">
																	<div class="portfoli_wrter_nam_info">
																		<span>John Corner</span>
																		<div>Chemistry Class</div>
																	</div>
																</div>
															</div>
														</div>
													</blockquote>
												</div>
												<!-- Quote 2 -->
												<div class="item">
													<blockquote>
														<div class="row">
															<div class="col-sm-6 width_auto_testimonial">
																<div class="testimonial_video">
																	<video controls="" poster="<?php echo base_url() ?>assets/img/book-session-img-02.jpg">
																		<source src="<?php echo base_url() ?>assets/video/how_it_work.mp4" type="video/mp4">
																		<source src="<?php echo base_url() ?>assets/video/how_it_work.ogg" type="video/ogg">
																		Your browser does not support HTML5 video.
																	</video>
																</div>
																<div class="testim_blog_info">
																	<div class="portfoli_wrter_nam_info">
																		<span>John Corner</span>
																		<div>Chemistry Class</div>
																	</div>
																</div>
															</div>
														</div>
													</blockquote>
												</div>
												<!-- Quote 3 -->
												<div class="item">
													<blockquote>
														<div class="row">
															<div class="col-sm-6 width_auto_testimonial">
																<div class="testimonial_video">
																	<video controls="" poster="<?php echo base_url() ?>assets/img/tak_toure.jpg">
																		<source src="<?php echo base_url() ?>assets/video/how_it_work.mp4" type="video/mp4">
																		<source src="<?php echo base_url() ?>assets/video/how_it_work.ogg" type="video/ogg">
																		Your browser does not support HTML5 video.
																	</video>
																</div>
																<div class="testim_blog_info">
																	<div class="portfoli_wrter_nam_info">
																		<span>John Corner</span>
																		<div>Chemistry Class</div>
																	</div>
																</div>
															</div>
														</div>
													</blockquote>
												</div>
											</div>
											<!-- Bottom Carousel Indicators -->
											<ol class="carousel-indicators">
												<li data-target="#quote-carousel" data-slide-to="0" class="active">
												</li>
												<li data-target="#quote-carousel" data-slide-to="1">
												</li>
												<li data-target="#quote-carousel" data-slide-to="2">
												</li>
											</ol>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- Testimonail section end here-->
				<!-- footer section start Here -->
				<!-- <footer class="footer-section" style="background-image:url(assets/img/footer-img.jpg);">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="footer_details">
									<div class="row">
										<div class="col-md-4 col-sm-6">
											<div class="foot_blogs">
												<span class="foot-logo">
													<a href="index.html">
														<img src="<?php //echo base_url() ?>assets/img/loginlogo.png" class="img-responsive" alt="logo"/>
													</a>
												</span>
												<div class="foot_web_info">
													<p>
														Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br><br> when an unknown printer took a galley of type and scrambled it to make a type specimen book.
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-4 col-sm-6">
											<div class="foot_blogs">
												<div class="fot_blo_head">
													<h4>Contact</h4>
												</div>
												<div class="foot_web_info_sec">
													<div class="cont_us_link">
														<span>Email : </span>
														<a href="mailto:info@yoursciencetutor.com">info@yoursciencetutor.com</a>
													</div>
													<p>
														Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br><br> when an unknown printer took a galley of type and scrambled it to make a type specimen book.
													</p>
												</div>
											</div>
										</div>
										<div class="col-md-3 pull-right col-sm-6">
											<div class="foot_blogs">
												<div class="fot_blo_head">
													<h4>Useful Link</h4>
												</div>
												<div class="foot_web_info_sec">
													<ul>
														<li><a href="index.html">Home</a></li>
														<li><a href="about_us.html">About Us</a></li>
														<li><a href="contact_us.html">Contact Us</a></li>
														<li><a href="faq.html">FAQs</a></li>
														<li><a href="privacy_policy.html">Privacy Policy</a></li>
														<li><a href="terms_and_service.html">Terms and Services</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="soacial-footer-links">
									<ul>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Facebook</span><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Google-plus</span><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Twitter</span><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Snapchat</span><i class="fa fa-snapchat" aria-hidden="true"></i></a></li>
										<li><a href="#." class="tooltip"><span class="tooltiptext">Instagram</span><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
									 </ul>
								</div>
							</div>
						</div>
					</div>
					<div class="foot_not_info">
						<p>Copyright &copy; 2018 Your Science Tutor. All rights reserved.</p>
					</div>
				</footer> -->
				<!-- footer section end Here -->
				
			</div>