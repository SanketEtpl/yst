		<!-- Page wrapper Start here-->
		
			<!-- header section end here-->
			<!-- banner section start here-->
			<section class="banner_section" style="background-image:url(./assets/admin/img/mid-bg.jpg);">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="banner_content">
								<h4>HOME</h4>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- banner section end here-->
			<!-- pie chart section start here-->
			<section class="piechart_section">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="piechart_details">
								<div class="row">
									<div class="col-md-5">
										<div class="pie_chart_blogs">
											<div class="pie_chart_head">
												<h4>ACTIVE USERS</h4>
											</div>
											<div class="pioe_chart_set_here_sec">
												<img src="<?php echo base_url();?>assets/admin/img/active_pie_chart.png" class="img-responsive" alt="piechart"/>
											</div>
										</div>
									</div>
									<div class="col-md-7">
										<div class="pie_chart_blogs pie_chart_bg_color">
											<div class="pie_chart_head_sec">
												<h4>Purchased</h4>
												<span class="pie_chart_dropdown">
													<span class="pie_chart_dropdown_cart"></span>
													<select class="form-control pie_chart_dropdown_select">
														<option>GCSE</option>
														<option>CCSE</option>
													</select>
												</span>	
											</div>
											<div class="pioe_chart_set_here">
												<img src="<?php echo base_url();?>assets/admin/img/purches_bar.jpg" class="img-responsive" alt="piechart"/>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- pie chart section end here-->
			<!-- performer section start here-->
			<section class="performer_section">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="piechart_details">
								<div class="row">
									<div class="col-md-5 col-sm-12 col-xs-12">
										<div class="performer_wrapp">
											<div class="performer_heading_title">
												BEST PERFORMER
											</div>
											<div class="performer_heading_body">
												<div id="myCarousel" class="carousel slide" data-ride="carousel">
													<ol class="carousel-indicators carosal_bullet_bottom">
														<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
														<li data-target="#myCarousel" data-slide-to="1"></li>
														<li data-target="#myCarousel" data-slide-to="2"></li>
														<li data-target="#myCarousel" data-slide-to="3"></li>
													</ol>
													<div class="carousel-inner">
														<div class="item active">
															<div class="performer_profile_info">
																<div class="performer_profile_img"></div>
																<div class="performer_profile_Uname">Petey Cruiser</div>
																<div class="performer_profile_score">Best Score - 99(Physics)</div>
															</div>
														</div>
														<div class="item">
															<div class="performer_profile_info">
																<div class="performer_profile_img"></div>
																<div class="performer_profile_Uname">Petey Cruiser</div>
																<div class="performer_profile_score">Best Score - 99(Physics)</div>
															</div>
														</div>
														<div class="item">
															<div class="performer_profile_info">
																<div class="performer_profile_img"></div>
																<div class="performer_profile_Uname">Petey Cruiser</div>
																<div class="performer_profile_score">Best Score - 99(Physics)</div>
															</div>
														</div>
														<div class="item">
															<div class="performer_profile_info">
																<div class="performer_profile_img"></div>
																<div class="performer_profile_Uname">Petey Cruiser</div>
																<div class="performer_profile_score">Best Score - 99(Physics)</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- -->
										<div class="performer_wrapp margin_top">
											<div class="performer_heading_title">
												POOR PERFORMER
											</div>
											<div class="performer_heading_body">
												<div id="myCarousel_2" class="carousel slide" data-ride="carousel">
													<ol class="carousel-indicators carosal_bullet_bottom">
														<li data-target="#myCarousel_2" data-slide-to="0" class="active"></li>
														<li data-target="#myCarousel_2" data-slide-to="1"></li>
														<li data-target="#myCarousel_2" data-slide-to="2"></li>
														<li data-target="#myCarousel_2" data-slide-to="3"></li>
													</ol>
													<div class="carousel-inner">
														<div class="item active">
															<div class="performer_profile_info">
																<div class="performer_profile_img poor_performer_profile_img"></div>
																<div class="performer_profile_Uname">Gail Forcewind</div>
																<div class="performer_profile_score">Best Score - 35(Chemistry)</div>
															</div>
														</div>
														<div class="item">
															<div class="performer_profile_info">
																<div class="performer_profile_img poor_performer_profile_img"></div>
																<div class="performer_profile_Uname">Gail Forcewind</div>
																<div class="performer_profile_score">Best Score - 35(Chemistry)</div>
															</div>
														</div>
														<div class="item">
															<div class="performer_profile_info">
																<div class="performer_profile_img poor_performer_profile_img"></div>
																<div class="performer_profile_Uname">Gail Forcewind</div>
																<div class="performer_profile_score">Best Score - 35(Chemistry)</div>
															</div>
														</div>
														<div class="item">
															<div class="performer_profile_info">
																<div class="performer_profile_img poor_performer_profile_img"></div>
																<div class="performer_profile_Uname">Gail Forcewind</div>
																<div class="performer_profile_score">Best Score - 35(Chemistry)</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-7">
										<div class="pie_chart_blogs pie_chart_bg_color">
											<div class="pie_chart_head_radio_sec">
												<label class="radio_container">All Courses
													<input type="radio" checked="checked" name="radio">
													<span class="radio_checkmark"></span>
												</label>
												<label class="radio_container">All Subject
													<input type="radio" checked="checked" name="radio">
													<span class="radio_checkmark"></span>
												</label>	
												<label class="radio_container">GCSE
													<input type="radio" checked="checked" name="radio">
													<span class="radio_checkmark"></span>
												</label>
												<label class="radio_container">A-Level
													<input type="radio" checked="checked" name="radio">
													<span class="radio_checkmark"></span>
												</label>	
											</div>
											<div class="pioe_chart_set_here">
												<img src="<?php echo base_url();?>assets/admin/img/nostudents.jpg" class="img-responsive" alt="piechart"/>
											</div>
										</div>
									</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- performer section end here-->
			<!-- testimonial section start here-->
			<section class="testimonial_section">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="testmonial_details">
								<div class="section_heading">
									<h4>TESTIMONIAL</h4>
								</div>
								<span class="view_all"><a href="<?php echo base_url();?>testimonial">View All</a></span>
								<div class="testimonail_all_member">
									<div class="row">
								<?php //print_r($alltestim);
								$i=3;
								foreach($alltestim as $val)
								if($i>0){?>

										<div class="col-md-4 col-sm-4 col-xs-12">
											<div class="testominail_blogs">
												<div class="testi_m_profile">
													<span><img src="<?php echo base_url();?>assets/admin/img/image-1.png" class="img-responsive img-circle img-thumbnail" alt="testimonial"/></span>
													<div class="testi_m_profile_name">
														<h4><?php echo $val['first_name'].' '.$val['last_name'];?></h4>
													<div class="star_rat">
													
														<?php $limit=5;
															  $rating=$val['rate'];
															  $remain=$limit-$rating;
														for($j=1;$j<=$rating;$j++){?>

														<a href="#."><i class="fa fa-star" aria-hidden="true"></i></a>
													     <?php }
													     for($j=1;$j<=$remain;$j++){?>

														<a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a>
													     <?php } ?>

														</div>
													</div>
												</div>
												<div class="test_profil_info">
													<?php echo $val['comment'];?>
												</div>
											</div>
										</div>
										<?php  $i--;}?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- testimonial section end here-->
			
