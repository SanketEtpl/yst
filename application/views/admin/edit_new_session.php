
<!-- banner section start here-->
<section class="banner_section" style="background-image:url(./assets/admin/img/mid-bg.jpg);">
<div class="container">
<div class="row">
<div class="col-md-12">
	<div class="banner_content">
		<h4>EDIT SCHEDULE A NEW SESSION</h4>
	</div>
</div>
</div>
</div>
</section>
<!-- banner section end here-->
<!-- edit schedule section start here-->
<section class="web_section_common">
<div class="container">
<div class="row">
   <div class="col-md-8">
       <div class="alert alert-success" style="display:none">
        <strong id="msg" ></strong> 
       </div>
    </div> 
<div class="col-md-12">
	<div class="section_details schedule_margin_bottom">
		<div class="section_heading_sec dotted_bottom_border">
			<h4>SCHEDULE A GROUP SESSION</h4>
		</div>
		<div class="schedul_session_form">
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<form  id="update_session">
					<div class="scedule_info_details">
						<div class="row">
							<div class="col-md-6 col-sm-4 col-xs-12">
								<div class="form_set">
									<label>Course</label>
									<select class="form-control course-id" onchange="getSubjectlist('edit');" id="edit_course" name="course_id">
										<option selected>Select Course</option>
										<?php if(!empty($course_list)) { 
											foreach($course_list as $course) { 
										?>
										<option value="<?php echo $course['course_id']?>"><?php echo $course['course_name']?></option>
										<?php  } } ?>
									</select>
									<span class="err_course" style="color: red; display: none;">Please select course.</span>
								</div>
								<script type="text/javascript">
								  $('#edit_course').val('<?php echo $session_details[0]['course_id']?>');
							    </script>
							</div>
							<div class="col-md-6 col-sm-4 col-xs-12">
								<div class="form_set">
									<label>Subject</label>
									<select class="form-control select_subject" id="edit_subject" onchange="getTopiclist('edit');" name="subject_id">
										<option selected>Select Subject</option>
									</select>
									<span class="err_subject" style="color: red; display: none;">Please select subject.</span>
								</div>
							</div>
							<script type="text/javascript">
								$('#edit_subject').html('<option value ="<?php echo $session_details[0]['subject_id']?>"><?php echo $session_details[0]['subject_name']?></option>');
							</script>
						</div>
						<div class="row">	
							<div class="col-md-12 col-sm-4 col-xs-12">
								<div class="form_set">
									<label>Topic</label>
									<select class="form-control select_topic" id="edit_topic" name="topic_id">
										<option selected>Select Topic</option>
									</select>
									<span class="err_topic" style="color: red; display: none;">Please select topic.</span>
								</div>
							</div>
							<script type="text/javascript">
								$('#edit_topic').html('<option value ="<?php echo $session_details[0]['topic_id']?>"><?php echo $session_details[0]['topic_name']?></option>');
							</script>
							<div class="col-md-6 col-sm-4 col-xs-12">
								<div class="form_set">
									<label>Session Title</label>
									<input class="form-control" id="edit_title" name="title" value="<?php echo $session_details[0]['title']?>">
									<span class="err_title" style="color: red; display: none;">Please enter title.</span>
								</div>
							</div>
							<div class="col-md-6 col-sm-4 col-xs-12">
								<div class="form_set">
									<label>Date</label>
									<div class="form-group calend_div">
										<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
											<input class="form-control calender_set" type="text" value="<?php echo $session_details[0]['date']?>" name="date" id="edit_date" readonly="">
											<span class="input-group-addon custom_input_calend"><i class="fa fa-calendar" aria-hidden="true"></i></span>
										</div>
										<input type="hidden" id="dtp_input2" value="" />
									</div>
									    <span class="err_date" style="color: red; display: none;">Please select date.</span>
								</div>
							</div>
							<div class="col-md-6 col-sm-4 col-xs-12">
								<div class="form_set">
									<label>Start Time</label>
									<input class="form-control" id="edit_start_time" name="start_time" value="<?php echo date('H:i:s',strtotime($session_details[0]['start_time']))?>">
									<!-- <span class="input-group-addon custom_input_calend"><span class="glyphicon glyphicon-time"></span></span> -->
									<span class="err_stime" style="color: red; display: none;">Please select start time.</span>
								</div>
							</div>
							<div class="col-md-6 col-sm-4 col-xs-12">
								<div class="form_set">
									<label>End Time</label>
									<input class="form-control" id="edit_end_time" name="end_time" value="<?php echo date('H:i:s',strtotime($session_details[0]['end_time']))?>">
									<span class="err_etime" style="color: red; display: none;">Please select end time.</span>
								</div>
							</div>
							<div class="col-md-6 col-sm-4 col-xs-12">
								<div class="form_set">
									<label>Total Duration (In minutes)</label>
									<input class="form-control" id="edit_duration" name="duration_in_minutes" readonly="" value="<?php echo $session_details[0]['duration_in_minutes']?>">
									<span class="err_duration" style="color: red; display: none;">Please enter duration.</span>
								</div>
							</div>
							<div class="col-md-6 col-sm-4 col-xs-12">
								<div class="form_set">
									<label>Cost</label>
									<input class="form-control" id="edit_cost" name="cost" value="<?php echo $session_details[0]['cost']?>">
									<span class="err_cost" style="color: red; display: none;">Please enter cost.</span>
								</div>
							</div>
							<input type="hidden" name="conducted_flag" value="group">
							<input type="hidden" name="session_id" value="<?php echo base64_decode($_GET['id'])?>">
						</div>
					</div>
					<div class="schedul_btn_group">
						<!-- <a href="#." class="btn save_cancel_btn" data-toggle="modal" data-target="#success_add_user_popup">Save</a> -->
						<button type="button" id="btnupdate" class="btn save_cancel_btn" onclick="updateSession();">Update</button>
						<a href="#." class="btn save_cancel_btn">Cancel</a>
					</div>
					</form>
				</div>
				<div class="col-md-6">
					<div class="calender_here">
						<div class="calender_heading">
							<h3>MY CALENDER</h3>
						</div>
						<!-- <div class="calender">
							<ul class="month">
								<li>
								  <h4>January</h4>
								  <h5>2016</h5>
								</li>
								<span class="prev"><img src="<?php echo base_url();?>assets/admin/img/arrow_left.png" class="img-responsive arro_size" alt="arrow"/></span>
								<span class="next"><img src="<?php echo base_url();?>assets/admin/img/arrow_right.png" class="img-responsive arro_size" alt="arrow"/></span>
							</ul>
							<ul class="weeks">
								<li>Sa</li>
								<li>Su</li>
								<li>Mo</li>
								<li>Tu</li>
								<li>We</li>
								<li>Th</li>
								<li>Fr</li>
							</ul>
							<ul class="days">

							</ul>
						</div> -->
						<div id="edit_session_calender"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</section>			
<!-- edit schedule section end here-->
<!-- attendees table section start here-->
<section class="attendees_table_section">
<div class="container">
<div class="row">
<div class="col-md-12">
	<div class="section_details">
		<div class="section_heading_sec">
			<h4>ATTENDEES</h4>
			<a href="#." data-toggle="modal" data-toggle="modal" data-target="#add_attendees_popup" class="btn new-comm-btn">Add Attendees</a>
		</div>
		<div class="cours_table">
			<div class="table-responsive">
			  <table class="table cours_table_set">
				<thead>
				  <tr>
					<th>SN</th>
					<th>Name</th>
				  </tr>
				</thead>
				<tbody>
				<?php if(!empty($invite_list)) { 
					$i=1;
					foreach($invite_list as $invite){ 
				?>
				  <tr>
					<td><?php echo $i?></td>
					<td><?php echo $invite['first_name'].' '.$invite['last_name']?></td>
				  </tr>
				<?php $i++ ; } } ?>
				</tbody>
			 </table>
			</div>
		</div>
		<div class="add_topic_btn">
			<a href="#." class="btn save_cancel_btn">Update</a>
			<a href="#." class="btn save_cancel_btn">Cancel</a>
		</div>
	</div>
</div>
</div>
</div>
</section>
<!-- attendees table section end here-->
<!-- popup start Here -->

<!--add attendees popup start here-->
<div class="modal fade" id="add_attendees_popup" role="dialog" >
<div class="modal-dialog add_topic_width_popup">
  <!-- Modal content-->
  <div class="modal-content">
	<div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal">
		 <img src="<?php echo base_url();?>assets/admin/img/pop_up_close.png" class="img-responsive" alt="close">
	  </button>
	</div>
	<div class="modal-body">
		<div class="moel_details_new_here_set">
			<div class="section_heading_sec dotted_bottom_border">
				<h4>ADD ATTENDEES</h4>
			</div>
			<div class="model_content">
				<div class="attendees_serch_form">
					<div class="row">
						<div class="col-md-6">
							<div class="form_set">
								<label>Search</label>
								<input class="form-control" id="Search_student" placeholder="Enter Student Name"/>
							</div>
						</div>
					</div>
				</div>
				<div class="cours_table_new">
					<div class="table-responsive">
					  <table class="table cours_table_set_sec">
							<thead>
								<tr>
									<th>SN</th>
									<th>Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>01</td>
									<td>Will Power</td>
									<td>
										<div class="plus_table_link_new">
											<a href="#." data-toggle="modal" data-target="#delet_cours_popup">
												<i class="fa fa-plus-circle" aria-hidden="true"></i>
											</a>
										</div>
									</td>
								</tr>
								<tr>
									<td>02</td>
									<td>Will Power</td>
									<td>
										<div class="plus_table_link_new">
											<a href="#." data-toggle="modal" data-target="#delet_cours_popup">
												<i class="fa fa-plus-circle" aria-hidden="true"></i>
											</a>
										</div>
									</td>
								</tr>
								<tr>
									<td>03</td>
									<td>Will Power</td>
									<td>
										<div class="plus_table_link_new">
											<a href="#." data-toggle="modal" data-target="#delet_cours_popup">
												<i class="fa fa-plus-circle" aria-hidden="true"></i>
											</a>
										</div>
									</td>
								</tr>
								<tr>
									<td>04</td>
									<td>Will Power</td>
									<td>
										<div class="plus_table_link_new">
											<a href="#." data-toggle="modal" data-target="#delet_cours_popup">
												<i class="fa fa-plus-circle" aria-hidden="true"></i>
											</a>
										</div>
									</td>
								</tr>
								<tr>
									<td>05</td>
									<td>Will Power</td>
									<td>
										<div class="plus_table_link_new">
											<a href="#." data-toggle="modal" data-target="#delet_cours_popup">
												<i class="fa fa-plus-circle" aria-hidden="true"></i>
											</a>
										</div>
									</td>
								</tr>
							</tbody>
					   </table>
					</div>
				</div>
			</div>
			<div class="add_topic_btn_new send_request_pop_btn">
				<a href="#." class="btn send_request_btn" data-dismiss="modal" data-toggle="modal" data-target="#sent_request_popup">Send Request</a>
				<a href="#." class="btn save_cancel_btn">Reset</a>
				<a href="#." class="btn save_cancel_btn" data-dismiss="modal">Cancel</a>
			</div>
		</div>
	</div>
  </div>
</div>
</div>
<!--add attendees popup end here-->

<!--new group added popup start here-->
<div class="modal fade" id="sent_request_popup" role="dialog">
<div class="modal-dialog delet_cours_popup_width">
  <!-- Modal content-->
  <div class="modal-content">
	<div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal">
		 <img src="<?php echo base_url();?>assets/admin/img/pop_up_close.png" class="img-responsive" alt="close"/>
	  </button>
	</div>
	<div class="modal-body">
		<div class="moel_details">
			<span class="model_header">
				<img src="<?php echo base_url();?>assets/admin/img/sent_requesr.png" class="img-responsive" alt="sentrequest"/>
				<h5>Sent Request</h5>
			</span>
			<div class="model_content">
				<p>Request is sent to the users.</p>
				<div class="pop_btn_sect_sec">
					<a href="#." id="addes_succe_remo_back_pop" class="btn popup_coom_btn_new margin_zero" data-toggle="modal" data-target="#updated_user_popup" data-dismiss="modal">OK</a>
				</div>
			</div>
		</div>
	</div>
  </div>
</div>
</div>
<!--new group added popup end here-->
<!--event popup start here-->
<div class="modal fade" id="event_content_popup" role="dialog">
	<div class="modal-dialog add_topic_width_popup">
	<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal">
			      <img src="<?php echo base_url();?>assets/admin/img/pop_up_close.png" class="img-responsive" alt="close"/>
			    </button>
			</div>
			<div class="modal-body">
			<div class="moel_details_new">
				<div class="section_heading_sec dotted_bottom_border">
				<h4>SESSION DETAILS</h4>
				</div>
				<div class="model_content">
					<div class="add_topic_form">
					    <div class="session_list_blog_list_here">
							<h4 class="session_heading_date heading_sec">
								
							</h4>
							<div class="session_blog_list">
								<ul>
									<li><span>Date:</span><span class="s_date"></span></li>
									<li><span>Session Type:</span><span class="type"></span></li>
									<li><span>Course:</span><span class="course"></span></li>
									<li><span>Subject:</span><span class="subject"></span></li>
									<li><span>Topic:</span><span class="topic"></span></li>
								</ul>
							</div>
					    </div>
					</div>
				</div>
			 </div>
			</div>
		</div>
	</div>
</div>
<!--set availability popup end here-->
<script src="<?php echo base_url()?>assets/js/session.js"></script>
<script>
 $("#edit_session_calender").fullCalendar({  
        header: {
            left: 'prev,next today',
            center: 'title',
            right:  'prevYear,nextYear, agendaDay,agendaWeek,month'
		},
		events :
		[
			<?php 
			 if(!empty($upcoming_session)){
		     	for ($i=0; $i <count($upcoming_session); $i++) { 
			?> 
				{
					title : '<?php echo $upcoming_session[$i]['title']; ?>',
					start : '<?php echo $upcoming_session[$i]['date'].'T'.$upcoming_session[$i]['start_time']; ?>',
					link : '<?php echo $upcoming_session[$i]['session_id']; ?>',
					course_name : '<?php echo $upcoming_session[$i]['course_name']; ?>',
					subject_name : '<?php echo $upcoming_session[$i]['subject_name']; ?>',
					topic_name : '<?php echo $upcoming_session[$i]['topic_name']; ?>',
					date : '<?php echo $upcoming_session[$i]['date']; ?>',
					conducted_flag : '<?php echo $upcoming_session[$i]['conducted_flag']; ?>',
					backgroundColor: '#4c9cff',
					eventType : 'UpcomingSession'
				},
			<?php }  } ?>
			<?php
			if(!empty($past_session)) {  
				for ($i=0; $i <count($past_session); $i++) { ?> 
				{
					title : '<?php echo $past_session[$i]['title']; ?>',
					start : '<?php echo $past_session[$i]['date'].'T'.$past_session[$i]['start_time']; ?>',
					link : '<?php echo $past_session[$i]['session_id']; ?>',
					course_name : '<?php echo $past_session[$i]['course_name']; ?>',
					subject_name : '<?php echo $past_session[$i]['subject_name']; ?>',
					topic_name : '<?php echo $past_session[$i]['topic_name']; ?>',
					date : '<?php echo $past_session[$i]['date']; ?>',
					conducted_flag : '<?php echo $past_session[$i]['conducted_flag']; ?>',
					backgroundColor: '#3c763d',
					eventType : 'PastSession'
				},
			<?php } } ?>
			<?php 
			if(!empty($availability_date)) { 
				for ($i=0; $i <count($availability_date); $i++) { ?> 
				{
					start : '<?php echo $availability_date[$i]['date'].'T'.$availability_date[$i]['from_time']; ?>',
					end : '<?php echo $availability_date[$i]['date'].'T'.$availability_date[$i]['to_time']; ?>',
					link : '<?php echo $availability_date[$i]['availability_id']; ?>',
					backgroundColor: '#FFA500',
					eventType : 'Available'
				},
			<?php } } ?>
		],
		eventClick: function(event){
			if(event.eventType == 'PastSession'){
				$('.heading_sec').html(event.date);
				$('.s_date').html(event.date);
				$('.type').html(event.conducted_flag);
				$('.course').html(event.course_name);
				$('.subject').html(event.subject_name);
				$('.topic').html(event.topic_name);
				$('#event_content_popup').modal('show');
			}
			else if(event.eventType == 'UpcomingSession'){
				$('.heading_sec').html(event.date);
				$('.s_date').html(event.date);
				$('.type').html(event.conducted_flag);
				$('.course').html(event.course_name);
				$('.subject').html(event.subject_name);
				$('.topic').html(event.topic_name);
				$('#event_content_popup').modal('show');
			}
		}
		
	});
</script>