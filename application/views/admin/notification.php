<!-- banner section start here-->
<section class="banner_section" style="background-image:url(./assets/admin/img/mid-bg.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>NOTIFICATION</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- edit schedule section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details margin_bottom">
					<div class="notification_blog">
						<div class="notifiaction_info_list_active notifiaction_info_list">
							<span class="date_of_notification_active">Today 02-03-2018 04:00 pm</span>
							<a href="<?php echo base_url();?>view_notification_details"><div class="noti_list_pass">
								<h4>NEW VIDEO ON BOTANY HAS BEEN ADDED</h4>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
								<img src="<?php echo base_url();?>assets/admin/img/right-arrow.png" class="img-responsive notif_link" alt="noti_arrow">
							</div></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- edit schedule section end here-->
				