<!-- banner section start here-->
<section class="banner_section" style="background-image:url(./assets/admin/img/mid-bg.jpg);">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="banner_content">
				<h4>MANAGE CONTENT</h4>
			</div>
		</div>
	</div>
</div>
</section>
<!-- banner section end here-->

<!-- edit schedule section start here-->
<section class="web_section_common">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="section_details">
				<div id="horizontalTab">
					<div class="my_account_tabbing">
						<ul class="nav nav-tabs resp-tabs-list">
							<li class="active"><a class="content_width" data-toggle="tab" href="#logo">Pictures/Logo</a></li>
							<li><a class="content_width" data-toggle="tab" href="#short_clip">Short Clip</a></li>
							<li><a class="content_width" data-toggle="tab" href="#testimonials">Testimonials</a></li>
							<li><a class="content_width" data-toggle="tab" href="#approach">My Approach</a></li>
							<li><a class="content_width" data-toggle="tab" href="#aboutme">About Me</a></li>
							<li><a class="content_width" data-toggle="tab" href="#contactus">Contact Us</a></li>
							<li><a class="content_width" data-toggle="tab" href="#FAQ">FAQ</a></li>
							<li><a class="content_width" data-toggle="tab" href="#tandc">Terms and Conditions</a></li>
							<li><a class="content_width" data-toggle="tab" href="#ppolicy">Privacy Policy</a></li>
							<li><a class="content_width" data-toggle="tab" href="#manage_offers">Manage Offers</a></li>
						</ul>

						<div class="col-md-12">
							<div class="alert alert-success" style="display:none">
							  <strong id="msg" ></strong> 
							</div>
						</div>

						<div class="tab-content resp-tabs-container">
							<div id="logo" class="tab-pane fade in active">
								<div class="tabs_set_my_account">
									<div class="section_details">
										<div class="section_heading_sec dotted_bottom_border">
											<h4>PICTURES LOGO</h4>
										</div>
										<?php if(!empty($picture)){
										foreach ($picture as $key => $value) {?>
										<div class="manage_content_logo">
											<div class="col-md-3 col-sm-3">
												<div class="manage_content_logo_img">
													<img class="img-responsive" src="<?php echo base_url();?>uploads/manage_content/picture/<?php echo $value['image'];?>" alt="Logo" />
												</div>
											</div>
											<div class="col-md-9 col-sm-9">
												<div class="manage_content_logo_name dotted_bottom_border">
													<span class="manage_content_logo_text">
														NAME
													</span>
													<span class="manage_content_logo_edit-delete">
														<a onclick="editPic(<?php echo $value['id'];?>)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
														<a onclick="deletePic(<?php echo $value['id'];?>)"><i class="fa fa-trash" aria-hidden="true"></i></a>
													</span>
												</div>
												<div class="manage_content_group_business">
													<?php echo $value['name'];?>
												</div>
											</div>
										</div>
										<?php } }?>
										<div class="col-md-12">
											<div class="manage_content_other_pictures_btn">
												<a onclick="show_pic_form()"><i class="fa fa-plus" aria-hidden="true"></i> Other Pictures</a>
											</div>
										</div>
										<!--***********-->
										<div class="manage_content_logo" id="pic_div" style="display:none;">
											<form id="pic_from" enctype="multipart/form-data">
											<div class="col-md-12">
											<div class="col-md-3 col-sm-3">
												<!--<div class="manage_content_logo_img">
													<div class="custom-file-input content_file_width">
														<input type="file" >
														<button type="button" class="btn upload_btn content_upload_img"><i class="fa fa-upload" aria-hidden="true"></i></button>
													</div>		
												</div>-->
												<div class="manage_content_logo_name dotted_bottom_border">
													<span class="manage_content_logo_text">
														Upload Picture
													</span>
												</div>
												<div class="manage_content_logo_img">
													<input type="file" name="image" class="form-control" placeholder="UPLOAD FILE">
												</div>
												<div class="manage_content_logo_img" id="picImgDiv" style="display:none">
													<img class="img-responsive" src="" id="pic_image" alt="Logo" />
												</div>
											   
											</div>
											<div class="col-md-9 col-sm-9">
												<div class="manage_content_logo_name dotted_bottom_border">
													<span class="manage_content_logo_text">
														NAME
													</span>
												</div>
												<div class="manage_content_group_business">
													<div class="col-md-6 col-sm-3">
													<input type="text" name="name" class="form-control" id="name">
													<input type="hidden" name="id">
													</div>
													<input type="hidden" name="type" id="type" value="picture">
													<div class="col-md-6 col-sm-3">
													<div class="manage_content_other_pictures_btn">
														<a onclick="savePic()"> Submit</a>
													</div>
												</div>
												
												</div>
											</div>										
											</div>
											</form>
										</div>
										<!--***********-->
										
									</div>
								</div>
							</div>
							<div id="short_clip" class="tab-pane fade">
								<div class="tabs_set_my_account">
									<div class="section_details">
										<div class="section_heading_sec dotted_bottom_border">
											<h4>SHORT CLIP</h4>
										</div>
										<?php if(!empty($clip)){
										foreach ($clip as $key => $value) {?>
										<div class="manage_content_logo">
											<div class="col-md-4 col-sm-4">
												<div class="manage_content_logo_img">
													<img class="img-responsive" src="<?php echo base_url();?>uploads/manage_content/clip/<?php echo $value['image'];?>" alt="short_clip" />
												</div>
											</div>
											<div class="col-md-8 col-sm-8">
												<div class="manage_content_logo_name dotted_bottom_border">
													<span class="manage_content_logo_text">
														NAME
													</span>
													<span class="manage_content_logo_edit-delete">
														<a onclick="deleteClip(<?php echo $value['id'];?>)"><i class="fa fa-trash" aria-hidden="true"></i></a>
													</span>
												</div>
												<div class="manage_content_group_business">
													<?php echo $value['name'];?>
												</div>
											</div>
										</div>
										<?php } }?>

										<div class="col-md-12">
											<div class="manage_content_other_pictures_btn">
												<a onclick="show_clip_form()"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
											</div>
										</div>

										<div class="manage_content_logo" id="clip_div" style="display:none;">
										<form id="clip_from" enctype="multipart/form-data">
											<div class="col-md-4 col-sm-4">
												<!--<div class="manage_content_logo_img">
													<div class="custom-file-input content_file_width">
														<input type="file">
														<button type="button" class="btn upload_btn content_upload_img"><i class="fa fa-upload" aria-hidden="true"></i></button>
														<input type="text" class="form-control content_upload_section" placeholder="UPLOAD FILE">
													</div>
												</div>-->
												<div class="manage_content_logo_name dotted_bottom_border">
													<span class="manage_content_logo_text">
														Upload Clip
													</span>
												</div>
												<div class="manage_content_logo_img">
													<input type="file" name="image" class="form-control" placeholder="UPLOAD FILE">
												</div>
											</div>
											<div class="col-md-8 col-sm-8">
												<div class="manage_content_logo_name dotted_bottom_border">
													<span class="manage_content_logo_text">NAME</span>
												</div>
												<div class="manage_content_group_business">
													<input class="form-control enter_clip_name" name="name" id="name" type="text" placeholder="Enter Name">
													<input type="hidden" name="type" id="type" value="clip">
													
												</div>
												<div class="add_topic_btn short_clip_btn">
													<a onclick="saveClip();" class="btn save_cancel_btn" >Save</a>
													<a href="#." class="btn save_cancel_btn">Cancel</a>
												</div>
											</div>
										</form>
										</div>


									</div>
								</div>
							</div>
							<div id="testimonials" class="tab-pane fade">
								<div class="tabs_set_my_account">
									<div class="section_details">
										<div class="section_heading_sec dotted_bottom_border">
											<h4>TESTIMONIALS</h4>
										</div>
										<?php if(!empty($testimonial)){
										foreach ($testimonial as $key => $value) {?>							
										<div class="testimonials_manage_content_logo">
											<div class="col-md-2 col-sm-2">
												<div class="">
													<img class=" testimonials_profile_pic img-responsive" src="<?php echo base_url();?>uploads/manage_content/testimonials/<?php echo $value['image'];?>" alt="testimonial" />
												</div>
											</div>
											<div class="col-md-5 col-sm-5">
												<div class="manage_content_logo_name dotted_bottom_border">
													<span class="manage_content_logo_text">
														NAME
													</span>
												</div>
												<div class="manage_content_group_business">
													<?php echo $value['name'];?>
												</div>
											</div>
											<div class="col-md-5 col-sm-5 right_padding">
												<div class="manage_content_logo_name dotted_bottom_border">
													<span class="manage_content_logo_text">
														RATINGS
													</span>
													<span class="manage_content_logo_edit-delete">
														<a onclick="editTestim(<?php echo $value['id'];?>);"><i class="fa fa-pencil" aria-hidden="true"></i></a>
														<a onclick="deleteTestim(<?php echo $value['id'];?>)"><i class="fa fa-trash" aria-hidden="true"></i></a>
													</span>
												</div>
												<div class="manage_content_group_business rating_fa_color">
													<?php $limit=5;
														  $rating=$value['rating'];
														  $remain=$limit-$rating;
													for($i=1;$i<=$rating;$i++){?>

													<a href="#."><i class="fa fa-star" aria-hidden="true"></i></a>
												     <?php }
												     for($i=1;$i<=$remain;$i++){?>

													<a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a>
												     <?php } ?>
												</div>
											</div>
											<div class="col-md-10 col-sm-10 right_padding">
												<div class="manage_content_logo_name dotted_bottom_border">
													<span class="manage_content_logo_text">
														COMMENTS
													</span>
												</div>
												<div class="manage_content_comment_text">
													<?php echo $value['comments'];?>
												</div>
											</div>	
										</div>
										<?php } }?>

									</div>
									<div class="col-md-12">
										<div class="manage_content_other_pictures_btn">
											<a onclick="show_testi_form()"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
										</div>
									</div>
									<div class="testimonials_manage_content_logo" id="testi_div" style="display:none;">
										<form id="testi_form">
										<div class="col-md-3 col-sm-3">
											<!--<div class="manage_content_logo_img">
												<div class="custom-file-input testimonials_content_img_width">
													<input type="file">
													<button type="button" class="btn upload_btn content_upload_img"><i class="fa fa-upload" aria-hidden="true"></i></button>
													<input type="text" class="form-control testimonials_img_upload_section" placeholder="UPLOAD IMAGE">
												</div>
											</div>-->
											<div class="manage_content_logo_name dotted_bottom_border">
													<span class="manage_content_logo_text">
														Upload Image
													</span>
												</div>
												<div class="manage_content_logo_img">
													<input type="file" name="image" class="form-control" placeholder="UPLOAD FILE">
												</div>

												<div class="manage_content_logo_img" id="testimImgDiv" style="display:none">
													<img class="img-responsive" src="" id="testim_image" alt="Logo" />
												</div>
										</div>
										<div class="col-md-9 col-sm-9 right_padding">
											<div class="col-md-6">
												<div class="manage_content_logo_name dotted_bottom_border">
													<span class="manage_content_logo_text">NAME</span>
												</div>
												<div class="manage_content_group_business">
													<input class="form-control" type="text" placeholder="Enter Name" name="name">
													<input type="hidden" name="id">
												</div>
											
											</div>
											<div class="col-md-6 right_padding">
												<div class="manage_content_logo_name dotted_bottom_border">
													<span class="manage_content_logo_text">
														RATINGS
													</span>
												</div>
												<div class="star_ratting_testimonial_views">
												<ul>
													<li class="rating_star" value="1"><a href="#."><i class="star1 fa fa-star-o" aria-hidden="true"></i></a></li>
													<li class="rating_star" value="2"><a href="#."><i class="star2 fa fa-star-o" aria-hidden="true"></i></a></li>
													<li class="rating_star" value="3"><a href="#."><i class="star3 fa fa-star-o" aria-hidden="true"></i></a></li>
													<li class="rating_star" value="4"><a href="#."><i class="star4 fa fa-star-o" aria-hidden="true"></i></a></li>
													<li class="rating_star" value="5"><a href="#."><i class="star5 fa fa-star-o" aria-hidden="true"></i></a></li>
												</ul>
											</div>
											<input type="hidden" name="rating" value="" id="rating_star">
									
											</div>
											<div class="col-md-12 right_padding">
												<div class="manage_content_logo_name dotted_bottom_border">
													<span class="manage_content_logo_text">COMMENTS</span>
												</div>
												<div class="manage_content_group_business ">
													<textarea class="form-control testimonials_textarea" placeholder="Enter Comments" name="comments"></textarea>
												</div>
											</div>
											<div class="col-md-12">
												<div class="add_topic_btn short_clip_btn">
													<a onclick="saveTestim()" class="btn save_cancel_btn" data-toggle="modal" data-target="#add_course_popup">Save</a>
													<a onclick="show_testi_form()" class="btn save_cancel_btn">Cancel</a>
												</div>
											</div>	
										</div>
										</form>
									</div>
								</div>
							</div>
							<div id="approach" class="tab-pane fade">
								<div class="tabs_set_my_account">
									<div class="section_details">
										<div class="section_heading_sec dotted_bottom_border">
											<h4>MY APPROACH</h4>
										</div>
										<?php if(!empty($my_approach)){
											foreach ($my_approach as $key => $ma) {?>
											
										<div class="testimonials_manage_content_logo">
											<div class="col-md-12 right_padding left_padding">
												<form id="approach_form">
												<div class="manage_content_logo_edit-delete">
													<a onclick="editApproach(<?php echo $ma['id'];?>)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
													<!--<a onclick="deleteApproach(<?php //echo $ma['id'];?>)"><i class="fa fa-trash" aria-hidden="true"></i></a>-->
												</div>
												<div class="approach_text_area">
													<textarea readonly="" class="form-control testimonials_textarea" placeholder="Enter Comments" name="content" id="approach_content"><?php echo $ma['content'];?></textarea>
													<input type="hidden" name="key_type" value="my_approach">
												</div>
												<div class="add_topic_btn short_clip_btn" id="saving" style="display: none">
													<a onclick="saveContent('my_approach')" class="btn save_cancel_btn" data-toggle="modal" data-target="#add_course_popup">Save</a>
													<a href="#." class="btn save_cancel_btn">Cancel</a>
												</div>
											    </form>
											</div>
										</div>
									<?php } } ?>
									</div>
								</div>
							</div>
							<div id="aboutme" class="tab-pane fade">
								<div class="tabs_set_my_account">
									<div class="section_details">
										<div class="section_heading_sec dotted_bottom_border">
											<h4>ABOUT ME</h4>
										</div>
										<?php if(!empty($about_me)){
										foreach ($about_me as $key => $am) {?>
										<div class="testimonials_manage_content_logo">
											<div class="col-md-12 right_padding left_padding">
												<form id="aboutme_form">
												<div class="manage_content_logo_edit-delete">
													<a onclick="editAbout(<?php echo $am['id'];?>)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
													<!-- <a href="#."><i class="fa fa-trash" aria-hidden="true"></i></a> -->
												</div>
												<div class="approach_text_area">
													<textarea class="form-control testimonials_textarea" placeholder="Enter Comments" readonly="" name="content" id="aboutme_content"><?php echo $am['content'];?></textarea>
													<input type="hidden" name="key_type" value="about_me">
												</div>
												<div class="add_topic_btn short_clip_btn" id="saving1" style="display: none">
													<a onclick="saveContent('about_me')" class="btn save_cancel_btn" data-toggle="modal" data-target="#add_course_popup">Save</a>
													<a href="#." class="btn save_cancel_btn">Cancel</a>
												</div>
											</form>
											</div>
										</div>
									<?php } }?>
									</div>
								</div>
							</div>
							<div id="contactus" class="tab-pane fade">
								<div class="tabs_set_my_account">
									<div class="section_details">
										<div class="section_heading_sec dotted_bottom_border">
											<h4>CONTACT US</h4>
										</div>
										<?php if(!empty($about_me)){ 
											foreach ($contact_us as $key => $cu) {?>
										<div class="testimonials_manage_content_logo">
											<div class="col-md-12 right_padding left_padding">
												<form id="contact_form">
												<div class="manage_content_logo_edit-delete">
													<a onclick="editContact(<?php echo $cu['id'];?>)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
													<!-- <a href="#."><i class="fa fa-trash" aria-hidden="true"></i></a> -->
												</div>
												<div class="approach_text_area">
													<textarea class="form-control testimonials_textarea" placeholder="Enter Comments" name="content" id="contact_content" readonly=""><?php echo $cu['content'];?></textarea>
													<input type="hidden" name="key_type" value="contact_us">
												</div>
												<div class="add_topic_btn short_clip_btn" id="saving2" style="display: none">
													<a onclick="saveContent('contact_us')" class="btn save_cancel_btn" data-toggle="modal" data-target="#add_course_popup">Save</a>
													<a href="#." class="btn save_cancel_btn">Cancel</a>
												</div>
											</form>
											</div>
										</div>
										<?php } }?>
									</div>
								</div>
							</div>
							<div id="FAQ" class="tab-pane fade">
								<div class="tabs_set_my_account">
							
									<div class="section_details">
										<div class="section_heading_sec dotted_bottom_border">
											<h4>FAQ'S</h4>
										</div>
										<div class="col-md-12 right_padding left_padding ">
											<div class="manage_content_logo_edit-delete fa_bottom_padding">
												<!--<a href="#."><i class="fa fa-pencil" aria-hidden="true"></i></a>
												<a href="#."><i class="fa fa-trash" aria-hidden="true"></i></a>-->
												<a href="#."><i class="fa fa-upload" aria-hidden="true"></i></a>
											</div>
										</div>
									<?php if(!empty($faqList)){
										$i=1;foreach ($faqList as $key => $value) {?>
										<div class="col-md-12 dotted_top_border">
											<div class="faq_ans_qua">
												<div class="col-md-12 right_padding left_padding ">
												<div class="manage_content_logo_edit-delete fa_bottom_padding">
													<a onclick="editFaq(<?php echo $value["id"];?>)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
													<a onclick="deleteFaq(<?php echo $value["id"];?>)"><i class="fa fa-trash" aria-hidden="true"></i></a>
												</div>
											</div>
												<span class="faq_questions"><b>Q<?php echo $i;?>:- > <?php echo $value['question'];?></b></span>
												<span class="faq_answer"><b>Answer:-</b> > <?php echo $value['answer'];?></span>
											</div>
										</div>
									<?php $i++;} }?>
											
										<div class="col-md-12">
											<div class="faq_add_mode_btn">
												<a onclick="show_faqform()"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
											</div>
										</div>

										<div id="faq_div" style="display:none">
										<div class="col-md-12" >
											<div class="input_questions_ans">
												<form id="faq_form">
													<div class="form-group">
														<label for="questions">Question</label>
														<input type="hidden" name="id" id="id">
														<input type="text" name="question" class="form-control" id="question">
													</div>
													<div class="form-group">
														<label for="answer">Answer</label>
														<textarea class="form-control testimonials_textarea textarea_height" name="answer" id="answer"></textarea>
													</div>
												</form>
											</div>
										</div>
										<div class="col-md-12">
											<div class="add_topic_btn short_clip_btn faq_ans_qua">
												<a onclick="save_faq()" class="btn save_cancel_btn" >Save</a>
												<a href="#." class="btn save_cancel_btn">Cancel</a>
											</div>
										</div>	
										</div>

									</div>

								</div>
							</div>
							<div id="tandc" class="tab-pane fade">
								<div class="tabs_set_my_account">
									<div class="section_details">
										<div class="section_heading_sec dotted_bottom_border">
											<h4>TERMS AND CONDITIONS</h4>
										</div>
										<?php if(!empty($terms_condition)){
											foreach ($terms_condition as $key => $tc) {?>
										<div class="testimonials_manage_content_logo">
											<div class="col-md-12 right_padding left_padding">
												<form id="terms_form">
												<div class="manage_content_logo_edit-delete">
													<a onclick="editTerms(<?php echo $tc['id'];?>)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
													<!-- <a href="#."><i class="fa fa-trash" aria-hidden="true"></i></a> -->
												</div>
												<div class="approach_text_area">
													<textarea class="form-control testimonials_textarea" name="content" id="terms_content" readonly="">
														<?php echo $tc['content'];?>
													</textarea>
													<input type="hidden" name="key_type" value="terms_condition">
												</div>
												<div class="add_topic_btn short_clip_btn" id="saving3" style="display: none;">
													<a onclick="saveTerms('terms_condition')" class="btn save_cancel_btn" data-toggle="modal" data-target="#add_course_popup">Save</a>
													<a href="#." class="btn save_cancel_btn">Cancel</a>
												</div>
											</form>
											</div>
										</div>
									<?php } }?>
									</div>
								</div>
							</div>
							<div id="ppolicy" class="tab-pane fade">
								<div class="tabs_set_my_account">
									<div class="section_details">
										<div class="section_heading_sec dotted_bottom_border">
											<h4>PRIVACY POLICY</h4>
										</div>
										<?php  if(!empty($privacy_policy)){ 
											foreach ($privacy_policy as $key => $pp) {?>
										<div class="testimonials_manage_content_logo">
											<div class="col-md-12 right_padding left_padding">
												<form id="privacy_form">
												<div class="manage_content_logo_edit-delete">
													<a onclick="editPrivacy(<?php echo $pp['id'];?>)"><i class="fa fa-pencil" aria-hidden="true"></i></a>
												</div>
												<div class="approach_text_area">
													<textarea class="form-control testimonials_textarea" name="content" id="privacy_content" readonly="">
														<?php echo $pp['content'];?>
													</textarea>
													<input type="hidden" name="key_type" value="privacy_policy">
												</div>
												<div class="add_topic_btn short_clip_btn" id="saving4" style="display: none">
													<a onclick="saveContent('privacy_policy')" class="btn save_cancel_btn" data-toggle="modal" data-target="#add_course_popup">Save</a>
													<a href="#." class="btn save_cancel_btn">Cancel</a>
												</div>
											</form>
											</div>
										</div>
									<?php } }?>
									</div>
								</div>
							</div>

							<div id="manage_offers" class="tab-pane fade">
								<div class="tabs_set_my_account">
									<div class="section_details">
										<div class="section_heading_sec dotted_bottom_border">
											<h4>MANAGE OFFERS</h4>
										</div>
										
										<?php //echo"<pre>";print_r($offerList);exit;
										if(!empty($offerList)){
										foreach ($offerList as $key => $value) {?>
										
										<div class="testimonials_manage_content_logo">
											<div class="col-md-12 right_padding left_padding">
												<div class="dotted_bottom_border">
													<span class="manage_content_logo_text">TITLE</span>
													<span class="manage_content_logo_edit-delete">
														<a onclick="edit_offer('<?php echo $value['id'];?>')"><i class="fa fa-pencil" aria-hidden="true"></i></a>
														 <a onclick="delete_offer('<?php echo $value['id'];?>')" ><i class="fa fa-trash"></i></a>
													</span>
												</div>
												<div class="manage_content_group_business">
													<?php echo $value['title'];?>
												</div>
											</div>
											<div class="col-md-6 col-sm-6 left_padding">
												<div class="manage_content_logo_name dotted_bottom_border manage_content_logo_text">
													APPLICABLE FROM
												</div>
												<div class="manage_content_group_business">
													<?php echo $value['applicable_from_date'];?>
												</div>
											</div>
											<div class="col-md-6 col-sm-6 right_padding">
												<div class="manage_content_logo_name dotted_bottom_border manage_content_logo_text">
													APPLICABLE TO
												</div>
												<div class="manage_content_group_business">
													<?php echo $value['applicable_to_date'];?>
												</div>
											</div>
											<div class="col-md-12 right_padding left_padding">
												<div class="manage_content_logo_name dotted_bottom_border">
													<span class="manage_content_logo_text">
														DETAILS
													</span>
												</div>
												<div class="manage_content_comment_text manage_content_group_business">
													<?php echo $value['details'];?>
												</div>
											</div>	
										</div>

									 <?php }}?>							
							            
									</div>

									<div class="col-md-12 right_padding left_padding">
										<div class="faq_add_mode_btn">
											<a href="#." onclick="show_form()"><i class="fa fa-plus" aria-hidden="true"></i> Add More</a>
										</div>
									</div>
								</div>

								<!--************manage offer form**************-->
								<form id="form" action="" method="">
								<div class="testimonials_manage_content_logo" id="manage_content_logo_form" style="display:none">
									<div class="col-md-12 right_padding left_padding">
										<div class="dotted_bottom_border">
											<span class="manage_content_logo_text">TITLE</span>
										</div>
										<div class="manage_content_group_business">
											<input type="text" name="title" id="title">
											<input type="hidden" name="id" id="id">
										</div>
									</div>
									<div class="col-md-6 col-sm-6 left_padding">
										<div class="manage_content_logo_name dotted_bottom_border manage_content_logo_text">
											APPLICABLE FROM
										</div>
										<div class="manage_content_group_business">
											<input type="text" name="applicable_from_date" id="applicable_from_date">
										</div>
									</div>
									<div class="col-md-6 col-sm-6 right_padding">
										<div class="manage_content_logo_name dotted_bottom_border manage_content_logo_text">
											APPLICABLE TO
										</div>
										<div class="manage_content_group_business">
											<input type="text" name="applicable_to_date" id="applicable_to_date">
										</div>
									</div>
									<div class="col-md-12 right_padding left_padding">
										<div class="manage_content_logo_name dotted_bottom_border">
											<span class="manage_content_logo_text">
												DETAILS
											</span>
										</div>
										<div class="manage_content_comment_text manage_content_group_business">
											<textarea name="details" id="details"></textarea>
										</div>
									</div>	
									<div class="col-md-12 right_padding left_padding">
										<div class="faq_add_mode_btn">
											<a href="#." onclick="save_offer()"> Submit</a>
										</div>
									</div>
								</div>
								</form>
								<!--**************************-->

							</div>
							

						</div>	
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
</section>
<!-- edit schedule section end here-->
<!-- popup start Here -->
<!--added course popup start here-->
<div class="modal fade" id="add_course_popup" role="dialog">
<div class="modal-dialog delet_cours_popup_width">
	<!-- Modal content-->
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">
				<img src="<?php echo base_url();?>assets/admin/img/pop_up_close.png" class="img-responsive" alt="close"/>
			</button>
		</div>
		<div class="modal-body manage_content_logo">
			<div class="moel_details">
				<div class="model_content">
					<p>Your Changes has been saved.</p>
					<div class="pop_btn_sect_sec">
						<a href="" class="btn popup_coom_btn_new margin_zero" data-dismiss="modal" data-toggle="modal">OK</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!--added course popup end here-->	  
<!-- popup end Here -->  
</div>
<script>
$(document).ready(function () {
$('#horizontalTab').easyResponsiveTabs({
type: 'default', //Types: default, vertical, accordion           
width: 'auto', //auto or any width like 600px
fit: true,   // 100% fit in a container
closed: 'accordion', // Start closed if in accordion view
});
$('#verticalTab').easyResponsiveTabs({
type: 'vertical',
width: 'auto',
fit: true
});
});
</script>

<!--==========Manage offer==============//-->
<script src="<?php echo base_url() ?>assets/js/manage_offer.js"></script>
<!--datepicker-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!--==========Manage faq==============//-->
<script src="<?php echo base_url() ?>assets/js/manage_faq.js"></script>
<script src="<?php echo base_url() ?>assets/js/manage_pictures.js"></script>
<script src="<?php echo base_url() ?>assets/js/manage_clip.js"></script>
<script src="<?php echo base_url() ?>assets/js/manage_testimonial.js"></script>


<script src="<?php echo base_url() ?>assets/js/manage_content.js"></script>

 	

