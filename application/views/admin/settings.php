
<!-- banner section start here-->
<section class="banner_section" style="background-image:url(./assets/admin/img/mid-bg.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>SETTING</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- schedule new session section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					
					<div class="section_heading_sec dotted_bottom_border">
						<h4>CHANGE PASSWORD</h4>
					</div>
					<div class="settings_session_form">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="alert alert-success" style="display: none"></div>
								<form id="form">
								<div class="scedule_info_details">
									<div class="row">
										<div class="col-md-12">
											
											<div class="form_set">
												<label>Old Password</label>
												<input type="password" class="form-control" id="oldpassword" name="oldpassword"/>
												<input type="hidden" id="oldPassDB" value="<?php print_r($oldPass);?>">
												<span style="display: none;color: red" id="oldp_err">Old password is not match!</span>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form_set">
												<label>New Password</label>
												<input type="password" class="form-control" id="new_password" name="new_password"/>
												<span style="display: none;color: red" id="err_npass"></span>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form_set">
												<label>Re-type Password</label>
												<input type="password" class="form-control" id="rtype_password" name="rtype_password"/>
												<span style="display: none;color: red" id="retypep_err"> Password is mismatch!</span>
											</div>
										</div>
										<span style="display: none;color: red" id="passmiss_err">Enter valid password!</span>
									</div>
								</div>
								<div class="schedul_btn_group">
									<a onclick="changePass()" class="btn save_cancel_btn">Update</a>
									<a href="#." class="btn save_cancel_btn">Cancel</a>
								</div>
							</form>
							</div>

							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="setting_main_sid_img">
									<div class="setting_right_img" style="background-image:url(./img/setting_back_img.png);"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- schedule new session section end here-->
<script src="<?php echo base_url() ?>assets/js/setting.js"></script>