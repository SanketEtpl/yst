<!-- banner section start here-->
<section class="banner_section" style="background-image:url(./assets/admin/img/mid-bg.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>MANAGE SESSION</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- manage session section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
		<div class="col-md-8">
	       <div class="alert alert-success" style="display:none">
	        <strong id="msg" ></strong> 
	       </div>
	    </div> 
			<div class="col-md-12">
				<div class="section_details">
					<div class="section_heading_sec">
						<h4>SESSION LIST</h4>
						<div class="addes_new_search_box">
							<!-- <input type="text" class="form-control" placeholder="Search"/> -->
							<a href="<?php echo base_url()?>schedule_new_session" class="btn new-comm-btn manage_session_responsive_btn">Schedule a New Session</a>
						</div>
					</div>
					<div class="cours_table">
						<div class="table-responsive">
						  <table class="table cours_table_set" id="manage_session">
							<thead>
							  <tr>
								<th>SN</th>
								<th>Session</th>
								<th>Course</th>
								<th>Subject</th>
								<th>Topic</th>
								<th>Date</th>
								<th>Start time</th>
								<th>End time</th>
								<th>Duration</th>
								<th>Cost</th>
								<th>Action</th>
							  </tr>
							</thead>
							<tbody>
							<?php if(!empty($session_list)) { 
								$i=1;
								foreach($session_list as $session) { 
							?>
							  <tr>
								<td><?php echo $i;?></td>
								<td><?php echo $session['title'];?></td>
								<td><?php echo $session['course_name'];?></td>
								<td><?php echo $session['subject_name'];?></td>
								<td class="topic_nam_table_width_new"><?php echo $session['topic_name'];?></td>
								<td><?php echo $session['date'];?></td>
								<td><?php echo date('H:i:s',strtotime($session['start_time']));?></td>
								<td><?php echo date('H:i:s',strtotime($session['end_time']));?></td>
								<td><?php echo $session['duration_in_minutes'];?></td>
								<td><?php echo $session['cost'];?></td>
								<td>
									<div class="delet_edit_table_link_new">
										<a href="<?php echo base_url()?>edit_new_session?id=<?php echo base64_encode($session['session_id'])?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
										<a class="select_delete" session_id="<?php echo $session['session_id']?>" onclick="deleteSession('<?php echo $session['session_id']?>');"><i class="fa fa-trash" aria-hidden="true"></i></a>
									</div>
								</td>
							  </tr>
							  <?php $i++ ; }  } ?>
							</tbody>
						 </table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- manage session section end here-->
<script src="<?php echo base_url();?>assets/js/session.js"></script>>