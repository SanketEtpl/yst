
<!-- banner section start here-->
<section class="banner_section" style="background-image:url(./assets/admin/img/mid-bg.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>SCHEDULE A NEW SESSION</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- schedule new session section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="col-md-8">
	       <div class="alert alert-success" style="display:none">
	        <strong id="msg" ></strong> 
	       </div>
	    </div> 
		<div class="row">
			<div class="col-md-12">
				<div class="section_details schedule_margin_bottom">
					<div class="section_heading_sec dotted_bottom_border">
						<h4>SCHEDULE A GROUP SESSION</h4>
					</div>
					<div class="schedul_session_form">
						<div class="row">
							<div class="col-md-6 col-sm-12 col-xs-12">
							<form  id="form_session">
								<div class="scedule_info_details">
									<div class="row">
										<div class="col-md-6 col-sm-4 col-xs-12">
											<div class="form_set">
												<label>Course</label>
												<select class="form-control course-id" onchange="getSubjectlist();" id="select_course" name="course_id">
													<option selected>Select Course</option>
													<?php if(!empty($course_list)) { 
														foreach($course_list as $course) { 
													?>
													<option value="<?php echo $course['course_id']?>"><?php echo $course['course_name']?></option>
													<?php  } } ?>
												</select>
												<span class="err_course" style="color: red; display: none;">Please select course.</span>
											</div>
										</div>
										<div class="col-md-6 col-sm-4 col-xs-12">
											<div class="form_set">
												<label>Subject</label>
												<select class="form-control select_subject" id="select_subject" onchange="getTopiclist();" name="subject_id">
													<option selected>Select Subject</option>
												</select>
												<span class="err_subject" style="color: red; display: none;">Please select subject.</span>
											</div>
										</div>
									</div>
									<div class="row">	
										<div class="col-md-12 col-sm-4 col-xs-12">
											<div class="form_set">
												<label>Topic</label>
												<select class="form-control select_topic" id="select_topic" name="topic_id">
													<option selected>Select Topic</option>
												</select>
												<span class="err_topic" style="color: red; display: none;">Please select topic.</span>
											</div>
										</div>
										<div class="col-md-6 col-sm-4 col-xs-12">
											<div class="form_set">
												<label>Session Title</label>
												<input class="form-control" id="select_title" name="title" placeholder="Session Title">
												<span class="err_title" style="color: red; display: none;">Please enter title.</span>
											</div>
										</div>
										<div class="col-md-6 col-sm-4 col-xs-12">
											<div class="form_set">
												<label>Date</label>
												<div class="form-group calend_div">
													<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
														<input class="form-control calender_set" type="text" value="" name="date" id="select_date" readonly="" placeholder="Date">
														<span class="input-group-addon custom_input_calend"><i class="fa fa-calendar" aria-hidden="true"></i></span>
													</div>
													<input type="hidden" id="dtp_input2" value="" />
												</div>
												    <span class="err_date" style="color: red; display: none;">Please select date.</span>
											</div>
										</div>
								       <!--  <div class="col-md-6 col-sm-4 col-xs-12">
										    <div class="row">
										        <div class='col-sm-6'>
										            <div class="form-group">
										                <div class='input-group date' id='datetimepicker3'>
										                    <input type='text' class="form-control" />
										                    <span class="input-group-addon">
										                        <span class="glyphicon glyphicon-time"></span>
										                    </span>
										                </div>
										            </div>
										        </div>
										        <script type="text/javascript">
										            $(function () {
										                $('#datetimepicker3').datetimepicker({
										                    format: 'LT'
										                });
										            });
										        </script>
										    </div>
										</div> -->
										<div class="col-md-6 col-sm-4 col-xs-12">
											<div class="form_set">
												<label>Start Time</label>
												<!-- <input class="form-control" id="start_time" name="start_time" placeholder="hh:mm:ss"> -->
												<div class="input_boxes_set">
													<div class="form-group calend_div_new">
														<div class="input-group date form_time" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
															<input class="form-control calender_set required_check" size="16" type="text"  name="start_time" id="start_time" placeholder="00:00">
															<div class="input-group-addon custom_input_calend"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
														</div>
														<input type="hidden" id="dtp_input3" value="">
													</div>
												</div>
												<span class="err_stime" style="color: red; display: none;">Please select start time.</span>
											</div>
										</div>
										<div class="col-md-6 col-sm-4 col-xs-12">
											<div class="form_set">
												<label>End Time</label>
												<!-- <input class="form-control" id="end_time" name="end_time" placeholder="hh:mm:ss"> -->
												<div class="input_boxes_set">
													<div class="form-group calend_div_new">
														<div class="input-group date form_time" data-date="" data-date-format="hh:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
															<input class="form-control calender_set required_check" size="16" type="text"  name="end_time" id="end_time" placeholder="00:00">
															<div class="input-group-addon custom_input_calend"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
														</div>
														<input type="hidden" id="dtp_input3" value="">
													</div>
												</div>
												<span class="err_etime" style="color: red; display: none;">Please select end time.</span>
											</div>
										</div>
										<div class="col-md-6 col-sm-4 col-xs-12">
											<div class="form_set">
												<label>Total Duration (In minutes)</label>
												<input class="form-control" id="duration" name="duration_in_minutes" readonly="" placeholder="Total Duration (In minutes)">
												<span class="err_duration" style="color: red; display: none;">Please enter duration.</span>
											</div>
										</div>
										<div class="col-md-6 col-sm-4 col-xs-12">
											<div class="form_set">
												<label>Cost</label>
												<input class="form-control" id="cost" name="cost" placeholder="Cost">
												<span class="err_cost" style="color: red; display: none;">Please enter cost.</span>
											</div>
										</div>
										<input type="hidden" name="conducted_flag" value="group">
									</div>
								</div>
								<div class="schedul_btn_group">
									<!-- <a href="#." class="btn save_cancel_btn" data-toggle="modal" data-target="#success_add_user_popup">Save</a> -->
									<button type="button" id="btnLogin" class="btn save_cancel_btn" onclick="addSession();">Save</button>
									<a href="#." class="btn save_cancel_btn">Cancel</a>
								</div>
								</form>
							</div>
							<div class="col-md-6">
								<div class="calender_here">
									<div class="calender_heading">
										<h3>MY CALENDER</h3>
									</div>
									<!-- <div class="calender">
										<ul class="month">
											<li>
												<h4>January</h4>
												<h5>2016</h5>
											</li>
											<span class="prev"><img src="<?php echo base_url();?>assets/admin/img/arrow_left.png" class="img-responsive arro_size" alt="arrow"/></span>
											<span class="next"><img src="<?php echo base_url();?>assets/admin/img/arrow_right.png" class="img-responsive arro_size" alt="arrow"/></span>
										</ul>
										<ul class="weeks">
											<li>Sa</li>
											<li>Su</li>
											<li>Mo</li>
											<li>Tu</li>
											<li>We</li>
											<li>Th</li>
											<li>Fr</li>
										</ul>
										<ul class="days">

										</ul>
									</div> -->
									<div id="session_calender"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- schedule new session section end here-->

<!-- popup start Here -->
<!--new group added popup start here-->
<div class="modal fade" id="success_add_user_popup" role="dialog">
	<div class="modal-dialog delet_cours_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url();?>assets/admin/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="moel_details">
					<span class="model_header annim_img">
						<img src="<?php echo base_url();?>assets/admin/img/add_course.png" class="img-responsive" alt="forgotpassword"/>
						<h5>New Group</h5>
					</span>
					<div class="model_content">
						<p>New Group Session has been added by you.</p>
						<div class="pop_btn_sect_sec">
							<a href="#." id="addes_succe_remo_back_pop" class="btn popup_coom_btn_new margin_zero" data-toggle="modal" data-target="#updated_user_popup" data-dismiss="modal">OK</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--new group added popup end here-->

<!--new group updated popup start here-->
<div class="modal fade" id="updated_user_popup" role="dialog">
	<div class="modal-dialog delet_cours_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url();?>assets/admin/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="moel_details">
					<span class="model_header annim_img">
						<img src="<?php echo base_url();?>assets/admin/img/add_course.png" class="img-responsive" alt="forgotpassword"/>
						<h5>New Group Updated</h5>
					</span>
					<div class="model_content">
						<p>New Group Session has been Updated by you.</p>
						<div class="pop_btn_sect_sec">
							<a href="#." id="addes_succe_remo_back_pop" class="btn popup_coom_btn_new margin_zero" data-toggle="modal" data-target="#link_add_segister_popup" data-dismiss="modal">OK</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--new group updated popup end here-->

<!--new group updated popup start here-->
<div class="modal fade" id="link_add_segister_popup" role="dialog">
	<div class="modal-dialog delet_cours_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url();?>assets/admin/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="moel_details">
					<div class="model_content set_rest_nam_pop">
						<p>Ammy j, has been added as a attendee in this group session.Link has been sent to resistered email id</p>
						<div class="pop_btn_sect_sec">
							<a href="#." id="addes_succe_remo_back_pop" class="btn popup_coom_btn_new margin_zero" data-dismiss="modal">OK</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--new group updated popup end here-->
<!--event popup start here-->
<div class="modal fade" id="event_content_popup" role="dialog">
	<div class="modal-dialog add_topic_width_popup">
	<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal">
			      <img src="<?php echo base_url();?>assets/admin/img/pop_up_close.png" class="img-responsive" alt="close"/>
			    </button>
			</div>
			<div class="modal-body">
			<div class="moel_details_new">
				<div class="section_heading_sec dotted_bottom_border">
				<h4>SESSION DETAILS</h4>
				</div>
				<div class="model_content">
					<div class="add_topic_form">
					    <div class="session_list_blog_list_here">
							<h4 class="session_heading_date heading_sec">
								
							</h4>
							<div class="session_blog_list">
								<ul>
									<li><span>Date:</span><span class="session_date"></span></li>
									<li><span>Session Type:</span><span class="type"></span></li>
									<li><span>Course:</span><span class="course"></span></li>
									<li><span>Subject:</span><span class="subject"></span></li>
									<li><span>Topic:</span><span class="topic"></span></li>
								</ul>
							</div>
					    </div>
					</div>
				</div>
			 </div>
			</div>
		</div>
	</div>
</div>
<!--set availability popup end here-->
<!-- popup end Here -->
<script src="<?php echo base_url()?>assets/js/session.js"></script>
<script>
 $("#session_calender").fullCalendar({  
        header: {
            left: 'prev,next today',
            center: 'title',
            right:  'prevYear,nextYear, agendaDay,agendaWeek,month'
		},
		events :
		[
			<?php 
			 if(!empty($upcoming_session)){
		     	for ($i=0; $i <count($upcoming_session); $i++) { 
			?> 
				{
					title : '<?php echo $upcoming_session[$i]['title']; ?>',
					start : '<?php echo $upcoming_session[$i]['date'].'T'.$upcoming_session[$i]['start_time']; ?>',
					link : '<?php echo $upcoming_session[$i]['session_id']; ?>',
					course_name : '<?php echo $upcoming_session[$i]['course_name']; ?>',
					subject_name : '<?php echo $upcoming_session[$i]['subject_name']; ?>',
					topic_name : '<?php echo $upcoming_session[$i]['topic_name']; ?>',
					date : '<?php echo $upcoming_session[$i]['date']; ?>',
					conducted_flag : '<?php echo $upcoming_session[$i]['conducted_flag']; ?>',
					backgroundColor: '#4c9cff',
					eventType : 'UpcomingSession'
				},
			<?php }  } ?>
			<?php
			if(!empty($past_session)) {  
				for ($i=0; $i <count($past_session); $i++) { ?> 
				{
					title : '<?php echo $past_session[$i]['title']; ?>',
					start : '<?php echo $past_session[$i]['date'].'T'.$past_session[$i]['start_time']; ?>',
					link : '<?php echo $past_session[$i]['session_id']; ?>',
					course_name : '<?php echo $past_session[$i]['course_name']; ?>',
					subject_name : '<?php echo $past_session[$i]['subject_name']; ?>',
					topic_name : '<?php echo $past_session[$i]['topic_name']; ?>',
					date : '<?php echo $past_session[$i]['date']; ?>',
					conducted_flag : '<?php echo $past_session[$i]['conducted_flag']; ?>',
					backgroundColor: '#3c763d',
					eventType : 'PastSession'
				},
			<?php } } ?>
			<?php 
			if(!empty($availability_date)) { 
				for ($i=0; $i <count($availability_date); $i++) { ?> 
				{
					start : '<?php echo $availability_date[$i]['date'].'T'.$availability_date[$i]['from_time']; ?>',
					end : '<?php echo $availability_date[$i]['date'].'T'.$availability_date[$i]['to_time']; ?>',
					link : '<?php echo $availability_date[$i]['availability_id']; ?>',
					backgroundColor: '#FFA500',
					eventType : 'Available'
				},
			<?php } } ?>
		],
		eventClick: function(event){
			if(event.eventType == 'PastSession'){
				$('.heading_sec').html(event.date);
				$('.session_date').html(event.date);
				$('.type').html(event.conducted_flag);
				$('.course').html(event.course_name);
				$('.subject').html(event.subject_name);
				$('.topic').html(event.topic_name);
				$('#event_content_popup').modal('show');
			}
			else if(event.eventType == 'UpcomingSession'){
				$('.heading_sec').html(event.date);
				$('.session_date').html(event.date);
				$('.type').html(event.conducted_flag);
				$('.course').html(event.course_name);
				$('.subject').html(event.subject_name);
				$('.topic').html(event.topic_name);
				$('#event_content_popup').modal('show');
			}
		}
		
	});
</script>