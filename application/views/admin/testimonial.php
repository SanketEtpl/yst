
<!-- banner section start here-->
<section class="banner_section" style="background-image:url(./assets/admin/img/mid-bg.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>TESTIMONIALS</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- schedule new session section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					<div class="row">
						<?php foreach($alltestim as $val){?>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="testominail_blogs testominail_margin_bottom">
								<div class="testi_m_profile">
									<span><img src="<?php echo base_url();?>assets/admin/img/image-1.png" class="img-responsive img-circle img-thumbnail" alt="testimonial"></span>
									<div class="testi_m_profile_name">
										<h4><?php echo $val['first_name'].' '.$val['last_name'];?></h4>
										<div class="star_rat">
											<?php $limit=5;
												  $rating=$val['rate'];
												  $remain=$limit-$rating;
											for($j=1;$j<=$rating;$j++){?>

											<a href="#."><i class="fa fa-star" aria-hidden="true"></i></a>
										     <?php }
										     for($j=1;$j<=$remain;$j++){?>

											<a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a>
										     <?php } ?>
										</div>
									</div>
									<div class="testimonial_delete_btn">
										<a href="#." data-toggle="modal" data-target="#testimonial_delete_popup"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
									</div>
								</div>
								<div class="test_profil_info">
									<div class="user_testominail_text">
										<span class="user_testominail_text_heading">COURSE:</span>
										<span class="user_testominail_text_subtext">
											<?php echo $val['course_name'];?></span>
									</div>
									<div class="user_testominail_text">
										<span class="user_testominail_text_heading">SUBJECT:</span>
										<span class="user_testominail_text_subtext"><?php echo $val['subject_name'];?></span>
									</div>
									<div class="user_testominail_text">
										<span class="user_testominail_text_heading">TOPIC:</span>
										<span class="user_testominail_text_subtext"><?php echo $val['topic_name'];?></span>
									</div>
									<!--<div class="user_testominail_text">
										<span class="user_testominail_text_heading">LEVEL:</span>
										<span class="user_testominail_text_subtext">10</span>
									</div>-->
									<div class="user_testominail_text">
										<span class="user_testominail_text_heading">COMMENTS:</span>
										<span class="user_testominail_text_subtext testominail_text_size "><?php echo $val['comment'];?></span>
										</div>
									</div>
								</div>
							</div>
							<?php }?>
									
									
										
												
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
							<!-- schedule new session section end here-->

							<!-- popup start Here -->
							<!--Schedule session popup start here-->
							<div class="modal fade" id="testimonial_delete_popup" role="dialog">
								<div class="modal-dialog testimonial_delete_width_popup">
									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">
												<img src="<?php echo base_url();?>assets/admin/img/pop_up_close.png" class="img-responsive" alt="close"/>
											</button>
										</div>
										<div class="modal-body testimonial_delete_pop">
											<div class="moel_details_new">
												<div class="section_heading_sec testimonial_delete_heading">
													<div class="testimonial_delete_img">
														<img class="img-responsive" src="<?php echo base_url();?>assets/admin/img/delete.png" alt="delete" /> 
													</div>
													<h4>Delete Testimonial</h4>
												</div>
												<div class="model_content">
													<div class="testimonial_delete_text">
														Do you want to delete this Testimonial?
													</div>	
													<div class="add_topic_btn_new testimonial_no_border">
														<a href="#." class="btn save_cancel_btn" >Yes</a>
														<a href="#." class="btn save_cancel_btn" data-dismiss="modal">No</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--Schedule session popup end here-->
							<!-- popup end Here -->
