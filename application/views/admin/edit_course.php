
<!-- banner section start here-->
<!-- <section class="banner_section" style="background-image:url(./assets/admin/img/mid-bg.jpg);">-->
	<section class="banner_section" style="background-image:url('<?php echo base_url()?>assets/admin/img/mid-bg.jpg')">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>EDIT COURSE</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- manage add section start here-->
<section class="web_section_common section_padding">
	<div class="container">
		<div class="row">
		<div class="col-md-8">
	       <div class="alert alert-success" style="display:none">
	        <strong id="msg" ></strong> 
	       </div>
        </div> 
			<div class="col-md-12">
				<div class="section_details_topic_add">
					<div class="add_course_main_forms">
						<div class="col-md-3 col-sm-6 col-sm-12">
							<div class="form_set">

								<label>Course</label>
								<select class="form-control select_course" onchange="getSubjectlist()">
									<option value="select">Select course</option>
									<?php if(!empty($course_list)) {  
										foreach($course_list as $course) { 
									?>
									<option value="<?php echo $course['course_id']?>"><?php echo $course['course_name'];?></option>
									<?php } } ?>
								</select>
								<script type="text/javascript">
									$('.select_course').val('<?php echo $course_id?>');
								</script>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-sm-12">
							<div class="form_set">
								<label>Subject</label>
								<select class="form-control select_subject" onchange="getTopicdetails();">
									<option value="select">Select subject</option>
									<?php if(!empty($subject_list)) { 
										foreach($subject_list as $subject) { 
									?>
									<option value="<?php echo $subject['subject_id']?>"><?php echo $subject['subject_name']?></option>
									<?php } } ?>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<form id="update_topic_form" enctype=multipart/form-data> 
<section class="web_section_common section_padding section_top_padding">
	<div class="container">
	    <input type="hidden" name="count" value="1" id="hidden_count" class="hidden_count">
		<div class="row add_more_topic">
			<div class="col-md-12 add_course_bg_color">
				<div class="row add_course_padding">
					<div class="col-md-6 col-sm-12 col-xs-12">
					    <input type="hidden" name="topic1[topic_id]" value="" id="id1">
						<div class="form_set">
							<label>Topic</label>
							<input class="form-control select_topic" id="topic1" name="topic1[topic_name]">
							<span class="err_topic1" style="color: red; display: none;">Please enter topic.</span>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive">
						<div class="form_set">
							<label>Duration</label>
							<div class="hours_input_set">
							  <input class="form-control" id="duration1" name="topic1[duration]">
							  <span class="err_duration1" style="color: red; display: none;">Please enter duration.</span>
							  <!-- <a class="btn">Hours</a> -->
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive">
						<div class="form_set">
							<label>Cost</label>
							<input class="form-control" id="cost1" name="topic1[cost]">
							<span class="err_cost1" style="color: red; display: none;">Please enter cost.</span>
						</div>
					</div>
				</div>		
				<div class="row add_course_padding">
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive">
						<div class="form_set">
							<label>Preview Video</label>
							<div class="view_upload_file">
								<div class="custom-file-input custom_file_width">
									<input type="file">
									<input type="text" class="form-control" placeholder="Upload" id="preview_video1" name="topic1[preview_video]">
									<button type="button" class="btn upload_btn"><i class="fa fa-upload" aria-hidden="true"></i></button>
									<span class="err_preview1" style="color: red; display: none;">Please upload preview video.</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive ">
						<div class="form_set">
							<label>Question Bank</label>
							<div class="view_upload_file">
								<div class="custom-file-input custom_file_width">
									<input type="file">
									<input type="text" class="form-control" placeholder="Upload" id="question_bank1" name="topic1[question_bank]">
									<button type="button" class="btn upload_btn"><i class="fa fa-upload" aria-hidden="true"></i></button>
									<span class="err_question1" style="color: red; display: none;">Please upload question bank.</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive">
						<div class="form_set">
							<label>Notes</label>
							<div class="view_upload_file">
								<div class="custom-file-input custom_file_width">
									<input type="file">
									<input type="text" class="form-control" placeholder="Upload" id="notes1" name="topic1[notes]">
									<button type="button" class="btn upload_btn"><i class="fa fa-upload" aria-hidden="true"></i></button>
									<span class="err_notes1" style="color: red; display: none;">Please upload notes.</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive">
						<div class="form_set">
							<label>Lesson Video</label>
							<div class="view_upload_file">
								<div class="custom-file-input custom_file_width">
									<input type="file">
									<input type="text" class="form-control" placeholder="Upload" id="lession_video1" name="topic1[lession_video]">
									<button type="button" class="btn upload_btn"><i class="fa fa-upload" aria-hidden="true"></i></button>
									<span class="err_lession1" style="color: red; display: none;">Please upload lession video.</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<section class="add_more_btn">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="section_heading_sec">
							<a onclick="addMoretopic();" class="btn new-comm-btn"><i class="fa fa-plus"></i>&nbsp; Add More</a>
						</div>
					</div>
				</div>
			</div>	
		</section>	
		<section class="add_more_btn">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="add_topic_btn">
						<button type="button" id="btnUpdate" class="btn save_cancel_btn" onclick='update_topic();'>Update</button>
							<a href="#." class="btn save_cancel_btn">Cancel</a>
						</div>
					</div>
				</div>
			</div>	
		</section>	
	</div>
</section>	
</form>
<!-- manage add course section end here-->
<!-- popup start Here -->
<!--Preview view video popup start here-->
<div class="modal fade" id="priview_video_set_popup" role="dialog">
	<div class="modal-dialog ">
	  <!-- Modal content-->
	  <div class="modal-content">
		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal">
			 <img src="<?php echo base_url();?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
		  </button>
		</div>
		<div class="modal-body">
			<div class="moel_details_new">
				<div class="section_heading_sec">
					<h4>PRIVIEW VIDEO</h4>
				</div>
				<div class="model_content">
					<div class="video_topic_details">
						<div class="topic_name">
							<label>Topic Name:</label>
							<small>Quadratic Equation</small>
						</div>
						<div class="tutor">
							<label>Tutor:</label>
							<small>Jimmy J</small>
						</div>
					</div>
					<div class="video_set_pop_main">
						<video id="video" poster="<?php echo base_url()?>assets/img/video_pop_img.jpg" controls="">
							<source src="<?php echo base_url()?>assets/video/how_it_work.mp4" type="video/mp4">
						</video>
					</div>
				</div>
			</div>
		</div>
	  </div>
	</div>
</div>
<!-- Preview view video popup end here -->
<!-- Update course popup start here -->
<div class="modal fade" id="update_course_popup" role="dialog">
	<div class="modal-dialog delet_cours_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url()?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body testimonial_delete_pop">
				<div class="moel_details">
					<span class="model_header">
						<img src="<?php echo base_url()?>assets/img/correct.png" class="img-responsive" alt="correct"/>
						<h5>Updated Course</h5>
					</span>
					<div class="model_content">
						<p>Course has been updated by you</p>
						<div class="pop_btn_sect_sec">
							<a href="" class="btn popup_coom_btn_new margin_zero" data-dismiss="modal" data-toggle="modal">OK</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Update course popup end here-->	   
<!-- popup end Here -->
<script src="<?php echo base_url()?>assets/admin/js/course.js"></script>