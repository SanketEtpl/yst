<!-- banner section start here-->
<section class="banner_section" style="background-image:url(./assets/admin/img/mid-bg.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>MY ACCOUNT</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- edit schedule section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					<div class="my_account_tabbing margin_bottom">
						<ul class="nav nav-tabs margin_bottom">
						  <li><a data-toggle="tab" href="#payment_history">Payment History</a></li>
						  <li class="active"><a data-toggle="tab" href="#session_history">Session History</a></li>
						</ul>
					<div class="section_heading_sec">
						<h4>SESSION HISTORY / AVERAGE RATINGS</h4>
					</div>
					<div class="avareg_star_rating">
						<span>Average Ratings :</span>
						<div class="star_ratting">
							<ul>
								<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
								<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
								<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
								<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
								<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
							</ul>
						</div>
						<a href="<?php echo base_url()?>my_account" class="back_page"><img src="<?php echo base_url();?>assets/admin/img/back_arrow.png" class="img-responsive" alt="back"/></a>
					</div>
					<div class="cours_table">
						<div class="table-responsive">
						  <table class="table cours_table_set">
							<thead>
							  <tr>
								<th>SN</th>
								<th>Date</th>
								<th>Name</th>
								<th>Ratings</th>
							  </tr>
							</thead>
							<tbody>
							  <tr>
								<td>01</td>
								<td>01/07/2018</td>
								<td>Holy Graham</td>
								<td>
									<div class="star_ratting_table_views">
										<ul>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
										</ul>
									</div>
								</td>
							  </tr>
							  <tr>
								<td>02</td>
								<td>12/07/2018</td>
								<td>Frank N.stein</td>
								<td>
									<div class="star_ratting_table_views">
										<ul>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
										</ul>
									</div>
								</td>
							  </tr>
							  <tr>
								<td>03</td>
								<td>13/07/2018</td>
								<td>Cam L.Stein</td>
								<td>
									<div class="star_ratting_table_views">
										<ul>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
										</ul>
									</div>
								</td>
							  </tr>
							  <tr>
								<td>04</td>
								<td>24/07/2018</td>
								<td>Cam L.Toe</td>
								<td>
									<div class="star_ratting_table_views">
										<ul>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
										</ul>
									</div>
								</td>
							  </tr>
							  <tr>
								<td>05</td>
								<td>25/07/2018</td>
								<td>Pat Agonic</td>
								<td>
									<div class="star_ratting_table_views">
										<ul>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
										</ul>
									</div>
								</td>
							  </tr>
							  <tr>
								<td>06</td>
								<td>2/08/2018</td>
								<td>Barry Cade</td>
								<td>
									<div class="star_ratting_table_views">
										<ul>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
										</ul>
									</div>
								</td>
							  </tr>
							  <tr>
								<td>07</td>
								<td>6/08/2018</td>
								<td>Phil Anthropist</td>
								<td>
									<div class="star_ratting_table_views">
										<ul>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star" aria-hidden="true"></i></a></li>
											<li><a href="#."><i class="fa fa-star-o" aria-hidden="true"></i></a></li>
										</ul>
									</div>
								</td>
							  </tr>
							</tbody>
						 </table>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- edit schedule section end here-->



