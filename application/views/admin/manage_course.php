<!-- header section end here-->
<!-- banner section start here-->
<section class="banner_section" style="background-image:url(./assets/admin/img/mid-bg.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>MANAGE COURSE</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- manage course section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					<div class="section_heading_sec">
						<h4>COURSES</h4>
						<a href="<?php echo base_url()?>add_new_course" class="btn new-comm-btn"><i class="fa fa-plus"></i>&nbsp; Manage Course</a>
					</div>
					<div class="cours_table">
						<div class="table-responsive">
							<table id="table" class="table cours_table_set">
								<thead>
									<tr>
										<th>SN</th>
										<th>Posted Date</th>
										<th>Course</th>
										<th>Cost</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<?php if(!empty($course_list)) { 
									$i=1;
									foreach($course_list as $course){
								?>
									<tr>
										<td><?php echo $i?></td>
										<td><?php echo $course['created_at'];?></td>
										<td><?php echo $course['course_name'];?></td>
										<td><?php echo '$'.$course['cost'];?></td>
										<td><div class="delet_edit_table_link">
											<a href="<?php echo base_url()?>edit_course/<?php echo $course['course_id'];?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
											<!-- <a href="#." data-toggle="modal" data-target="#delet_cours_popup"><i class="fa fa-trash"></i></a> -->
										</div></td>
									</tr>
								<?php $i++ ; } } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- manage course section end here-->
<!-- footer section start here-->

<script src="<?php echo base_url(); ?>assets/admin/js/course.js"></script>