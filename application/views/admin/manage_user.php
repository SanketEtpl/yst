<!-- banner section start here-->
<section class="banner_section" style="background-image:url(./assets/admin/img/mid-bg.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>MANAGE USER</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- manage course section start here-->
<section class="web_section_common">
	<div class="container">
		 <div class="col-md-8">
	       <div class="alert alert-success" style="display:none">
	        <strong id="msg" ></strong> 
	       </div>
	    </div>
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					<div class="section_heading_sec">
						<h4>ALL USER LIST</h4>
						<div class="addes_new_search_box">
							<!-- <input type="text" class="form-control" placeholder="Search"/> -->
							<a href="#." class="btn new-comm-btn manage_user_responsive_btn" data-toggle="modal" data-target="#add_new_new_popup">Add New User</a>
						</div>
					</div>
					<div class="cours_table">
						<div class="table-responsive">
							<table class="table cours_table_set" id="stud_table">
								<thead>
									<tr>
										<th>SN</th>
										<th>Name</th>
										<th>Contact</th>
										<th>Email ID</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($student_list)) { 
										$i=1;
										foreach($student_list as $student) { 
											?>
											<tr>
												<td><?php echo $i?></td>
												<td><?php echo $student['first_name'].' '.$student['last_name']?></td>
												<td><?php echo $student['contact_no']?></td>
												<td><?php echo $student['email_id']?></td>
												<?php $sid=$student['student_id'];?>
												<td><div class="delet_edit_table_link">
													<a href="<?php echo base_url()?>view_course_details/<?php echo $sid ;?>"><i class="fa fa-eye" aria-hidden="true"></i></a>

													
												</div></td>
											</tr>
											<?php $i++; } } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- manage course section end here-->

		<!--Add new user popup start here-->
		<div class="modal fade" id="add_new_new_popup" role="dialog">
			<div class="modal-dialog add_topic_width_popup add_new_user_pop_width">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<img src="<?php echo base_url();?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
						</button>
					</div>
					<form id="addnewuser">
					<div class="modal-body">
						<div class="moel_details_new">
							<div class="section_heading_sec dotted_bottom_border">
								<h4>ADD NEW USER</h4>
							</div>
							<div class="model_content">
								<div class="add_topic_form">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="form_set">
												<label>First Name</label>
												<input type="text" class="form-control" placeholder="First Name" name = "first_name" id="first_name" onChange={this.onChange}>
                                                <span class="err_fname" style="color: red; display: none;">Please enter first name.</span>
											</div>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="form_set">
												<label>Last Name</label>
												<input type="text" class="form-control" placeholder="Last Name" name="last_name" id="last_name" onChange={this.onChange}>
                                                <span class="err_lname" style="color: red; display: none;">Please enter last name.</span>
											</div>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="form_set">
												<label>Email ID</label>
												<input type="text" class="form-control" name="email_id" id="email_id" placeholder="montycarlo@yst.com" onChange={this.onChange}>
                                                <span class="err_email" style="color: red; display: none;">Please enter valid email address.</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="moel_details_new_sec">
							<div class="add_topic_btn_new">
								<a  onclick="addUser();" class="btn save_cancel_btn add_user_pop_btn_width" >ADD</a>
								<a href="#." class="btn save_cancel_btn add_user_pop_btn_width" data-dismiss="modal">Cancel</a>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
		<!--Add new user popup end here-->
		<!--added course popup start here-->
		<div class="modal fade" id="added_user_popup" role="dialog">
			<div class="modal-dialog delet_cours_popup_width">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<img src="assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
						</button>
					</div>
					<div class="modal-body">
						<div class="moel_details">
							<span class="model_header annim_img">
								<img src="assets/img/add_course.png" class="img-responsive" alt="forgotpassword"/>
								<h5>Add User</h5>
							</span>
							<div class="model_content">
								<p>New User has been added by you.</p>
								<div class="pop_btn_sect_sec">
									<a href="" class="btn popup_coom_btn_new margin_zero" data-dismiss="modal" data-toggle="modal">OK</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- popup end Here -->

<script src="<?php echo base_url();?>assets/js/student.js"></script>

