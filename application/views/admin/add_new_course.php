<!-- banner section start here-->
<section class="banner_section" style="background-image:url(./assets/admin/img/mid-bg.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>MANAGE COURSE</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- manage add section start here-->
<section class="web_section_common section_padding padding_set_new">
    <div class="col-md-8">
       <div class="alert alert-success" style="display:none">
        <strong id="msg" ></strong> 
       </div>
    </div> 
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details_topic_add">
					<div class="add_course_main_forms">
						<div class="col-md-3 col-sm-6 col-sm-12">
							<div class="form_set">
								<label>Course</label>
								<select class="form-control select_course"  onchange="getSubjectlist()" id="course_name">
								  <option value="select">Select Course</option>
								<?php if(!empty($course_list)) { 
									foreach($course_list as $course) { 
								?>
									<option value="<?php echo $course['course_id']?>"><?php echo $course['course_name']?></option>
								<?php } } ?>
								</select>
								<span class="err_course" style="color: red; display: none;">Please select course.</span>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-sm-12">
							<div class="form_set">
								<label>Subject</label>
								<select class="form-control select_subject" id="subject_name">
									<option value="select">Select Subject</option>
								</select>
								<span class="err_subject" style="color: red; display: none;">Please select subject.</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<form id="topic_form" enctype=multipart/form-data>
<section class="web_section_common section_padding common_sec">
	<div class="container">
	    <input type="hidden" name="count" value="1" id="hidden_count" class="hidden_count">
		<div class="row add_more_topic" id="add_more_topic">
			<div class="col-md-12 add_course_bg_color">
				<div class="row add_course_padding">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="form_set">
							<label>Topic</label>
							<input class="form-control select_topic" id="topic1" name="topic1[topic_name]">
							<span class="err_topic1" style="color: red; display: none;">Please enter topic.</span>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive">
						<div class="form_set">
							<label>Duration</label>
							<div class="hours_input_set">
							  <input class="form-control" id="duration1" name="topic1[duration]">
							  <span class="err_duration1" style="color: red; display: none;">Please enter duration.</span>
							  <!-- <a class="btn">Hours</a> -->
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive">
						<div class="form_set">
							<label>Cost</label>
							<input class="form-control" id="cost1" name="topic1[cost]">
							<span class="err_cost1" style="color: red; display: none;">Please enter cost.</span>
						</div>
					</div>
				</div>		
				<div class="row add_course_padding">
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive">
						<div class="form_set">
							<div class="form_label_info_link">
								<label>Preview Video</label>
								<div class="tooltip">
									<a href="#." class="new_link_info "><i class="fa fa-info-circle" aria-hidden="true"></i></a>
									<span class="tooltiptext_new">
										<ul>
											<li>Option 1</li>
											<li>Option 2</li>
											<li>Option 3</li>
											<li>Option 4</li>
										</ul>
									</span>
								</div>
							</div>
							<div class="view_upload_file ">
								<div class="custom-file-input custom_file_width ">
									<input type="file" name="topic1[preview_video]">
									<input type="text" class="form-control" placeholder="Upload" id="preview_video1" name="topic1[preview_video]">
									<button type="button" class="btn upload_btn"><i class="fa fa-upload" aria-hidden="true"></i></button>
							        <span class="err_preview1" style="color: red; display: none;">Please upload preview video.</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive ">
						<div class="form_set">
							<div class="form_label_info_link">
								<label>Question Bank</label>
								<div class="tooltip">
									<a href="#." class="new_link_info "><i class="fa fa-info-circle" aria-hidden="true"></i></a>
									<span class="tooltiptext_new">
										<ul>
											<li>Option 1</li>
											<li>Option 2</li>
											<li>Option 3</li>
											<li>Option 4</li>
										</ul>
									</span>
								</div>
							</div>
							<div class="view_upload_file">
								<div class="custom-file-input custom_file_width">
									<input type="file" name="topic1[question_bank]">
									<input type="text" class="form-control" placeholder="Upload" id="question_bank1" name="topic1[question_bank]">
									<button type="button" class="btn upload_btn"><i class="fa fa-upload" aria-hidden="true"></i></button>
							        <span class="err_question1" style="color: red; display: none;">Please upload question bank.</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive">
						<div class="form_set">
							<div class="form_label_info_link">
								<label>Notes</label>
								<div class="tooltip">
									<a href="#." class="new_link_info "><i class="fa fa-info-circle" aria-hidden="true"></i></a>
									<span class="tooltiptext_new">
										<ul>
											<li>Option 1</li>
											<li>Option 2</li>
											<li>Option 3</li>
											<li>Option 4</li>
										</ul>
									</span>
								</div>
							</div>
							<div class="view_upload_file">
								<div class="custom-file-input custom_file_width">
									<input type="file" name="topic1[notes]">
									<input type="text" class="form-control" placeholder="Upload" id="notes1" name="topic1[notes]">
									<button type="button" class="btn upload_btn"><i class="fa fa-upload" aria-hidden="true"></i></button>
							        <span class="err_notes1" style="color: red; display: none;">Please upload notes.</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 add_new_course_responsive">
						<div class="form_set">
							<div class="form_label_info_link">
								<label>Lesson Video</label>
								<div class="tooltip">
									<a href="#." class="new_link_info "><i class="fa fa-info-circle" aria-hidden="true"></i></a>
									<span class="tooltiptext_new">
										<ul>
											<li>Option 1</li>
											<li>Option 2</li>
											<li>Option 3</li>
											<li>Option 4</li>
										</ul>
									</span>
								</div>
							</div>
							<div class="view_upload_file">
								<div class="custom-file-input custom_file_width">
									<input type="file" name="topic1[lession_video]">
									<input type="text" class="form-control" placeholder="Upload" id="lession_video1" name="topic1[lession_video]">
									<button type="button" class="btn upload_btn"><i class="fa fa-upload" aria-hidden="true"></i></button>
							        <span class="err_lession1" style="color: red; display: none;">Please upload lession video.</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>	
<div class="cols_set_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">	
				<div class="section_heading_sec">
					<a onclick="addMoretopic();" class="btn new-comm-btn"><i class="fa fa-plus"></i>&nbsp; Add More</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- manage add section stop here-->	
<!-- Save Btn section start here-->
<section class="add_more_btn">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="add_topic_btn">
					<div class="total_cours_cost">
						<div class="total_course_cost">
							<form>
								<label class="total_cost_lable" for="total_cost">Total Course Cost</label>
								<input type="text" class="form-control total_cost_text_area total_course_cost_input_box" id="total_cost">
							</form>
						</div>
					</div>
					<button type="button" id="btnSave" class="btn save_cancel_btn" onclick='add_topic();'>Save</button>
						<a href="#." class="btn save_cancel_btn">Cancel</a>
				</div>
			</div>
		</div>
	</div>	
</section>	
<!-- Save Btn section Stop here-->
<!-- Total Course Cost section start here -->
<section class="add_more_btn">
	<div class="container">
		<div class="row">
			<div class="col-md-12 top_border_total_course">
				
			</div>
		</div>
	</div>		
</section>
<!-- Total Course Cost section Stop here-->
</form>

<!-- popup start Here -->
<!--added course popup start here-->
<div class="modal fade" id="add_course_popup" role="dialog">
	<div class="modal-dialog delet_cours_popup_width">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url();?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="moel_details">
					<span class="model_header annim_img">
						<img src="<?php echo base_url();?>assets/admin/img/add_course.png" class="img-responsive" alt="forgotpassword"/>
						<h5>Add Course</h5>
					</span>
					<div class="model_content">
						<p>New Course has been added by you.</p>
						<div class="pop_btn_sect_sec">
							<a href="" class="btn popup_coom_btn_new margin_zero" data-dismiss="modal" data-toggle="modal">OK</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--added course popup end here-->
<!-- popup end Here -->
<script src="<?php echo base_url()?>assets/admin/js/course.js"></script>