<!-- banner section start here-->
<section class="banner_section" style="background-image:url(./assets/admin/img/mid-bg.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>MY ACCOUNT</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- edit schedule section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					<div class="my_account_tabbing">
						<ul class="nav nav-tabs">
						  <li class="active"><a data-toggle="tab" href="#payment_history">Payment History</a></li>
						  <li><a data-toggle="tab" href="#session_history">Session History</a></li>
						</ul>
						<div class="tab-content">
						  <div id="payment_history" class="tab-pane fade in active">
								<div class="tabs_set_my_account">
									<div class="section_details">
										<div class="section_heading_sec">
											<h4>PAYMENT HISTORY</h4>
										</div>
										<div class="cours_table">
											<div class="table-responsive margin_bottom">
											  <table class="table cours_table_set">
												<thead>
												  <tr>
													<th>SN</th>
													<th>Date</th>
													<th>Transaction ID</th>
													<th>Name</th>
													<!-- <th>Subscription Plan</th> -->
													<th>Amount</th>
													<th>Status</th>
												  </tr>
												</thead>
												<tbody>
												<?php $i=1;foreach ($purchase_history as $key => $value) {?>
												  <tr>
													<td><?php echo $i;?></td>
													<td><?php 
													$middle = strtotime($value['purchase_date']);    
													$new_date = date('Y-m-d', $middle);
													echo $new_date ;?></td>
													<td><?php echo $value['transaction_id'];?></td>
													<td><?php echo $value['first_name'].$value['last_name'];?></td>
													<!-- <td>Course Wise</td> -->
													<td>$<?php echo $value['amount'];?></td>
													<td>
														<?php if($value['status']==1){
															$status='Pending';
														}else{
															$status='Recived';
														}?>
														<span class="pending_status"><?php echo $status;?></span>
													</td>
												  </tr>
												  <?php $i++; } ?>
												</tbody>
											 </table>
											</div>
										</div>
									</div>
								</div>
						  </div>
						  <div id="session_history" class="tab-pane fade">
								<div class="tabs_set_my_account">
									<div class="section_details">
										<div class="section_heading_sec">
											<h4>SESSION HISTORY</h4>
										</div>
										<div class="cours_table">
											<div class="table-responsive margin_bottom">
											  <table class="table cours_table_set">
												<thead>
												  <tr>
													<th>SN</th>
													<th>Date</th>
													<th>Session Type</th>
													<th>No.of Attendees</th>
													<th>Avg.Ratings</th>
													<!-- <th>Level</th> -->
													<th>Course</th>
													<th>Subject</th>
													<th>Topic</th>
												  </tr>
												</thead>
												<tbody>
												  <tr>
													<td>01</td>
													<td>01/07/2018</td>
													<td>Group</td>
													<td>45</td>
													<td>1/5 <span class="ratting_show"><a href="<?php echo base_url()?>average_rating"><i class="fa fa-eye" aria-hidden="true"></i></a></span></td>
													<!-- <td>10</td> -->
													<td>Biology</td>
													<td>Botany</td>
													<td>Lorem Ipsum is simply dummy text</td>
												  </tr>
												</tbody>
											 </table>
											</div>
										</div>
									</div>
								</div>
						  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- edit schedule section end here-->
				