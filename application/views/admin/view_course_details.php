
<!-- banner section start here-->
<section class="banner_section" style="background-image:url(./assets/admin/img/mid-bg.jpg);">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_content">
					<h4>MANAGE USER</h4>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- banner section end here-->
<!-- manage course section start here-->
<section class="web_section_common">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details">
					<div class="section_heading_sec dotted_bottom_border">
						<h4>VIEW USER DETAILS</h4>
						<div class="addes_new_search_box responsive_search_box">
							<a href="<?php echo base_url()?>manage_user" class="btn save_cancel_btn" data-dismiss="modal">Back</a>
						</div>
					</div>
					<?php if(!empty($user_details)) foreach ($user_details as $key => $value) ?>
					
					<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-6 manage_course_responsive_view">
							<div class="user_manage_info">
								<div class="user_manage_info_title">First Name</div>
								<div class="user_manage_info_name"><?php echo $value['first_name'];?></div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 manage_course_responsive_view">
							<div class="user_manage_info">
								<div class="user_manage_info_title">Last Name</div>
								<div class="user_manage_info_name"><?php echo $value['last_name'];?></div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 manage_course_responsive_view">
							<div class="user_manage_info">
								<div class="user_manage_info_title">Contact Number</div>
								<div class="user_manage_info_name"><?php echo $value['contact_no'];?></div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 manage_course_responsive_view">
							<div class="user_manage_info">
								<div class="user_manage_info_title">Email ID</div>
								<div class="user_manage_info_name"><?php echo $value['email_id'];?></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="user_manage_info margin_top address_margin_top">
								<div class="user_manage_info_title">Address</div>
								<div class="user_manage_info_name"><?php echo $value['address'];?></div>
							</div>
						</div>
					</div>	
				</div>
			</div>
			<div class="col-md-12 margin_top">
				<div class="section_details">
					<div class="section_heading_sec dotted_bottom_border">
						<h4>ADDRESS DETAILS</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2 col-sm-6 col-xs-6 manage_course_responsive_view">
						<div class="user_manage_info">
							<div class="user_manage_info_title">Country</div>
							<div class="user_manage_info_name"><?php echo $value['country_name'];?></div>
						</div>
					</div>
					<div class="col-md-2 col-sm-6 col-xs-6 manage_course_responsive_view">
						<div class="user_manage_info">
							<div class="user_manage_info_title">State</div>
							<div class="user_manage_info_name"><?php echo $value['state_name'];?></div>
						</div>
					</div>
					<div class="col-md-2 col-sm-6 col-xs-6 manage_course_responsive_view">
						<div class="user_manage_info">
							<div class="user_manage_info_title">City</div>
							<div class="user_manage_info_name"><?php echo $value['city_name'];?></div>
						</div>
					</div>
					<div class="col-md-2 col-sm-6 col-xs-6 manage_course_responsive_view">
						<div class="user_manage_info">
							<div class="user_manage_info_title">Zip Code</div>
							<div class="user_manage_info_name"><?php echo $value['zip_code'];?></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 margin_top">
				<div class="section_details">
					<div class="section_heading_sec dotted_bottom_border">
						<h4>COURSES</h4>
					</div>
				</div>
				<div class="cours_table">
					<div class="table-responsive">
						<table class="table cours_table_set manage_course_size">
							<thead>
								<tr>
									<th >SN</th>
									<th>Date</th>
									<th>Level</th>	
									<th>Course</th>
									<th>Subject</th>
									<th>Topic</th>
									<th>Test Score</th>
									<th>Cost</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>01</td>
									<td>01/07/2018</td>
									<td>10</td>
									<td>Biology</td>
									<td>Botany</td>
									<td>Lorem ipsum dolor sit amet,</td>
									<td>7/10</td>
									<td>$230</td>
								</tr>
								<tr>
									<td>02</td>
									<td>12/07/2018</td>
									<td>11</td>
									<td>Biology</td>
									<td>Anthology</td>
									<td>Consectetur adipisicing elit,</td>
									<td>8/10</td>
									<td>$230</td>
								</tr>
								<tr>
									<td>03</td>
									<td>13/07/2018</td>
									<td>12</td>
									<td>Physics</td>
									<td>Plants</td>
									<td>Sed do eiusmod tempor</td>
									<td>5/10</td>
									<td>$230</td>
								</tr>
								<tr>
									<td>04</td>
									<td>24/07/2018</td>
									<td>12</td>
									<td>Biology</td>
									<td>Quantum Physics</td>
									<td>Lorem ipsum dolor sit amet,</td>
									<td>6/10</td>
									<td>$230</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- manage course section end here -->
<!-- Progress Graph section Start here -->
<section class="courses_details_wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section_details margin_bottom">
					<div class="section_heading_sec dotted_bottom_border">
						<h4>PROGRESS GRAPH</h4>
					</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12 left_padding margin_bottom">
				<img class="img-responsive manage_course_graph_img" src="<?php echo base_url();?>assets/admin/img/physics.jpg" alt="physics">
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12 right_padding margin_bottom">
					<img class="img-responsive manage_course_graph_img" src="<?php echo base_url();?>assets/admin/img/biology.jpg" alt="biology">
				</div>
			</div>
		</div>
	</div>
</section>

<!--view user details popup start here-->
<div class="modal fade" id="view_user_details_popup" role="dialog">
	<div class="modal-dialog add_topic_width_popup">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<img src="<?php echo base_url();?>assets/img/pop_up_close.png" class="img-responsive" alt="close"/>
				</button>
			</div>
			<div class="modal-body">
				<div class="moel_details_new">
					<div class="section_heading_sec">
						<h4>ADD NEW USER</h4>
					</div>
					<div class="model_content">
						<div class="add_topic_form">
							<div class="row">
								<div class="col-md-4">
									<div class="form_set">
										<label>First Name</label>
										<div class="user_added_date">
											Monty
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form_set">
										<label>Last Name</label>
										<div class="user_added_date">
											Carlo
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form_set">
										<label>Contact Number</label>
										<div class="user_added_date">
											9876543210
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form_set">
										<label>Email ID</label>
										<div class="user_added_date">
											montycarlo@yst.com
										</div>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form_set">
										<label>Address</label>
										<div class="user_added_date">
											Lorem Ipsum is simply dummy text of the printing and typesetting industry.
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="moel_details_new_here">
					<div class="section_heading_sec">
						<h4>ADDRESS DETAILS</h4>
					</div>
					<div class="model_content">
						<div class="add_topic_form">
							<div class="row">
								<div class="col-md-3">
									<div class="form_set">
										<label>Country</label>
										<div class="user_added_date">
											USA
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form_set">
										<label>State</label>
										<div class="user_added_date">
											Taxes(TX)
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form_set">
										<label>City</label>
										<div class="user_added_date">
											Austin
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form_set">
										<label>Zip Code</label>
										<div class="user_added_date">
											78701
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="moel_details_new_here">
					<div class="section_heading_sec">
						<h4>COUSES</h4>
					</div>
					<div class="model_content">
						<div class="add_topic_form">
							<div class="cours_table_new">
								<div class="table-responsive">
									<table class="table cours_table_set_sec">
										<thead>
											<tr>
												<th>SN</th>
												<th>Date</th>
												<th>Level</th>
												<th>Course</th>
												<th>Subject</th>
												<th>Topic</th>
												<th>Test Score</th>
												<th>Cost</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>01</td>
												<td>01/07/2018</td>
												<td>10</td>
												<td>Boilogy</td>
												<td>Botany</td>
												<td class="topic_nam_table_width_new">Lorem Ipsum is simply dummy </td>
												<td>7/10</td>
												<td>$230</td>
												<td>
													<div class="delet_edit_table_link_new">
														<a href="#." data-toggle="modal" data-target="#delet_cours_popup"><i class="fa fa-trash" aria-hidden="true"></i></a>
													</div>
												</td>
											</tr>
											<tr>
												<td>02</td>
												<td>12/07/2018</td>
												<td>11</td>
												<td>Boilogy</td>
												<td>Anthology</td>
												<td class="topic_nam_table_width_new">Lorem Ipsum is simply dummy </td>
												<td>8/10</td>
												<td>$230</td>
												<td>
													<div class="delet_edit_table_link_new">
														<a href="#." data-toggle="modal" data-target="#delet_cours_popup"><i class="fa fa-trash" aria-hidden="true"></i></a>
													</div>
												</td>
											</tr>
											<tr>
												<td>03</td>
												<td>13/07/2018</td>
												<td>12</td>
												<td>Physics</td>
												<td>Plants</td>
												<td class="topic_nam_table_width_new">Lorem Ipsum is simply dummy </td>
												<td>5/10</td>
												<td>$230</td>
												<td>
													<div class="delet_edit_table_link_new">
														<a href="#." data-toggle="modal" data-target="#delet_cours_popup"><i class="fa fa-trash" aria-hidden="true"></i></a>
													</div>
												</td>
											</tr>
											<tr>
												<td>04</td>
												<td>24/07/2018</td>
												<td>12</td>
												<td>Boilogy</td>
												<td>Quantum Physics</td>
												<td class="topic_nam_table_width_new">Lorem Ipsum is simply dummy </td>
												<td>6/10</td>
												<td>$230</td>
												<td>
													<div class="delet_edit_table_link_new">
														<a href="#." data-toggle="modal" data-target="#delet_cours_popup"><i class="fa fa-trash" aria-hidden="true"></i></a>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="add_topic_btn_new">
						<a href="#." class="btn save_cancel_btn" data-dismiss="modal" data-toggle="modal" data-target="#added_user_popup">ADD</a>
						<a href="#." class="btn save_cancel_btn" data-dismiss="modal">Cancel</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--view user details	 popup end here-->


