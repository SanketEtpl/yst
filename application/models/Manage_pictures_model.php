<?php 
class Manage_pictures_model extends CI_Model
{
    function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
	}

 	// ----- save offer----
	public function savePictureOrClip($insertArr)
	{//print_r($updateArr);exit;
		$proc_data = "'insert',
		'1',
		'".$insertArr['name']."',
		'".$insertArr['image']."',
		'1',
		'".$insertArr['type']."',
		@result";
		//print_r($proc_data);exit;
		$select_proc = $this->common_model->call_proc('sp_manage_pic_clip_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");//print_r($result);exit;
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//----get all picturesList-------
	public function picturesOrClipList($type)
	{
		$proc_data = "'select_all',
		'1',
		'test',
		'test',
		'1',
		'".$type."',
		@result";
		//print_r($proc_data);exit;
		$select_proc = $this->common_model->call_proc('sp_manage_pic_clip_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//----get pic by id-------
	public function get_pic_byId($key)
	{
		$proc_data = "'select_by_id',
		'".$key."',
		'test',
		'test',
		'1',
		'test',
		@result";
		//print_r($proc_data);exit;
		$select_proc = $this->common_model->call_proc('sp_manage_pic_clip_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

		 // ----- delete offer----
	public function deletePictureOrClip($key)
	{
		$proc_data = "'delete',
		'".$key."',
		'test',
		'test',
		'1',
		'test',
		@result";
		$select_proc = $this->common_model->call_proc('sp_manage_pic_clip_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	 // ----- update pictures----
	public function update_picturesOrCLip($updateArr)
	{//print_r($updateArr);exit;
		$proc_data = "'update',
		'".$updateArr['id']."',
		'".$updateArr['name']."',
		'".$updateArr['image']."',
     	'1',
 		'test',
		@result";
		//print_r($proc_data);exit;
		$select_proc = $this->common_model->call_proc('sp_manage_pic_clip_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");//print_r($result);exit;
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	 
	
}




?>