<?php 
class Manage_offer_model extends CI_Model
{
    function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
	}

	//----get all offers list-------
	public function offerList()
	{
		$proc_data = "'select_all',
		'1',
		'test',
		'".date('Y-m-d')."',
		'".date('Y-m-d')."',
		'test',
		'1',
		@result";
		//print_r($proc_data);exit;
		$select_proc = $this->common_model->call_proc('sp_manage_offer_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//----get offer by Id-------
	public function get_offer_byId($key)
	{
		$proc_data = "'select_by_id',
		'".$key."',
		'test',
		'".date('Y-m-d')."',
		'".date('Y-m-d')."',
		'test',
		'1',
		@result";
		//print_r($proc_data);exit;
		$select_proc = $this->common_model->call_proc('sp_manage_offer_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

		 // ----- delete offer----
	public function delete_offer($key)
	{
		$proc_data = "'delete',
		'".$key."',
		'test',
		'".date('Y-m-d')."',
		'".date('Y-m-d')."',
		'test',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_manage_offer_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	 // ----- update offer----
	public function update_offer($updateArr)
	{//print_r($updateArr);exit;
		$proc_data = "'update',
		'".$updateArr['id']."',
		'".$updateArr['title']."',
		'".$updateArr['applicable_from_date']."',
     	'".$updateArr['applicable_to_date']."',
		'".$updateArr['details']."',
		'1',
		@result";
		//print_r($proc_data);exit;
		$select_proc = $this->common_model->call_proc('sp_manage_offer_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");//print_r($result);exit;
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

 // ----- save offer----
	public function save_offer($insertArr)
	{//print_r($updateArr);exit;
		$proc_data = "'insert',
		'1',
		'".$insertArr['title']."',
		'".$insertArr['applicable_from_date']."',
     	'".$insertArr['applicable_to_date']."',
		'".$insertArr['details']."',
		'".$insertArr['is_deleted']."',
		@result";
		//print_r($proc_data);exit;
		$select_proc = $this->common_model->call_proc('sp_manage_offer_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");//print_r($result);exit;
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}
	 
	
}




?>