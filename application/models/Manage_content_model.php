<?php 
class Manage_content_model extends CI_Model
{
    function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
	}

	 // ----- get manage_content----
	public function manage_content($key)
	{
		$proc_data = "'select_content',
		'1',
		'test',
		'".$key."',
		'1',
		@result";
		//print_r($proc_data);exit;
		$select_proc = $this->common_model->call_proc('sp_manage_content',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

		 // ----- delete manage_content----
	public function delete_manage_content($key)
	{
		$proc_data = "'delete',
		'1',
		'test',
		'".$key."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_manage_content',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	 // ----- update manage_content----
	public function update_manage_content($updateArr)
	{//print_r($updateArr);exit;
		$proc_data = "'update',
		'1',
		'".$updateArr['content']."',
		'".$updateArr['key_type']."',
		'1',
		@result";
		//print_r($proc_data);exit;
		$select_proc = $this->common_model->call_proc('sp_manage_content',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");//print_r($result);exit;
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	 
	
}




?>