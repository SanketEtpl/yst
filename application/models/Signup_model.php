<?php 
class Signup_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
	}

     // ----- List of country-------
	public function countryList()
	{
		$proc_data = "'select_all',
		'1',
		'test',
		'test',
		'10',
		@result";
		$select_proc = $this->common_model->call_proc('sp_country_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}
	//---------------List of state by country id--------------------
	public function stateList($country_id)
	{
		$proc_data = "'select_state_bycountry',
		'1',
		'test',
		'".$country_id."',
		@result";
		$select_proc = $this->common_model->call_proc('sp_state_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}
	//-----------List of city by state id----------------------------
    public function cityList($state_id)
	{
		$proc_data = "'select_city_bystate',
		'1',
		'test',
		'".$state_id."',
		@result";
		$select_proc = $this->common_model->call_proc('sp_city_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}
	//--------------student registration-------------------
	public function signUp($data){
		//echo "<pre>";print_r($data);die;
		$proc_data = "'insert',
		'1',
		'".$data['first_name']."',
		'".$data['last_name']."',
		'".$data['email_id']."',
		'".$data['password']."',
		NULL,
		'".$data['contact_no']."',
		'".$data['country_id']."',
		'".$data['state_id']."',
		'".$data['city_id']."',
		'".$data['zip_code']."',
		NULL,
		NULL,
		'".$data['address']."',
		NULL,
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_student_master_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;


	}








}
?>