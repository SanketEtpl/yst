<?php 
class Student_model extends CI_Model
{
    function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
	}

	// ----- listing of student -------
	public function studentList()
	{
		$proc_data = "'select_all',
		'1',
		'test',
		'test',
		'test',
		'test',
		'".date('Y-m-d H:i:s')."',
		'10',
		'10',
		'10',
		'10',
		'test',
		'test',
		'test',
		'test',
		'10',
		'".date('Y-m-d H:i:s')."',
		'10',
		@result";
		$select_proc = $this->common_model->call_proc('sp_student_master_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//-----------add new student-------------
	public function addStudent($data){
		$proc_data = "'insert',
		'1',
		'".$data['first_name']."',
		'".$data['last_name']."',
		'".$data['email_id']."',
		'".$data['password']."',
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";

		$select_proc = $this->common_model->call_proc('sp_student_master_crud',$proc_data);
		
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}
	public function studentById($studentId)
	{
		//echo "<pre>";print_r($studentId);die;
		$proc_data = "'student_profile',
		'".$studentId."',
		'test',
		'test',
		'test',
		'test',
		'".date('Y-m-d H:i:s')."',
		'10',
		'10',
		'10',
		'10',
		'test',
		'test',
		'test',
		'test',
		'10',
		'".date('Y-m-d H:i:s')."',
		'10',
		@result";
		$select_proc = $this->common_model->call_proc('sp_student_master_crud',$proc_data);
		//echo $this->db->last_query();die;
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	
	}
	public function updateStudentProfile($data)
	{
		$proc_data = "'update',
		'".$data['student_id']."',
		'".$data['first_name']."',
		'".$data['last_name']."',
		'".$data['email_id']."',
		'".$data['password']."',
		'".$data['bday']."',
		'".$data['contact_no']."',
		'".$data['country_id']."',
		'".$data['state_id']."',
		'".$data['city_id']."',
		'".$data['zip_code']."',
		'".$data['gender']."',
		'".$data['school_name']."',
		'".$data['address']."',
		'".$data['level_id']."',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_student_master_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	public function changeStudentPassword($data)
	{
		//$this->common_model->update(TB_STUDENT,)

		print_r($data);
	}


	// ----- user details -------
	public function userDetailsList($studentId)
	{
		$proc_data = "'select',
		'".$studentId."',
		@result";
		$select_proc = $this->common_model->call_proc('sp_view_user_details',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}





}




?>