<?php 
class Session_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
	}

	// ----- listing of session -------
	public function sessionList()
	{
		$proc_data = "'select_all',
		'1',
		'test',
		'".date('H:i:s')."',
		'".date('H:i:s')."',
		'".date('Y-m-d')."',
		'10',
		'test',
		'10',
		'10',
		'10.12',
		'10',
		'".date('Y-m-d H:i:s')."',
		'10',
		@result";
		$select_proc = $this->common_model->call_proc('sp_session_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		//echo "<pre>";print_r($result);die;
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//--------save the session-------- 
	public function addSession($data)
	{
		//get tutor data
		$tutor_data = $this->common_model->select('tutor_id','tutor_master_ut',array('tutor_id'=>'1'));
		$tutor_id = isset($tutor_data[0]['tutor_id']) ? $tutor_data[0]['tutor_id'] : '';
		
		$proc_data = "'insert',
		'1',
		'".$data['title']."',
		'".$data['start_time']."',
		'".$data['end_time']."',
		'".$data['date']."',
		'".$data['topic_id']."',
		'".$data['conducted_flag']."',
		NULL,
		'".$data['duration_in_minutes']."',
		'".$data['cost']."',
		'".$tutor_id."',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_session_crud',$proc_data);
		//echo $this->db->last_query();die;
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//------------get session details----------
	public function getSessiondetails($data)
	{
		$proc_data = "'select_id',
		'".$data['session_id']."',
		'test',
		'".date('H:i:s')."',
		'".date('H:i:s')."',
		'".date('Y-m-d')."',
		'10',
		'test',
		'10',
		'10',
		'10.12',
		'10',
		'".date('Y-m-d H:i:s')."',
		'10',
		@result";
		$select_proc = $this->common_model->call_proc('sp_session_crud',$proc_data);
		//echo $this->db->last_query();die;
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//-------------update session details-----------
	public function updateSession($data)
	{
		$proc_data = "'update',
		'".$data['session_id']."',
		'".$data['title']."',
		'".$data['start_time']."',
		'".$data['end_time']."',
		'".$data['date']."',
		'".$data['topic_id']."',
		'".$data['conducted_flag']."',
		NULL,
		'".$data['duration_in_minutes']."',
		'".$data['cost']."',
		'1',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_session_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//-------------delete session-----------
	public function deleteSession($data)
	{
		$proc_data = "'delete',
		'".$data['session_id']."',
		'test',
		'".date('H:i:s')."',
		'".date('H:i:s')."',
		'".date('Y-m-d')."',
		'10',
		'test',
		'10',
		'10',
		'10.12',
		'10',
		'".date('Y-m-d H:i:s')."',
		'10',
		@result";
		$select_proc = $this->common_model->call_proc('sp_session_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}
	//get upcoming session list
	public function upcomingSessionList($student_id)
	{
		$proc_data = "'upcoming',
		'".$student_id."',
		@result";
		$select_proc = $this->common_model->call_proc('sp_payment_history',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		//echo "<pre>";print_r($result);die;
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//get past session list
	public function pastSessionList($student_id)
	{
		$proc_data = "'past',
		'".$student_id."',
		@result";
		$select_proc = $this->common_model->call_proc('sp_payment_history',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}











}





























?>