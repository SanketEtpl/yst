<?php 
class Topic_model extends CI_Model
{
    function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
	}

	 // ----- listing of topic under subjetc -------
	public function topicBySubId($subject_id)
	{
		$proc_data = "'selBySubId',
		'1',
		'1',
		'".$subject_id."',
		'1',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_topic_search',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	 // ----- listing of topic by id -------
	/*public function topicByTopicId($topic_id)
	{
		$proc_data = "'select_id',
		'".$topic_id."',
		'test',
		'test',
		'1',
		'1',
		'test',
		'test',
		'test',
		'test',
		'test',
		'test',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_topic_master_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}
*/

	// ----- listing of topic -------
	/*public function topicList()
	{
		//return 'hii';
		$proc_data = "'sel_join',
		'1',
		'1',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_topic_search',$proc_data);
	//	echo $this->db->last_query();exit;
		$result = $this->common_model->get_proc_result("@result as result");//print_r($result);exit();
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}*/

	// ----- course details-------
/*	public function courseDetails($topic_id,$subject_id,$course_id)
	{
		//return 'hii';
		$proc_data = "'course_details',
		'1',
		'".$course_id."',
		'".$subject_id."',
		'".$topic_id."',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_topic_search',$proc_data);
	//	echo $this->db->last_query();exit;
		$result = $this->common_model->get_proc_result("@result as result");//print_r($result);exit();
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}*/

	public function courseDetailsByTopicId($topic_id){
		//return 'hii';
		$proc_data = "'course_details_topicId',
		'1',
		'1',
		'1',
		'".$topic_id."',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_topic_search',$proc_data);
	    //	echo $this->db->last_query();exit;
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	public function courseDetailsBySubjectId($subject_id){
		$proc_data = "'course_details_subjectId',
		'1',
		'1',
		'".$subject_id."',
		'1',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_topic_search',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	public function courseDetailsByCourseId($course_id){
		$proc_data = "'course_details_courseId',
		'1',
		'".$course_id."',
		'1',
		'1',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_topic_search',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//-----------add new topic-------------
	public function addTopic($data,$course_id,$subject_id,$img){
		$proc_data = "'insert',
		'1',
		'".$data['topic_name']."',
		NULL,
		'".$course_id."',
		'".$subject_id."',
		'".$data['cost']."',
		'".$data['duration']."',
		'".$img['preview_video']."',
		'".$img['lession_video']."',
		'".$img['notes']."',
		'".$img['question_bank']."',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_topic_master_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		echo "<pre>";print_r($result);die;
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//--------------get Topic details---------------------------
	public function getTopicdetails($data){
		$proc_data = "'select_course_id',
		'1',
		'test',
		'test',
		'".$data['course_id']."',
		'".$data['subject_id']."',
		'01:00:00',
		'10',
		'test',
		'test',
		'test',
		'test',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_topic_master_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//-----------update new topic-------------
	public function updateTopic($data,$course_id,$subject_id,$img){
	  // echo "<pre>";print_r($data);die;
		if($img['preview_video'] != ''){
			$preview_video = $img['preview_video'];
		}
		else{
			$preview_video = $data['preview_video'];
		}
		if($img['lession_video'] != ''){
			$lession_video = $img['lession_video'];
		}
		else{
			$lession_video = $data['lession_video'];
		}
		if($img['notes'] != ''){
			$notes = $img['notes'];
		}
		else{
			$notes = $data['notes'];
		}
		if($img['question_bank'] != ''){
			$question_bank = $img['question_bank'];
		}
		else{
			$question_bank = $data['question_bank'];
		}
		$proc_data = "'update',
		'".$data['topic_id']."',
		'".$data['topic_name']."',
		NULL,
		'".$course_id."',
		'".$subject_id."',
		'".$data['cost']."',
		'".$data['duration']."',
		'".$preview_video."',
		'".$lession_video."',
		'".$notes."',
		'".$question_bank."',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_topic_master_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//get purchase topic list
	public function getPurchaseList(){
		$group_by = '';
		$cond_dr = "purchase_type_flag='course'";
		$jointype_dr  = array('topic_master_ut'=>"LEFT",'course_master_ut'=>"LEFT",'subject_master_ut'=>"LEFT");
        $join_dr = array('topic_master_ut'=>"topic_master_ut.course_id=purchase_ut.purchase_entity_id",
			'course_master_ut'=>"course_master_ut.course_id=topic_master_ut.course_id",
                'subject_master_ut'=>"subject_master_ut.subject_id=topic_master_ut.subject_id");
		$course_topic_data = $this->common_model->getMasterQuery("*",'purchase_ut', $cond_dr, array(), $join_dr, $jointype_dr,$group_by);           
		
		
		$cond_dr1 = "purchase_type_flag='subject'";
		$jointype_dr1  = array('topic_master_ut'=>"LEFT",'course_master_ut'=>"LEFT",'subject_master_ut'=>"LEFT");
        $join_dr1 = array('topic_master_ut'=>"topic_master_ut.subject_id=purchase_ut.purchase_entity_id",
			'course_master_ut'=>"course_master_ut.course_id=topic_master_ut.course_id",
                'subject_master_ut'=>"subject_master_ut.subject_id=topic_master_ut.subject_id");
		$subject_topic_data = $this->common_model->getMasterQuery("*",'purchase_ut', $cond_dr1, array(), $join_dr1, $jointype_dr1,$group_by);           
		
		$cond_dr2 = "purchase_type_flag='topic'";
		$jointype_dr2  = array('topic_master_ut'=>"LEFT",'course_master_ut'=>"LEFT",'subject_master_ut'=>"LEFT");
        $join_dr2 = array('topic_master_ut'=>"topic_master_ut.topic_id=purchase_ut.purchase_entity_id",
			'course_master_ut'=>"course_master_ut.course_id=topic_master_ut.course_id",
                'subject_master_ut'=>"subject_master_ut.subject_id=topic_master_ut.subject_id");
		$topic_data = $this->common_model->getMasterQuery("*",'purchase_ut', $cond_dr2, array(), $join_dr2, $jointype_dr2,$group_by);           

		$topic_list = array_merge($course_topic_data,$subject_topic_data,$topic_data);
		return $topic_list;
	}




}




?>