<?php 
class Manage_testimonials_model extends CI_Model
{
    function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
	}

 	// ----- save testimonial----
	public function saveTestimonials($insertArr)
	{
		$proc_data = "'insert',
		'1',
		'".$insertArr['name']."',
		'".$insertArr['image']."',
		'".$insertArr['rating']."',
		'".$insertArr['comments']."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_manage_testimonials_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//----get all testimonialsList-------
	public function testimonialsList()
	{
		$proc_data = "'select_all',
		'1',
		'test',
		'test',
		'1',
		'test',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_manage_testimonials_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//----get  testimonials bt id-------
	public function get_Testim_byId($key)
	{
		$proc_data = "'select_by_id',
		'".$key."',
		'test',
		'test',
		'1',
		'test',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_manage_testimonials_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

    // ----- delete testimonial----
	public function deleteTestimonials($key)
	{
		$proc_data = "'delete',
		'".$key."',
		'test',
		'test',
		'1',
		'test',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_manage_testimonials_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	 // ----- update fq----
	public function updateTestimonials($updateArr)
	{
		$proc_data = "'update',
		'".$updateArr['id']."',
		'".$updateArr['name']."',
		'".$updateArr['image']."',
	    '".$updateArr['rating']."',
	    '".$updateArr['comments']."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_manage_testimonials_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	// ----- add student testimonial----
	public function addTestimonial($insertArr)
	{
		$proc_data = "'student_test_insert',
		'1',
		'".$insertArr['topic_id']."',
		'".$insertArr['student_id']."',
		'".$insertArr['rating']."',
		'".$insertArr['comment']."',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_student_testimonial_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	// ----- get student testimonial----
	public function getTestimonial($data)
	{
		$proc_data = "'select_id',
		'".$data['testimonial_id']."',
		'1',
		'1',
		'1',
		'test',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_student_testimonial_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	// ----- update student testimonial----
	public function updateTestimonial($insertArr)
	{
		$proc_data = "'update',
		'".$insertArr['testimonial_id']."',
		'".$insertArr['topic_id']."',
		'".$insertArr['student_id']."',
		'".$insertArr['rating']."',
		'".$insertArr['comment']."',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_student_testimonial_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//------get all student testimonilas

	public function allTestimonialStudList()
	{
		$proc_data = "'select_all',
		'1',
		'1',
		'1',
		'1',
		'test',
		'".date('Y-m-d H:i:s')."',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_student_testimonial_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}
	 
	
}




?>