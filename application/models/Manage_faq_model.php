<?php 
class Manage_faq_model extends CI_Model
{
    function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
	}

 	// ----- save FAQ----
	public function saveFaq($insertArr)
	{//print_r($updateArr);exit;
		$proc_data = "'insert',
		'1',
		'".$insertArr['question']."',
		'".$insertArr['answer']."',
		'1',
		@result";
		//print_r($proc_data);exit;
		$select_proc = $this->common_model->call_proc('sp_manage_faq_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");//print_r($result);exit;
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//----get all picturesList-------
	public function faqList()
	{
		$proc_data = "'select_all',
		'1',
		'test',
		'test',
		'1',
		@result";
		//print_r($proc_data);exit;
		$select_proc = $this->common_model->call_proc('sp_manage_faq_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	//----get faq by id-------
	public function get_faq_byId($key)
	{
		$proc_data = "'select_by_id',
		'".$key."',
		'test',
		'test',
		'1',
		@result";
		//print_r($proc_data);exit;
		$select_proc = $this->common_model->call_proc('sp_manage_faq_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

		 // ----- delete offer----
	public function deleteFaq($key)
	{
		$proc_data = "'delete',
		'".$key."',
		'test',
		'test',
		'1',
		@result";
		$select_proc = $this->common_model->call_proc('sp_manage_faq_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	 // ----- update fq----
	public function updateFaq($updateArr)
	{//print_r($updateArr);exit;
		$proc_data = "'update',
		'".$updateArr['id']."',
		'".$updateArr['question']."',
	    '".$updateArr['answer']."',
		'1',
		@result";
		//print_r($proc_data);exit;
		$select_proc = $this->common_model->call_proc('sp_manage_faq_crud',$proc_data);
		$result = $this->common_model->get_proc_result("@result as result");//print_r($result);exit;
		$result1 = json_decode($result[0]['result'],true);
		return $result1;
	}

	 
	
}




?>